/**
 * Javascript for Lesion Encounters
 */

var jq, $body, $wrapper, $collection, $notes, $template;

if (jQuery) jq = jQuery ;

$.ajaxSetup({async:true});

var scripts_dir = serverRoot+'/scripts' ;

if (typeof CONDR == 'undefined') var CONDR={};
if (typeof CONDR.encounters == 'undefined') CONDR.encounters={};





//////////////////////////////////////////////////
// XNAT UTILITY FUNCTIONS
//////////////////////////////////////////////////


// fetch the existing records
// if '_array' paramater is passed (with an existing array)
// the ResultSet.Result array will be appended to it
// no duplicate check is done
XNAT.app.ResultSetResultArray = function(_url,_array,_async,_sort_by,_callbacks){

    _url = _url || '';
    _array = _array || [];
    _async = _async || false ;
    _sort_by = _sort_by || null ;

    // if the _async parameter is either 'async' or true, make async: true, otherwise not
    var async_ = (_async == 'async');

    var totalRecords = '';

    if (_url > ''){
        var doAJAX = $.ajax({
            type: 'GET',
            url: _url,
            dataType: 'json',
            async: async_,
            //async: false,
            success: function(json){
                XNAT.data.resultSet = json.ResultSet ;
                XNAT.data.resultCount = 0;
                totalRecords = json.ResultSet.totalRecords ;
                $.each(json.ResultSet.Result, function(){
                    _array.push(this);
                    XNAT.data.resultCount++ ;
                });
            }
        });
        // callbacks with returned ajax data
        if (typeof _callbacks == 'object'){
            if (typeof _callbacks.done == 'function'){
                doAJAX.done(function(data,textStatus,jqXHR){
                    if (data && data > ''){
                        _callbacks.done(data,textStatus,jqXHR);
                    }
                });
            }
            if (typeof _callbacks.fail == 'function'){
                doAJAX.fail(function(data,textStatus,error){
                    _callbacks.fail(data,textStatus,error);
                });
            }
            if (typeof _callbacks.always == 'function'){
                doAJAX.always(function(data_or_jqXHR,textStatus,jqXHR_or_error){
                    _callbacks.always(data_or_jqXHR,textStatus,jqXHR_or_error);
                });
            }
        }
        // sort the _array
        if (_sort_by !== null && _sort_by > ''){
            _array.sort(function(a,b){
                if (a[_sort_by] > b[_sort_by]){
                    return 1;
                }
                if (a[_sort_by] < b[_sort_by]){
                    return -1;
                }
                return 0;
            });
        }
    }

    // we can do something with the data above, or return the array here
    return _array ;
};



// get the data returned in an 'items' JSON object
XNAT.app.itemData = function(_url,_async,_callbacks){

    _url = _url || '';
    _async = (_async && _async !== '' && _async !== null) ? _async : false ;

    var data_obj={}, children=[], meta_obj={};

    // if the _async parameter is either 'async' or true, make (async_ == true), otherwise not
    var async_ = (_async == 'async');

    if (_url > ''){
        var doAJAX = $.ajax({
            type: 'GET',
            url: _url,
            dataType: 'json',
            async: async_,
            success: function(json){
                $.each(json.items, function(){
                    var _items = this ;
                    if (_items.meta.isHistory === false){ // make sure it's the latest record
                        data_obj = _items.data_fields ;
                        children = _items.children ;
                        meta_obj = _items.meta ;
                    }
                });
            }
        });
        // callbacks with returned ajax data
        if (typeof _callbacks == 'object'){
            if (typeof _callbacks.handleDone == 'function'){
                doAJAX.done(function(data,textStatus,jqXHR){
                    _callbacks.handleDone(data,textStatus,jqXHR);
                });
            }
            if (typeof _callbacks.handleFail == 'function'){
                doAJAX.fail(function(data,textStatus,error){
                    _callbacks.handleFail(data,textStatus,error);
                });
            }
            if (typeof _callbacks.handleAlways == 'function'){
                doAJAX.always(function(data_or_jqXHR,textStatus,jqXHR_or_error){
                    _callbacks.handleAlways(data_or_jqXHR,textStatus,jqXHR_or_error);
                });
            }
        }
    }
    //XNAT.data.itemDataFields = data_obj ;
    //XNAT.data.itemMeta = meta_obj ;
    return { data: data_obj , data_fields: data_obj, meta: meta_obj } ;
};



//////////////////////////////////////////////////
// SETUP THE ENCOUNTER STUFF
//////////////////////////////////////////////////
CONDR.encounters.recordCount = 0 ;

CONDR.encounters.changedForm = false ; // has the form been changed? (not currently using this)
CONDR.encounters.completedForm = false ; // is the form complete?
CONDR.encounters.newRecords = []; // what records have we created at this time?
CONDR.encounters.editedRecords = []; // which records have beed edited?




//////////////////////////////////////////////////
// RENDER THE CLINICAL ENCOUNTER COLLECTION
//////////////////////////////////////////////////
// renderRecords takes an array of the lesion records and spits them into
// a table inside the $container element (a jQuery DOM object)
CONDR.encounters.renderClinRecords = function(_array,$container){

    //
    // 1) the _array param comes in from AJAX
    // 2) (re?)render the stuff

    // setting these strings as vars here for easy future modification
    var lesion_id       = 'condr_mets:lesiondata/id';
    var lesion_label    = 'condr_mets:lesiondata/label';
    var lesion_num      = 'condr_mets:lesiondata/nodenum';
    var lesion_hemi     = 'condr_mets:lesiondata/hemisphere';
    var lesion_loc      = 'condr_mets:lesiondata/location';
    var lesion_date     = 'condr_mets:lesiondata/dateofdiagnosis';
    //var lesion_desc     = 'condr_mets:lesiondata/description'; // don't need this for encounters?
    var enc_id          = 'condr_mets:metsclinencdata/id';
    var enc_label       = 'label';
    var enc_num         = 'condr_mets:metsclinencdata/nodenum'; // NODE NUMBER FOR ENCOUNTER ENTRY - NOT USED?
    var enc_coll_id     = 'condr_mets:metsclinencdata/sacollid'; // ID FOR ENCOUNTER COLLECTION - NOT USED?
    var enc_lesion_id   = 'condr_mets:metsclinencdata/salinkid'; // ID FOR LESION (SAME AS lesiondata/id) - NOT USED?
    var enc_plan        = 'condr_mets:metsclinencdata/treatmentplan';
    var enc_tumor_vol   = 'condr_mets:metsclinencdata/tumorvolume';
    var enc_response    = 'condr_mets:metsclinencdata/responsetotreatment';
    var enc_prog        = 'condr_mets:metsclinencdata/progressionstatus';
    var enc_status      = 'condr_mets:metsclinenccollection/karperfstatus';
    var enc_steroid     = 'condr_mets:metsclinenccollection/steroiddose';
    var enc_notes       = 'condr_mets:metsclinencdata/encnotes';
    var enc_coll_date   = 'condr_mets:metsclinenccollection/encdate'; // DATE FOR ENCOUNTER COLLECTION - NOT USED?
    var enc_uri         = 'URI';

    var records_html = '\n' +
        '<table id="lesion_records" ' +
        //'cellpadding="0" ' +
        //'cellspacing="0" ' +
        'data-subject="'+ XNAT.data.context.subjectID +'">' + '\n' +
        '   <tr>' + '\n' +
        '       <th class="underscore">#</th>' + '\n' +
        '       <th class="underscore">Hemisphere</th>' + '\n' +
        '       <th class="underscore">Location</th>' + '\n' +
        '       <th class="underscore">Date of <br>Diagnosis</th>' + '\n' +
        '       <th class="underscore">Enhancing <br>Tumor <br>Volume (cc)</th>' + '\n' +
        '       <th class="underscore">Response to <br>Treatment</th>' + '\n' +
//        '       <th class="underscore">Progression Status</th>' + '\n' +
        '       <th class="underscore">Treatment <br>Plan</th>' + '\n' +
        '       <th class="underscore notes">Notes</th>' + '\n' + // notes
        '       <th class="underscore edit"></th>' + '\n' + // edit link
        '   </tr>' +
        '' ;

    var orphan='', orphans=[], _count=0 ;

    if (_array && (_array !== null) && (_array !== '') && (_array > [])){

        _array.sort(function(a,b){
            return a[enc_num] - b[enc_num];
        });

        ////if (console.log) console.log(_array.toString());

        $.each(_array,function(i){

            var lesion={};
            lesion.id = this[lesion_id];
            lesion.count = this[lesion_num];
            lesion.label = this[lesion_label];
            lesion.hemi = this[lesion_hemi];
            lesion.loc = this[lesion_loc];
            lesion.date = this[lesion_date];
            
            //if (console.log) console.log(lesion.date);

            var this_lesion_date = new SplitDate(lesion.date,'iso');
            var this_enc_coll_date = new SplitDate(this[enc_coll_date],'iso');

            //if (console.log) console.log(this_lesion_date.iso);
            //if (console.log) console.log(this_enc_coll_date.iso);

            // date_num is a property returned from SplitDate
            if (this[lesion_num]+'' != '0' /* && this_lesion_date.date_num <= this_enc_coll_date.date_num */){

                _count++ ;

                var date = (lesion.date > '' || lesion.date > 0) ? this_lesion_date : {} ;

                // date stuff

                if (date > {}){
                    date.display = this_lesion_date.iso ;  // 2013-09-01
                }
                else {
                    date.display = '&mdash;';
                }

                var notes = '&mdash;';
                if (this[enc_notes] > ''){
                    notes = '' +
                        '<span class="tip_icon note" style="margin-left:-8px;position:absolute;left:50%;top:2px;">' + //
                            '<span class="tip shadowed" style="width:200px;left:-222px;z-index:10000;white-space:normal;">' +
                                this[enc_notes] +
                            '</span>' +
                        '</span>' +
                    ''
                }

                records_html += '\n' +
                    '<tr class="record" data-lesion-id="'+ lesion.id +'" data-encounter-id="'+ this[enc_id] +'" id="'+ this[enc_id] +'" title="Lesion label: '+ lesion.label +', Encounter label: '+ this[enc_label] +'">' + '\n' +
                    '   <td class="lesion" data-nodenum="'+ this[enc_num] +'" data-count="'+ lesion.count +'">'+ this[enc_num] /* lesion.count */ +'</td>' + '\n' +
                    '   <td class="hemisphere" data-val="'+ lesion.hemi +'">'+ lesion.hemi +'</td>' + '\n' +
                    '   <td class="location" data-val="'+ lesion.loc +'">'+ lesion.loc +'</td>' + '\n' +
                    '   <td class="diagnosis date" data-val="'+ date.val +'" data-year="'+ date.yyyy +'" data-month="'+ date.mm +'" data-day="'+ date.dd +'" style="font-family:Courier,monospace">'+ this_lesion_date.iso +'</td>' + '\n' +
                    '   <td class="tumor_vol" style="text-align:center;" data-val="'+ this[enc_tumor_vol] +'">'+ this[enc_tumor_vol] +'</td>' + '\n' +
                    '   <td class="response" data-val="'+ this[enc_response] +'">'+ this[enc_response] +'</td>' + '\n' +
//                    '   <td class="progression" data-val="'+ this[enc_prog] +'">'+ this[enc_prog] +'</td>' + '\n' +
                    '   <td class="plan" data-val="'+ this[enc_plan] +'">'+ this[enc_plan] +'</td>' + '\n' +
                    '   <td class="notes" style="position:relative;text-align:center;">' + notes + '</td>' + '\n' +
                    '   <td class="edit"><a class="edit record btn nolink" style="text-decoration:underline;" href="'+ this[enc_uri] +'">edit</a></td>' + '\n' +
                    '</tr>' + '\n' +
                    '';
            }
            else {
                // grab the first orphan
                if (orphan === ''){
                    CONDR.encounters.orphan = orphan = this[lesion_id];
                }
                // keep a list of all the orphans
                orphans.push(this[lesion_id]);
                //has_records = false ;
            }
        });

        CONDR.encounters.orphans = orphans ; // array of orphans (with nodenum:0)

        if (_count === 0){
            records_html += '\n' +
                '<tr class="no_records">' + '\n' +
                '   <td colspan="10" style="text-align:center;"><p style="margin:20px auto;">(no records)</p></td>' +
                '</tr>' + '\n' +
                '';
        }
    }
    else {
        records_html += '\n' +
            '<tr class="no_records">' + '\n' +
            '   <td colspan="10" style="text-align:center;"><p style="margin:20px auto;">(no records)</p></td>' +
            '</tr>' + '\n' +
            '';
    }

    records_html += '</table>';

    $container.html(records_html);

    // style the new and edited records
    // have to do this every time the list is rendered
//    if (CONDR.encounters.newRecords.length > 0){
//        (function(){
//            var records = '#'+CONDR.encounters.newRecords.join('.record, #');
//            $(records).css('color','#180');
//        })();
//    }
//    if (CONDR.encounters.editedRecords.length > 0){
//        (function(){
//            var records = '#'+CONDR.encounters.editedRecords.join('.record, #');
//            $(records).css('background','#ffd');
//        })();
//    }


    //$('#'+CONDR.encounters.lesionID+'.record').css('background','#ffd');


    CONDR.encounters.recordCount = parseInt(_count);

    $('#lesions_at_encounter').find('b').text(_count);

};






//////////////////////////////////////////////////
// EDIT CLINICAL ENCOUNTER DATA RECORD
//////////////////////////////////////////////////
// a little setup
CONDR.encounters.modalCount = 0;
CONDR.encounters.modalOpts = {};
//
CONDR.encounters.editClinRecord = function($record, $template, _content, _opts){
    xModalLoadingOpen({title:'Loading data...'});
    // if there's content, use that, if not check for $template parameter and use that or use empty string
    _content = _content || ($template && $template > '') ? $template.html() : '(error)';
    _opts = _opts || {} ;
    //var $record = $(this).closest('tr.record');
    var uri = $record.find('a.edit.btn').attr('href');
//    alert(uri);
    var page = uri+'?format=html';

    //CONDR.encounters.completedForm = false ;
    var clinEncID = $record.attr('id');

    var recordJSON = CONDR.encounters.itemData = XNAT.app.itemData(uri+'?format=json',false);
    var recordData = recordJSON.data ;

    //if (console.log) console.log(recordData);

    var modal_opts = {};
    modal_opts.id = 'edit_'+recordData.id;
    modal_opts.title = 'Edit Encounter Data';
    modal_opts.closeBtn = 'hide';
    modal_opts.content = _content ;
    //modal_opts.footerContent = '<a href="#" class="cancel_record btn" style="position:relative;top:10px;">Cancel</a>';
    modal_opts.okLabel = 'Save and Close';
    modal_opts.okClose = 'no';
    modal_opts.okAction = function(){
        var $modal = $('#'+modal_opts.id);
        //if (CONDR.encounters.formCompleted($modal)){
            xModalLoadingOpen({title:'Saving Encounter Data...'});
            var saveRecord = $.ajax({
                type: 'PUT',
                cache: false,
                async: true,
                url:
                    serverRoot + '/data/projects/CONDR_METS' +
                        '/subjects/' + XNAT.data.context.subjectID +
                        '/experiments/' + clinEncID +
                        '?xsiType=condr_mets:metsClinEncData' +
                        '&condr_mets:metsClinEncData/tumorVolume=' + $modal.find('[name="tumor_volume"]').val() + /* treatment_volume - float */
                        '&condr_mets:metsClinEncData/responseToTreatment=' + $modal.find('[name="response"]').val() + /* response - string */
                        //'&condr_mets:metsClinEncData/progressionStatus=' + $modal.find('[name="progression"]').val() +
                        '&condr_mets:metsClinEncData/treatmentPlan=' + $modal.find('[name="plan"]').val() +
                        '&condr_mets:metsClinEncData/encNotes=' + encodeURIComponent($modal.find('[name="enc_data_notes"]').val()) + /* notes - string */
                        '&allowDataDeletion=true' +
                        '&XNAT_CSRF=' + csrfToken +
                        ''
            });
            saveRecord.done(function(exptID){
                var reRenderEncounterData = setInterval(function(){
                    if (exptID > ''){ //check if selected option is loaded
                        clearInterval(reRenderEncounterData); // do we clear the interval first or last?
                        CONDR.encounters.editedRecords.push(exptID);
                        //CONDR.encounters.collection = new XNAT.app.ResultSetResultArray(url);
                        //CONDR.encounters.renderClinRecords(CONDR.encounters.collection,$collection);
                        CONDR.encounters.renderClinRecords(CONDR.encounters.collection(), $('#clin_enc_coll'));
                        xModalCloseNew(modal_opts.id);
                        xModalLoadingClose();
                    }
                },100);
            });
            saveRecord.fail(function(data, status, error){
                //xModalCloseNew(modal_opts.id);
                xModalMessage('error','data: '+data+'<br><br> status: '+status+'<br><br> error: '+error);
                xModalLoadingClose();
            });
        //}
        //else {
        //    xModalMessage('Missing Data', 'Please choose a <b>hemisphere</b> and <b>location</b> and enter the <b>date of diagnosis</b> and submit the data again.', 'OK');
        //}
    };
    modal_opts.cancel = 'show';
    modal_opts.cancelAction = function(){
        CONDR.encounters.renderClinRecords(CONDR.encounters.collection(), $('#clin_enc_coll'));
    };
    modal_opts.beforeShow = function(){

        var
            $modal = $('#'+modal_opts.id),
            $form = $modal.find('form.record'),

            encID = recordData.ID || '',
            encLabel = recordData.label || '',
            saCollID = recordData.saCollID || '', // *encounter* collection ID?
            saLinkID = recordData.saLinkID || '',
            nodeNum = recordData.nodeNum || '',
            subject_ID = recordData.subject_ID || '',
            tumorVolume = recordData.tumorVolume || '',
            responseToTreatment = recordData.responseToTreatment || '',
            //progressionStatus = recordData.progressionStatus || '',
            treatmentPlan = recordData.treatmentPlan || '',
            encNotes = recordData.encNotes || '';

        $form.find('h4 > b').append(nodeNum + ' - edit');

        var source_html = $form.html();

        var new_html = source_html.
            replace(/{{ENCOUNTER_ID}}/g, encID).
            replace(/{{ENCOUNTER_LABEL}}/g, encLabel).
            replace(/{{COLLECTION_ID}}/g, saCollID).
            replace(/{{LESION_ID}}/g, saLinkID).
            replace(/{{NODE}}/g, nodeNum).
            replace(/{{SUBJECT_LABEL}}/g, XNAT.data.context.subjectLabel).
            replace(/{{SUBJECT_ID}}/g, subject_ID).
            replace(/{{COUNT}}/g, nodeNum).
            replace(/{{TUMOR_VOLUME}}/g, tumorVolume).
            replace(/{{RESPONSE}}/g, responseToTreatment).
            //replace(/{{PROGRESSION}}/g, progressionStatus).
            replace(/{{TREATMENT_PLAN}}/g, treatmentPlan).
            replace(/{{NOTES}}/g, encNotes);

        $form.html(new_html);

        $form.find('[name="tumor_volume"]').val(tumorVolume);
        $form.find('[name="response"]').val(responseToTreatment).change();
        //$form.find('[name="progression"]').val(progressionStatus).change();
        $form.find('[name="plan"]').val(treatmentPlan).change();
        $form.find('[name="enc_data_notes"]').val(encNotes).text(encNotes);

    };
    new xModal.Modal(modal_opts);
    xModalLoadingClose();
};





//////////////////////////////////////////////////
// GET THE NOTES
//////////////////////////////////////////////////
CONDR.encounters.getCollNotes = function(_id){

    _id = _id || CONDR.encounters.collID ;

    CONDR.encounters.JSONuri = serverRoot + '/data/experiments/'+ _id +'?format=json';
    CONDR.encounters.collJSON = XNAT.app.itemData( CONDR.encounters.JSONuri , false );

    if (typeof CONDR.encounters.collJSON.data.note != 'undefined'){
        CONDR.encounters.collNotes = CONDR.encounters.collJSON.data.note ;
    }
    else {
        CONDR.encounters.collNotes = false ;
    }
};
CONDR.encounters.getCollNotes();








CONDR.encounters.notesModal = function(_content){
    var modal_opts={};
    modal_opts.id = 'clin_notes_modal';
    modal_opts.width = 500;
    modal_opts.height = 300;
    modal_opts.title = 'Collection Notes';
    modal_opts.content = _content ;
    modal_opts.scroll = 'nope';
    modal_opts.closeBtn = 'hide';
    modal_opts.okLabel = 'Save and Close';
    modal_opts.okAction = function(){
        xModalLoadingOpen({title:'Saving notes...'});
        xModal.closeModal = false ;
        var $this_modal = $('#clin_notes_modal');
        var notes = $this_modal.find('textarea').val();
        //alert(notes);
        var saveNotes = $.ajax({
            type: 'PUT',
            cache: false,
            async: true,
            url: serverRoot +
                '/data/projects/CONDR_METS' +
                '/subjects/'+XNAT.data.context.subjectID+'' +
                '/experiments/'+CONDR.encounters.collID+
                '?xsiType=condr_mets:metsClinEncCollection' +
                '&condr_mets:metsClinEncCollection/note=' + encodeURIComponent(notes) +
                '&allowDataDeletion=true' +
                '&XNAT_CSRF=' + csrfToken
        });
        saveNotes.done(function(_id){
            var reRenderNotes = setInterval(function(){
                if (_id > ''){ //check if selected option is loaded
                    clearInterval(reRenderNotes); // do we clear the interval first or last?
                    CONDR.encounters.getCollNotes(_id);
                    if (CONDR.encounters.collNotes > ''){
                        $('#add_notes').hide();
                        $notes.find('p').text(CONDR.encounters.collNotes);
                        $notes.show();
                    }
                    else {
                        $('#add_notes').show();
                        $notes.find('p').text('');
                        $notes.hide();
                    }
                    xModalLoadingClose();
                }
            },100);
        });
    };
    modal_opts.beforeShow = function(){
        xModal.closeModal = true ;
        //alert('Wait!');
        var $this_modal = $('#notes_modal');
        //$this_modal.drags({handle:'.title'});
    };
    new xModal.Modal(modal_opts);
};



// do some stuff after the DOM loads
$(function(){

    $body = $('body');
    $wrapper = $('#CONDR_encounters');
    $notes = $('#clin_enc_coll_notes');

    // make sure the lesionTracker.css is loaded
    if (!($('link[href*="lesionTracker.css"]').length)){
        $('head').append('<link type="text/css" rel="stylesheet" href="'+ scripts_dir + '/lesionTracker/lesionTracker.css">');
    }

    $body.on('click','a.btn, a.nolink',function(e){
        e.preventDefault();
    });

    $wrapper.on('change','select[name="treated_at_encounter"]',function(){
        var val = $(this).val();
        var $record = $(this).closest('tr.record');
        var expt_id = $record.attr('id');
        if (val == '1'){
            CONDR.encounters.editClinRecord($record,$('#clin_enc_modal'));
        }
        else {
            var modal_opts={};
            modal_opts.width = 400;
            modal_opts.height = 200;
            modal_opts.scroll = 'negatory'; // anything besides 'yes' will disable scrolling
            modal_opts.content = 'Clicking "OK" will reset the encounter data for this lesion. Would you like to continue?' ;
            modal_opts.okAction = function(){
                var resetData = $.ajax({
                    type: 'PUT',
                    cache: false,
                    async: false,
                    url: serverRoot + '/data/projects/CONDR_METS' +
                        '/subjects/' + XNAT.data.context.subjectID +
                        '/experiments/' + expt_id +
                        '?xsiType=condr_mets:metsRadEncData' +
                        '&condr_mets:metsRadEncData/treated=0' +
                        '&condr_mets:metsRadEncData/treatmentVolume=' +
                        '&condr_mets:metsRadEncData/treatmentDose=' +
                        '&condr_mets:metsRadEncData/isodoseLine=' + //
                        '&condr_mets:metsRadEncData/lesionType=' +
                        '&condr_mets:metsRadEncData/numberOfFractions=' +
                        '&condr_mets:metsRadEncData/encNotes=' +
                        '&allowDataDeletion=true' +
                        '&XNAT_CSRF=' + csrfToken
                });
                resetData.done(function(){
//                    alert('data has been reset');
                    CONDR.encounters.renderClinRecords(CONDR.encounters.collection(), $('#clin_enc_coll'));
                })
            };
            modal_opts.cancelAction = function(){
                CONDR.encounters.renderClinRecords(CONDR.encounters.collection(), $('#clin_enc_coll'));
            };
            xModalConfirm(modal_opts);
        }
    });

    $wrapper.on('click','a.edit.record',function(e){
        var $record = $(this).closest('tr.record');
        CONDR.encounters.editClinRecord($record,$('#clin_enc_modal'));
    });


    // ADD NOTES
    //
    $wrapper.on('click','#add_notes',function(){
        var modal_content = '' +
            '<textarea style="width:450px;height:175px;"></textarea>' + //
            '';
        CONDR.encounters.notesModal(modal_content);
    });


    // EDIT NOTES
    //
    $notes.find('a.edit.notes').click(function(){
        var notes = $notes.find('p').text();
        var modal_content = '' +
            '<textarea style="width:450px;height:175px;">' + //
            notes +
            '</textarea>' +
            '';
        CONDR.encounters.notesModal(modal_content);
    });

});


// done loading
$(window).load(function(){

    CONDR.encounters.renderClinRecords(CONDR.encounters.collection(), $('#clin_enc_coll'));

    $('#CONDR_encounters').find('.container').fadeIn(100);

    if (CONDR.encounters.collNotes !== false && CONDR.encounters.collNotes > ''){
        //$notes.find('p').text(CONDR.encounters.collNotes);
        $('#add_notes').hide();
        $notes.show();
    }
    else {
        $notes.hide();
        $('#add_notes').show();
    }

});

