/**
 * @author sunilk@mokacreativellc.com (Sunil Kumar)
 */



/**
 * Class containing various utility methods.
 *
 * @constructor
 */
goog.provide('nrg');
nrg = function() {};
goog.exportSymbol('nrg', nrg);




/**
 * Class containing various global 
 * 
 * @constructor
 */
goog.provide("nrg.globals");
nrg.globals = function(){};
goog.exportSymbol('nrg.globals', nrg.globals);




nrg.globals.prototype.fontSizeS = /** @const @type {number}*/ 10;
nrg.globals.prototype.fontSizeM = /** @const @type {number}*/ 13;
nrg.globals.prototype.fontSizeL = /** @const @type {number}*/ 16;
nrg.globals.prototype.fontFamily = /** @const @type {string}*/ 'Helvetica, Helvetica neue, Arial, sans-serif';
