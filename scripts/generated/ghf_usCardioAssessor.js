/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function ghf_usCardioAssessor(){
this.xsiType="ghf:usCardioAssessor";

	this.getSchemaElementName=function(){
		return "usCardioAssessor";
	}

	this.getFullSchemaElementName=function(){
		return "ghf:usCardioAssessor";
	}
this.extension=dynamicJSLoad('xnat_imageAssessorData','generated/xnat_imageAssessorData.js');

	this.Seqstudynumber=null;


	function getSeqstudynumber() {
		return this.Seqstudynumber;
	}
	this.getSeqstudynumber=getSeqstudynumber;


	function setSeqstudynumber(v){
		this.Seqstudynumber=v;
	}
	this.setSeqstudynumber=setSeqstudynumber;

	this.Analyzableecho=null;


	function getAnalyzableecho() {
		return this.Analyzableecho;
	}
	this.getAnalyzableecho=getAnalyzableecho;


	function setAnalyzableecho(v){
		this.Analyzableecho=v;
	}
	this.setAnalyzableecho=setAnalyzableecho;


	this.isAnalyzableecho=function(defaultValue) {
		if(this.Analyzableecho==null)return defaultValue;
		if(this.Analyzableecho=="1" || this.Analyzableecho==true)return true;
		return false;
	}

	this.Image2dquality=null;


	function getImage2dquality() {
		return this.Image2dquality;
	}
	this.getImage2dquality=getImage2dquality;


	function setImage2dquality(v){
		this.Image2dquality=v;
	}
	this.setImage2dquality=setImage2dquality;

	this.Imagedpquality=null;


	function getImagedpquality() {
		return this.Imagedpquality;
	}
	this.getImagedpquality=getImagedpquality;


	function setImagedpquality(v){
		this.Imagedpquality=v;
	}
	this.setImagedpquality=setImagedpquality;

	this.Dominantechorhythm=null;


	function getDominantechorhythm() {
		return this.Dominantechorhythm;
	}
	this.getDominantechorhythm=getDominantechorhythm;


	function setDominantechorhythm(v){
		this.Dominantechorhythm=v;
	}
	this.setDominantechorhythm=setDominantechorhythm;

	this.Heartrate=null;


	function getHeartrate() {
		return this.Heartrate;
	}
	this.getHeartrate=getHeartrate;


	function setHeartrate(v){
		this.Heartrate=v;
	}
	this.setHeartrate=setHeartrate;

	this.Lvotd1=null;


	function getLvotd1() {
		return this.Lvotd1;
	}
	this.getLvotd1=getLvotd1;


	function setLvotd1(v){
		this.Lvotd1=v;
	}
	this.setLvotd1=setLvotd1;

	this.Lvidd=null;


	function getLvidd() {
		return this.Lvidd;
	}
	this.getLvidd=getLvidd;


	function setLvidd(v){
		this.Lvidd=v;
	}
	this.setLvidd=setLvidd;

	this.Ivs=null;


	function getIvs() {
		return this.Ivs;
	}
	this.getIvs=getIvs;


	function setIvs(v){
		this.Ivs=v;
	}
	this.setIvs=setIvs;

	this.Pw=null;


	function getPw() {
		return this.Pw;
	}
	this.getPw=getPw;


	function setPw(v){
		this.Pw=v;
	}
	this.setPw=setPw;

	this.Alwt=null;


	function getAlwt() {
		return this.Alwt;
	}
	this.getAlwt=getAlwt;


	function setAlwt(v){
		this.Alwt=v;
	}
	this.setAlwt=setAlwt;

	this.Lvids=null;


	function getLvids() {
		return this.Lvids;
	}
	this.getLvids=getLvids;


	function setLvids(v){
		this.Lvids=v;
	}
	this.setLvids=setLvids;

	this.Lvedv=null;


	function getLvedv() {
		return this.Lvedv;
	}
	this.getLvedv=getLvedv;


	function setLvedv(v){
		this.Lvedv=v;
	}
	this.setLvedv=setLvedv;

	this.Lvesv=null;


	function getLvesv() {
		return this.Lvesv;
	}
	this.getLvesv=getLvesv;


	function setLvesv(v){
		this.Lvesv=v;
	}
	this.setLvesv=setLvesv;

	this.V1=null;


	function getV1() {
		return this.V1;
	}
	this.getV1=getV1;


	function setV1(v){
		this.V1=v;
	}
	this.setV1=setV1;

	this.Lvottvi=null;


	function getLvottvi() {
		return this.Lvottvi;
	}
	this.getLvottvi=getLvottvi;


	function setLvottvi(v){
		this.Lvottvi=v;
	}
	this.setLvottvi=setLvottvi;

	this.Valvstenosis_aortic=null;


	function getValvstenosis_aortic() {
		return this.Valvstenosis_aortic;
	}
	this.getValvstenosis_aortic=getValvstenosis_aortic;


	function setValvstenosis_aortic(v){
		this.Valvstenosis_aortic=v;
	}
	this.setValvstenosis_aortic=setValvstenosis_aortic;

	this.Valvstenosis_mitral=null;


	function getValvstenosis_mitral() {
		return this.Valvstenosis_mitral;
	}
	this.getValvstenosis_mitral=getValvstenosis_mitral;


	function setValvstenosis_mitral(v){
		this.Valvstenosis_mitral=v;
	}
	this.setValvstenosis_mitral=setValvstenosis_mitral;

	this.Valvstenosis_tricuspid=null;


	function getValvstenosis_tricuspid() {
		return this.Valvstenosis_tricuspid;
	}
	this.getValvstenosis_tricuspid=getValvstenosis_tricuspid;


	function setValvstenosis_tricuspid(v){
		this.Valvstenosis_tricuspid=v;
	}
	this.setValvstenosis_tricuspid=setValvstenosis_tricuspid;

	this.Valvstenosis_pulmonic=null;


	function getValvstenosis_pulmonic() {
		return this.Valvstenosis_pulmonic;
	}
	this.getValvstenosis_pulmonic=getValvstenosis_pulmonic;


	function setValvstenosis_pulmonic(v){
		this.Valvstenosis_pulmonic=v;
	}
	this.setValvstenosis_pulmonic=setValvstenosis_pulmonic;

	this.Valvregurg_aortic=null;


	function getValvregurg_aortic() {
		return this.Valvregurg_aortic;
	}
	this.getValvregurg_aortic=getValvregurg_aortic;


	function setValvregurg_aortic(v){
		this.Valvregurg_aortic=v;
	}
	this.setValvregurg_aortic=setValvregurg_aortic;

	this.Valvregurg_mitral=null;


	function getValvregurg_mitral() {
		return this.Valvregurg_mitral;
	}
	this.getValvregurg_mitral=getValvregurg_mitral;


	function setValvregurg_mitral(v){
		this.Valvregurg_mitral=v;
	}
	this.setValvregurg_mitral=setValvregurg_mitral;

	this.Valvregurg_tricuspid=null;


	function getValvregurg_tricuspid() {
		return this.Valvregurg_tricuspid;
	}
	this.getValvregurg_tricuspid=getValvregurg_tricuspid;


	function setValvregurg_tricuspid(v){
		this.Valvregurg_tricuspid=v;
	}
	this.setValvregurg_tricuspid=setValvregurg_tricuspid;

	this.Valvregurg_pulmonic=null;


	function getValvregurg_pulmonic() {
		return this.Valvregurg_pulmonic;
	}
	this.getValvregurg_pulmonic=getValvregurg_pulmonic;


	function setValvregurg_pulmonic(v){
		this.Valvregurg_pulmonic=v;
	}
	this.setValvregurg_pulmonic=setValvregurg_pulmonic;

	this.Tr=null;


	function getTr() {
		return this.Tr;
	}
	this.getTr=getTr;


	function setTr(v){
		this.Tr=v;
	}
	this.setTr=setTr;

	this.E=null;


	function getE() {
		return this.E;
	}
	this.getE=getE;


	function setE(v){
		this.E=v;
	}
	this.setE=setE;

	this.A=null;


	function getA() {
		return this.A;
	}
	this.getA=getA;


	function setA(v){
		this.A=v;
	}
	this.setA=setA;

	this.Mitraldt=null;


	function getMitraldt() {
		return this.Mitraldt;
	}
	this.getMitraldt=getMitraldt;


	function setMitraldt(v){
		this.Mitraldt=v;
	}
	this.setMitraldt=setMitraldt;

	this.Eml=null;


	function getEml() {
		return this.Eml;
	}
	this.getEml=getEml;


	function setEml(v){
		this.Eml=v;
	}
	this.setEml=setEml;

	this.Ems=null;


	function getEms() {
		return this.Ems;
	}
	this.getEms=getEms;


	function setEms(v){
		this.Ems=v;
	}
	this.setEms=setEms;

	this.Primarysig=null;


	function getPrimarysig() {
		return this.Primarysig;
	}
	this.getPrimarysig=getPrimarysig;


	function setPrimarysig(v){
		this.Primarysig=v;
	}
	this.setPrimarysig=setPrimarysig;

	this.Secondarysig=null;


	function getSecondarysig() {
		return this.Secondarysig;
	}
	this.getSecondarysig=getSecondarysig;


	function setSecondarysig(v){
		this.Secondarysig=v;
	}
	this.setSecondarysig=setSecondarysig;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageAssessorData"){
				return this.Imageassessordata ;
			} else 
			if(xmlPath.startsWith("imageAssessorData")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Imageassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Imageassessordata!=undefined)return this.Imageassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="seqStudyNumber"){
				return this.Seqstudynumber ;
			} else 
			if(xmlPath=="analyzableEcho"){
				return this.Analyzableecho ;
			} else 
			if(xmlPath=="image2dQuality"){
				return this.Image2dquality ;
			} else 
			if(xmlPath=="imageDpQuality"){
				return this.Imagedpquality ;
			} else 
			if(xmlPath=="dominantEchoRhythm"){
				return this.Dominantechorhythm ;
			} else 
			if(xmlPath=="heartrate"){
				return this.Heartrate ;
			} else 
			if(xmlPath=="lvotD1"){
				return this.Lvotd1 ;
			} else 
			if(xmlPath=="lvidd"){
				return this.Lvidd ;
			} else 
			if(xmlPath=="ivs"){
				return this.Ivs ;
			} else 
			if(xmlPath=="pw"){
				return this.Pw ;
			} else 
			if(xmlPath=="alwt"){
				return this.Alwt ;
			} else 
			if(xmlPath=="lvids"){
				return this.Lvids ;
			} else 
			if(xmlPath=="lvedv"){
				return this.Lvedv ;
			} else 
			if(xmlPath=="lvesv"){
				return this.Lvesv ;
			} else 
			if(xmlPath=="v1"){
				return this.V1 ;
			} else 
			if(xmlPath=="lvotTvi"){
				return this.Lvottvi ;
			} else 
			if(xmlPath=="valvStenosis/aortic"){
				return this.Valvstenosis_aortic ;
			} else 
			if(xmlPath=="valvStenosis/mitral"){
				return this.Valvstenosis_mitral ;
			} else 
			if(xmlPath=="valvStenosis/tricuspid"){
				return this.Valvstenosis_tricuspid ;
			} else 
			if(xmlPath=="valvStenosis/pulmonic"){
				return this.Valvstenosis_pulmonic ;
			} else 
			if(xmlPath=="valvRegurg/aortic"){
				return this.Valvregurg_aortic ;
			} else 
			if(xmlPath=="valvRegurg/mitral"){
				return this.Valvregurg_mitral ;
			} else 
			if(xmlPath=="valvRegurg/tricuspid"){
				return this.Valvregurg_tricuspid ;
			} else 
			if(xmlPath=="valvRegurg/pulmonic"){
				return this.Valvregurg_pulmonic ;
			} else 
			if(xmlPath=="tr"){
				return this.Tr ;
			} else 
			if(xmlPath=="e"){
				return this.E ;
			} else 
			if(xmlPath=="a"){
				return this.A ;
			} else 
			if(xmlPath=="mitralDt"){
				return this.Mitraldt ;
			} else 
			if(xmlPath=="eml"){
				return this.Eml ;
			} else 
			if(xmlPath=="ems"){
				return this.Ems ;
			} else 
			if(xmlPath=="primarySig"){
				return this.Primarysig ;
			} else 
			if(xmlPath=="secondarySig"){
				return this.Secondarysig ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageAssessorData"){
				this.Imageassessordata=value;
			} else 
			if(xmlPath.startsWith("imageAssessorData")){
				xmlPath=xmlPath.substring(17);
				if(xmlPath=="")return this.Imageassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Imageassessordata!=undefined){
					this.Imageassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Imageassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Imageassessordata= instanciateObject("xnat:imageAssessorData");//omUtils.js
						}
						if(options && options.where)this.Imageassessordata.setProperty(options.where.field,options.where.value);
						this.Imageassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="seqStudyNumber"){
				this.Seqstudynumber=value;
			} else 
			if(xmlPath=="analyzableEcho"){
				this.Analyzableecho=value;
			} else 
			if(xmlPath=="image2dQuality"){
				this.Image2dquality=value;
			} else 
			if(xmlPath=="imageDpQuality"){
				this.Imagedpquality=value;
			} else 
			if(xmlPath=="dominantEchoRhythm"){
				this.Dominantechorhythm=value;
			} else 
			if(xmlPath=="heartrate"){
				this.Heartrate=value;
			} else 
			if(xmlPath=="lvotD1"){
				this.Lvotd1=value;
			} else 
			if(xmlPath=="lvidd"){
				this.Lvidd=value;
			} else 
			if(xmlPath=="ivs"){
				this.Ivs=value;
			} else 
			if(xmlPath=="pw"){
				this.Pw=value;
			} else 
			if(xmlPath=="alwt"){
				this.Alwt=value;
			} else 
			if(xmlPath=="lvids"){
				this.Lvids=value;
			} else 
			if(xmlPath=="lvedv"){
				this.Lvedv=value;
			} else 
			if(xmlPath=="lvesv"){
				this.Lvesv=value;
			} else 
			if(xmlPath=="v1"){
				this.V1=value;
			} else 
			if(xmlPath=="lvotTvi"){
				this.Lvottvi=value;
			} else 
			if(xmlPath=="valvStenosis/aortic"){
				this.Valvstenosis_aortic=value;
			} else 
			if(xmlPath=="valvStenosis/mitral"){
				this.Valvstenosis_mitral=value;
			} else 
			if(xmlPath=="valvStenosis/tricuspid"){
				this.Valvstenosis_tricuspid=value;
			} else 
			if(xmlPath=="valvStenosis/pulmonic"){
				this.Valvstenosis_pulmonic=value;
			} else 
			if(xmlPath=="valvRegurg/aortic"){
				this.Valvregurg_aortic=value;
			} else 
			if(xmlPath=="valvRegurg/mitral"){
				this.Valvregurg_mitral=value;
			} else 
			if(xmlPath=="valvRegurg/tricuspid"){
				this.Valvregurg_tricuspid=value;
			} else 
			if(xmlPath=="valvRegurg/pulmonic"){
				this.Valvregurg_pulmonic=value;
			} else 
			if(xmlPath=="tr"){
				this.Tr=value;
			} else 
			if(xmlPath=="e"){
				this.E=value;
			} else 
			if(xmlPath=="a"){
				this.A=value;
			} else 
			if(xmlPath=="mitralDt"){
				this.Mitraldt=value;
			} else 
			if(xmlPath=="eml"){
				this.Eml=value;
			} else 
			if(xmlPath=="ems"){
				this.Ems=value;
			} else 
			if(xmlPath=="primarySig"){
				this.Primarysig=value;
			} else 
			if(xmlPath=="secondarySig"){
				this.Secondarysig=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="seqStudyNumber"){
			return "field_data";
		}else if (xmlPath=="analyzableEcho"){
			return "field_data";
		}else if (xmlPath=="image2dQuality"){
			return "field_data";
		}else if (xmlPath=="imageDpQuality"){
			return "field_data";
		}else if (xmlPath=="dominantEchoRhythm"){
			return "field_data";
		}else if (xmlPath=="heartrate"){
			return "field_data";
		}else if (xmlPath=="lvotD1"){
			return "field_data";
		}else if (xmlPath=="lvidd"){
			return "field_data";
		}else if (xmlPath=="ivs"){
			return "field_data";
		}else if (xmlPath=="pw"){
			return "field_data";
		}else if (xmlPath=="alwt"){
			return "field_data";
		}else if (xmlPath=="lvids"){
			return "field_data";
		}else if (xmlPath=="lvedv"){
			return "field_data";
		}else if (xmlPath=="lvesv"){
			return "field_data";
		}else if (xmlPath=="v1"){
			return "field_data";
		}else if (xmlPath=="lvotTvi"){
			return "field_data";
		}else if (xmlPath=="valvStenosis/aortic"){
			return "field_data";
		}else if (xmlPath=="valvStenosis/mitral"){
			return "field_data";
		}else if (xmlPath=="valvStenosis/tricuspid"){
			return "field_data";
		}else if (xmlPath=="valvStenosis/pulmonic"){
			return "field_data";
		}else if (xmlPath=="valvRegurg/aortic"){
			return "field_data";
		}else if (xmlPath=="valvRegurg/mitral"){
			return "field_data";
		}else if (xmlPath=="valvRegurg/tricuspid"){
			return "field_data";
		}else if (xmlPath=="valvRegurg/pulmonic"){
			return "field_data";
		}else if (xmlPath=="tr"){
			return "field_data";
		}else if (xmlPath=="e"){
			return "field_data";
		}else if (xmlPath=="a"){
			return "field_data";
		}else if (xmlPath=="mitralDt"){
			return "field_data";
		}else if (xmlPath=="eml"){
			return "field_data";
		}else if (xmlPath=="ems"){
			return "field_data";
		}else if (xmlPath=="primarySig"){
			return "field_data";
		}else if (xmlPath=="secondarySig"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<ghf:UsCardioAssessor";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</ghf:UsCardioAssessor>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Seqstudynumber!=null){
			xmlTxt+="\n<ghf:seqStudyNumber";
			xmlTxt+=">";
			xmlTxt+=this.Seqstudynumber.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:seqStudyNumber>";
		}
		if (this.Analyzableecho!=null){
			xmlTxt+="\n<ghf:analyzableEcho";
			xmlTxt+=">";
			xmlTxt+=this.Analyzableecho;
			xmlTxt+="</ghf:analyzableEcho>";
		}
		if (this.Image2dquality!=null){
			xmlTxt+="\n<ghf:image2dQuality";
			xmlTxt+=">";
			xmlTxt+=this.Image2dquality.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:image2dQuality>";
		}
		if (this.Imagedpquality!=null){
			xmlTxt+="\n<ghf:imageDpQuality";
			xmlTxt+=">";
			xmlTxt+=this.Imagedpquality.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:imageDpQuality>";
		}
		if (this.Dominantechorhythm!=null){
			xmlTxt+="\n<ghf:dominantEchoRhythm";
			xmlTxt+=">";
			xmlTxt+=this.Dominantechorhythm.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:dominantEchoRhythm>";
		}
		if (this.Heartrate!=null){
			xmlTxt+="\n<ghf:heartrate";
			xmlTxt+=">";
			xmlTxt+=this.Heartrate;
			xmlTxt+="</ghf:heartrate>";
		}
		if (this.Lvotd1!=null){
			xmlTxt+="\n<ghf:lvotD1";
			xmlTxt+=">";
			xmlTxt+=this.Lvotd1;
			xmlTxt+="</ghf:lvotD1>";
		}
		if (this.Lvidd!=null){
			xmlTxt+="\n<ghf:lvidd";
			xmlTxt+=">";
			xmlTxt+=this.Lvidd;
			xmlTxt+="</ghf:lvidd>";
		}
		if (this.Ivs!=null){
			xmlTxt+="\n<ghf:ivs";
			xmlTxt+=">";
			xmlTxt+=this.Ivs;
			xmlTxt+="</ghf:ivs>";
		}
		if (this.Pw!=null){
			xmlTxt+="\n<ghf:pw";
			xmlTxt+=">";
			xmlTxt+=this.Pw;
			xmlTxt+="</ghf:pw>";
		}
		if (this.Alwt!=null){
			xmlTxt+="\n<ghf:alwt";
			xmlTxt+=">";
			xmlTxt+=this.Alwt;
			xmlTxt+="</ghf:alwt>";
		}
		if (this.Lvids!=null){
			xmlTxt+="\n<ghf:lvids";
			xmlTxt+=">";
			xmlTxt+=this.Lvids;
			xmlTxt+="</ghf:lvids>";
		}
		if (this.Lvedv!=null){
			xmlTxt+="\n<ghf:lvedv";
			xmlTxt+=">";
			xmlTxt+=this.Lvedv;
			xmlTxt+="</ghf:lvedv>";
		}
		if (this.Lvesv!=null){
			xmlTxt+="\n<ghf:lvesv";
			xmlTxt+=">";
			xmlTxt+=this.Lvesv;
			xmlTxt+="</ghf:lvesv>";
		}
		if (this.V1!=null){
			xmlTxt+="\n<ghf:v1";
			xmlTxt+=">";
			xmlTxt+=this.V1;
			xmlTxt+="</ghf:v1>";
		}
		if (this.Lvottvi!=null){
			xmlTxt+="\n<ghf:lvotTvi";
			xmlTxt+=">";
			xmlTxt+=this.Lvottvi;
			xmlTxt+="</ghf:lvotTvi>";
		}
			var child0=0;
			var att0=0;
			if(this.Valvstenosis_aortic!=null)
			child0++;
			if(this.Valvstenosis_pulmonic!=null)
			child0++;
			if(this.Valvstenosis_tricuspid!=null)
			child0++;
			if(this.Valvstenosis_mitral!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<ghf:valvStenosis";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Valvstenosis_aortic!=null){
			xmlTxt+="\n<ghf:aortic";
			xmlTxt+=">";
			xmlTxt+=this.Valvstenosis_aortic.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:aortic>";
		}
		if (this.Valvstenosis_mitral!=null){
			xmlTxt+="\n<ghf:mitral";
			xmlTxt+=">";
			xmlTxt+=this.Valvstenosis_mitral.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:mitral>";
		}
		if (this.Valvstenosis_tricuspid!=null){
			xmlTxt+="\n<ghf:tricuspid";
			xmlTxt+=">";
			xmlTxt+=this.Valvstenosis_tricuspid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:tricuspid>";
		}
		if (this.Valvstenosis_pulmonic!=null){
			xmlTxt+="\n<ghf:pulmonic";
			xmlTxt+=">";
			xmlTxt+=this.Valvstenosis_pulmonic.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:pulmonic>";
		}
				xmlTxt+="\n</ghf:valvStenosis>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.Valvregurg_pulmonic!=null)
			child1++;
			if(this.Valvregurg_mitral!=null)
			child1++;
			if(this.Valvregurg_tricuspid!=null)
			child1++;
			if(this.Valvregurg_aortic!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<ghf:valvRegurg";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Valvregurg_aortic!=null){
			xmlTxt+="\n<ghf:aortic";
			xmlTxt+=">";
			xmlTxt+=this.Valvregurg_aortic.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:aortic>";
		}
		if (this.Valvregurg_mitral!=null){
			xmlTxt+="\n<ghf:mitral";
			xmlTxt+=">";
			xmlTxt+=this.Valvregurg_mitral.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:mitral>";
		}
		if (this.Valvregurg_tricuspid!=null){
			xmlTxt+="\n<ghf:tricuspid";
			xmlTxt+=">";
			xmlTxt+=this.Valvregurg_tricuspid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:tricuspid>";
		}
		if (this.Valvregurg_pulmonic!=null){
			xmlTxt+="\n<ghf:pulmonic";
			xmlTxt+=">";
			xmlTxt+=this.Valvregurg_pulmonic.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:pulmonic>";
		}
				xmlTxt+="\n</ghf:valvRegurg>";
			}
			}

		if (this.Tr!=null){
			xmlTxt+="\n<ghf:tr";
			xmlTxt+=">";
			xmlTxt+=this.Tr;
			xmlTxt+="</ghf:tr>";
		}
		if (this.E!=null){
			xmlTxt+="\n<ghf:e";
			xmlTxt+=">";
			xmlTxt+=this.E;
			xmlTxt+="</ghf:e>";
		}
		if (this.A!=null){
			xmlTxt+="\n<ghf:a";
			xmlTxt+=">";
			xmlTxt+=this.A;
			xmlTxt+="</ghf:a>";
		}
		if (this.Mitraldt!=null){
			xmlTxt+="\n<ghf:mitralDt";
			xmlTxt+=">";
			xmlTxt+=this.Mitraldt;
			xmlTxt+="</ghf:mitralDt>";
		}
		if (this.Eml!=null){
			xmlTxt+="\n<ghf:eml";
			xmlTxt+=">";
			xmlTxt+=this.Eml;
			xmlTxt+="</ghf:eml>";
		}
		if (this.Ems!=null){
			xmlTxt+="\n<ghf:ems";
			xmlTxt+=">";
			xmlTxt+=this.Ems;
			xmlTxt+="</ghf:ems>";
		}
		if (this.Primarysig!=null){
			xmlTxt+="\n<ghf:primarySig";
			xmlTxt+=">";
			xmlTxt+=this.Primarysig.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:primarySig>";
		}
		if (this.Secondarysig!=null){
			xmlTxt+="\n<ghf:secondarySig";
			xmlTxt+=">";
			xmlTxt+=this.Secondarysig.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ghf:secondarySig>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Seqstudynumber!=null) return true;
		if (this.Analyzableecho!=null) return true;
		if (this.Image2dquality!=null) return true;
		if (this.Imagedpquality!=null) return true;
		if (this.Dominantechorhythm!=null) return true;
		if (this.Heartrate!=null) return true;
		if (this.Lvotd1!=null) return true;
		if (this.Lvidd!=null) return true;
		if (this.Ivs!=null) return true;
		if (this.Pw!=null) return true;
		if (this.Alwt!=null) return true;
		if (this.Lvids!=null) return true;
		if (this.Lvedv!=null) return true;
		if (this.Lvesv!=null) return true;
		if (this.V1!=null) return true;
		if (this.Lvottvi!=null) return true;
			if(this.Valvstenosis_aortic!=null) return true;
			if(this.Valvstenosis_pulmonic!=null) return true;
			if(this.Valvstenosis_tricuspid!=null) return true;
			if(this.Valvstenosis_mitral!=null) return true;
			if(this.Valvregurg_pulmonic!=null) return true;
			if(this.Valvregurg_mitral!=null) return true;
			if(this.Valvregurg_tricuspid!=null) return true;
			if(this.Valvregurg_aortic!=null) return true;
		if (this.Tr!=null) return true;
		if (this.E!=null) return true;
		if (this.A!=null) return true;
		if (this.Mitraldt!=null) return true;
		if (this.Eml!=null) return true;
		if (this.Ems!=null) return true;
		if (this.Primarysig!=null) return true;
		if (this.Secondarysig!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
