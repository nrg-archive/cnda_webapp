/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function adir_adir2007Data(){
this.xsiType="adir:adir2007Data";

	this.getSchemaElementName=function(){
		return "adir2007Data";
	}

	this.getFullSchemaElementName=function(){
		return "adir:adir2007Data";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Adirver=null;


	function getAdirver() {
		return this.Adirver;
	}
	this.getAdirver=getAdirver;


	function setAdirver(v){
		this.Adirver=v;
	}
	this.setAdirver=setAdirver;

	this.Atotal=null;


	function getAtotal() {
		return this.Atotal;
	}
	this.getAtotal=getAtotal;


	function setAtotal(v){
		this.Atotal=v;
	}
	this.setAtotal=setAtotal;

	this.AtotalNote=null;


	function getAtotalNote() {
		return this.AtotalNote;
	}
	this.getAtotalNote=getAtotalNote;


	function setAtotalNote(v){
		this.AtotalNote=v;
	}
	this.setAtotalNote=setAtotalNote;

	this.BvTotal=null;


	function getBvTotal() {
		return this.BvTotal;
	}
	this.getBvTotal=getBvTotal;


	function setBvTotal(v){
		this.BvTotal=v;
	}
	this.setBvTotal=setBvTotal;

	this.BvTotalNote=null;


	function getBvTotalNote() {
		return this.BvTotalNote;
	}
	this.getBvTotalNote=getBvTotalNote;


	function setBvTotalNote(v){
		this.BvTotalNote=v;
	}
	this.setBvTotalNote=setBvTotalNote;

	this.BnvTotal=null;


	function getBnvTotal() {
		return this.BnvTotal;
	}
	this.getBnvTotal=getBnvTotal;


	function setBnvTotal(v){
		this.BnvTotal=v;
	}
	this.setBnvTotal=setBnvTotal;

	this.BnvTotalNote=null;


	function getBnvTotalNote() {
		return this.BnvTotalNote;
	}
	this.getBnvTotalNote=getBnvTotalNote;


	function setBnvTotalNote(v){
		this.BnvTotalNote=v;
	}
	this.setBnvTotalNote=setBnvTotalNote;

	this.Ctotal=null;


	function getCtotal() {
		return this.Ctotal;
	}
	this.getCtotal=getCtotal;


	function setCtotal(v){
		this.Ctotal=v;
	}
	this.setCtotal=setCtotal;

	this.CtotalNote=null;


	function getCtotalNote() {
		return this.CtotalNote;
	}
	this.getCtotalNote=getCtotalNote;


	function setCtotalNote(v){
		this.CtotalNote=v;
	}
	this.setCtotalNote=setCtotalNote;

	this.Dtotal=null;


	function getDtotal() {
		return this.Dtotal;
	}
	this.getDtotal=getDtotal;


	function setDtotal(v){
		this.Dtotal=v;
	}
	this.setDtotal=setDtotal;

	this.DtotalNote=null;


	function getDtotalNote() {
		return this.DtotalNote;
	}
	this.getDtotalNote=getDtotalNote;


	function setDtotalNote(v){
		this.DtotalNote=v;
	}
	this.setDtotalNote=setDtotalNote;

	this.AdiRdx=null;


	function getAdiRdx() {
		return this.AdiRdx;
	}
	this.getAdiRdx=getAdiRdx;


	function setAdiRdx(v){
		this.AdiRdx=v;
	}
	this.setAdiRdx=setAdiRdx;

	this.AdiRcl=null;


	function getAdiRcl() {
		return this.AdiRcl;
	}
	this.getAdiRcl=getAdiRcl;


	function setAdiRcl(v){
		this.AdiRcl=v;
	}
	this.setAdiRcl=setAdiRcl;

	this.Scoreform_resprltnsubj=null;


	function getScoreform_resprltnsubj() {
		return this.Scoreform_resprltnsubj;
	}
	this.getScoreform_resprltnsubj=getScoreform_resprltnsubj;


	function setScoreform_resprltnsubj(v){
		this.Scoreform_resprltnsubj=v;
	}
	this.setScoreform_resprltnsubj=setScoreform_resprltnsubj;

	this.Scoreform_algused=null;


	function getScoreform_algused() {
		return this.Scoreform_algused;
	}
	this.getScoreform_algused=getScoreform_algused;


	function setScoreform_algused(v){
		this.Scoreform_algused=v;
	}
	this.setScoreform_algused=setScoreform_algused;

	this.Scoreform_sectiona1_dirgaze_code=null;


	function getScoreform_sectiona1_dirgaze_code() {
		return this.Scoreform_sectiona1_dirgaze_code;
	}
	this.getScoreform_sectiona1_dirgaze_code=getScoreform_sectiona1_dirgaze_code;


	function setScoreform_sectiona1_dirgaze_code(v){
		this.Scoreform_sectiona1_dirgaze_code=v;
	}
	this.setScoreform_sectiona1_dirgaze_code=setScoreform_sectiona1_dirgaze_code;

	this.Scoreform_sectiona1_dirgaze_score=null;


	function getScoreform_sectiona1_dirgaze_score() {
		return this.Scoreform_sectiona1_dirgaze_score;
	}
	this.getScoreform_sectiona1_dirgaze_score=getScoreform_sectiona1_dirgaze_score;


	function setScoreform_sectiona1_dirgaze_score(v){
		this.Scoreform_sectiona1_dirgaze_score=v;
	}
	this.setScoreform_sectiona1_dirgaze_score=setScoreform_sectiona1_dirgaze_score;

	this.Scoreform_sectiona1_socsmile_code=null;


	function getScoreform_sectiona1_socsmile_code() {
		return this.Scoreform_sectiona1_socsmile_code;
	}
	this.getScoreform_sectiona1_socsmile_code=getScoreform_sectiona1_socsmile_code;


	function setScoreform_sectiona1_socsmile_code(v){
		this.Scoreform_sectiona1_socsmile_code=v;
	}
	this.setScoreform_sectiona1_socsmile_code=setScoreform_sectiona1_socsmile_code;

	this.Scoreform_sectiona1_socsmile_score=null;


	function getScoreform_sectiona1_socsmile_score() {
		return this.Scoreform_sectiona1_socsmile_score;
	}
	this.getScoreform_sectiona1_socsmile_score=getScoreform_sectiona1_socsmile_score;


	function setScoreform_sectiona1_socsmile_score(v){
		this.Scoreform_sectiona1_socsmile_score=v;
	}
	this.setScoreform_sectiona1_socsmile_score=setScoreform_sectiona1_socsmile_score;

	this.Scoreform_sectiona1_facexpr_code=null;


	function getScoreform_sectiona1_facexpr_code() {
		return this.Scoreform_sectiona1_facexpr_code;
	}
	this.getScoreform_sectiona1_facexpr_code=getScoreform_sectiona1_facexpr_code;


	function setScoreform_sectiona1_facexpr_code(v){
		this.Scoreform_sectiona1_facexpr_code=v;
	}
	this.setScoreform_sectiona1_facexpr_code=setScoreform_sectiona1_facexpr_code;

	this.Scoreform_sectiona1_facexpr_score=null;


	function getScoreform_sectiona1_facexpr_score() {
		return this.Scoreform_sectiona1_facexpr_score;
	}
	this.getScoreform_sectiona1_facexpr_score=getScoreform_sectiona1_facexpr_score;


	function setScoreform_sectiona1_facexpr_score(v){
		this.Scoreform_sectiona1_facexpr_score=v;
	}
	this.setScoreform_sectiona1_facexpr_score=setScoreform_sectiona1_facexpr_score;

	this.Scoreform_sectiona2_imagplay_code=null;


	function getScoreform_sectiona2_imagplay_code() {
		return this.Scoreform_sectiona2_imagplay_code;
	}
	this.getScoreform_sectiona2_imagplay_code=getScoreform_sectiona2_imagplay_code;


	function setScoreform_sectiona2_imagplay_code(v){
		this.Scoreform_sectiona2_imagplay_code=v;
	}
	this.setScoreform_sectiona2_imagplay_code=setScoreform_sectiona2_imagplay_code;

	this.Scoreform_sectiona2_imagplay_score=null;


	function getScoreform_sectiona2_imagplay_score() {
		return this.Scoreform_sectiona2_imagplay_score;
	}
	this.getScoreform_sectiona2_imagplay_score=getScoreform_sectiona2_imagplay_score;


	function setScoreform_sectiona2_imagplay_score(v){
		this.Scoreform_sectiona2_imagplay_score=v;
	}
	this.setScoreform_sectiona2_imagplay_score=setScoreform_sectiona2_imagplay_score;

	this.Scoreform_sectiona2_intchild_code=null;


	function getScoreform_sectiona2_intchild_code() {
		return this.Scoreform_sectiona2_intchild_code;
	}
	this.getScoreform_sectiona2_intchild_code=getScoreform_sectiona2_intchild_code;


	function setScoreform_sectiona2_intchild_code(v){
		this.Scoreform_sectiona2_intchild_code=v;
	}
	this.setScoreform_sectiona2_intchild_code=setScoreform_sectiona2_intchild_code;

	this.Scoreform_sectiona2_intchild_score=null;


	function getScoreform_sectiona2_intchild_score() {
		return this.Scoreform_sectiona2_intchild_score;
	}
	this.getScoreform_sectiona2_intchild_score=getScoreform_sectiona2_intchild_score;


	function setScoreform_sectiona2_intchild_score(v){
		this.Scoreform_sectiona2_intchild_score=v;
	}
	this.setScoreform_sectiona2_intchild_score=setScoreform_sectiona2_intchild_score;

	this.Scoreform_sectiona2_respappr_code=null;


	function getScoreform_sectiona2_respappr_code() {
		return this.Scoreform_sectiona2_respappr_code;
	}
	this.getScoreform_sectiona2_respappr_code=getScoreform_sectiona2_respappr_code;


	function setScoreform_sectiona2_respappr_code(v){
		this.Scoreform_sectiona2_respappr_code=v;
	}
	this.setScoreform_sectiona2_respappr_code=setScoreform_sectiona2_respappr_code;

	this.Scoreform_sectiona2_respappr_score=null;


	function getScoreform_sectiona2_respappr_score() {
		return this.Scoreform_sectiona2_respappr_score;
	}
	this.getScoreform_sectiona2_respappr_score=getScoreform_sectiona2_respappr_score;


	function setScoreform_sectiona2_respappr_score(v){
		this.Scoreform_sectiona2_respappr_score=v;
	}
	this.setScoreform_sectiona2_respappr_score=setScoreform_sectiona2_respappr_score;

	this.Scoreform_sectiona2_grpplay_code=null;


	function getScoreform_sectiona2_grpplay_code() {
		return this.Scoreform_sectiona2_grpplay_code;
	}
	this.getScoreform_sectiona2_grpplay_code=getScoreform_sectiona2_grpplay_code;


	function setScoreform_sectiona2_grpplay_code(v){
		this.Scoreform_sectiona2_grpplay_code=v;
	}
	this.setScoreform_sectiona2_grpplay_code=setScoreform_sectiona2_grpplay_code;

	this.Scoreform_sectiona2_grpplay_score=null;


	function getScoreform_sectiona2_grpplay_score() {
		return this.Scoreform_sectiona2_grpplay_score;
	}
	this.getScoreform_sectiona2_grpplay_score=getScoreform_sectiona2_grpplay_score;


	function setScoreform_sectiona2_grpplay_score(v){
		this.Scoreform_sectiona2_grpplay_score=v;
	}
	this.setScoreform_sectiona2_grpplay_score=setScoreform_sectiona2_grpplay_score;

	this.Scoreform_sectiona2_friendships_code=null;


	function getScoreform_sectiona2_friendships_code() {
		return this.Scoreform_sectiona2_friendships_code;
	}
	this.getScoreform_sectiona2_friendships_code=getScoreform_sectiona2_friendships_code;


	function setScoreform_sectiona2_friendships_code(v){
		this.Scoreform_sectiona2_friendships_code=v;
	}
	this.setScoreform_sectiona2_friendships_code=setScoreform_sectiona2_friendships_code;

	this.Scoreform_sectiona2_friendships_score=null;


	function getScoreform_sectiona2_friendships_score() {
		return this.Scoreform_sectiona2_friendships_score;
	}
	this.getScoreform_sectiona2_friendships_score=getScoreform_sectiona2_friendships_score;


	function setScoreform_sectiona2_friendships_score(v){
		this.Scoreform_sectiona2_friendships_score=v;
	}
	this.setScoreform_sectiona2_friendships_score=setScoreform_sectiona2_friendships_score;

	this.Scoreform_sectiona3_showattn_code=null;


	function getScoreform_sectiona3_showattn_code() {
		return this.Scoreform_sectiona3_showattn_code;
	}
	this.getScoreform_sectiona3_showattn_code=getScoreform_sectiona3_showattn_code;


	function setScoreform_sectiona3_showattn_code(v){
		this.Scoreform_sectiona3_showattn_code=v;
	}
	this.setScoreform_sectiona3_showattn_code=setScoreform_sectiona3_showattn_code;

	this.Scoreform_sectiona3_showattn_score=null;


	function getScoreform_sectiona3_showattn_score() {
		return this.Scoreform_sectiona3_showattn_score;
	}
	this.getScoreform_sectiona3_showattn_score=getScoreform_sectiona3_showattn_score;


	function setScoreform_sectiona3_showattn_score(v){
		this.Scoreform_sectiona3_showattn_score=v;
	}
	this.setScoreform_sectiona3_showattn_score=setScoreform_sectiona3_showattn_score;

	this.Scoreform_sectiona3_offshare_code=null;


	function getScoreform_sectiona3_offshare_code() {
		return this.Scoreform_sectiona3_offshare_code;
	}
	this.getScoreform_sectiona3_offshare_code=getScoreform_sectiona3_offshare_code;


	function setScoreform_sectiona3_offshare_code(v){
		this.Scoreform_sectiona3_offshare_code=v;
	}
	this.setScoreform_sectiona3_offshare_code=setScoreform_sectiona3_offshare_code;

	this.Scoreform_sectiona3_offshare_score=null;


	function getScoreform_sectiona3_offshare_score() {
		return this.Scoreform_sectiona3_offshare_score;
	}
	this.getScoreform_sectiona3_offshare_score=getScoreform_sectiona3_offshare_score;


	function setScoreform_sectiona3_offshare_score(v){
		this.Scoreform_sectiona3_offshare_score=v;
	}
	this.setScoreform_sectiona3_offshare_score=setScoreform_sectiona3_offshare_score;

	this.Scoreform_sectiona3_shareenjoy_code=null;


	function getScoreform_sectiona3_shareenjoy_code() {
		return this.Scoreform_sectiona3_shareenjoy_code;
	}
	this.getScoreform_sectiona3_shareenjoy_code=getScoreform_sectiona3_shareenjoy_code;


	function setScoreform_sectiona3_shareenjoy_code(v){
		this.Scoreform_sectiona3_shareenjoy_code=v;
	}
	this.setScoreform_sectiona3_shareenjoy_code=setScoreform_sectiona3_shareenjoy_code;

	this.Scoreform_sectiona3_shareenjoy_score=null;


	function getScoreform_sectiona3_shareenjoy_score() {
		return this.Scoreform_sectiona3_shareenjoy_score;
	}
	this.getScoreform_sectiona3_shareenjoy_score=getScoreform_sectiona3_shareenjoy_score;


	function setScoreform_sectiona3_shareenjoy_score(v){
		this.Scoreform_sectiona3_shareenjoy_score=v;
	}
	this.setScoreform_sectiona3_shareenjoy_score=setScoreform_sectiona3_shareenjoy_score;

	this.Scoreform_sectiona4_othbodycomm_code=null;


	function getScoreform_sectiona4_othbodycomm_code() {
		return this.Scoreform_sectiona4_othbodycomm_code;
	}
	this.getScoreform_sectiona4_othbodycomm_code=getScoreform_sectiona4_othbodycomm_code;


	function setScoreform_sectiona4_othbodycomm_code(v){
		this.Scoreform_sectiona4_othbodycomm_code=v;
	}
	this.setScoreform_sectiona4_othbodycomm_code=setScoreform_sectiona4_othbodycomm_code;

	this.Scoreform_sectiona4_othbodycomm_score=null;


	function getScoreform_sectiona4_othbodycomm_score() {
		return this.Scoreform_sectiona4_othbodycomm_score;
	}
	this.getScoreform_sectiona4_othbodycomm_score=getScoreform_sectiona4_othbodycomm_score;


	function setScoreform_sectiona4_othbodycomm_score(v){
		this.Scoreform_sectiona4_othbodycomm_score=v;
	}
	this.setScoreform_sectiona4_othbodycomm_score=setScoreform_sectiona4_othbodycomm_score;

	this.Scoreform_sectiona4_offcomf_code=null;


	function getScoreform_sectiona4_offcomf_code() {
		return this.Scoreform_sectiona4_offcomf_code;
	}
	this.getScoreform_sectiona4_offcomf_code=getScoreform_sectiona4_offcomf_code;


	function setScoreform_sectiona4_offcomf_code(v){
		this.Scoreform_sectiona4_offcomf_code=v;
	}
	this.setScoreform_sectiona4_offcomf_code=setScoreform_sectiona4_offcomf_code;

	this.Scoreform_sectiona4_offcomf_score=null;


	function getScoreform_sectiona4_offcomf_score() {
		return this.Scoreform_sectiona4_offcomf_score;
	}
	this.getScoreform_sectiona4_offcomf_score=getScoreform_sectiona4_offcomf_score;


	function setScoreform_sectiona4_offcomf_score(v){
		this.Scoreform_sectiona4_offcomf_score=v;
	}
	this.setScoreform_sectiona4_offcomf_score=setScoreform_sectiona4_offcomf_score;

	this.Scoreform_sectiona4_qualsocovert_code=null;


	function getScoreform_sectiona4_qualsocovert_code() {
		return this.Scoreform_sectiona4_qualsocovert_code;
	}
	this.getScoreform_sectiona4_qualsocovert_code=getScoreform_sectiona4_qualsocovert_code;


	function setScoreform_sectiona4_qualsocovert_code(v){
		this.Scoreform_sectiona4_qualsocovert_code=v;
	}
	this.setScoreform_sectiona4_qualsocovert_code=setScoreform_sectiona4_qualsocovert_code;

	this.Scoreform_sectiona4_qualsocovert_score=null;


	function getScoreform_sectiona4_qualsocovert_score() {
		return this.Scoreform_sectiona4_qualsocovert_score;
	}
	this.getScoreform_sectiona4_qualsocovert_score=getScoreform_sectiona4_qualsocovert_score;


	function setScoreform_sectiona4_qualsocovert_score(v){
		this.Scoreform_sectiona4_qualsocovert_score=v;
	}
	this.setScoreform_sectiona4_qualsocovert_score=setScoreform_sectiona4_qualsocovert_score;

	this.Scoreform_sectiona4_inapfacexpr_code=null;


	function getScoreform_sectiona4_inapfacexpr_code() {
		return this.Scoreform_sectiona4_inapfacexpr_code;
	}
	this.getScoreform_sectiona4_inapfacexpr_code=getScoreform_sectiona4_inapfacexpr_code;


	function setScoreform_sectiona4_inapfacexpr_code(v){
		this.Scoreform_sectiona4_inapfacexpr_code=v;
	}
	this.setScoreform_sectiona4_inapfacexpr_code=setScoreform_sectiona4_inapfacexpr_code;

	this.Scoreform_sectiona4_inapfacexpr_score=null;


	function getScoreform_sectiona4_inapfacexpr_score() {
		return this.Scoreform_sectiona4_inapfacexpr_score;
	}
	this.getScoreform_sectiona4_inapfacexpr_score=getScoreform_sectiona4_inapfacexpr_score;


	function setScoreform_sectiona4_inapfacexpr_score(v){
		this.Scoreform_sectiona4_inapfacexpr_score=v;
	}
	this.setScoreform_sectiona4_inapfacexpr_score=setScoreform_sectiona4_inapfacexpr_score;

	this.Scoreform_sectiona4_apprsocresp_code=null;


	function getScoreform_sectiona4_apprsocresp_code() {
		return this.Scoreform_sectiona4_apprsocresp_code;
	}
	this.getScoreform_sectiona4_apprsocresp_code=getScoreform_sectiona4_apprsocresp_code;


	function setScoreform_sectiona4_apprsocresp_code(v){
		this.Scoreform_sectiona4_apprsocresp_code=v;
	}
	this.setScoreform_sectiona4_apprsocresp_code=setScoreform_sectiona4_apprsocresp_code;

	this.Scoreform_sectiona4_apprsocresp_score=null;


	function getScoreform_sectiona4_apprsocresp_score() {
		return this.Scoreform_sectiona4_apprsocresp_score;
	}
	this.getScoreform_sectiona4_apprsocresp_score=getScoreform_sectiona4_apprsocresp_score;


	function setScoreform_sectiona4_apprsocresp_score(v){
		this.Scoreform_sectiona4_apprsocresp_score=v;
	}
	this.setScoreform_sectiona4_apprsocresp_score=setScoreform_sectiona4_apprsocresp_score;

	this.Scoreform_sectionb1_pointexprint_code=null;


	function getScoreform_sectionb1_pointexprint_code() {
		return this.Scoreform_sectionb1_pointexprint_code;
	}
	this.getScoreform_sectionb1_pointexprint_code=getScoreform_sectionb1_pointexprint_code;


	function setScoreform_sectionb1_pointexprint_code(v){
		this.Scoreform_sectionb1_pointexprint_code=v;
	}
	this.setScoreform_sectionb1_pointexprint_code=setScoreform_sectionb1_pointexprint_code;

	this.Scoreform_sectionb1_pointexprint_score=null;


	function getScoreform_sectionb1_pointexprint_score() {
		return this.Scoreform_sectionb1_pointexprint_score;
	}
	this.getScoreform_sectionb1_pointexprint_score=getScoreform_sectionb1_pointexprint_score;


	function setScoreform_sectionb1_pointexprint_score(v){
		this.Scoreform_sectionb1_pointexprint_score=v;
	}
	this.setScoreform_sectionb1_pointexprint_score=setScoreform_sectionb1_pointexprint_score;

	this.Scoreform_sectionb1_nodding_code=null;


	function getScoreform_sectionb1_nodding_code() {
		return this.Scoreform_sectionb1_nodding_code;
	}
	this.getScoreform_sectionb1_nodding_code=getScoreform_sectionb1_nodding_code;


	function setScoreform_sectionb1_nodding_code(v){
		this.Scoreform_sectionb1_nodding_code=v;
	}
	this.setScoreform_sectionb1_nodding_code=setScoreform_sectionb1_nodding_code;

	this.Scoreform_sectionb1_nodding_score=null;


	function getScoreform_sectionb1_nodding_score() {
		return this.Scoreform_sectionb1_nodding_score;
	}
	this.getScoreform_sectionb1_nodding_score=getScoreform_sectionb1_nodding_score;


	function setScoreform_sectionb1_nodding_score(v){
		this.Scoreform_sectionb1_nodding_score=v;
	}
	this.setScoreform_sectionb1_nodding_score=setScoreform_sectionb1_nodding_score;

	this.Scoreform_sectionb1_headshak_code=null;


	function getScoreform_sectionb1_headshak_code() {
		return this.Scoreform_sectionb1_headshak_code;
	}
	this.getScoreform_sectionb1_headshak_code=getScoreform_sectionb1_headshak_code;


	function setScoreform_sectionb1_headshak_code(v){
		this.Scoreform_sectionb1_headshak_code=v;
	}
	this.setScoreform_sectionb1_headshak_code=setScoreform_sectionb1_headshak_code;

	this.Scoreform_sectionb1_headshak_score=null;


	function getScoreform_sectionb1_headshak_score() {
		return this.Scoreform_sectionb1_headshak_score;
	}
	this.getScoreform_sectionb1_headshak_score=getScoreform_sectionb1_headshak_score;


	function setScoreform_sectionb1_headshak_score(v){
		this.Scoreform_sectionb1_headshak_score=v;
	}
	this.setScoreform_sectionb1_headshak_score=setScoreform_sectionb1_headshak_score;

	this.Scoreform_sectionb1_convinstrgest_code=null;


	function getScoreform_sectionb1_convinstrgest_code() {
		return this.Scoreform_sectionb1_convinstrgest_code;
	}
	this.getScoreform_sectionb1_convinstrgest_code=getScoreform_sectionb1_convinstrgest_code;


	function setScoreform_sectionb1_convinstrgest_code(v){
		this.Scoreform_sectionb1_convinstrgest_code=v;
	}
	this.setScoreform_sectionb1_convinstrgest_code=setScoreform_sectionb1_convinstrgest_code;

	this.Scoreform_sectionb1_convinstrgest_score=null;


	function getScoreform_sectionb1_convinstrgest_score() {
		return this.Scoreform_sectionb1_convinstrgest_score;
	}
	this.getScoreform_sectionb1_convinstrgest_score=getScoreform_sectionb1_convinstrgest_score;


	function setScoreform_sectionb1_convinstrgest_score(v){
		this.Scoreform_sectionb1_convinstrgest_score=v;
	}
	this.setScoreform_sectionb1_convinstrgest_score=setScoreform_sectionb1_convinstrgest_score;

	this.Scoreform_sectionb4_spontimit_code=null;


	function getScoreform_sectionb4_spontimit_code() {
		return this.Scoreform_sectionb4_spontimit_code;
	}
	this.getScoreform_sectionb4_spontimit_code=getScoreform_sectionb4_spontimit_code;


	function setScoreform_sectionb4_spontimit_code(v){
		this.Scoreform_sectionb4_spontimit_code=v;
	}
	this.setScoreform_sectionb4_spontimit_code=setScoreform_sectionb4_spontimit_code;

	this.Scoreform_sectionb4_spontimit_score=null;


	function getScoreform_sectionb4_spontimit_score() {
		return this.Scoreform_sectionb4_spontimit_score;
	}
	this.getScoreform_sectionb4_spontimit_score=getScoreform_sectionb4_spontimit_score;


	function setScoreform_sectionb4_spontimit_score(v){
		this.Scoreform_sectionb4_spontimit_score=v;
	}
	this.setScoreform_sectionb4_spontimit_score=setScoreform_sectionb4_spontimit_score;

	this.Scoreform_sectionb4_imagplay_code=null;


	function getScoreform_sectionb4_imagplay_code() {
		return this.Scoreform_sectionb4_imagplay_code;
	}
	this.getScoreform_sectionb4_imagplay_code=getScoreform_sectionb4_imagplay_code;


	function setScoreform_sectionb4_imagplay_code(v){
		this.Scoreform_sectionb4_imagplay_code=v;
	}
	this.setScoreform_sectionb4_imagplay_code=setScoreform_sectionb4_imagplay_code;

	this.Scoreform_sectionb4_imagplay_score=null;


	function getScoreform_sectionb4_imagplay_score() {
		return this.Scoreform_sectionb4_imagplay_score;
	}
	this.getScoreform_sectionb4_imagplay_score=getScoreform_sectionb4_imagplay_score;


	function setScoreform_sectionb4_imagplay_score(v){
		this.Scoreform_sectionb4_imagplay_score=v;
	}
	this.setScoreform_sectionb4_imagplay_score=setScoreform_sectionb4_imagplay_score;

	this.Scoreform_sectionb4_imitplay_code=null;


	function getScoreform_sectionb4_imitplay_code() {
		return this.Scoreform_sectionb4_imitplay_code;
	}
	this.getScoreform_sectionb4_imitplay_code=getScoreform_sectionb4_imitplay_code;


	function setScoreform_sectionb4_imitplay_code(v){
		this.Scoreform_sectionb4_imitplay_code=v;
	}
	this.setScoreform_sectionb4_imitplay_code=setScoreform_sectionb4_imitplay_code;

	this.Scoreform_sectionb4_imitplay_score=null;


	function getScoreform_sectionb4_imitplay_score() {
		return this.Scoreform_sectionb4_imitplay_score;
	}
	this.getScoreform_sectionb4_imitplay_score=getScoreform_sectionb4_imitplay_score;


	function setScoreform_sectionb4_imitplay_score(v){
		this.Scoreform_sectionb4_imitplay_score=v;
	}
	this.setScoreform_sectionb4_imitplay_score=setScoreform_sectionb4_imitplay_score;

	this.Scoreform_sectionb2_socverb_code=null;


	function getScoreform_sectionb2_socverb_code() {
		return this.Scoreform_sectionb2_socverb_code;
	}
	this.getScoreform_sectionb2_socverb_code=getScoreform_sectionb2_socverb_code;


	function setScoreform_sectionb2_socverb_code(v){
		this.Scoreform_sectionb2_socverb_code=v;
	}
	this.setScoreform_sectionb2_socverb_code=setScoreform_sectionb2_socverb_code;

	this.Scoreform_sectionb2_socverb_score=null;


	function getScoreform_sectionb2_socverb_score() {
		return this.Scoreform_sectionb2_socverb_score;
	}
	this.getScoreform_sectionb2_socverb_score=getScoreform_sectionb2_socverb_score;


	function setScoreform_sectionb2_socverb_score(v){
		this.Scoreform_sectionb2_socverb_score=v;
	}
	this.setScoreform_sectionb2_socverb_score=setScoreform_sectionb2_socverb_score;

	this.Scoreform_sectionb2_recipconv_code=null;


	function getScoreform_sectionb2_recipconv_code() {
		return this.Scoreform_sectionb2_recipconv_code;
	}
	this.getScoreform_sectionb2_recipconv_code=getScoreform_sectionb2_recipconv_code;


	function setScoreform_sectionb2_recipconv_code(v){
		this.Scoreform_sectionb2_recipconv_code=v;
	}
	this.setScoreform_sectionb2_recipconv_code=setScoreform_sectionb2_recipconv_code;

	this.Scoreform_sectionb2_recipconv_score=null;


	function getScoreform_sectionb2_recipconv_score() {
		return this.Scoreform_sectionb2_recipconv_score;
	}
	this.getScoreform_sectionb2_recipconv_score=getScoreform_sectionb2_recipconv_score;


	function setScoreform_sectionb2_recipconv_score(v){
		this.Scoreform_sectionb2_recipconv_score=v;
	}
	this.setScoreform_sectionb2_recipconv_score=setScoreform_sectionb2_recipconv_score;

	this.Scoreform_sectionb3_stereoutter_code=null;


	function getScoreform_sectionb3_stereoutter_code() {
		return this.Scoreform_sectionb3_stereoutter_code;
	}
	this.getScoreform_sectionb3_stereoutter_code=getScoreform_sectionb3_stereoutter_code;


	function setScoreform_sectionb3_stereoutter_code(v){
		this.Scoreform_sectionb3_stereoutter_code=v;
	}
	this.setScoreform_sectionb3_stereoutter_code=setScoreform_sectionb3_stereoutter_code;

	this.Scoreform_sectionb3_stereoutter_score=null;


	function getScoreform_sectionb3_stereoutter_score() {
		return this.Scoreform_sectionb3_stereoutter_score;
	}
	this.getScoreform_sectionb3_stereoutter_score=getScoreform_sectionb3_stereoutter_score;


	function setScoreform_sectionb3_stereoutter_score(v){
		this.Scoreform_sectionb3_stereoutter_score=v;
	}
	this.setScoreform_sectionb3_stereoutter_score=setScoreform_sectionb3_stereoutter_score;

	this.Scoreform_sectionb3_inapprquest_code=null;


	function getScoreform_sectionb3_inapprquest_code() {
		return this.Scoreform_sectionb3_inapprquest_code;
	}
	this.getScoreform_sectionb3_inapprquest_code=getScoreform_sectionb3_inapprquest_code;


	function setScoreform_sectionb3_inapprquest_code(v){
		this.Scoreform_sectionb3_inapprquest_code=v;
	}
	this.setScoreform_sectionb3_inapprquest_code=setScoreform_sectionb3_inapprquest_code;

	this.Scoreform_sectionb3_inapprquest_score=null;


	function getScoreform_sectionb3_inapprquest_score() {
		return this.Scoreform_sectionb3_inapprquest_score;
	}
	this.getScoreform_sectionb3_inapprquest_score=getScoreform_sectionb3_inapprquest_score;


	function setScoreform_sectionb3_inapprquest_score(v){
		this.Scoreform_sectionb3_inapprquest_score=v;
	}
	this.setScoreform_sectionb3_inapprquest_score=setScoreform_sectionb3_inapprquest_score;

	this.Scoreform_sectionb3_pronomrevers_code=null;


	function getScoreform_sectionb3_pronomrevers_code() {
		return this.Scoreform_sectionb3_pronomrevers_code;
	}
	this.getScoreform_sectionb3_pronomrevers_code=getScoreform_sectionb3_pronomrevers_code;


	function setScoreform_sectionb3_pronomrevers_code(v){
		this.Scoreform_sectionb3_pronomrevers_code=v;
	}
	this.setScoreform_sectionb3_pronomrevers_code=setScoreform_sectionb3_pronomrevers_code;

	this.Scoreform_sectionb3_pronomrevers_score=null;


	function getScoreform_sectionb3_pronomrevers_score() {
		return this.Scoreform_sectionb3_pronomrevers_score;
	}
	this.getScoreform_sectionb3_pronomrevers_score=getScoreform_sectionb3_pronomrevers_score;


	function setScoreform_sectionb3_pronomrevers_score(v){
		this.Scoreform_sectionb3_pronomrevers_score=v;
	}
	this.setScoreform_sectionb3_pronomrevers_score=setScoreform_sectionb3_pronomrevers_score;

	this.Scoreform_sectionb3_neologidio_code=null;


	function getScoreform_sectionb3_neologidio_code() {
		return this.Scoreform_sectionb3_neologidio_code;
	}
	this.getScoreform_sectionb3_neologidio_code=getScoreform_sectionb3_neologidio_code;


	function setScoreform_sectionb3_neologidio_code(v){
		this.Scoreform_sectionb3_neologidio_code=v;
	}
	this.setScoreform_sectionb3_neologidio_code=setScoreform_sectionb3_neologidio_code;

	this.Scoreform_sectionb3_neologidio_score=null;


	function getScoreform_sectionb3_neologidio_score() {
		return this.Scoreform_sectionb3_neologidio_score;
	}
	this.getScoreform_sectionb3_neologidio_score=getScoreform_sectionb3_neologidio_score;


	function setScoreform_sectionb3_neologidio_score(v){
		this.Scoreform_sectionb3_neologidio_score=v;
	}
	this.setScoreform_sectionb3_neologidio_score=setScoreform_sectionb3_neologidio_score;

	this.Scoreform_sectionc1_unuspreocup_code=null;


	function getScoreform_sectionc1_unuspreocup_code() {
		return this.Scoreform_sectionc1_unuspreocup_code;
	}
	this.getScoreform_sectionc1_unuspreocup_code=getScoreform_sectionc1_unuspreocup_code;


	function setScoreform_sectionc1_unuspreocup_code(v){
		this.Scoreform_sectionc1_unuspreocup_code=v;
	}
	this.setScoreform_sectionc1_unuspreocup_code=setScoreform_sectionc1_unuspreocup_code;

	this.Scoreform_sectionc1_unuspreocup_score=null;


	function getScoreform_sectionc1_unuspreocup_score() {
		return this.Scoreform_sectionc1_unuspreocup_score;
	}
	this.getScoreform_sectionc1_unuspreocup_score=getScoreform_sectionc1_unuspreocup_score;


	function setScoreform_sectionc1_unuspreocup_score(v){
		this.Scoreform_sectionc1_unuspreocup_score=v;
	}
	this.setScoreform_sectionc1_unuspreocup_score=setScoreform_sectionc1_unuspreocup_score;

	this.Scoreform_sectionc1_circumints_code=null;


	function getScoreform_sectionc1_circumints_code() {
		return this.Scoreform_sectionc1_circumints_code;
	}
	this.getScoreform_sectionc1_circumints_code=getScoreform_sectionc1_circumints_code;


	function setScoreform_sectionc1_circumints_code(v){
		this.Scoreform_sectionc1_circumints_code=v;
	}
	this.setScoreform_sectionc1_circumints_code=setScoreform_sectionc1_circumints_code;

	this.Scoreform_sectionc1_circumints_score=null;


	function getScoreform_sectionc1_circumints_score() {
		return this.Scoreform_sectionc1_circumints_score;
	}
	this.getScoreform_sectionc1_circumints_score=getScoreform_sectionc1_circumints_score;


	function setScoreform_sectionc1_circumints_score(v){
		this.Scoreform_sectionc1_circumints_score=v;
	}
	this.setScoreform_sectionc1_circumints_score=setScoreform_sectionc1_circumints_score;

	this.Scoreform_sectionc2_verbrits_code=null;


	function getScoreform_sectionc2_verbrits_code() {
		return this.Scoreform_sectionc2_verbrits_code;
	}
	this.getScoreform_sectionc2_verbrits_code=getScoreform_sectionc2_verbrits_code;


	function setScoreform_sectionc2_verbrits_code(v){
		this.Scoreform_sectionc2_verbrits_code=v;
	}
	this.setScoreform_sectionc2_verbrits_code=setScoreform_sectionc2_verbrits_code;

	this.Scoreform_sectionc2_verbrits_score=null;


	function getScoreform_sectionc2_verbrits_score() {
		return this.Scoreform_sectionc2_verbrits_score;
	}
	this.getScoreform_sectionc2_verbrits_score=getScoreform_sectionc2_verbrits_score;


	function setScoreform_sectionc2_verbrits_score(v){
		this.Scoreform_sectionc2_verbrits_score=v;
	}
	this.setScoreform_sectionc2_verbrits_score=setScoreform_sectionc2_verbrits_score;

	this.Scoreform_sectionc2_compulrits_code=null;


	function getScoreform_sectionc2_compulrits_code() {
		return this.Scoreform_sectionc2_compulrits_code;
	}
	this.getScoreform_sectionc2_compulrits_code=getScoreform_sectionc2_compulrits_code;


	function setScoreform_sectionc2_compulrits_code(v){
		this.Scoreform_sectionc2_compulrits_code=v;
	}
	this.setScoreform_sectionc2_compulrits_code=setScoreform_sectionc2_compulrits_code;

	this.Scoreform_sectionc2_compulrits_score=null;


	function getScoreform_sectionc2_compulrits_score() {
		return this.Scoreform_sectionc2_compulrits_score;
	}
	this.getScoreform_sectionc2_compulrits_score=getScoreform_sectionc2_compulrits_score;


	function setScoreform_sectionc2_compulrits_score(v){
		this.Scoreform_sectionc2_compulrits_score=v;
	}
	this.setScoreform_sectionc2_compulrits_score=setScoreform_sectionc2_compulrits_score;

	this.Scoreform_sectionc3_handfingmanns_code=null;


	function getScoreform_sectionc3_handfingmanns_code() {
		return this.Scoreform_sectionc3_handfingmanns_code;
	}
	this.getScoreform_sectionc3_handfingmanns_code=getScoreform_sectionc3_handfingmanns_code;


	function setScoreform_sectionc3_handfingmanns_code(v){
		this.Scoreform_sectionc3_handfingmanns_code=v;
	}
	this.setScoreform_sectionc3_handfingmanns_code=setScoreform_sectionc3_handfingmanns_code;

	this.Scoreform_sectionc3_handfingmanns_score=null;


	function getScoreform_sectionc3_handfingmanns_score() {
		return this.Scoreform_sectionc3_handfingmanns_score;
	}
	this.getScoreform_sectionc3_handfingmanns_score=getScoreform_sectionc3_handfingmanns_score;


	function setScoreform_sectionc3_handfingmanns_score(v){
		this.Scoreform_sectionc3_handfingmanns_score=v;
	}
	this.setScoreform_sectionc3_handfingmanns_score=setScoreform_sectionc3_handfingmanns_score;

	this.Scoreform_sectionc3_othcomplxmanns_code=null;


	function getScoreform_sectionc3_othcomplxmanns_code() {
		return this.Scoreform_sectionc3_othcomplxmanns_code;
	}
	this.getScoreform_sectionc3_othcomplxmanns_code=getScoreform_sectionc3_othcomplxmanns_code;


	function setScoreform_sectionc3_othcomplxmanns_code(v){
		this.Scoreform_sectionc3_othcomplxmanns_code=v;
	}
	this.setScoreform_sectionc3_othcomplxmanns_code=setScoreform_sectionc3_othcomplxmanns_code;

	this.Scoreform_sectionc3_othcomplxmanns_score=null;


	function getScoreform_sectionc3_othcomplxmanns_score() {
		return this.Scoreform_sectionc3_othcomplxmanns_score;
	}
	this.getScoreform_sectionc3_othcomplxmanns_score=getScoreform_sectionc3_othcomplxmanns_score;


	function setScoreform_sectionc3_othcomplxmanns_score(v){
		this.Scoreform_sectionc3_othcomplxmanns_score=v;
	}
	this.setScoreform_sectionc3_othcomplxmanns_score=setScoreform_sectionc3_othcomplxmanns_score;

	this.Scoreform_sectionc4_repetuseobjs_code=null;


	function getScoreform_sectionc4_repetuseobjs_code() {
		return this.Scoreform_sectionc4_repetuseobjs_code;
	}
	this.getScoreform_sectionc4_repetuseobjs_code=getScoreform_sectionc4_repetuseobjs_code;


	function setScoreform_sectionc4_repetuseobjs_code(v){
		this.Scoreform_sectionc4_repetuseobjs_code=v;
	}
	this.setScoreform_sectionc4_repetuseobjs_code=setScoreform_sectionc4_repetuseobjs_code;

	this.Scoreform_sectionc4_repetuseobjs_score=null;


	function getScoreform_sectionc4_repetuseobjs_score() {
		return this.Scoreform_sectionc4_repetuseobjs_score;
	}
	this.getScoreform_sectionc4_repetuseobjs_score=getScoreform_sectionc4_repetuseobjs_score;


	function setScoreform_sectionc4_repetuseobjs_score(v){
		this.Scoreform_sectionc4_repetuseobjs_score=v;
	}
	this.setScoreform_sectionc4_repetuseobjs_score=setScoreform_sectionc4_repetuseobjs_score;

	this.Scoreform_sectionc4_unussensints_code=null;


	function getScoreform_sectionc4_unussensints_code() {
		return this.Scoreform_sectionc4_unussensints_code;
	}
	this.getScoreform_sectionc4_unussensints_code=getScoreform_sectionc4_unussensints_code;


	function setScoreform_sectionc4_unussensints_code(v){
		this.Scoreform_sectionc4_unussensints_code=v;
	}
	this.setScoreform_sectionc4_unussensints_code=setScoreform_sectionc4_unussensints_code;

	this.Scoreform_sectionc4_unussensints_score=null;


	function getScoreform_sectionc4_unussensints_score() {
		return this.Scoreform_sectionc4_unussensints_score;
	}
	this.getScoreform_sectionc4_unussensints_score=getScoreform_sectionc4_unussensints_score;


	function setScoreform_sectionc4_unussensints_score(v){
		this.Scoreform_sectionc4_unussensints_score=v;
	}
	this.setScoreform_sectionc4_unussensints_score=setScoreform_sectionc4_unussensints_score;

	this.Scoreform_sectiond_agefirstnot_code=null;


	function getScoreform_sectiond_agefirstnot_code() {
		return this.Scoreform_sectiond_agefirstnot_code;
	}
	this.getScoreform_sectiond_agefirstnot_code=getScoreform_sectiond_agefirstnot_code;


	function setScoreform_sectiond_agefirstnot_code(v){
		this.Scoreform_sectiond_agefirstnot_code=v;
	}
	this.setScoreform_sectiond_agefirstnot_code=setScoreform_sectiond_agefirstnot_code;

	this.Scoreform_sectiond_agefirstnot_score=null;


	function getScoreform_sectiond_agefirstnot_score() {
		return this.Scoreform_sectiond_agefirstnot_score;
	}
	this.getScoreform_sectiond_agefirstnot_score=getScoreform_sectiond_agefirstnot_score;


	function setScoreform_sectiond_agefirstnot_score(v){
		this.Scoreform_sectiond_agefirstnot_score=v;
	}
	this.setScoreform_sectiond_agefirstnot_score=setScoreform_sectiond_agefirstnot_score;

	this.Scoreform_sectiond_agefirstword_code=null;


	function getScoreform_sectiond_agefirstword_code() {
		return this.Scoreform_sectiond_agefirstword_code;
	}
	this.getScoreform_sectiond_agefirstword_code=getScoreform_sectiond_agefirstword_code;


	function setScoreform_sectiond_agefirstword_code(v){
		this.Scoreform_sectiond_agefirstword_code=v;
	}
	this.setScoreform_sectiond_agefirstword_code=setScoreform_sectiond_agefirstword_code;

	this.Scoreform_sectiond_agefirstword_score=null;


	function getScoreform_sectiond_agefirstword_score() {
		return this.Scoreform_sectiond_agefirstword_score;
	}
	this.getScoreform_sectiond_agefirstword_score=getScoreform_sectiond_agefirstword_score;


	function setScoreform_sectiond_agefirstword_score(v){
		this.Scoreform_sectiond_agefirstword_score=v;
	}
	this.setScoreform_sectiond_agefirstword_score=setScoreform_sectiond_agefirstword_score;

	this.Scoreform_sectiond_agefirstphrase_code=null;


	function getScoreform_sectiond_agefirstphrase_code() {
		return this.Scoreform_sectiond_agefirstphrase_code;
	}
	this.getScoreform_sectiond_agefirstphrase_code=getScoreform_sectiond_agefirstphrase_code;


	function setScoreform_sectiond_agefirstphrase_code(v){
		this.Scoreform_sectiond_agefirstphrase_code=v;
	}
	this.setScoreform_sectiond_agefirstphrase_code=setScoreform_sectiond_agefirstphrase_code;

	this.Scoreform_sectiond_agefirstphrase_score=null;


	function getScoreform_sectiond_agefirstphrase_score() {
		return this.Scoreform_sectiond_agefirstphrase_score;
	}
	this.getScoreform_sectiond_agefirstphrase_score=getScoreform_sectiond_agefirstphrase_score;


	function setScoreform_sectiond_agefirstphrase_score(v){
		this.Scoreform_sectiond_agefirstphrase_score=v;
	}
	this.setScoreform_sectiond_agefirstphrase_score=setScoreform_sectiond_agefirstphrase_score;

	this.Scoreform_sectiond_agefirstevdnt_code=null;


	function getScoreform_sectiond_agefirstevdnt_code() {
		return this.Scoreform_sectiond_agefirstevdnt_code;
	}
	this.getScoreform_sectiond_agefirstevdnt_code=getScoreform_sectiond_agefirstevdnt_code;


	function setScoreform_sectiond_agefirstevdnt_code(v){
		this.Scoreform_sectiond_agefirstevdnt_code=v;
	}
	this.setScoreform_sectiond_agefirstevdnt_code=setScoreform_sectiond_agefirstevdnt_code;

	this.Scoreform_sectiond_agefirstevdnt_score=null;


	function getScoreform_sectiond_agefirstevdnt_score() {
		return this.Scoreform_sectiond_agefirstevdnt_score;
	}
	this.getScoreform_sectiond_agefirstevdnt_score=getScoreform_sectiond_agefirstevdnt_score;


	function setScoreform_sectiond_agefirstevdnt_score(v){
		this.Scoreform_sectiond_agefirstevdnt_score=v;
	}
	this.setScoreform_sectiond_agefirstevdnt_score=setScoreform_sectiond_agefirstevdnt_score;

	this.Scoreform_sectiond_agefirstmanifst_code=null;


	function getScoreform_sectiond_agefirstmanifst_code() {
		return this.Scoreform_sectiond_agefirstmanifst_code;
	}
	this.getScoreform_sectiond_agefirstmanifst_code=getScoreform_sectiond_agefirstmanifst_code;


	function setScoreform_sectiond_agefirstmanifst_code(v){
		this.Scoreform_sectiond_agefirstmanifst_code=v;
	}
	this.setScoreform_sectiond_agefirstmanifst_code=setScoreform_sectiond_agefirstmanifst_code;

	this.Scoreform_sectiond_agefirstmanifst_score=null;


	function getScoreform_sectiond_agefirstmanifst_score() {
		return this.Scoreform_sectiond_agefirstmanifst_score;
	}
	this.getScoreform_sectiond_agefirstmanifst_score=getScoreform_sectiond_agefirstmanifst_score;


	function setScoreform_sectiond_agefirstmanifst_score(v){
		this.Scoreform_sectiond_agefirstmanifst_score=v;
	}
	this.setScoreform_sectiond_agefirstmanifst_score=setScoreform_sectiond_agefirstmanifst_score;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="adirVer"){
				return this.Adirver ;
			} else 
			if(xmlPath=="a_total"){
				return this.Atotal ;
			} else 
			if(xmlPath=="a_total_note"){
				return this.AtotalNote ;
			} else 
			if(xmlPath=="b_v_total"){
				return this.BvTotal ;
			} else 
			if(xmlPath=="b_v_total_note"){
				return this.BvTotalNote ;
			} else 
			if(xmlPath=="b_nv_total"){
				return this.BnvTotal ;
			} else 
			if(xmlPath=="b_nv_total_note"){
				return this.BnvTotalNote ;
			} else 
			if(xmlPath=="c_total"){
				return this.Ctotal ;
			} else 
			if(xmlPath=="c_total_note"){
				return this.CtotalNote ;
			} else 
			if(xmlPath=="d_total"){
				return this.Dtotal ;
			} else 
			if(xmlPath=="d_total_note"){
				return this.DtotalNote ;
			} else 
			if(xmlPath=="adi_r_dx"){
				return this.AdiRdx ;
			} else 
			if(xmlPath=="adi_r_cl"){
				return this.AdiRcl ;
			} else 
			if(xmlPath=="ScoreForm/respRltnSubj"){
				return this.Scoreform_resprltnsubj ;
			} else 
			if(xmlPath=="ScoreForm/algUsed"){
				return this.Scoreform_algused ;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/dirGaze/code"){
				return this.Scoreform_sectiona1_dirgaze_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/dirGaze/score"){
				return this.Scoreform_sectiona1_dirgaze_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/socSmile/code"){
				return this.Scoreform_sectiona1_socsmile_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/socSmile/score"){
				return this.Scoreform_sectiona1_socsmile_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/facExpr/code"){
				return this.Scoreform_sectiona1_facexpr_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/facExpr/score"){
				return this.Scoreform_sectiona1_facexpr_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/imagPlay/code"){
				return this.Scoreform_sectiona2_imagplay_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/imagPlay/score"){
				return this.Scoreform_sectiona2_imagplay_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/intChild/code"){
				return this.Scoreform_sectiona2_intchild_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/intChild/score"){
				return this.Scoreform_sectiona2_intchild_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/respAppr/code"){
				return this.Scoreform_sectiona2_respappr_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/respAppr/score"){
				return this.Scoreform_sectiona2_respappr_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/grpPlay/code"){
				return this.Scoreform_sectiona2_grpplay_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/grpPlay/score"){
				return this.Scoreform_sectiona2_grpplay_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/friendships/code"){
				return this.Scoreform_sectiona2_friendships_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/friendships/score"){
				return this.Scoreform_sectiona2_friendships_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/showAttn/code"){
				return this.Scoreform_sectiona3_showattn_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/showAttn/score"){
				return this.Scoreform_sectiona3_showattn_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/offShare/code"){
				return this.Scoreform_sectiona3_offshare_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/offShare/score"){
				return this.Scoreform_sectiona3_offshare_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/shareEnjoy/code"){
				return this.Scoreform_sectiona3_shareenjoy_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/shareEnjoy/score"){
				return this.Scoreform_sectiona3_shareenjoy_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/othBodyComm/code"){
				return this.Scoreform_sectiona4_othbodycomm_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/othBodyComm/score"){
				return this.Scoreform_sectiona4_othbodycomm_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/offComf/code"){
				return this.Scoreform_sectiona4_offcomf_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/offComf/score"){
				return this.Scoreform_sectiona4_offcomf_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/qualSocOvert/code"){
				return this.Scoreform_sectiona4_qualsocovert_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/qualSocOvert/score"){
				return this.Scoreform_sectiona4_qualsocovert_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/inapFacExpr/code"){
				return this.Scoreform_sectiona4_inapfacexpr_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/inapFacExpr/score"){
				return this.Scoreform_sectiona4_inapfacexpr_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/apprSocResp/code"){
				return this.Scoreform_sectiona4_apprsocresp_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/apprSocResp/score"){
				return this.Scoreform_sectiona4_apprsocresp_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/pointExprInt/code"){
				return this.Scoreform_sectionb1_pointexprint_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/pointExprInt/score"){
				return this.Scoreform_sectionb1_pointexprint_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/nodding/code"){
				return this.Scoreform_sectionb1_nodding_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/nodding/score"){
				return this.Scoreform_sectionb1_nodding_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/headShak/code"){
				return this.Scoreform_sectionb1_headshak_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/headShak/score"){
				return this.Scoreform_sectionb1_headshak_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/convInstrGest/code"){
				return this.Scoreform_sectionb1_convinstrgest_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/convInstrGest/score"){
				return this.Scoreform_sectionb1_convinstrgest_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/spontImit/code"){
				return this.Scoreform_sectionb4_spontimit_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/spontImit/score"){
				return this.Scoreform_sectionb4_spontimit_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/imagPlay/code"){
				return this.Scoreform_sectionb4_imagplay_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/imagPlay/score"){
				return this.Scoreform_sectionb4_imagplay_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/imitPlay/code"){
				return this.Scoreform_sectionb4_imitplay_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/imitPlay/score"){
				return this.Scoreform_sectionb4_imitplay_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB2/socVerb/code"){
				return this.Scoreform_sectionb2_socverb_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB2/socVerb/score"){
				return this.Scoreform_sectionb2_socverb_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB2/recipConv/code"){
				return this.Scoreform_sectionb2_recipconv_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB2/recipConv/score"){
				return this.Scoreform_sectionb2_recipconv_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/stereoUtter/code"){
				return this.Scoreform_sectionb3_stereoutter_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/stereoUtter/score"){
				return this.Scoreform_sectionb3_stereoutter_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/inapprQuest/code"){
				return this.Scoreform_sectionb3_inapprquest_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/inapprQuest/score"){
				return this.Scoreform_sectionb3_inapprquest_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/pronomRevers/code"){
				return this.Scoreform_sectionb3_pronomrevers_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/pronomRevers/score"){
				return this.Scoreform_sectionb3_pronomrevers_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/neologIdio/code"){
				return this.Scoreform_sectionb3_neologidio_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/neologIdio/score"){
				return this.Scoreform_sectionb3_neologidio_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionC1/unusPreocup/code"){
				return this.Scoreform_sectionc1_unuspreocup_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionC1/unusPreocup/score"){
				return this.Scoreform_sectionc1_unuspreocup_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionC1/circumInts/code"){
				return this.Scoreform_sectionc1_circumints_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionC1/circumInts/score"){
				return this.Scoreform_sectionc1_circumints_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionC2/verbRits/code"){
				return this.Scoreform_sectionc2_verbrits_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionC2/verbRits/score"){
				return this.Scoreform_sectionc2_verbrits_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionC2/compulRits/code"){
				return this.Scoreform_sectionc2_compulrits_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionC2/compulRits/score"){
				return this.Scoreform_sectionc2_compulrits_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionC3/handFingManns/code"){
				return this.Scoreform_sectionc3_handfingmanns_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionC3/handFingManns/score"){
				return this.Scoreform_sectionc3_handfingmanns_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionC3/othComplxManns/code"){
				return this.Scoreform_sectionc3_othcomplxmanns_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionC3/othComplxManns/score"){
				return this.Scoreform_sectionc3_othcomplxmanns_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionC4/repetUseObjs/code"){
				return this.Scoreform_sectionc4_repetuseobjs_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionC4/repetUseObjs/score"){
				return this.Scoreform_sectionc4_repetuseobjs_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionC4/unusSensInts/code"){
				return this.Scoreform_sectionc4_unussensints_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionC4/unusSensInts/score"){
				return this.Scoreform_sectionc4_unussensints_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstNot/code"){
				return this.Scoreform_sectiond_agefirstnot_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstNot/score"){
				return this.Scoreform_sectiond_agefirstnot_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstWord/code"){
				return this.Scoreform_sectiond_agefirstword_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstWord/score"){
				return this.Scoreform_sectiond_agefirstword_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstPhrase/code"){
				return this.Scoreform_sectiond_agefirstphrase_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstPhrase/score"){
				return this.Scoreform_sectiond_agefirstphrase_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstEvdnt/code"){
				return this.Scoreform_sectiond_agefirstevdnt_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstEvdnt/score"){
				return this.Scoreform_sectiond_agefirstevdnt_score ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstManifst/code"){
				return this.Scoreform_sectiond_agefirstmanifst_code ;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstManifst/score"){
				return this.Scoreform_sectiond_agefirstmanifst_score ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="adirVer"){
				this.Adirver=value;
			} else 
			if(xmlPath=="a_total"){
				this.Atotal=value;
			} else 
			if(xmlPath=="a_total_note"){
				this.AtotalNote=value;
			} else 
			if(xmlPath=="b_v_total"){
				this.BvTotal=value;
			} else 
			if(xmlPath=="b_v_total_note"){
				this.BvTotalNote=value;
			} else 
			if(xmlPath=="b_nv_total"){
				this.BnvTotal=value;
			} else 
			if(xmlPath=="b_nv_total_note"){
				this.BnvTotalNote=value;
			} else 
			if(xmlPath=="c_total"){
				this.Ctotal=value;
			} else 
			if(xmlPath=="c_total_note"){
				this.CtotalNote=value;
			} else 
			if(xmlPath=="d_total"){
				this.Dtotal=value;
			} else 
			if(xmlPath=="d_total_note"){
				this.DtotalNote=value;
			} else 
			if(xmlPath=="adi_r_dx"){
				this.AdiRdx=value;
			} else 
			if(xmlPath=="adi_r_cl"){
				this.AdiRcl=value;
			} else 
			if(xmlPath=="ScoreForm/respRltnSubj"){
				this.Scoreform_resprltnsubj=value;
			} else 
			if(xmlPath=="ScoreForm/algUsed"){
				this.Scoreform_algused=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/dirGaze/code"){
				this.Scoreform_sectiona1_dirgaze_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/dirGaze/score"){
				this.Scoreform_sectiona1_dirgaze_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/socSmile/code"){
				this.Scoreform_sectiona1_socsmile_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/socSmile/score"){
				this.Scoreform_sectiona1_socsmile_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/facExpr/code"){
				this.Scoreform_sectiona1_facexpr_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA1/facExpr/score"){
				this.Scoreform_sectiona1_facexpr_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/imagPlay/code"){
				this.Scoreform_sectiona2_imagplay_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/imagPlay/score"){
				this.Scoreform_sectiona2_imagplay_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/intChild/code"){
				this.Scoreform_sectiona2_intchild_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/intChild/score"){
				this.Scoreform_sectiona2_intchild_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/respAppr/code"){
				this.Scoreform_sectiona2_respappr_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/respAppr/score"){
				this.Scoreform_sectiona2_respappr_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/grpPlay/code"){
				this.Scoreform_sectiona2_grpplay_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/grpPlay/score"){
				this.Scoreform_sectiona2_grpplay_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/friendships/code"){
				this.Scoreform_sectiona2_friendships_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA2/friendships/score"){
				this.Scoreform_sectiona2_friendships_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/showAttn/code"){
				this.Scoreform_sectiona3_showattn_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/showAttn/score"){
				this.Scoreform_sectiona3_showattn_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/offShare/code"){
				this.Scoreform_sectiona3_offshare_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/offShare/score"){
				this.Scoreform_sectiona3_offshare_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/shareEnjoy/code"){
				this.Scoreform_sectiona3_shareenjoy_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA3/shareEnjoy/score"){
				this.Scoreform_sectiona3_shareenjoy_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/othBodyComm/code"){
				this.Scoreform_sectiona4_othbodycomm_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/othBodyComm/score"){
				this.Scoreform_sectiona4_othbodycomm_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/offComf/code"){
				this.Scoreform_sectiona4_offcomf_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/offComf/score"){
				this.Scoreform_sectiona4_offcomf_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/qualSocOvert/code"){
				this.Scoreform_sectiona4_qualsocovert_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/qualSocOvert/score"){
				this.Scoreform_sectiona4_qualsocovert_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/inapFacExpr/code"){
				this.Scoreform_sectiona4_inapfacexpr_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/inapFacExpr/score"){
				this.Scoreform_sectiona4_inapfacexpr_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/apprSocResp/code"){
				this.Scoreform_sectiona4_apprsocresp_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionA4/apprSocResp/score"){
				this.Scoreform_sectiona4_apprsocresp_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/pointExprInt/code"){
				this.Scoreform_sectionb1_pointexprint_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/pointExprInt/score"){
				this.Scoreform_sectionb1_pointexprint_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/nodding/code"){
				this.Scoreform_sectionb1_nodding_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/nodding/score"){
				this.Scoreform_sectionb1_nodding_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/headShak/code"){
				this.Scoreform_sectionb1_headshak_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/headShak/score"){
				this.Scoreform_sectionb1_headshak_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/convInstrGest/code"){
				this.Scoreform_sectionb1_convinstrgest_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB1/convInstrGest/score"){
				this.Scoreform_sectionb1_convinstrgest_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/spontImit/code"){
				this.Scoreform_sectionb4_spontimit_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/spontImit/score"){
				this.Scoreform_sectionb4_spontimit_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/imagPlay/code"){
				this.Scoreform_sectionb4_imagplay_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/imagPlay/score"){
				this.Scoreform_sectionb4_imagplay_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/imitPlay/code"){
				this.Scoreform_sectionb4_imitplay_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB4/imitPlay/score"){
				this.Scoreform_sectionb4_imitplay_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB2/socVerb/code"){
				this.Scoreform_sectionb2_socverb_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB2/socVerb/score"){
				this.Scoreform_sectionb2_socverb_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB2/recipConv/code"){
				this.Scoreform_sectionb2_recipconv_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB2/recipConv/score"){
				this.Scoreform_sectionb2_recipconv_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/stereoUtter/code"){
				this.Scoreform_sectionb3_stereoutter_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/stereoUtter/score"){
				this.Scoreform_sectionb3_stereoutter_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/inapprQuest/code"){
				this.Scoreform_sectionb3_inapprquest_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/inapprQuest/score"){
				this.Scoreform_sectionb3_inapprquest_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/pronomRevers/code"){
				this.Scoreform_sectionb3_pronomrevers_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/pronomRevers/score"){
				this.Scoreform_sectionb3_pronomrevers_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/neologIdio/code"){
				this.Scoreform_sectionb3_neologidio_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionB3/neologIdio/score"){
				this.Scoreform_sectionb3_neologidio_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC1/unusPreocup/code"){
				this.Scoreform_sectionc1_unuspreocup_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC1/unusPreocup/score"){
				this.Scoreform_sectionc1_unuspreocup_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC1/circumInts/code"){
				this.Scoreform_sectionc1_circumints_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC1/circumInts/score"){
				this.Scoreform_sectionc1_circumints_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC2/verbRits/code"){
				this.Scoreform_sectionc2_verbrits_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC2/verbRits/score"){
				this.Scoreform_sectionc2_verbrits_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC2/compulRits/code"){
				this.Scoreform_sectionc2_compulrits_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC2/compulRits/score"){
				this.Scoreform_sectionc2_compulrits_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC3/handFingManns/code"){
				this.Scoreform_sectionc3_handfingmanns_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC3/handFingManns/score"){
				this.Scoreform_sectionc3_handfingmanns_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC3/othComplxManns/code"){
				this.Scoreform_sectionc3_othcomplxmanns_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC3/othComplxManns/score"){
				this.Scoreform_sectionc3_othcomplxmanns_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC4/repetUseObjs/code"){
				this.Scoreform_sectionc4_repetuseobjs_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC4/repetUseObjs/score"){
				this.Scoreform_sectionc4_repetuseobjs_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC4/unusSensInts/code"){
				this.Scoreform_sectionc4_unussensints_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionC4/unusSensInts/score"){
				this.Scoreform_sectionc4_unussensints_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstNot/code"){
				this.Scoreform_sectiond_agefirstnot_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstNot/score"){
				this.Scoreform_sectiond_agefirstnot_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstWord/code"){
				this.Scoreform_sectiond_agefirstword_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstWord/score"){
				this.Scoreform_sectiond_agefirstword_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstPhrase/code"){
				this.Scoreform_sectiond_agefirstphrase_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstPhrase/score"){
				this.Scoreform_sectiond_agefirstphrase_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstEvdnt/code"){
				this.Scoreform_sectiond_agefirstevdnt_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstEvdnt/score"){
				this.Scoreform_sectiond_agefirstevdnt_score=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstManifst/code"){
				this.Scoreform_sectiond_agefirstmanifst_code=value;
			} else 
			if(xmlPath=="ScoreForm/sectionD/ageFirstManifst/score"){
				this.Scoreform_sectiond_agefirstmanifst_score=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="adirVer"){
			return "field_data";
		}else if (xmlPath=="a_total"){
			return "field_data";
		}else if (xmlPath=="a_total_note"){
			return "field_data";
		}else if (xmlPath=="b_v_total"){
			return "field_data";
		}else if (xmlPath=="b_v_total_note"){
			return "field_data";
		}else if (xmlPath=="b_nv_total"){
			return "field_data";
		}else if (xmlPath=="b_nv_total_note"){
			return "field_data";
		}else if (xmlPath=="c_total"){
			return "field_data";
		}else if (xmlPath=="c_total_note"){
			return "field_data";
		}else if (xmlPath=="d_total"){
			return "field_data";
		}else if (xmlPath=="d_total_note"){
			return "field_data";
		}else if (xmlPath=="adi_r_dx"){
			return "field_data";
		}else if (xmlPath=="adi_r_cl"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/respRltnSubj"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/algUsed"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA1/dirGaze/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA1/dirGaze/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA1/socSmile/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA1/socSmile/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA1/facExpr/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA1/facExpr/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA2/imagPlay/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA2/imagPlay/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA2/intChild/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA2/intChild/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA2/respAppr/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA2/respAppr/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA2/grpPlay/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA2/grpPlay/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA2/friendships/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA2/friendships/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA3/showAttn/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA3/showAttn/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA3/offShare/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA3/offShare/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA3/shareEnjoy/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA3/shareEnjoy/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA4/othBodyComm/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA4/othBodyComm/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA4/offComf/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA4/offComf/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA4/qualSocOvert/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA4/qualSocOvert/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA4/inapFacExpr/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA4/inapFacExpr/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA4/apprSocResp/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionA4/apprSocResp/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB1/pointExprInt/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB1/pointExprInt/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB1/nodding/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB1/nodding/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB1/headShak/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB1/headShak/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB1/convInstrGest/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB1/convInstrGest/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB4/spontImit/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB4/spontImit/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB4/imagPlay/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB4/imagPlay/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB4/imitPlay/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB4/imitPlay/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB2/socVerb/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB2/socVerb/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB2/recipConv/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB2/recipConv/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB3/stereoUtter/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB3/stereoUtter/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB3/inapprQuest/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB3/inapprQuest/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB3/pronomRevers/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB3/pronomRevers/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB3/neologIdio/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionB3/neologIdio/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC1/unusPreocup/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC1/unusPreocup/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC1/circumInts/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC1/circumInts/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC2/verbRits/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC2/verbRits/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC2/compulRits/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC2/compulRits/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC3/handFingManns/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC3/handFingManns/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC3/othComplxManns/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC3/othComplxManns/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC4/repetUseObjs/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC4/repetUseObjs/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC4/unusSensInts/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionC4/unusSensInts/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/ageFirstNot/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/ageFirstNot/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/ageFirstWord/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/ageFirstWord/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/ageFirstPhrase/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/ageFirstPhrase/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/ageFirstEvdnt/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/ageFirstEvdnt/score"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/ageFirstManifst/code"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionD/ageFirstManifst/score"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<adir:ADIR2007";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</adir:ADIR2007>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Adirver!=null){
			xmlTxt+="\n<adir:adirVer";
			xmlTxt+=">";
			xmlTxt+=this.Adirver.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adir:adirVer>";
		}
		if (this.Atotal!=null){
			xmlTxt+="\n<adir:a_total";
			xmlTxt+=">";
			xmlTxt+=this.Atotal;
			xmlTxt+="</adir:a_total>";
		}
		if (this.AtotalNote!=null){
			xmlTxt+="\n<adir:a_total_note";
			xmlTxt+=">";
			xmlTxt+=this.AtotalNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adir:a_total_note>";
		}
		if (this.BvTotal!=null){
			xmlTxt+="\n<adir:b_v_total";
			xmlTxt+=">";
			xmlTxt+=this.BvTotal;
			xmlTxt+="</adir:b_v_total>";
		}
		if (this.BvTotalNote!=null){
			xmlTxt+="\n<adir:b_v_total_note";
			xmlTxt+=">";
			xmlTxt+=this.BvTotalNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adir:b_v_total_note>";
		}
		if (this.BnvTotal!=null){
			xmlTxt+="\n<adir:b_nv_total";
			xmlTxt+=">";
			xmlTxt+=this.BnvTotal;
			xmlTxt+="</adir:b_nv_total>";
		}
		if (this.BnvTotalNote!=null){
			xmlTxt+="\n<adir:b_nv_total_note";
			xmlTxt+=">";
			xmlTxt+=this.BnvTotalNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adir:b_nv_total_note>";
		}
		if (this.Ctotal!=null){
			xmlTxt+="\n<adir:c_total";
			xmlTxt+=">";
			xmlTxt+=this.Ctotal;
			xmlTxt+="</adir:c_total>";
		}
		if (this.CtotalNote!=null){
			xmlTxt+="\n<adir:c_total_note";
			xmlTxt+=">";
			xmlTxt+=this.CtotalNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adir:c_total_note>";
		}
		if (this.Dtotal!=null){
			xmlTxt+="\n<adir:d_total";
			xmlTxt+=">";
			xmlTxt+=this.Dtotal;
			xmlTxt+="</adir:d_total>";
		}
		if (this.DtotalNote!=null){
			xmlTxt+="\n<adir:d_total_note";
			xmlTxt+=">";
			xmlTxt+=this.DtotalNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adir:d_total_note>";
		}
		if (this.AdiRdx!=null){
			xmlTxt+="\n<adir:adi_r_dx";
			xmlTxt+=">";
			xmlTxt+=this.AdiRdx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adir:adi_r_dx>";
		}
		if (this.AdiRcl!=null){
			xmlTxt+="\n<adir:adi_r_cl";
			xmlTxt+=">";
			xmlTxt+=this.AdiRcl.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adir:adi_r_cl>";
		}
			var child0=0;
			var att0=0;
			if(this.Scoreform_sectiond_agefirstmanifst_score!=null)
			child0++;
			if(this.Scoreform_sectiona1_socsmile_code!=null)
			child0++;
			if(this.Scoreform_sectionb1_nodding_code!=null)
			child0++;
			if(this.Scoreform_sectionb1_pointexprint_code!=null)
			child0++;
			if(this.Scoreform_sectionb1_convinstrgest_code!=null)
			child0++;
			if(this.Scoreform_sectionc4_unussensints_score!=null)
			child0++;
			if(this.Scoreform_sectionc2_verbrits_code!=null)
			child0++;
			if(this.Scoreform_sectionb1_headshak_score!=null)
			child0++;
			if(this.Scoreform_sectiona3_offshare_score!=null)
			child0++;
			if(this.Scoreform_sectionb4_spontimit_score!=null)
			child0++;
			if(this.Scoreform_sectiond_agefirstphrase_code!=null)
			child0++;
			if(this.Scoreform_sectionc3_handfingmanns_code!=null)
			child0++;
			if(this.Scoreform_sectionb2_socverb_score!=null)
			child0++;
			if(this.Scoreform_sectionc1_circumints_score!=null)
			child0++;
			if(this.Scoreform_sectiona2_friendships_code!=null)
			child0++;
			if(this.Scoreform_sectiona4_qualsocovert_score!=null)
			child0++;
			if(this.Scoreform_sectiona2_imagplay_score!=null)
			child0++;
			if(this.Scoreform_sectionb3_neologidio_score!=null)
			child0++;
			if(this.Scoreform_sectiona1_facexpr_score!=null)
			child0++;
			if(this.Scoreform_sectiona3_offshare_code!=null)
			child0++;
			if(this.Scoreform_sectiona4_inapfacexpr_score!=null)
			child0++;
			if(this.Scoreform_sectiona1_dirgaze_code!=null)
			child0++;
			if(this.Scoreform_sectionb2_recipconv_code!=null)
			child0++;
			if(this.Scoreform_sectiona2_friendships_score!=null)
			child0++;
			if(this.Scoreform_sectiona2_respappr_score!=null)
			child0++;
			if(this.Scoreform_sectiona1_facexpr_code!=null)
			child0++;
			if(this.Scoreform_sectiona2_grpplay_code!=null)
			child0++;
			if(this.Scoreform_sectiona4_offcomf_score!=null)
			child0++;
			if(this.Scoreform_sectiona2_grpplay_score!=null)
			child0++;
			if(this.Scoreform_sectiona1_socsmile_score!=null)
			child0++;
			if(this.Scoreform_sectionb3_inapprquest_code!=null)
			child0++;
			if(this.Scoreform_sectiona4_apprsocresp_code!=null)
			child0++;
			if(this.Scoreform_sectionb1_pointexprint_score!=null)
			child0++;
			if(this.Scoreform_sectionc1_unuspreocup_code!=null)
			child0++;
			if(this.Scoreform_sectionb1_headshak_code!=null)
			child0++;
			if(this.Scoreform_algused!=null)
			child0++;
			if(this.Scoreform_sectiona2_respappr_code!=null)
			child0++;
			if(this.Scoreform_sectiona4_othbodycomm_code!=null)
			child0++;
			if(this.Scoreform_sectiona3_shareenjoy_score!=null)
			child0++;
			if(this.Scoreform_sectionb2_socverb_code!=null)
			child0++;
			if(this.Scoreform_sectiond_agefirstnot_score!=null)
			child0++;
			if(this.Scoreform_sectiona3_showattn_score!=null)
			child0++;
			if(this.Scoreform_sectiona1_dirgaze_score!=null)
			child0++;
			if(this.Scoreform_sectiond_agefirstevdnt_score!=null)
			child0++;
			if(this.Scoreform_sectiond_agefirstevdnt_code!=null)
			child0++;
			if(this.Scoreform_sectionb4_imagplay_code!=null)
			child0++;
			if(this.Scoreform_sectionb3_neologidio_code!=null)
			child0++;
			if(this.Scoreform_sectionb4_imitplay_code!=null)
			child0++;
			if(this.Scoreform_sectionc4_repetuseobjs_code!=null)
			child0++;
			if(this.Scoreform_sectiond_agefirstnot_code!=null)
			child0++;
			if(this.Scoreform_sectionb1_nodding_score!=null)
			child0++;
			if(this.Scoreform_sectionb3_pronomrevers_score!=null)
			child0++;
			if(this.Scoreform_sectiona3_shareenjoy_code!=null)
			child0++;
			if(this.Scoreform_sectionc1_circumints_code!=null)
			child0++;
			if(this.Scoreform_sectionb3_stereoutter_score!=null)
			child0++;
			if(this.Scoreform_resprltnsubj!=null)
			child0++;
			if(this.Scoreform_sectionc3_othcomplxmanns_code!=null)
			child0++;
			if(this.Scoreform_sectiona2_imagplay_code!=null)
			child0++;
			if(this.Scoreform_sectiona3_showattn_code!=null)
			child0++;
			if(this.Scoreform_sectionc4_repetuseobjs_score!=null)
			child0++;
			if(this.Scoreform_sectiond_agefirstword_score!=null)
			child0++;
			if(this.Scoreform_sectiond_agefirstmanifst_code!=null)
			child0++;
			if(this.Scoreform_sectionb4_imitplay_score!=null)
			child0++;
			if(this.Scoreform_sectionb4_imagplay_score!=null)
			child0++;
			if(this.Scoreform_sectionc2_compulrits_score!=null)
			child0++;
			if(this.Scoreform_sectiona4_qualsocovert_code!=null)
			child0++;
			if(this.Scoreform_sectiona4_othbodycomm_score!=null)
			child0++;
			if(this.Scoreform_sectionb4_spontimit_code!=null)
			child0++;
			if(this.Scoreform_sectionc3_othcomplxmanns_score!=null)
			child0++;
			if(this.Scoreform_sectiona2_intchild_score!=null)
			child0++;
			if(this.Scoreform_sectiona4_inapfacexpr_code!=null)
			child0++;
			if(this.Scoreform_sectionb3_inapprquest_score!=null)
			child0++;
			if(this.Scoreform_sectionc2_compulrits_code!=null)
			child0++;
			if(this.Scoreform_sectionb3_pronomrevers_code!=null)
			child0++;
			if(this.Scoreform_sectionb3_stereoutter_code!=null)
			child0++;
			if(this.Scoreform_sectiond_agefirstphrase_score!=null)
			child0++;
			if(this.Scoreform_sectionb1_convinstrgest_score!=null)
			child0++;
			if(this.Scoreform_sectionc3_handfingmanns_score!=null)
			child0++;
			if(this.Scoreform_sectionc4_unussensints_code!=null)
			child0++;
			if(this.Scoreform_sectionc1_unuspreocup_score!=null)
			child0++;
			if(this.Scoreform_sectionb2_recipconv_score!=null)
			child0++;
			if(this.Scoreform_sectiona2_intchild_code!=null)
			child0++;
			if(this.Scoreform_sectiona4_apprsocresp_score!=null)
			child0++;
			if(this.Scoreform_sectionc2_verbrits_score!=null)
			child0++;
			if(this.Scoreform_sectiond_agefirstword_code!=null)
			child0++;
			if(this.Scoreform_sectiona4_offcomf_code!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<adir:ScoreForm";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_resprltnsubj!=null){
			xmlTxt+="\n<adir:respRltnSubj";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_resprltnsubj.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adir:respRltnSubj>";
		}
		if (this.Scoreform_algused!=null){
			xmlTxt+="\n<adir:algUsed";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_algused.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adir:algUsed>";
		}
			var child1=0;
			var att1=0;
			if(this.Scoreform_sectiona1_dirgaze_code!=null)
			child1++;
			if(this.Scoreform_sectiona1_socsmile_code!=null)
			child1++;
			if(this.Scoreform_sectiona1_facexpr_score!=null)
			child1++;
			if(this.Scoreform_sectiona1_facexpr_code!=null)
			child1++;
			if(this.Scoreform_sectiona1_socsmile_score!=null)
			child1++;
			if(this.Scoreform_sectiona1_dirgaze_score!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<adir:sectionA1";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child2=0;
			var att2=0;
			if(this.Scoreform_sectiona1_dirgaze_code!=null)
			child2++;
			if(this.Scoreform_sectiona1_dirgaze_score!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<adir:dirGaze";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona1_dirgaze_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona1_dirgaze_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona1_dirgaze_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona1_dirgaze_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:dirGaze>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Scoreform_sectiona1_socsmile_code!=null)
			child3++;
			if(this.Scoreform_sectiona1_socsmile_score!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<adir:socSmile";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona1_socsmile_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona1_socsmile_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona1_socsmile_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona1_socsmile_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:socSmile>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Scoreform_sectiona1_facexpr_score!=null)
			child4++;
			if(this.Scoreform_sectiona1_facexpr_code!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<adir:facExpr";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona1_facexpr_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona1_facexpr_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona1_facexpr_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona1_facexpr_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:facExpr>";
			}
			}

				xmlTxt+="\n</adir:sectionA1>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Scoreform_sectiona2_intchild_code!=null)
			child5++;
			if(this.Scoreform_sectiona2_grpplay_code!=null)
			child5++;
			if(this.Scoreform_sectiona2_grpplay_score!=null)
			child5++;
			if(this.Scoreform_sectiona2_imagplay_score!=null)
			child5++;
			if(this.Scoreform_sectiona2_imagplay_code!=null)
			child5++;
			if(this.Scoreform_sectiona2_intchild_score!=null)
			child5++;
			if(this.Scoreform_sectiona2_friendships_code!=null)
			child5++;
			if(this.Scoreform_sectiona2_friendships_score!=null)
			child5++;
			if(this.Scoreform_sectiona2_respappr_code!=null)
			child5++;
			if(this.Scoreform_sectiona2_respappr_score!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<adir:sectionA2";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child6=0;
			var att6=0;
			if(this.Scoreform_sectiona2_imagplay_score!=null)
			child6++;
			if(this.Scoreform_sectiona2_imagplay_code!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<adir:imagPlay";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona2_imagplay_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona2_imagplay_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona2_imagplay_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona2_imagplay_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:imagPlay>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.Scoreform_sectiona2_intchild_score!=null)
			child7++;
			if(this.Scoreform_sectiona2_intchild_code!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<adir:intChild";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona2_intchild_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona2_intchild_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona2_intchild_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona2_intchild_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:intChild>";
			}
			}

			var child8=0;
			var att8=0;
			if(this.Scoreform_sectiona2_respappr_code!=null)
			child8++;
			if(this.Scoreform_sectiona2_respappr_score!=null)
			child8++;
			if(child8>0 || att8>0){
				xmlTxt+="\n<adir:respAppr";
			if(child8==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona2_respappr_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona2_respappr_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona2_respappr_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona2_respappr_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:respAppr>";
			}
			}

			var child9=0;
			var att9=0;
			if(this.Scoreform_sectiona2_grpplay_score!=null)
			child9++;
			if(this.Scoreform_sectiona2_grpplay_code!=null)
			child9++;
			if(child9>0 || att9>0){
				xmlTxt+="\n<adir:grpPlay";
			if(child9==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona2_grpplay_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona2_grpplay_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona2_grpplay_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona2_grpplay_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:grpPlay>";
			}
			}

			var child10=0;
			var att10=0;
			if(this.Scoreform_sectiona2_friendships_code!=null)
			child10++;
			if(this.Scoreform_sectiona2_friendships_score!=null)
			child10++;
			if(child10>0 || att10>0){
				xmlTxt+="\n<adir:friendships";
			if(child10==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona2_friendships_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona2_friendships_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona2_friendships_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona2_friendships_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:friendships>";
			}
			}

				xmlTxt+="\n</adir:sectionA2>";
			}
			}

			var child11=0;
			var att11=0;
			if(this.Scoreform_sectiona3_shareenjoy_code!=null)
			child11++;
			if(this.Scoreform_sectiona3_offshare_score!=null)
			child11++;
			if(this.Scoreform_sectiona3_showattn_score!=null)
			child11++;
			if(this.Scoreform_sectiona3_showattn_code!=null)
			child11++;
			if(this.Scoreform_sectiona3_shareenjoy_score!=null)
			child11++;
			if(this.Scoreform_sectiona3_offshare_code!=null)
			child11++;
			if(child11>0 || att11>0){
				xmlTxt+="\n<adir:sectionA3";
			if(child11==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child12=0;
			var att12=0;
			if(this.Scoreform_sectiona3_showattn_score!=null)
			child12++;
			if(this.Scoreform_sectiona3_showattn_code!=null)
			child12++;
			if(child12>0 || att12>0){
				xmlTxt+="\n<adir:showAttn";
			if(child12==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona3_showattn_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona3_showattn_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona3_showattn_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona3_showattn_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:showAttn>";
			}
			}

			var child13=0;
			var att13=0;
			if(this.Scoreform_sectiona3_offshare_score!=null)
			child13++;
			if(this.Scoreform_sectiona3_offshare_code!=null)
			child13++;
			if(child13>0 || att13>0){
				xmlTxt+="\n<adir:offShare";
			if(child13==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona3_offshare_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona3_offshare_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona3_offshare_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona3_offshare_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:offShare>";
			}
			}

			var child14=0;
			var att14=0;
			if(this.Scoreform_sectiona3_shareenjoy_code!=null)
			child14++;
			if(this.Scoreform_sectiona3_shareenjoy_score!=null)
			child14++;
			if(child14>0 || att14>0){
				xmlTxt+="\n<adir:shareEnjoy";
			if(child14==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona3_shareenjoy_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona3_shareenjoy_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona3_shareenjoy_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona3_shareenjoy_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:shareEnjoy>";
			}
			}

				xmlTxt+="\n</adir:sectionA3>";
			}
			}

			var child15=0;
			var att15=0;
			if(this.Scoreform_sectiona4_offcomf_score!=null)
			child15++;
			if(this.Scoreform_sectiona4_qualsocovert_code!=null)
			child15++;
			if(this.Scoreform_sectiona4_othbodycomm_score!=null)
			child15++;
			if(this.Scoreform_sectiona4_apprsocresp_code!=null)
			child15++;
			if(this.Scoreform_sectiona4_offcomf_code!=null)
			child15++;
			if(this.Scoreform_sectiona4_apprsocresp_score!=null)
			child15++;
			if(this.Scoreform_sectiona4_inapfacexpr_code!=null)
			child15++;
			if(this.Scoreform_sectiona4_qualsocovert_score!=null)
			child15++;
			if(this.Scoreform_sectiona4_inapfacexpr_score!=null)
			child15++;
			if(this.Scoreform_sectiona4_othbodycomm_code!=null)
			child15++;
			if(child15>0 || att15>0){
				xmlTxt+="\n<adir:sectionA4";
			if(child15==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child16=0;
			var att16=0;
			if(this.Scoreform_sectiona4_othbodycomm_score!=null)
			child16++;
			if(this.Scoreform_sectiona4_othbodycomm_code!=null)
			child16++;
			if(child16>0 || att16>0){
				xmlTxt+="\n<adir:othBodyComm";
			if(child16==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona4_othbodycomm_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona4_othbodycomm_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona4_othbodycomm_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona4_othbodycomm_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:othBodyComm>";
			}
			}

			var child17=0;
			var att17=0;
			if(this.Scoreform_sectiona4_offcomf_score!=null)
			child17++;
			if(this.Scoreform_sectiona4_offcomf_code!=null)
			child17++;
			if(child17>0 || att17>0){
				xmlTxt+="\n<adir:offComf";
			if(child17==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona4_offcomf_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona4_offcomf_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona4_offcomf_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona4_offcomf_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:offComf>";
			}
			}

			var child18=0;
			var att18=0;
			if(this.Scoreform_sectiona4_qualsocovert_score!=null)
			child18++;
			if(this.Scoreform_sectiona4_qualsocovert_code!=null)
			child18++;
			if(child18>0 || att18>0){
				xmlTxt+="\n<adir:qualSocOvert";
			if(child18==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona4_qualsocovert_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona4_qualsocovert_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona4_qualsocovert_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona4_qualsocovert_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:qualSocOvert>";
			}
			}

			var child19=0;
			var att19=0;
			if(this.Scoreform_sectiona4_inapfacexpr_score!=null)
			child19++;
			if(this.Scoreform_sectiona4_inapfacexpr_code!=null)
			child19++;
			if(child19>0 || att19>0){
				xmlTxt+="\n<adir:inapFacExpr";
			if(child19==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona4_inapfacexpr_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona4_inapfacexpr_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona4_inapfacexpr_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona4_inapfacexpr_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:inapFacExpr>";
			}
			}

			var child20=0;
			var att20=0;
			if(this.Scoreform_sectiona4_apprsocresp_score!=null)
			child20++;
			if(this.Scoreform_sectiona4_apprsocresp_code!=null)
			child20++;
			if(child20>0 || att20>0){
				xmlTxt+="\n<adir:apprSocResp";
			if(child20==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiona4_apprsocresp_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona4_apprsocresp_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiona4_apprsocresp_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiona4_apprsocresp_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:apprSocResp>";
			}
			}

				xmlTxt+="\n</adir:sectionA4>";
			}
			}

			var child21=0;
			var att21=0;
			if(this.Scoreform_sectionb1_nodding_score!=null)
			child21++;
			if(this.Scoreform_sectionb1_convinstrgest_code!=null)
			child21++;
			if(this.Scoreform_sectionb1_headshak_code!=null)
			child21++;
			if(this.Scoreform_sectionb1_headshak_score!=null)
			child21++;
			if(this.Scoreform_sectionb1_convinstrgest_score!=null)
			child21++;
			if(this.Scoreform_sectionb1_pointexprint_code!=null)
			child21++;
			if(this.Scoreform_sectionb1_pointexprint_score!=null)
			child21++;
			if(this.Scoreform_sectionb1_nodding_code!=null)
			child21++;
			if(child21>0 || att21>0){
				xmlTxt+="\n<adir:sectionB1";
			if(child21==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child22=0;
			var att22=0;
			if(this.Scoreform_sectionb1_pointexprint_score!=null)
			child22++;
			if(this.Scoreform_sectionb1_pointexprint_code!=null)
			child22++;
			if(child22>0 || att22>0){
				xmlTxt+="\n<adir:pointExprInt";
			if(child22==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb1_pointexprint_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb1_pointexprint_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb1_pointexprint_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb1_pointexprint_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:pointExprInt>";
			}
			}

			var child23=0;
			var att23=0;
			if(this.Scoreform_sectionb1_nodding_score!=null)
			child23++;
			if(this.Scoreform_sectionb1_nodding_code!=null)
			child23++;
			if(child23>0 || att23>0){
				xmlTxt+="\n<adir:nodding";
			if(child23==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb1_nodding_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb1_nodding_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb1_nodding_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb1_nodding_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:nodding>";
			}
			}

			var child24=0;
			var att24=0;
			if(this.Scoreform_sectionb1_headshak_score!=null)
			child24++;
			if(this.Scoreform_sectionb1_headshak_code!=null)
			child24++;
			if(child24>0 || att24>0){
				xmlTxt+="\n<adir:headShak";
			if(child24==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb1_headshak_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb1_headshak_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb1_headshak_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb1_headshak_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:headShak>";
			}
			}

			var child25=0;
			var att25=0;
			if(this.Scoreform_sectionb1_convinstrgest_code!=null)
			child25++;
			if(this.Scoreform_sectionb1_convinstrgest_score!=null)
			child25++;
			if(child25>0 || att25>0){
				xmlTxt+="\n<adir:convInstrGest";
			if(child25==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb1_convinstrgest_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb1_convinstrgest_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb1_convinstrgest_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb1_convinstrgest_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:convInstrGest>";
			}
			}

				xmlTxt+="\n</adir:sectionB1>";
			}
			}

			var child26=0;
			var att26=0;
			if(this.Scoreform_sectionb4_spontimit_code!=null)
			child26++;
			if(this.Scoreform_sectionb4_imitplay_code!=null)
			child26++;
			if(this.Scoreform_sectionb4_spontimit_score!=null)
			child26++;
			if(this.Scoreform_sectionb4_imitplay_score!=null)
			child26++;
			if(this.Scoreform_sectionb4_imagplay_score!=null)
			child26++;
			if(this.Scoreform_sectionb4_imagplay_code!=null)
			child26++;
			if(child26>0 || att26>0){
				xmlTxt+="\n<adir:sectionB4";
			if(child26==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child27=0;
			var att27=0;
			if(this.Scoreform_sectionb4_spontimit_code!=null)
			child27++;
			if(this.Scoreform_sectionb4_spontimit_score!=null)
			child27++;
			if(child27>0 || att27>0){
				xmlTxt+="\n<adir:spontImit";
			if(child27==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb4_spontimit_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb4_spontimit_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb4_spontimit_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb4_spontimit_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:spontImit>";
			}
			}

			var child28=0;
			var att28=0;
			if(this.Scoreform_sectionb4_imagplay_score!=null)
			child28++;
			if(this.Scoreform_sectionb4_imagplay_code!=null)
			child28++;
			if(child28>0 || att28>0){
				xmlTxt+="\n<adir:imagPlay";
			if(child28==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb4_imagplay_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb4_imagplay_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb4_imagplay_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb4_imagplay_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:imagPlay>";
			}
			}

			var child29=0;
			var att29=0;
			if(this.Scoreform_sectionb4_imitplay_code!=null)
			child29++;
			if(this.Scoreform_sectionb4_imitplay_score!=null)
			child29++;
			if(child29>0 || att29>0){
				xmlTxt+="\n<adir:imitPlay";
			if(child29==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb4_imitplay_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb4_imitplay_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb4_imitplay_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb4_imitplay_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:imitPlay>";
			}
			}

				xmlTxt+="\n</adir:sectionB4>";
			}
			}

			var child30=0;
			var att30=0;
			if(this.Scoreform_sectionb2_recipconv_score!=null)
			child30++;
			if(this.Scoreform_sectionb2_socverb_code!=null)
			child30++;
			if(this.Scoreform_sectionb2_socverb_score!=null)
			child30++;
			if(this.Scoreform_sectionb2_recipconv_code!=null)
			child30++;
			if(child30>0 || att30>0){
				xmlTxt+="\n<adir:sectionB2";
			if(child30==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child31=0;
			var att31=0;
			if(this.Scoreform_sectionb2_socverb_code!=null)
			child31++;
			if(this.Scoreform_sectionb2_socverb_score!=null)
			child31++;
			if(child31>0 || att31>0){
				xmlTxt+="\n<adir:socVerb";
			if(child31==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb2_socverb_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb2_socverb_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb2_socverb_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb2_socverb_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:socVerb>";
			}
			}

			var child32=0;
			var att32=0;
			if(this.Scoreform_sectionb2_recipconv_score!=null)
			child32++;
			if(this.Scoreform_sectionb2_recipconv_code!=null)
			child32++;
			if(child32>0 || att32>0){
				xmlTxt+="\n<adir:recipConv";
			if(child32==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb2_recipconv_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb2_recipconv_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb2_recipconv_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb2_recipconv_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:recipConv>";
			}
			}

				xmlTxt+="\n</adir:sectionB2>";
			}
			}

			var child33=0;
			var att33=0;
			if(this.Scoreform_sectionb3_neologidio_code!=null)
			child33++;
			if(this.Scoreform_sectionb3_neologidio_score!=null)
			child33++;
			if(this.Scoreform_sectionb3_pronomrevers_score!=null)
			child33++;
			if(this.Scoreform_sectionb3_stereoutter_code!=null)
			child33++;
			if(this.Scoreform_sectionb3_inapprquest_score!=null)
			child33++;
			if(this.Scoreform_sectionb3_pronomrevers_code!=null)
			child33++;
			if(this.Scoreform_sectionb3_stereoutter_score!=null)
			child33++;
			if(this.Scoreform_sectionb3_inapprquest_code!=null)
			child33++;
			if(child33>0 || att33>0){
				xmlTxt+="\n<adir:sectionB3";
			if(child33==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child34=0;
			var att34=0;
			if(this.Scoreform_sectionb3_stereoutter_code!=null)
			child34++;
			if(this.Scoreform_sectionb3_stereoutter_score!=null)
			child34++;
			if(child34>0 || att34>0){
				xmlTxt+="\n<adir:stereoUtter";
			if(child34==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb3_stereoutter_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb3_stereoutter_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb3_stereoutter_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb3_stereoutter_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:stereoUtter>";
			}
			}

			var child35=0;
			var att35=0;
			if(this.Scoreform_sectionb3_inapprquest_score!=null)
			child35++;
			if(this.Scoreform_sectionb3_inapprquest_code!=null)
			child35++;
			if(child35>0 || att35>0){
				xmlTxt+="\n<adir:inapprQuest";
			if(child35==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb3_inapprquest_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb3_inapprquest_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb3_inapprquest_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb3_inapprquest_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:inapprQuest>";
			}
			}

			var child36=0;
			var att36=0;
			if(this.Scoreform_sectionb3_pronomrevers_score!=null)
			child36++;
			if(this.Scoreform_sectionb3_pronomrevers_code!=null)
			child36++;
			if(child36>0 || att36>0){
				xmlTxt+="\n<adir:pronomRevers";
			if(child36==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb3_pronomrevers_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb3_pronomrevers_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb3_pronomrevers_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb3_pronomrevers_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:pronomRevers>";
			}
			}

			var child37=0;
			var att37=0;
			if(this.Scoreform_sectionb3_neologidio_code!=null)
			child37++;
			if(this.Scoreform_sectionb3_neologidio_score!=null)
			child37++;
			if(child37>0 || att37>0){
				xmlTxt+="\n<adir:neologIdio";
			if(child37==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionb3_neologidio_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb3_neologidio_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionb3_neologidio_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionb3_neologidio_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:neologIdio>";
			}
			}

				xmlTxt+="\n</adir:sectionB3>";
			}
			}

			var child38=0;
			var att38=0;
			if(this.Scoreform_sectionc1_unuspreocup_score!=null)
			child38++;
			if(this.Scoreform_sectionc1_unuspreocup_code!=null)
			child38++;
			if(this.Scoreform_sectionc1_circumints_score!=null)
			child38++;
			if(this.Scoreform_sectionc1_circumints_code!=null)
			child38++;
			if(child38>0 || att38>0){
				xmlTxt+="\n<adir:sectionC1";
			if(child38==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child39=0;
			var att39=0;
			if(this.Scoreform_sectionc1_unuspreocup_score!=null)
			child39++;
			if(this.Scoreform_sectionc1_unuspreocup_code!=null)
			child39++;
			if(child39>0 || att39>0){
				xmlTxt+="\n<adir:unusPreocup";
			if(child39==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc1_unuspreocup_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc1_unuspreocup_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionc1_unuspreocup_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc1_unuspreocup_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:unusPreocup>";
			}
			}

			var child40=0;
			var att40=0;
			if(this.Scoreform_sectionc1_circumints_score!=null)
			child40++;
			if(this.Scoreform_sectionc1_circumints_code!=null)
			child40++;
			if(child40>0 || att40>0){
				xmlTxt+="\n<adir:circumInts";
			if(child40==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc1_circumints_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc1_circumints_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionc1_circumints_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc1_circumints_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:circumInts>";
			}
			}

				xmlTxt+="\n</adir:sectionC1>";
			}
			}

			var child41=0;
			var att41=0;
			if(this.Scoreform_sectionc2_verbrits_score!=null)
			child41++;
			if(this.Scoreform_sectionc2_compulrits_score!=null)
			child41++;
			if(this.Scoreform_sectionc2_verbrits_code!=null)
			child41++;
			if(this.Scoreform_sectionc2_compulrits_code!=null)
			child41++;
			if(child41>0 || att41>0){
				xmlTxt+="\n<adir:sectionC2";
			if(child41==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child42=0;
			var att42=0;
			if(this.Scoreform_sectionc2_verbrits_score!=null)
			child42++;
			if(this.Scoreform_sectionc2_verbrits_code!=null)
			child42++;
			if(child42>0 || att42>0){
				xmlTxt+="\n<adir:verbRits";
			if(child42==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc2_verbrits_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc2_verbrits_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionc2_verbrits_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc2_verbrits_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:verbRits>";
			}
			}

			var child43=0;
			var att43=0;
			if(this.Scoreform_sectionc2_compulrits_score!=null)
			child43++;
			if(this.Scoreform_sectionc2_compulrits_code!=null)
			child43++;
			if(child43>0 || att43>0){
				xmlTxt+="\n<adir:compulRits";
			if(child43==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc2_compulrits_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc2_compulrits_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionc2_compulrits_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc2_compulrits_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:compulRits>";
			}
			}

				xmlTxt+="\n</adir:sectionC2>";
			}
			}

			var child44=0;
			var att44=0;
			if(this.Scoreform_sectionc3_othcomplxmanns_code!=null)
			child44++;
			if(this.Scoreform_sectionc3_othcomplxmanns_score!=null)
			child44++;
			if(this.Scoreform_sectionc3_handfingmanns_code!=null)
			child44++;
			if(this.Scoreform_sectionc3_handfingmanns_score!=null)
			child44++;
			if(child44>0 || att44>0){
				xmlTxt+="\n<adir:sectionC3";
			if(child44==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child45=0;
			var att45=0;
			if(this.Scoreform_sectionc3_handfingmanns_code!=null)
			child45++;
			if(this.Scoreform_sectionc3_handfingmanns_score!=null)
			child45++;
			if(child45>0 || att45>0){
				xmlTxt+="\n<adir:handFingManns";
			if(child45==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc3_handfingmanns_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc3_handfingmanns_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionc3_handfingmanns_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc3_handfingmanns_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:handFingManns>";
			}
			}

			var child46=0;
			var att46=0;
			if(this.Scoreform_sectionc3_othcomplxmanns_code!=null)
			child46++;
			if(this.Scoreform_sectionc3_othcomplxmanns_score!=null)
			child46++;
			if(child46>0 || att46>0){
				xmlTxt+="\n<adir:othComplxManns";
			if(child46==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc3_othcomplxmanns_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc3_othcomplxmanns_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionc3_othcomplxmanns_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc3_othcomplxmanns_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:othComplxManns>";
			}
			}

				xmlTxt+="\n</adir:sectionC3>";
			}
			}

			var child47=0;
			var att47=0;
			if(this.Scoreform_sectionc4_unussensints_code!=null)
			child47++;
			if(this.Scoreform_sectionc4_unussensints_score!=null)
			child47++;
			if(this.Scoreform_sectionc4_repetuseobjs_code!=null)
			child47++;
			if(this.Scoreform_sectionc4_repetuseobjs_score!=null)
			child47++;
			if(child47>0 || att47>0){
				xmlTxt+="\n<adir:sectionC4";
			if(child47==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child48=0;
			var att48=0;
			if(this.Scoreform_sectionc4_repetuseobjs_score!=null)
			child48++;
			if(this.Scoreform_sectionc4_repetuseobjs_code!=null)
			child48++;
			if(child48>0 || att48>0){
				xmlTxt+="\n<adir:repetUseObjs";
			if(child48==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc4_repetuseobjs_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc4_repetuseobjs_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionc4_repetuseobjs_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc4_repetuseobjs_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:repetUseObjs>";
			}
			}

			var child49=0;
			var att49=0;
			if(this.Scoreform_sectionc4_unussensints_code!=null)
			child49++;
			if(this.Scoreform_sectionc4_unussensints_score!=null)
			child49++;
			if(child49>0 || att49>0){
				xmlTxt+="\n<adir:unusSensInts";
			if(child49==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionc4_unussensints_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc4_unussensints_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectionc4_unussensints_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionc4_unussensints_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:unusSensInts>";
			}
			}

				xmlTxt+="\n</adir:sectionC4>";
			}
			}

			var child50=0;
			var att50=0;
			if(this.Scoreform_sectiond_agefirstphrase_score!=null)
			child50++;
			if(this.Scoreform_sectiond_agefirstmanifst_code!=null)
			child50++;
			if(this.Scoreform_sectiond_agefirstword_score!=null)
			child50++;
			if(this.Scoreform_sectiond_agefirstnot_score!=null)
			child50++;
			if(this.Scoreform_sectiond_agefirstmanifst_score!=null)
			child50++;
			if(this.Scoreform_sectiond_agefirstnot_code!=null)
			child50++;
			if(this.Scoreform_sectiond_agefirstphrase_code!=null)
			child50++;
			if(this.Scoreform_sectiond_agefirstevdnt_code!=null)
			child50++;
			if(this.Scoreform_sectiond_agefirstevdnt_score!=null)
			child50++;
			if(this.Scoreform_sectiond_agefirstword_code!=null)
			child50++;
			if(child50>0 || att50>0){
				xmlTxt+="\n<adir:sectionD";
			if(child50==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child51=0;
			var att51=0;
			if(this.Scoreform_sectiond_agefirstnot_score!=null)
			child51++;
			if(this.Scoreform_sectiond_agefirstnot_code!=null)
			child51++;
			if(child51>0 || att51>0){
				xmlTxt+="\n<adir:ageFirstNot";
			if(child51==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiond_agefirstnot_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_agefirstnot_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiond_agefirstnot_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_agefirstnot_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:ageFirstNot>";
			}
			}

			var child52=0;
			var att52=0;
			if(this.Scoreform_sectiond_agefirstword_code!=null)
			child52++;
			if(this.Scoreform_sectiond_agefirstword_score!=null)
			child52++;
			if(child52>0 || att52>0){
				xmlTxt+="\n<adir:ageFirstWord";
			if(child52==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiond_agefirstword_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_agefirstword_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiond_agefirstword_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_agefirstword_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:ageFirstWord>";
			}
			}

			var child53=0;
			var att53=0;
			if(this.Scoreform_sectiond_agefirstphrase_score!=null)
			child53++;
			if(this.Scoreform_sectiond_agefirstphrase_code!=null)
			child53++;
			if(child53>0 || att53>0){
				xmlTxt+="\n<adir:ageFirstPhrase";
			if(child53==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiond_agefirstphrase_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_agefirstphrase_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiond_agefirstphrase_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_agefirstphrase_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:ageFirstPhrase>";
			}
			}

			var child54=0;
			var att54=0;
			if(this.Scoreform_sectiond_agefirstevdnt_code!=null)
			child54++;
			if(this.Scoreform_sectiond_agefirstevdnt_score!=null)
			child54++;
			if(child54>0 || att54>0){
				xmlTxt+="\n<adir:ageFirstEvdnt";
			if(child54==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiond_agefirstevdnt_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_agefirstevdnt_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiond_agefirstevdnt_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_agefirstevdnt_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:ageFirstEvdnt>";
			}
			}

			var child55=0;
			var att55=0;
			if(this.Scoreform_sectiond_agefirstmanifst_code!=null)
			child55++;
			if(this.Scoreform_sectiond_agefirstmanifst_score!=null)
			child55++;
			if(child55>0 || att55>0){
				xmlTxt+="\n<adir:ageFirstManifst";
			if(child55==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectiond_agefirstmanifst_code!=null){
			xmlTxt+="\n<adir:code";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_agefirstmanifst_code;
			xmlTxt+="</adir:code>";
		}
		if (this.Scoreform_sectiond_agefirstmanifst_score!=null){
			xmlTxt+="\n<adir:score";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectiond_agefirstmanifst_score;
			xmlTxt+="</adir:score>";
		}
				xmlTxt+="\n</adir:ageFirstManifst>";
			}
			}

				xmlTxt+="\n</adir:sectionD>";
			}
			}

				xmlTxt+="\n</adir:ScoreForm>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Adirver!=null) return true;
		if (this.Atotal!=null) return true;
		if (this.AtotalNote!=null) return true;
		if (this.BvTotal!=null) return true;
		if (this.BvTotalNote!=null) return true;
		if (this.BnvTotal!=null) return true;
		if (this.BnvTotalNote!=null) return true;
		if (this.Ctotal!=null) return true;
		if (this.CtotalNote!=null) return true;
		if (this.Dtotal!=null) return true;
		if (this.DtotalNote!=null) return true;
		if (this.AdiRdx!=null) return true;
		if (this.AdiRcl!=null) return true;
			if(this.Scoreform_sectiond_agefirstmanifst_score!=null) return true;
			if(this.Scoreform_sectiona1_socsmile_code!=null) return true;
			if(this.Scoreform_sectionb1_nodding_code!=null) return true;
			if(this.Scoreform_sectionb1_pointexprint_code!=null) return true;
			if(this.Scoreform_sectionb1_convinstrgest_code!=null) return true;
			if(this.Scoreform_sectionc4_unussensints_score!=null) return true;
			if(this.Scoreform_sectionc2_verbrits_code!=null) return true;
			if(this.Scoreform_sectionb1_headshak_score!=null) return true;
			if(this.Scoreform_sectiona3_offshare_score!=null) return true;
			if(this.Scoreform_sectionb4_spontimit_score!=null) return true;
			if(this.Scoreform_sectiond_agefirstphrase_code!=null) return true;
			if(this.Scoreform_sectionc3_handfingmanns_code!=null) return true;
			if(this.Scoreform_sectionb2_socverb_score!=null) return true;
			if(this.Scoreform_sectionc1_circumints_score!=null) return true;
			if(this.Scoreform_sectiona2_friendships_code!=null) return true;
			if(this.Scoreform_sectiona4_qualsocovert_score!=null) return true;
			if(this.Scoreform_sectiona2_imagplay_score!=null) return true;
			if(this.Scoreform_sectionb3_neologidio_score!=null) return true;
			if(this.Scoreform_sectiona1_facexpr_score!=null) return true;
			if(this.Scoreform_sectiona3_offshare_code!=null) return true;
			if(this.Scoreform_sectiona4_inapfacexpr_score!=null) return true;
			if(this.Scoreform_sectiona1_dirgaze_code!=null) return true;
			if(this.Scoreform_sectionb2_recipconv_code!=null) return true;
			if(this.Scoreform_sectiona2_friendships_score!=null) return true;
			if(this.Scoreform_sectiona2_respappr_score!=null) return true;
			if(this.Scoreform_sectiona1_facexpr_code!=null) return true;
			if(this.Scoreform_sectiona2_grpplay_code!=null) return true;
			if(this.Scoreform_sectiona4_offcomf_score!=null) return true;
			if(this.Scoreform_sectiona2_grpplay_score!=null) return true;
			if(this.Scoreform_sectiona1_socsmile_score!=null) return true;
			if(this.Scoreform_sectionb3_inapprquest_code!=null) return true;
			if(this.Scoreform_sectiona4_apprsocresp_code!=null) return true;
			if(this.Scoreform_sectionb1_pointexprint_score!=null) return true;
			if(this.Scoreform_sectionc1_unuspreocup_code!=null) return true;
			if(this.Scoreform_sectionb1_headshak_code!=null) return true;
			if(this.Scoreform_algused!=null) return true;
			if(this.Scoreform_sectiona2_respappr_code!=null) return true;
			if(this.Scoreform_sectiona4_othbodycomm_code!=null) return true;
			if(this.Scoreform_sectiona3_shareenjoy_score!=null) return true;
			if(this.Scoreform_sectionb2_socverb_code!=null) return true;
			if(this.Scoreform_sectiond_agefirstnot_score!=null) return true;
			if(this.Scoreform_sectiona3_showattn_score!=null) return true;
			if(this.Scoreform_sectiona1_dirgaze_score!=null) return true;
			if(this.Scoreform_sectiond_agefirstevdnt_score!=null) return true;
			if(this.Scoreform_sectiond_agefirstevdnt_code!=null) return true;
			if(this.Scoreform_sectionb4_imagplay_code!=null) return true;
			if(this.Scoreform_sectionb3_neologidio_code!=null) return true;
			if(this.Scoreform_sectionb4_imitplay_code!=null) return true;
			if(this.Scoreform_sectionc4_repetuseobjs_code!=null) return true;
			if(this.Scoreform_sectiond_agefirstnot_code!=null) return true;
			if(this.Scoreform_sectionb1_nodding_score!=null) return true;
			if(this.Scoreform_sectionb3_pronomrevers_score!=null) return true;
			if(this.Scoreform_sectiona3_shareenjoy_code!=null) return true;
			if(this.Scoreform_sectionc1_circumints_code!=null) return true;
			if(this.Scoreform_sectionb3_stereoutter_score!=null) return true;
			if(this.Scoreform_resprltnsubj!=null) return true;
			if(this.Scoreform_sectionc3_othcomplxmanns_code!=null) return true;
			if(this.Scoreform_sectiona2_imagplay_code!=null) return true;
			if(this.Scoreform_sectiona3_showattn_code!=null) return true;
			if(this.Scoreform_sectionc4_repetuseobjs_score!=null) return true;
			if(this.Scoreform_sectiond_agefirstword_score!=null) return true;
			if(this.Scoreform_sectiond_agefirstmanifst_code!=null) return true;
			if(this.Scoreform_sectionb4_imitplay_score!=null) return true;
			if(this.Scoreform_sectionb4_imagplay_score!=null) return true;
			if(this.Scoreform_sectionc2_compulrits_score!=null) return true;
			if(this.Scoreform_sectiona4_qualsocovert_code!=null) return true;
			if(this.Scoreform_sectiona4_othbodycomm_score!=null) return true;
			if(this.Scoreform_sectionb4_spontimit_code!=null) return true;
			if(this.Scoreform_sectionc3_othcomplxmanns_score!=null) return true;
			if(this.Scoreform_sectiona2_intchild_score!=null) return true;
			if(this.Scoreform_sectiona4_inapfacexpr_code!=null) return true;
			if(this.Scoreform_sectionb3_inapprquest_score!=null) return true;
			if(this.Scoreform_sectionc2_compulrits_code!=null) return true;
			if(this.Scoreform_sectionb3_pronomrevers_code!=null) return true;
			if(this.Scoreform_sectionb3_stereoutter_code!=null) return true;
			if(this.Scoreform_sectiond_agefirstphrase_score!=null) return true;
			if(this.Scoreform_sectionb1_convinstrgest_score!=null) return true;
			if(this.Scoreform_sectionc3_handfingmanns_score!=null) return true;
			if(this.Scoreform_sectionc4_unussensints_code!=null) return true;
			if(this.Scoreform_sectionc1_unuspreocup_score!=null) return true;
			if(this.Scoreform_sectionb2_recipconv_score!=null) return true;
			if(this.Scoreform_sectiona2_intchild_code!=null) return true;
			if(this.Scoreform_sectiona4_apprsocresp_score!=null) return true;
			if(this.Scoreform_sectionc2_verbrits_score!=null) return true;
			if(this.Scoreform_sectiond_agefirstword_code!=null) return true;
			if(this.Scoreform_sectiona4_offcomf_code!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
