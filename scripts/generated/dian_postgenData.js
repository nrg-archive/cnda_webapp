/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function dian_postgenData(){
this.xsiType="dian:postgenData";

	this.getSchemaElementName=function(){
		return "postgenData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:postgenData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Dtcouns=null;


	function getDtcouns() {
		return this.Dtcouns;
	}
	this.getDtcouns=getDtcouns;


	function setDtcouns(v){
		this.Dtcouns=v;
	}
	this.setDtcouns=setDtcouns;

	this.Dtresult=null;


	function getDtresult() {
		return this.Dtresult;
	}
	this.getDtresult=getDtresult;


	function setDtresult(v){
		this.Dtresult=v;
	}
	this.setDtresult=setDtresult;

	this.Pttalk=null;


	function getPttalk() {
		return this.Pttalk;
	}
	this.getPttalk=getPttalk;


	function setPttalk(v){
		this.Pttalk=v;
	}
	this.setPttalk=setPttalk;

	this.Supcouns=null;


	function getSupcouns() {
		return this.Supcouns;
	}
	this.getSupcouns=getSupcouns;


	function setSupcouns(v){
		this.Supcouns=v;
	}
	this.setSupcouns=setSupcouns;

	this.Supresult=null;


	function getSupresult() {
		return this.Supresult;
	}
	this.getSupresult=getSupresult;


	function setSupresult(v){
		this.Supresult=v;
	}
	this.setSupresult=setSupresult;

	this.Share=null;


	function getShare() {
		return this.Share;
	}
	this.getShare=getShare;


	function setShare(v){
		this.Share=v;
	}
	this.setShare=setShare;

	this.Sharewho=null;


	function getSharewho() {
		return this.Sharewho;
	}
	this.getSharewho=getSharewho;


	function setSharewho(v){
		this.Sharewho=v;
	}
	this.setSharewho=setSharewho;

	this.Shareoth=null;


	function getShareoth() {
		return this.Shareoth;
	}
	this.getShareoth=getShareoth;


	function setShareoth(v){
		this.Shareoth=v;
	}
	this.setShareoth=setShareoth;

	this.Emotion=null;


	function getEmotion() {
		return this.Emotion;
	}
	this.getEmotion=getEmotion;


	function setEmotion(v){
		this.Emotion=v;
	}
	this.setEmotion=setEmotion;

	this.Referral=null;


	function getReferral() {
		return this.Referral;
	}
	this.getReferral=getReferral;


	function setReferral(v){
		this.Referral=v;
	}
	this.setReferral=setReferral;

	this.Reftype=null;


	function getReftype() {
		return this.Reftype;
	}
	this.getReftype=getReftype;


	function setReftype(v){
		this.Reftype=v;
	}
	this.setReftype=setReftype;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="DTCOUNS"){
				return this.Dtcouns ;
			} else 
			if(xmlPath=="DTRESULT"){
				return this.Dtresult ;
			} else 
			if(xmlPath=="PTTALK"){
				return this.Pttalk ;
			} else 
			if(xmlPath=="SUPCOUNS"){
				return this.Supcouns ;
			} else 
			if(xmlPath=="SUPRESULT"){
				return this.Supresult ;
			} else 
			if(xmlPath=="SHARE"){
				return this.Share ;
			} else 
			if(xmlPath=="SHAREWHO"){
				return this.Sharewho ;
			} else 
			if(xmlPath=="SHAREOTH"){
				return this.Shareoth ;
			} else 
			if(xmlPath=="EMOTION"){
				return this.Emotion ;
			} else 
			if(xmlPath=="REFERRAL"){
				return this.Referral ;
			} else 
			if(xmlPath=="REFTYPE"){
				return this.Reftype ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="DTCOUNS"){
				this.Dtcouns=value;
			} else 
			if(xmlPath=="DTRESULT"){
				this.Dtresult=value;
			} else 
			if(xmlPath=="PTTALK"){
				this.Pttalk=value;
			} else 
			if(xmlPath=="SUPCOUNS"){
				this.Supcouns=value;
			} else 
			if(xmlPath=="SUPRESULT"){
				this.Supresult=value;
			} else 
			if(xmlPath=="SHARE"){
				this.Share=value;
			} else 
			if(xmlPath=="SHAREWHO"){
				this.Sharewho=value;
			} else 
			if(xmlPath=="SHAREOTH"){
				this.Shareoth=value;
			} else 
			if(xmlPath=="EMOTION"){
				this.Emotion=value;
			} else 
			if(xmlPath=="REFERRAL"){
				this.Referral=value;
			} else 
			if(xmlPath=="REFTYPE"){
				this.Reftype=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="DTCOUNS"){
			return "field_data";
		}else if (xmlPath=="DTRESULT"){
			return "field_data";
		}else if (xmlPath=="PTTALK"){
			return "field_data";
		}else if (xmlPath=="SUPCOUNS"){
			return "field_data";
		}else if (xmlPath=="SUPRESULT"){
			return "field_data";
		}else if (xmlPath=="SHARE"){
			return "field_data";
		}else if (xmlPath=="SHAREWHO"){
			return "field_data";
		}else if (xmlPath=="SHAREOTH"){
			return "field_data";
		}else if (xmlPath=="EMOTION"){
			return "field_data";
		}else if (xmlPath=="REFERRAL"){
			return "field_data";
		}else if (xmlPath=="REFTYPE"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:POSTGEN";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:POSTGEN>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Dtcouns!=null){
			xmlTxt+="\n<dian:DTCOUNS";
			xmlTxt+=">";
			xmlTxt+=this.Dtcouns;
			xmlTxt+="</dian:DTCOUNS>";
		}
		if (this.Dtresult!=null){
			xmlTxt+="\n<dian:DTRESULT";
			xmlTxt+=">";
			xmlTxt+=this.Dtresult;
			xmlTxt+="</dian:DTRESULT>";
		}
		if (this.Pttalk!=null){
			xmlTxt+="\n<dian:PTTALK";
			xmlTxt+=">";
			xmlTxt+=this.Pttalk;
			xmlTxt+="</dian:PTTALK>";
		}
		if (this.Supcouns!=null){
			xmlTxt+="\n<dian:SUPCOUNS";
			xmlTxt+=">";
			xmlTxt+=this.Supcouns;
			xmlTxt+="</dian:SUPCOUNS>";
		}
		if (this.Supresult!=null){
			xmlTxt+="\n<dian:SUPRESULT";
			xmlTxt+=">";
			xmlTxt+=this.Supresult;
			xmlTxt+="</dian:SUPRESULT>";
		}
		if (this.Share!=null){
			xmlTxt+="\n<dian:SHARE";
			xmlTxt+=">";
			xmlTxt+=this.Share;
			xmlTxt+="</dian:SHARE>";
		}
		if (this.Sharewho!=null){
			xmlTxt+="\n<dian:SHAREWHO";
			xmlTxt+=">";
			xmlTxt+=this.Sharewho.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:SHAREWHO>";
		}
		if (this.Shareoth!=null){
			xmlTxt+="\n<dian:SHAREOTH";
			xmlTxt+=">";
			xmlTxt+=this.Shareoth.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:SHAREOTH>";
		}
		if (this.Emotion!=null){
			xmlTxt+="\n<dian:EMOTION";
			xmlTxt+=">";
			xmlTxt+=this.Emotion;
			xmlTxt+="</dian:EMOTION>";
		}
		if (this.Referral!=null){
			xmlTxt+="\n<dian:REFERRAL";
			xmlTxt+=">";
			xmlTxt+=this.Referral;
			xmlTxt+="</dian:REFERRAL>";
		}
		if (this.Reftype!=null){
			xmlTxt+="\n<dian:REFTYPE";
			xmlTxt+=">";
			xmlTxt+=this.Reftype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:REFTYPE>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Dtcouns!=null) return true;
		if (this.Dtresult!=null) return true;
		if (this.Pttalk!=null) return true;
		if (this.Supcouns!=null) return true;
		if (this.Supresult!=null) return true;
		if (this.Share!=null) return true;
		if (this.Sharewho!=null) return true;
		if (this.Shareoth!=null) return true;
		if (this.Emotion!=null) return true;
		if (this.Referral!=null) return true;
		if (this.Reftype!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
