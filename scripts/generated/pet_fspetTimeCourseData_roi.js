/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function pet_fspetTimeCourseData_roi(){
this.xsiType="pet:fspetTimeCourseData_roi";

	this.getSchemaElementName=function(){
		return "fspetTimeCourseData_roi";
	}

	this.getFullSchemaElementName=function(){
		return "pet:fspetTimeCourseData_roi";
	}

	this.Nvox=null;


	function getNvox() {
		return this.Nvox;
	}
	this.getNvox=getNvox;


	function setNvox(v){
		this.Nvox=v;
	}
	this.setNvox=setNvox;

	this.Bp=null;


	function getBp() {
		return this.Bp;
	}
	this.getBp=getBp;


	function setBp(v){
		this.Bp=v;
	}
	this.setBp=setBp;

	this.R2Bp=null;


	function getR2Bp() {
		return this.R2Bp;
	}
	this.getR2Bp=getR2Bp;


	function setR2Bp(v){
		this.R2Bp=v;
	}
	this.setR2Bp=setR2Bp;

	this.IntcBp=null;


	function getIntcBp() {
		return this.IntcBp;
	}
	this.getIntcBp=getIntcBp;


	function setIntcBp(v){
		this.IntcBp=v;
	}
	this.setIntcBp=setIntcBp;

	this.BpRsf=null;


	function getBpRsf() {
		return this.BpRsf;
	}
	this.getBpRsf=getBpRsf;


	function setBpRsf(v){
		this.BpRsf=v;
	}
	this.setBpRsf=setBpRsf;

	this.R2Rsf=null;


	function getR2Rsf() {
		return this.R2Rsf;
	}
	this.getR2Rsf=getR2Rsf;


	function setR2Rsf(v){
		this.R2Rsf=v;
	}
	this.setR2Rsf=setR2Rsf;

	this.IntcRsf=null;


	function getIntcRsf() {
		return this.IntcRsf;
	}
	this.getIntcRsf=getIntcRsf;


	function setIntcRsf(v){
		this.IntcRsf=v;
	}
	this.setIntcRsf=setIntcRsf;

	this.BpPvc2c=null;


	function getBpPvc2c() {
		return this.BpPvc2c;
	}
	this.getBpPvc2c=getBpPvc2c;


	function setBpPvc2c(v){
		this.BpPvc2c=v;
	}
	this.setBpPvc2c=setBpPvc2c;

	this.R2Pvc2c=null;


	function getR2Pvc2c() {
		return this.R2Pvc2c;
	}
	this.getR2Pvc2c=getR2Pvc2c;


	function setR2Pvc2c(v){
		this.R2Pvc2c=v;
	}
	this.setR2Pvc2c=setR2Pvc2c;

	this.IntcPvc2c=null;


	function getIntcPvc2c() {
		return this.IntcPvc2c;
	}
	this.getIntcPvc2c=getIntcPvc2c;


	function setIntcPvc2c(v){
		this.IntcPvc2c=v;
	}
	this.setIntcPvc2c=setIntcPvc2c;

	this.Suvr=null;


	function getSuvr() {
		return this.Suvr;
	}
	this.getSuvr=getSuvr;


	function setSuvr(v){
		this.Suvr=v;
	}
	this.setSuvr=setSuvr;

	this.SuvrRsf=null;


	function getSuvrRsf() {
		return this.SuvrRsf;
	}
	this.getSuvrRsf=getSuvrRsf;


	function setSuvrRsf(v){
		this.SuvrRsf=v;
	}
	this.setSuvrRsf=setSuvrRsf;

	this.SuvrPvc2c=null;


	function getSuvrPvc2c() {
		return this.SuvrPvc2c;
	}
	this.getSuvrPvc2c=getSuvrPvc2c;


	function setSuvrPvc2c(v){
		this.SuvrPvc2c=v;
	}
	this.setSuvrPvc2c=setSuvrPvc2c;
	this.Addstats =new Array();

	function getAddstats() {
		return this.Addstats;
	}
	this.getAddstats=getAddstats;


	function addAddstats(v){
		this.Addstats.push(v);
	}
	this.addAddstats=addAddstats;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.PetFspettimecoursedataRoiId=null;


	function getPetFspettimecoursedataRoiId() {
		return this.PetFspettimecoursedataRoiId;
	}
	this.getPetFspettimecoursedataRoiId=getPetFspettimecoursedataRoiId;


	function setPetFspettimecoursedataRoiId(v){
		this.PetFspettimecoursedataRoiId=v;
	}
	this.setPetFspettimecoursedataRoiId=setPetFspettimecoursedataRoiId;

	this.rois_roi_pet_fspetTimeCourseDat_id_fk=null;


	this.getrois_roi_pet_fspetTimeCourseDat_id=function() {
		return this.rois_roi_pet_fspetTimeCourseDat_id_fk;
	}


	this.setrois_roi_pet_fspetTimeCourseDat_id=function(v){
		this.rois_roi_pet_fspetTimeCourseDat_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="NVox"){
				return this.Nvox ;
			} else 
			if(xmlPath=="BP"){
				return this.Bp ;
			} else 
			if(xmlPath=="R2_BP"){
				return this.R2Bp ;
			} else 
			if(xmlPath=="INTC_BP"){
				return this.IntcBp ;
			} else 
			if(xmlPath=="BP_RSF"){
				return this.BpRsf ;
			} else 
			if(xmlPath=="R2_RSF"){
				return this.R2Rsf ;
			} else 
			if(xmlPath=="INTC_RSF"){
				return this.IntcRsf ;
			} else 
			if(xmlPath=="BP_PVC2C"){
				return this.BpPvc2c ;
			} else 
			if(xmlPath=="R2_PVC2C"){
				return this.R2Pvc2c ;
			} else 
			if(xmlPath=="INTC_PVC2C"){
				return this.IntcPvc2c ;
			} else 
			if(xmlPath=="SUVR"){
				return this.Suvr ;
			} else 
			if(xmlPath=="SUVR_RSF"){
				return this.SuvrRsf ;
			} else 
			if(xmlPath=="SUVR_PVC2C"){
				return this.SuvrPvc2c ;
			} else 
			if(xmlPath=="addStats"){
				return this.Addstats ;
			} else 
			if(xmlPath.startsWith("addStats")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Addstats ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Addstats.length;whereCount++){

					var tempValue=this.Addstats[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Addstats[whereCount]);

					}

				}
				}else{

				whereArray=this.Addstats;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="pet_fspetTimeCourseData_roi_id"){
				return this.PetFspettimecoursedataRoiId ;
			} else 
			if(xmlPath=="rois_roi_pet_fspetTimeCourseDat_id"){
				return this.rois_roi_pet_fspetTimeCourseDat_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="NVox"){
				this.Nvox=value;
			} else 
			if(xmlPath=="BP"){
				this.Bp=value;
			} else 
			if(xmlPath=="R2_BP"){
				this.R2Bp=value;
			} else 
			if(xmlPath=="INTC_BP"){
				this.IntcBp=value;
			} else 
			if(xmlPath=="BP_RSF"){
				this.BpRsf=value;
			} else 
			if(xmlPath=="R2_RSF"){
				this.R2Rsf=value;
			} else 
			if(xmlPath=="INTC_RSF"){
				this.IntcRsf=value;
			} else 
			if(xmlPath=="BP_PVC2C"){
				this.BpPvc2c=value;
			} else 
			if(xmlPath=="R2_PVC2C"){
				this.R2Pvc2c=value;
			} else 
			if(xmlPath=="INTC_PVC2C"){
				this.IntcPvc2c=value;
			} else 
			if(xmlPath=="SUVR"){
				this.Suvr=value;
			} else 
			if(xmlPath=="SUVR_RSF"){
				this.SuvrRsf=value;
			} else 
			if(xmlPath=="SUVR_PVC2C"){
				this.SuvrPvc2c=value;
			} else 
			if(xmlPath=="addStats"){
				this.Addstats=value;
			} else 
			if(xmlPath.startsWith("addStats")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Addstats ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Addstats.length;whereCount++){

					var tempValue=this.Addstats[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Addstats[whereCount]);

					}

				}
				}else{

				whereArray=this.Addstats;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("xnat:addField");//omUtils.js
					}
					this.addAddstats(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="pet_fspetTimeCourseData_roi_id"){
				this.PetFspettimecoursedataRoiId=value;
			} else 
			if(xmlPath=="rois_roi_pet_fspetTimeCourseDat_id"){
				this.rois_roi_pet_fspetTimeCourseDat_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="addStats"){
			this.addAddstats(v);
		}
		else{
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="addStats"){
			return "http://nrg.wustl.edu/xnat:addField";
		}
		else{
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="NVox"){
			return "field_data";
		}else if (xmlPath=="BP"){
			return "field_data";
		}else if (xmlPath=="R2_BP"){
			return "field_data";
		}else if (xmlPath=="INTC_BP"){
			return "field_data";
		}else if (xmlPath=="BP_RSF"){
			return "field_data";
		}else if (xmlPath=="R2_RSF"){
			return "field_data";
		}else if (xmlPath=="INTC_RSF"){
			return "field_data";
		}else if (xmlPath=="BP_PVC2C"){
			return "field_data";
		}else if (xmlPath=="R2_PVC2C"){
			return "field_data";
		}else if (xmlPath=="INTC_PVC2C"){
			return "field_data";
		}else if (xmlPath=="SUVR"){
			return "field_data";
		}else if (xmlPath=="SUVR_RSF"){
			return "field_data";
		}else if (xmlPath=="SUVR_PVC2C"){
			return "field_data";
		}else if (xmlPath=="addStats"){
			return "field_NO_CHILD";
		}else if (xmlPath=="name"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<pet:fspetTimeCourseData_roi";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</pet:fspetTimeCourseData_roi>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.PetFspettimecoursedataRoiId!=null){
				if(hiddenCount++>0)str+=",";
				str+="pet_fspetTimeCourseData_roi_id=\"" + this.PetFspettimecoursedataRoiId + "\"";
			}
			if(this.rois_roi_pet_fspetTimeCourseDat_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="rois_roi_pet_fspetTimeCourseDat_id=\"" + this.rois_roi_pet_fspetTimeCourseDat_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		else attTxt+=" name=\"\"";//REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Nvox!=null){
			xmlTxt+="\n<pet:NVox";
			xmlTxt+=">";
			xmlTxt+=this.Nvox;
			xmlTxt+="</pet:NVox>";
		}
		if (this.Bp!=null){
			xmlTxt+="\n<pet:BP";
			xmlTxt+=">";
			xmlTxt+=this.Bp;
			xmlTxt+="</pet:BP>";
		}
		if (this.R2Bp!=null){
			xmlTxt+="\n<pet:R2_BP";
			xmlTxt+=">";
			xmlTxt+=this.R2Bp;
			xmlTxt+="</pet:R2_BP>";
		}
		if (this.IntcBp!=null){
			xmlTxt+="\n<pet:INTC_BP";
			xmlTxt+=">";
			xmlTxt+=this.IntcBp;
			xmlTxt+="</pet:INTC_BP>";
		}
		if (this.BpRsf!=null){
			xmlTxt+="\n<pet:BP_RSF";
			xmlTxt+=">";
			xmlTxt+=this.BpRsf;
			xmlTxt+="</pet:BP_RSF>";
		}
		if (this.R2Rsf!=null){
			xmlTxt+="\n<pet:R2_RSF";
			xmlTxt+=">";
			xmlTxt+=this.R2Rsf;
			xmlTxt+="</pet:R2_RSF>";
		}
		if (this.IntcRsf!=null){
			xmlTxt+="\n<pet:INTC_RSF";
			xmlTxt+=">";
			xmlTxt+=this.IntcRsf;
			xmlTxt+="</pet:INTC_RSF>";
		}
		if (this.BpPvc2c!=null){
			xmlTxt+="\n<pet:BP_PVC2C";
			xmlTxt+=">";
			xmlTxt+=this.BpPvc2c;
			xmlTxt+="</pet:BP_PVC2C>";
		}
		if (this.R2Pvc2c!=null){
			xmlTxt+="\n<pet:R2_PVC2C";
			xmlTxt+=">";
			xmlTxt+=this.R2Pvc2c;
			xmlTxt+="</pet:R2_PVC2C>";
		}
		if (this.IntcPvc2c!=null){
			xmlTxt+="\n<pet:INTC_PVC2C";
			xmlTxt+=">";
			xmlTxt+=this.IntcPvc2c;
			xmlTxt+="</pet:INTC_PVC2C>";
		}
		if (this.Suvr!=null){
			xmlTxt+="\n<pet:SUVR";
			xmlTxt+=">";
			xmlTxt+=this.Suvr;
			xmlTxt+="</pet:SUVR>";
		}
		if (this.SuvrRsf!=null){
			xmlTxt+="\n<pet:SUVR_RSF";
			xmlTxt+=">";
			xmlTxt+=this.SuvrRsf;
			xmlTxt+="</pet:SUVR_RSF>";
		}
		if (this.SuvrPvc2c!=null){
			xmlTxt+="\n<pet:SUVR_PVC2C";
			xmlTxt+=">";
			xmlTxt+=this.SuvrPvc2c;
			xmlTxt+="</pet:SUVR_PVC2C>";
		}
		for(var AddstatsCOUNT=0;AddstatsCOUNT<this.Addstats.length;AddstatsCOUNT++){
			xmlTxt +="\n<pet:addStats";
			xmlTxt +=this.Addstats[AddstatsCOUNT].getXMLAtts();
			if(this.Addstats[AddstatsCOUNT].xsiType!="xnat:addField"){
				xmlTxt+=" xsi:type=\"" + this.Addstats[AddstatsCOUNT].xsiType + "\"";
			}
			if (this.Addstats[AddstatsCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Addstats[AddstatsCOUNT].getXMLBody(preventComments);
					xmlTxt+="</pet:addStats>";
			}else {xmlTxt+="/>";}
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.PetFspettimecoursedataRoiId!=null) return true;
			if (this.rois_roi_pet_fspetTimeCourseDat_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Nvox!=null) return true;
		if (this.Bp!=null) return true;
		if (this.R2Bp!=null) return true;
		if (this.IntcBp!=null) return true;
		if (this.BpRsf!=null) return true;
		if (this.R2Rsf!=null) return true;
		if (this.IntcRsf!=null) return true;
		if (this.BpPvc2c!=null) return true;
		if (this.R2Pvc2c!=null) return true;
		if (this.IntcPvc2c!=null) return true;
		if (this.Suvr!=null) return true;
		if (this.SuvrRsf!=null) return true;
		if (this.SuvrPvc2c!=null) return true;
		if(this.Addstats.length>0) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
