/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function dian_eligData_elig(){
this.xsiType="dian:eligData_elig";

	this.getSchemaElementName=function(){
		return "eligData_elig";
	}

	this.getFullSchemaElementName=function(){
		return "dian:eligData_elig";
	}

	this.Recno=null;


	function getRecno() {
		return this.Recno;
	}
	this.getRecno=getRecno;


	function setRecno(v){
		this.Recno=v;
	}
	this.setRecno=setRecno;

	this.Date=null;


	function getDate() {
		return this.Date;
	}
	this.getDate=getDate;


	function setDate(v){
		this.Date=v;
	}
	this.setDate=setDate;

	this.Incage=null;


	function getIncage() {
		return this.Incage;
	}
	this.getIncage=getIncage;


	function setIncage(v){
		this.Incage=v;
	}
	this.setIncage=setIncage;

	this.Mutation=null;


	function getMutation() {
		return this.Mutation;
	}
	this.getMutation=getMutation;


	function setMutation(v){
		this.Mutation=v;
	}
	this.setMutation=setMutation;

	this.Muttype=null;


	function getMuttype() {
		return this.Muttype;
	}
	this.getMuttype=getMuttype;


	function setMuttype(v){
		this.Muttype=v;
	}
	this.setMuttype=setMuttype;

	this.Mutgene=null;


	function getMutgene() {
		return this.Mutgene;
	}
	this.getMutgene=getMutgene;


	function setMutgene(v){
		this.Mutgene=v;
	}
	this.setMutgene=setMutgene;

	this.Mutaa=null;


	function getMutaa() {
		return this.Mutaa;
	}
	this.getMutaa=getMutaa;


	function setMutaa(v){
		this.Mutaa=v;
	}
	this.setMutaa=setMutaa;

	this.Mutpos=null;


	function getMutpos() {
		return this.Mutpos;
	}
	this.getMutpos=getMutpos;


	function setMutpos(v){
		this.Mutpos=v;
	}
	this.setMutpos=setMutpos;

	this.Mutaachg=null;


	function getMutaachg() {
		return this.Mutaachg;
	}
	this.getMutaachg=getMutaachg;


	function setMutaachg(v){
		this.Mutaachg=v;
	}
	this.setMutaachg=setMutaachg;

	this.Mutdes=null;


	function getMutdes() {
		return this.Mutdes;
	}
	this.getMutdes=getMutdes;


	function setMutdes(v){
		this.Mutdes=v;
	}
	this.setMutdes=setMutdes;

	this.Inccog=null;


	function getInccog() {
		return this.Inccog;
	}
	this.getInccog=getInccog;


	function setInccog(v){
		this.Inccog=v;
	}
	this.setInccog=setInccog;

	this.Inclang=null;


	function getInclang() {
		return this.Inclang;
	}
	this.getInclang=getInclang;


	function setInclang(v){
		this.Inclang=v;
	}
	this.setInclang=setInclang;

	this.Inccs=null;


	function getInccs() {
		return this.Inccs;
	}
	this.getInccs=getInccs;


	function setInccs(v){
		this.Inccs=v;
	}
	this.setInccs=setInccs;

	this.Excmed=null;


	function getExcmed() {
		return this.Excmed;
	}
	this.getExcmed=getExcmed;


	function setExcmed(v){
		this.Excmed=v;
	}
	this.setExcmed=setExcmed;

	this.Excsnf=null;


	function getExcsnf() {
		return this.Excsnf;
	}
	this.getExcsnf=getExcsnf;


	function setExcsnf(v){
		this.Excsnf=v;
	}
	this.setExcsnf=setExcsnf;

	this.Excmri=null;


	function getExcmri() {
		return this.Excmri;
	}
	this.getExcmri=getExcmri;


	function setExcmri(v){
		this.Excmri=v;
	}
	this.setExcmri=setExcmri;

	this.Excpet=null;


	function getExcpet() {
		return this.Excpet;
	}
	this.getExcpet=getExcpet;


	function setExcpet(v){
		this.Excpet=v;
	}
	this.setExcpet=setExcpet;

	this.Excpet2=null;


	function getExcpet2() {
		return this.Excpet2;
	}
	this.getExcpet2=getExcpet2;


	function setExcpet2(v){
		this.Excpet2=v;
	}
	this.setExcpet2=setExcpet2;

	this.Exclp=null;


	function getExclp() {
		return this.Exclp;
	}
	this.getExclp=getExclp;


	function setExclp(v){
		this.Exclp=v;
	}
	this.setExclp=setExclp;

	this.Mutstat=null;


	function getMutstat() {
		return this.Mutstat;
	}
	this.getMutstat=getMutstat;


	function setMutstat(v){
		this.Mutstat=v;
	}
	this.setMutstat=setMutstat;

	this.Neuro=null;


	function getNeuro() {
		return this.Neuro;
	}
	this.getNeuro=getNeuro;


	function setNeuro(v){
		this.Neuro=v;
	}
	this.setNeuro=setNeuro;

	this.DianEligdataEligId=null;


	function getDianEligdataEligId() {
		return this.DianEligdataEligId;
	}
	this.getDianEligdataEligId=getDianEligdataEligId;


	function setDianEligdataEligId(v){
		this.DianEligdataEligId=v;
	}
	this.setDianEligdataEligId=setDianEligdataEligId;

	this.eliglist_elig_dian_eligData_id_fk=null;


	this.geteliglist_elig_dian_eligData_id=function() {
		return this.eliglist_elig_dian_eligData_id_fk;
	}


	this.seteliglist_elig_dian_eligData_id=function(v){
		this.eliglist_elig_dian_eligData_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				return this.Recno ;
			} else 
			if(xmlPath=="DATE"){
				return this.Date ;
			} else 
			if(xmlPath=="INCAGE"){
				return this.Incage ;
			} else 
			if(xmlPath=="MUTATION"){
				return this.Mutation ;
			} else 
			if(xmlPath=="MUTTYPE"){
				return this.Muttype ;
			} else 
			if(xmlPath=="MUTGENE"){
				return this.Mutgene ;
			} else 
			if(xmlPath=="MUTAA"){
				return this.Mutaa ;
			} else 
			if(xmlPath=="MUTPOS"){
				return this.Mutpos ;
			} else 
			if(xmlPath=="MUTAACHG"){
				return this.Mutaachg ;
			} else 
			if(xmlPath=="MUTDES"){
				return this.Mutdes ;
			} else 
			if(xmlPath=="INCCOG"){
				return this.Inccog ;
			} else 
			if(xmlPath=="INCLANG"){
				return this.Inclang ;
			} else 
			if(xmlPath=="INCCS"){
				return this.Inccs ;
			} else 
			if(xmlPath=="EXCMED"){
				return this.Excmed ;
			} else 
			if(xmlPath=="EXCSNF"){
				return this.Excsnf ;
			} else 
			if(xmlPath=="EXCMRI"){
				return this.Excmri ;
			} else 
			if(xmlPath=="EXCPET"){
				return this.Excpet ;
			} else 
			if(xmlPath=="EXCPET2"){
				return this.Excpet2 ;
			} else 
			if(xmlPath=="EXCLP"){
				return this.Exclp ;
			} else 
			if(xmlPath=="MUTSTAT"){
				return this.Mutstat ;
			} else 
			if(xmlPath=="NEURO"){
				return this.Neuro ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="dian_eligData_elig_id"){
				return this.DianEligdataEligId ;
			} else 
			if(xmlPath=="eliglist_elig_dian_eligData_id"){
				return this.eliglist_elig_dian_eligData_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				this.Recno=value;
			} else 
			if(xmlPath=="DATE"){
				this.Date=value;
			} else 
			if(xmlPath=="INCAGE"){
				this.Incage=value;
			} else 
			if(xmlPath=="MUTATION"){
				this.Mutation=value;
			} else 
			if(xmlPath=="MUTTYPE"){
				this.Muttype=value;
			} else 
			if(xmlPath=="MUTGENE"){
				this.Mutgene=value;
			} else 
			if(xmlPath=="MUTAA"){
				this.Mutaa=value;
			} else 
			if(xmlPath=="MUTPOS"){
				this.Mutpos=value;
			} else 
			if(xmlPath=="MUTAACHG"){
				this.Mutaachg=value;
			} else 
			if(xmlPath=="MUTDES"){
				this.Mutdes=value;
			} else 
			if(xmlPath=="INCCOG"){
				this.Inccog=value;
			} else 
			if(xmlPath=="INCLANG"){
				this.Inclang=value;
			} else 
			if(xmlPath=="INCCS"){
				this.Inccs=value;
			} else 
			if(xmlPath=="EXCMED"){
				this.Excmed=value;
			} else 
			if(xmlPath=="EXCSNF"){
				this.Excsnf=value;
			} else 
			if(xmlPath=="EXCMRI"){
				this.Excmri=value;
			} else 
			if(xmlPath=="EXCPET"){
				this.Excpet=value;
			} else 
			if(xmlPath=="EXCPET2"){
				this.Excpet2=value;
			} else 
			if(xmlPath=="EXCLP"){
				this.Exclp=value;
			} else 
			if(xmlPath=="MUTSTAT"){
				this.Mutstat=value;
			} else 
			if(xmlPath=="NEURO"){
				this.Neuro=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="dian_eligData_elig_id"){
				this.DianEligdataEligId=value;
			} else 
			if(xmlPath=="eliglist_elig_dian_eligData_id"){
				this.eliglist_elig_dian_eligData_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="RECNO"){
			return "field_data";
		}else if (xmlPath=="DATE"){
			return "field_data";
		}else if (xmlPath=="INCAGE"){
			return "field_data";
		}else if (xmlPath=="MUTATION"){
			return "field_data";
		}else if (xmlPath=="MUTTYPE"){
			return "field_data";
		}else if (xmlPath=="MUTGENE"){
			return "field_data";
		}else if (xmlPath=="MUTAA"){
			return "field_data";
		}else if (xmlPath=="MUTPOS"){
			return "field_data";
		}else if (xmlPath=="MUTAACHG"){
			return "field_data";
		}else if (xmlPath=="MUTDES"){
			return "field_data";
		}else if (xmlPath=="INCCOG"){
			return "field_data";
		}else if (xmlPath=="INCLANG"){
			return "field_data";
		}else if (xmlPath=="INCCS"){
			return "field_data";
		}else if (xmlPath=="EXCMED"){
			return "field_data";
		}else if (xmlPath=="EXCSNF"){
			return "field_data";
		}else if (xmlPath=="EXCMRI"){
			return "field_data";
		}else if (xmlPath=="EXCPET"){
			return "field_data";
		}else if (xmlPath=="EXCPET2"){
			return "field_data";
		}else if (xmlPath=="EXCLP"){
			return "field_data";
		}else if (xmlPath=="MUTSTAT"){
			return "field_data";
		}else if (xmlPath=="NEURO"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:eligData_elig";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:eligData_elig>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.DianEligdataEligId!=null){
				if(hiddenCount++>0)str+=",";
				str+="dian_eligData_elig_id=\"" + this.DianEligdataEligId + "\"";
			}
			if(this.eliglist_elig_dian_eligData_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="eliglist_elig_dian_eligData_id=\"" + this.eliglist_elig_dian_eligData_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Recno!=null){
			xmlTxt+="\n<dian:RECNO";
			xmlTxt+=">";
			xmlTxt+=this.Recno;
			xmlTxt+="</dian:RECNO>";
		}
		if (this.Date!=null){
			xmlTxt+="\n<dian:DATE";
			xmlTxt+=">";
			xmlTxt+=this.Date;
			xmlTxt+="</dian:DATE>";
		}
		if (this.Incage!=null){
			xmlTxt+="\n<dian:INCAGE";
			xmlTxt+=">";
			xmlTxt+=this.Incage;
			xmlTxt+="</dian:INCAGE>";
		}
		if (this.Mutation!=null){
			xmlTxt+="\n<dian:MUTATION";
			xmlTxt+=">";
			xmlTxt+=this.Mutation.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:MUTATION>";
		}
		if (this.Muttype!=null){
			xmlTxt+="\n<dian:MUTTYPE";
			xmlTxt+=">";
			xmlTxt+=this.Muttype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:MUTTYPE>";
		}
		if (this.Mutgene!=null){
			xmlTxt+="\n<dian:MUTGENE";
			xmlTxt+=">";
			xmlTxt+=this.Mutgene.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:MUTGENE>";
		}
		if (this.Mutaa!=null){
			xmlTxt+="\n<dian:MUTAA";
			xmlTxt+=">";
			xmlTxt+=this.Mutaa.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:MUTAA>";
		}
		if (this.Mutpos!=null){
			xmlTxt+="\n<dian:MUTPOS";
			xmlTxt+=">";
			xmlTxt+=this.Mutpos;
			xmlTxt+="</dian:MUTPOS>";
		}
		if (this.Mutaachg!=null){
			xmlTxt+="\n<dian:MUTAACHG";
			xmlTxt+=">";
			xmlTxt+=this.Mutaachg.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:MUTAACHG>";
		}
		if (this.Mutdes!=null){
			xmlTxt+="\n<dian:MUTDES";
			xmlTxt+=">";
			xmlTxt+=this.Mutdes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:MUTDES>";
		}
		if (this.Inccog!=null){
			xmlTxt+="\n<dian:INCCOG";
			xmlTxt+=">";
			xmlTxt+=this.Inccog;
			xmlTxt+="</dian:INCCOG>";
		}
		if (this.Inclang!=null){
			xmlTxt+="\n<dian:INCLANG";
			xmlTxt+=">";
			xmlTxt+=this.Inclang;
			xmlTxt+="</dian:INCLANG>";
		}
		if (this.Inccs!=null){
			xmlTxt+="\n<dian:INCCS";
			xmlTxt+=">";
			xmlTxt+=this.Inccs;
			xmlTxt+="</dian:INCCS>";
		}
		if (this.Excmed!=null){
			xmlTxt+="\n<dian:EXCMED";
			xmlTxt+=">";
			xmlTxt+=this.Excmed;
			xmlTxt+="</dian:EXCMED>";
		}
		if (this.Excsnf!=null){
			xmlTxt+="\n<dian:EXCSNF";
			xmlTxt+=">";
			xmlTxt+=this.Excsnf;
			xmlTxt+="</dian:EXCSNF>";
		}
		if (this.Excmri!=null){
			xmlTxt+="\n<dian:EXCMRI";
			xmlTxt+=">";
			xmlTxt+=this.Excmri;
			xmlTxt+="</dian:EXCMRI>";
		}
		if (this.Excpet!=null){
			xmlTxt+="\n<dian:EXCPET";
			xmlTxt+=">";
			xmlTxt+=this.Excpet;
			xmlTxt+="</dian:EXCPET>";
		}
		if (this.Excpet2!=null){
			xmlTxt+="\n<dian:EXCPET2";
			xmlTxt+=">";
			xmlTxt+=this.Excpet2;
			xmlTxt+="</dian:EXCPET2>";
		}
		if (this.Exclp!=null){
			xmlTxt+="\n<dian:EXCLP";
			xmlTxt+=">";
			xmlTxt+=this.Exclp;
			xmlTxt+="</dian:EXCLP>";
		}
		if (this.Mutstat!=null){
			xmlTxt+="\n<dian:MUTSTAT";
			xmlTxt+=">";
			xmlTxt+=this.Mutstat;
			xmlTxt+="</dian:MUTSTAT>";
		}
		if (this.Neuro!=null){
			xmlTxt+="\n<dian:NEURO";
			xmlTxt+=">";
			xmlTxt+=this.Neuro;
			xmlTxt+="</dian:NEURO>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.DianEligdataEligId!=null) return true;
			if (this.eliglist_elig_dian_eligData_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Recno!=null) return true;
		if (this.Date!=null) return true;
		if (this.Incage!=null) return true;
		if (this.Mutation!=null) return true;
		if (this.Muttype!=null) return true;
		if (this.Mutgene!=null) return true;
		if (this.Mutaa!=null) return true;
		if (this.Mutpos!=null) return true;
		if (this.Mutaachg!=null) return true;
		if (this.Mutdes!=null) return true;
		if (this.Inccog!=null) return true;
		if (this.Inclang!=null) return true;
		if (this.Inccs!=null) return true;
		if (this.Excmed!=null) return true;
		if (this.Excsnf!=null) return true;
		if (this.Excmri!=null) return true;
		if (this.Excpet!=null) return true;
		if (this.Excpet2!=null) return true;
		if (this.Exclp!=null) return true;
		if (this.Mutstat!=null) return true;
		if (this.Neuro!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
