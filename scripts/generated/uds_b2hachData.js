/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function uds_b2hachData(){
this.xsiType="uds:b2hachData";

	this.getSchemaElementName=function(){
		return "b2hachData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:b2hachData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Abrupt=null;


	function getAbrupt() {
		return this.Abrupt;
	}
	this.getAbrupt=getAbrupt;


	function setAbrupt(v){
		this.Abrupt=v;
	}
	this.setAbrupt=setAbrupt;

	this.Stepwise=null;


	function getStepwise() {
		return this.Stepwise;
	}
	this.getStepwise=getStepwise;


	function setStepwise(v){
		this.Stepwise=v;
	}
	this.setStepwise=setStepwise;

	this.Somatic=null;


	function getSomatic() {
		return this.Somatic;
	}
	this.getSomatic=getSomatic;


	function setSomatic(v){
		this.Somatic=v;
	}
	this.setSomatic=setSomatic;

	this.Emot=null;


	function getEmot() {
		return this.Emot;
	}
	this.getEmot=getEmot;


	function setEmot(v){
		this.Emot=v;
	}
	this.setEmot=setEmot;

	this.Hxhyper=null;


	function getHxhyper() {
		return this.Hxhyper;
	}
	this.getHxhyper=getHxhyper;


	function setHxhyper(v){
		this.Hxhyper=v;
	}
	this.setHxhyper=setHxhyper;

	this.Hxstroke=null;


	function getHxstroke() {
		return this.Hxstroke;
	}
	this.getHxstroke=getHxstroke;


	function setHxstroke(v){
		this.Hxstroke=v;
	}
	this.setHxstroke=setHxstroke;

	this.Foclsym=null;


	function getFoclsym() {
		return this.Foclsym;
	}
	this.getFoclsym=getFoclsym;


	function setFoclsym(v){
		this.Foclsym=v;
	}
	this.setFoclsym=setFoclsym;

	this.Foclsign=null;


	function getFoclsign() {
		return this.Foclsign;
	}
	this.getFoclsign=getFoclsign;


	function setFoclsign(v){
		this.Foclsign=v;
	}
	this.setFoclsign=setFoclsign;

	this.Hachin=null;


	function getHachin() {
		return this.Hachin;
	}
	this.getHachin=getHachin;


	function setHachin(v){
		this.Hachin=v;
	}
	this.setHachin=setHachin;

	this.Cvdcog=null;


	function getCvdcog() {
		return this.Cvdcog;
	}
	this.getCvdcog=getCvdcog;


	function setCvdcog(v){
		this.Cvdcog=v;
	}
	this.setCvdcog=setCvdcog;

	this.Strokcog=null;


	function getStrokcog() {
		return this.Strokcog;
	}
	this.getStrokcog=getStrokcog;


	function setStrokcog(v){
		this.Strokcog=v;
	}
	this.setStrokcog=setStrokcog;

	this.Cvdimag=null;


	function getCvdimag() {
		return this.Cvdimag;
	}
	this.getCvdimag=getCvdimag;


	function setCvdimag(v){
		this.Cvdimag=v;
	}
	this.setCvdimag=setCvdimag;

	this.Cvdimag1=null;


	function getCvdimag1() {
		return this.Cvdimag1;
	}
	this.getCvdimag1=getCvdimag1;


	function setCvdimag1(v){
		this.Cvdimag1=v;
	}
	this.setCvdimag1=setCvdimag1;

	this.Cvdimag2=null;


	function getCvdimag2() {
		return this.Cvdimag2;
	}
	this.getCvdimag2=getCvdimag2;


	function setCvdimag2(v){
		this.Cvdimag2=v;
	}
	this.setCvdimag2=setCvdimag2;

	this.Cvdimag3=null;


	function getCvdimag3() {
		return this.Cvdimag3;
	}
	this.getCvdimag3=getCvdimag3;


	function setCvdimag3(v){
		this.Cvdimag3=v;
	}
	this.setCvdimag3=setCvdimag3;

	this.Cvdimag4=null;


	function getCvdimag4() {
		return this.Cvdimag4;
	}
	this.getCvdimag4=getCvdimag4;


	function setCvdimag4(v){
		this.Cvdimag4=v;
	}
	this.setCvdimag4=setCvdimag4;

	this.Cvdimagx=null;


	function getCvdimagx() {
		return this.Cvdimagx;
	}
	this.getCvdimagx=getCvdimagx;


	function setCvdimagx(v){
		this.Cvdimagx=v;
	}
	this.setCvdimagx=setCvdimagx;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="ABRUPT"){
				return this.Abrupt ;
			} else 
			if(xmlPath=="STEPWISE"){
				return this.Stepwise ;
			} else 
			if(xmlPath=="SOMATIC"){
				return this.Somatic ;
			} else 
			if(xmlPath=="EMOT"){
				return this.Emot ;
			} else 
			if(xmlPath=="HXHYPER"){
				return this.Hxhyper ;
			} else 
			if(xmlPath=="HXSTROKE"){
				return this.Hxstroke ;
			} else 
			if(xmlPath=="FOCLSYM"){
				return this.Foclsym ;
			} else 
			if(xmlPath=="FOCLSIGN"){
				return this.Foclsign ;
			} else 
			if(xmlPath=="HACHIN"){
				return this.Hachin ;
			} else 
			if(xmlPath=="CVDCOG"){
				return this.Cvdcog ;
			} else 
			if(xmlPath=="STROKCOG"){
				return this.Strokcog ;
			} else 
			if(xmlPath=="CVDIMAG"){
				return this.Cvdimag ;
			} else 
			if(xmlPath=="CVDIMAG1"){
				return this.Cvdimag1 ;
			} else 
			if(xmlPath=="CVDIMAG2"){
				return this.Cvdimag2 ;
			} else 
			if(xmlPath=="CVDIMAG3"){
				return this.Cvdimag3 ;
			} else 
			if(xmlPath=="CVDIMAG4"){
				return this.Cvdimag4 ;
			} else 
			if(xmlPath=="CVDIMAGX"){
				return this.Cvdimagx ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="ABRUPT"){
				this.Abrupt=value;
			} else 
			if(xmlPath=="STEPWISE"){
				this.Stepwise=value;
			} else 
			if(xmlPath=="SOMATIC"){
				this.Somatic=value;
			} else 
			if(xmlPath=="EMOT"){
				this.Emot=value;
			} else 
			if(xmlPath=="HXHYPER"){
				this.Hxhyper=value;
			} else 
			if(xmlPath=="HXSTROKE"){
				this.Hxstroke=value;
			} else 
			if(xmlPath=="FOCLSYM"){
				this.Foclsym=value;
			} else 
			if(xmlPath=="FOCLSIGN"){
				this.Foclsign=value;
			} else 
			if(xmlPath=="HACHIN"){
				this.Hachin=value;
			} else 
			if(xmlPath=="CVDCOG"){
				this.Cvdcog=value;
			} else 
			if(xmlPath=="STROKCOG"){
				this.Strokcog=value;
			} else 
			if(xmlPath=="CVDIMAG"){
				this.Cvdimag=value;
			} else 
			if(xmlPath=="CVDIMAG1"){
				this.Cvdimag1=value;
			} else 
			if(xmlPath=="CVDIMAG2"){
				this.Cvdimag2=value;
			} else 
			if(xmlPath=="CVDIMAG3"){
				this.Cvdimag3=value;
			} else 
			if(xmlPath=="CVDIMAG4"){
				this.Cvdimag4=value;
			} else 
			if(xmlPath=="CVDIMAGX"){
				this.Cvdimagx=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="ABRUPT"){
			return "field_data";
		}else if (xmlPath=="STEPWISE"){
			return "field_data";
		}else if (xmlPath=="SOMATIC"){
			return "field_data";
		}else if (xmlPath=="EMOT"){
			return "field_data";
		}else if (xmlPath=="HXHYPER"){
			return "field_data";
		}else if (xmlPath=="HXSTROKE"){
			return "field_data";
		}else if (xmlPath=="FOCLSYM"){
			return "field_data";
		}else if (xmlPath=="FOCLSIGN"){
			return "field_data";
		}else if (xmlPath=="HACHIN"){
			return "field_data";
		}else if (xmlPath=="CVDCOG"){
			return "field_data";
		}else if (xmlPath=="STROKCOG"){
			return "field_data";
		}else if (xmlPath=="CVDIMAG"){
			return "field_data";
		}else if (xmlPath=="CVDIMAG1"){
			return "field_data";
		}else if (xmlPath=="CVDIMAG2"){
			return "field_data";
		}else if (xmlPath=="CVDIMAG3"){
			return "field_data";
		}else if (xmlPath=="CVDIMAG4"){
			return "field_data";
		}else if (xmlPath=="CVDIMAGX"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:B2HACH";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:B2HACH>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Abrupt!=null){
			xmlTxt+="\n<uds:ABRUPT";
			xmlTxt+=">";
			xmlTxt+=this.Abrupt;
			xmlTxt+="</uds:ABRUPT>";
		}
		if (this.Stepwise!=null){
			xmlTxt+="\n<uds:STEPWISE";
			xmlTxt+=">";
			xmlTxt+=this.Stepwise;
			xmlTxt+="</uds:STEPWISE>";
		}
		if (this.Somatic!=null){
			xmlTxt+="\n<uds:SOMATIC";
			xmlTxt+=">";
			xmlTxt+=this.Somatic;
			xmlTxt+="</uds:SOMATIC>";
		}
		if (this.Emot!=null){
			xmlTxt+="\n<uds:EMOT";
			xmlTxt+=">";
			xmlTxt+=this.Emot;
			xmlTxt+="</uds:EMOT>";
		}
		if (this.Hxhyper!=null){
			xmlTxt+="\n<uds:HXHYPER";
			xmlTxt+=">";
			xmlTxt+=this.Hxhyper;
			xmlTxt+="</uds:HXHYPER>";
		}
		if (this.Hxstroke!=null){
			xmlTxt+="\n<uds:HXSTROKE";
			xmlTxt+=">";
			xmlTxt+=this.Hxstroke;
			xmlTxt+="</uds:HXSTROKE>";
		}
		if (this.Foclsym!=null){
			xmlTxt+="\n<uds:FOCLSYM";
			xmlTxt+=">";
			xmlTxt+=this.Foclsym;
			xmlTxt+="</uds:FOCLSYM>";
		}
		if (this.Foclsign!=null){
			xmlTxt+="\n<uds:FOCLSIGN";
			xmlTxt+=">";
			xmlTxt+=this.Foclsign;
			xmlTxt+="</uds:FOCLSIGN>";
		}
		if (this.Hachin!=null){
			xmlTxt+="\n<uds:HACHIN";
			xmlTxt+=">";
			xmlTxt+=this.Hachin;
			xmlTxt+="</uds:HACHIN>";
		}
		if (this.Cvdcog!=null){
			xmlTxt+="\n<uds:CVDCOG";
			xmlTxt+=">";
			xmlTxt+=this.Cvdcog;
			xmlTxt+="</uds:CVDCOG>";
		}
		if (this.Strokcog!=null){
			xmlTxt+="\n<uds:STROKCOG";
			xmlTxt+=">";
			xmlTxt+=this.Strokcog;
			xmlTxt+="</uds:STROKCOG>";
		}
		if (this.Cvdimag!=null){
			xmlTxt+="\n<uds:CVDIMAG";
			xmlTxt+=">";
			xmlTxt+=this.Cvdimag;
			xmlTxt+="</uds:CVDIMAG>";
		}
		if (this.Cvdimag1!=null){
			xmlTxt+="\n<uds:CVDIMAG1";
			xmlTxt+=">";
			xmlTxt+=this.Cvdimag1;
			xmlTxt+="</uds:CVDIMAG1>";
		}
		if (this.Cvdimag2!=null){
			xmlTxt+="\n<uds:CVDIMAG2";
			xmlTxt+=">";
			xmlTxt+=this.Cvdimag2;
			xmlTxt+="</uds:CVDIMAG2>";
		}
		if (this.Cvdimag3!=null){
			xmlTxt+="\n<uds:CVDIMAG3";
			xmlTxt+=">";
			xmlTxt+=this.Cvdimag3;
			xmlTxt+="</uds:CVDIMAG3>";
		}
		if (this.Cvdimag4!=null){
			xmlTxt+="\n<uds:CVDIMAG4";
			xmlTxt+=">";
			xmlTxt+=this.Cvdimag4;
			xmlTxt+="</uds:CVDIMAG4>";
		}
		if (this.Cvdimagx!=null){
			xmlTxt+="\n<uds:CVDIMAGX";
			xmlTxt+=">";
			xmlTxt+=this.Cvdimagx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:CVDIMAGX>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Abrupt!=null) return true;
		if (this.Stepwise!=null) return true;
		if (this.Somatic!=null) return true;
		if (this.Emot!=null) return true;
		if (this.Hxhyper!=null) return true;
		if (this.Hxstroke!=null) return true;
		if (this.Foclsym!=null) return true;
		if (this.Foclsign!=null) return true;
		if (this.Hachin!=null) return true;
		if (this.Cvdcog!=null) return true;
		if (this.Strokcog!=null) return true;
		if (this.Cvdimag!=null) return true;
		if (this.Cvdimag1!=null) return true;
		if (this.Cvdimag2!=null) return true;
		if (this.Cvdimag3!=null) return true;
		if (this.Cvdimag4!=null) return true;
		if (this.Cvdimagx!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
