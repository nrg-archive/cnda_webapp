/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function clin_pregData_preg(){
this.xsiType="clin:pregData_preg";

	this.getSchemaElementName=function(){
		return "pregData_preg";
	}

	this.getFullSchemaElementName=function(){
		return "clin:pregData_preg";
	}

	this.Recno=null;


	function getRecno() {
		return this.Recno;
	}
	this.getRecno=getRecno;


	function setRecno(v){
		this.Recno=v;
	}
	this.setRecno=setRecno;

	this.Pregposs=null;


	function getPregposs() {
		return this.Pregposs;
	}
	this.getPregposs=getPregposs;


	function setPregposs(v){
		this.Pregposs=v;
	}
	this.setPregposs=setPregposs;

	this.Pregtest=null;


	function getPregtest() {
		return this.Pregtest;
	}
	this.getPregtest=getPregtest;


	function setPregtest(v){
		this.Pregtest=v;
	}
	this.setPregtest=setPregtest;

	this.Pregres=null;


	function getPregres() {
		return this.Pregres;
	}
	this.getPregres=getPregres;


	function setPregres(v){
		this.Pregres=v;
	}
	this.setPregres=setPregres;

	this.ClinPregdataPregId=null;


	function getClinPregdataPregId() {
		return this.ClinPregdataPregId;
	}
	this.getClinPregdataPregId=getClinPregdataPregId;


	function setClinPregdataPregId(v){
		this.ClinPregdataPregId=v;
	}
	this.setClinPregdataPregId=setClinPregdataPregId;

	this.preglist_preg_clin_pregData_id_fk=null;


	this.getpreglist_preg_clin_pregData_id=function() {
		return this.preglist_preg_clin_pregData_id_fk;
	}


	this.setpreglist_preg_clin_pregData_id=function(v){
		this.preglist_preg_clin_pregData_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				return this.Recno ;
			} else 
			if(xmlPath=="PREGPOSS"){
				return this.Pregposs ;
			} else 
			if(xmlPath=="PREGTEST"){
				return this.Pregtest ;
			} else 
			if(xmlPath=="PREGRES"){
				return this.Pregres ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="clin_pregData_preg_id"){
				return this.ClinPregdataPregId ;
			} else 
			if(xmlPath=="preglist_preg_clin_pregData_id"){
				return this.preglist_preg_clin_pregData_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				this.Recno=value;
			} else 
			if(xmlPath=="PREGPOSS"){
				this.Pregposs=value;
			} else 
			if(xmlPath=="PREGTEST"){
				this.Pregtest=value;
			} else 
			if(xmlPath=="PREGRES"){
				this.Pregres=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="clin_pregData_preg_id"){
				this.ClinPregdataPregId=value;
			} else 
			if(xmlPath=="preglist_preg_clin_pregData_id"){
				this.preglist_preg_clin_pregData_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="RECNO"){
			return "field_data";
		}else if (xmlPath=="PREGPOSS"){
			return "field_data";
		}else if (xmlPath=="PREGTEST"){
			return "field_data";
		}else if (xmlPath=="PREGRES"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<clin:pregData_preg";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</clin:pregData_preg>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.ClinPregdataPregId!=null){
				if(hiddenCount++>0)str+=",";
				str+="clin_pregData_preg_id=\"" + this.ClinPregdataPregId + "\"";
			}
			if(this.preglist_preg_clin_pregData_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="preglist_preg_clin_pregData_id=\"" + this.preglist_preg_clin_pregData_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Recno!=null){
			xmlTxt+="\n<clin:RECNO";
			xmlTxt+=">";
			xmlTxt+=this.Recno;
			xmlTxt+="</clin:RECNO>";
		}
		if (this.Pregposs!=null){
			xmlTxt+="\n<clin:PREGPOSS";
			xmlTxt+=">";
			xmlTxt+=this.Pregposs;
			xmlTxt+="</clin:PREGPOSS>";
		}
		if (this.Pregtest!=null){
			xmlTxt+="\n<clin:PREGTEST";
			xmlTxt+=">";
			xmlTxt+=this.Pregtest;
			xmlTxt+="</clin:PREGTEST>";
		}
		if (this.Pregres!=null){
			xmlTxt+="\n<clin:PREGRES";
			xmlTxt+=">";
			xmlTxt+=this.Pregres;
			xmlTxt+="</clin:PREGRES>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.ClinPregdataPregId!=null) return true;
			if (this.preglist_preg_clin_pregData_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Recno!=null) return true;
		if (this.Pregposs!=null) return true;
		if (this.Pregtest!=null) return true;
		if (this.Pregres!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
