/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function tissue_specData(){
this.xsiType="tissue:specData";

	this.getSchemaElementName=function(){
		return "specData";
	}

	this.getFullSchemaElementName=function(){
		return "tissue:specData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Contlabel=null;


	function getContlabel() {
		return this.Contlabel;
	}
	this.getContlabel=getContlabel;


	function setContlabel(v){
		this.Contlabel=v;
	}
	this.setContlabel=setContlabel;

	this.Colltime=null;


	function getColltime() {
		return this.Colltime;
	}
	this.getColltime=getColltime;


	function setColltime(v){
		this.Colltime=v;
	}
	this.setColltime=setColltime;

	this.Status=null;


	function getStatus() {
		return this.Status;
	}
	this.getStatus=getStatus;


	function setStatus(v){
		this.Status=v;
	}
	this.setStatus=setStatus;
	this.File =null;
	function getFile() {
		return this.File;
	}
	this.getFile=getFile;


	function setFile(v){
		this.File =v;
	}
	this.setFile=setFile;

	this.File_FileXnatAbstractresourceId=null;


	function getFile_FileXnatAbstractresourceId(){
		return this.File_FileXnatAbstractresourceId;
	}
	this.getFile_FileXnatAbstractresourceId=getFile_FileXnatAbstractresourceId;


	function setFile_FileXnatAbstractresourceId(v){
		this.File_FileXnatAbstractresourceId=v;
	}
	this.setFile_FileXnatAbstractresourceId=setFile_FileXnatAbstractresourceId;

	this.Volume=null;


	function getVolume() {
		return this.Volume;
	}
	this.getVolume=getVolume;


	function setVolume(v){
		this.Volume=v;
	}
	this.setVolume=setVolume;

	this.Unitsvolume=null;


	function getUnitsvolume() {
		return this.Unitsvolume;
	}
	this.getUnitsvolume=getUnitsvolume;


	function setUnitsvolume(v){
		this.Unitsvolume=v;
	}
	this.setUnitsvolume=setUnitsvolume;

	this.Mass=null;


	function getMass() {
		return this.Mass;
	}
	this.getMass=getMass;


	function setMass(v){
		this.Mass=v;
	}
	this.setMass=setMass;

	this.Unitsmass=null;


	function getUnitsmass() {
		return this.Unitsmass;
	}
	this.getUnitsmass=getUnitsmass;


	function setUnitsmass(v){
		this.Unitsmass=v;
	}
	this.setUnitsmass=setUnitsmass;

	this.Typecont=null;


	function getTypecont() {
		return this.Typecont;
	}
	this.getTypecont=getTypecont;


	function setTypecont(v){
		this.Typecont=v;
	}
	this.setTypecont=setTypecont;

	this.Altlabel1=null;


	function getAltlabel1() {
		return this.Altlabel1;
	}
	this.getAltlabel1=getAltlabel1;


	function setAltlabel1(v){
		this.Altlabel1=v;
	}
	this.setAltlabel1=setAltlabel1;

	this.Altlabel1ref=null;


	function getAltlabel1ref() {
		return this.Altlabel1ref;
	}
	this.getAltlabel1ref=getAltlabel1ref;


	function setAltlabel1ref(v){
		this.Altlabel1ref=v;
	}
	this.setAltlabel1ref=setAltlabel1ref;

	this.Altlabel2=null;


	function getAltlabel2() {
		return this.Altlabel2;
	}
	this.getAltlabel2=getAltlabel2;


	function setAltlabel2(v){
		this.Altlabel2=v;
	}
	this.setAltlabel2=setAltlabel2;

	this.Altlabel2ref=null;


	function getAltlabel2ref() {
		return this.Altlabel2ref;
	}
	this.getAltlabel2ref=getAltlabel2ref;


	function setAltlabel2ref(v){
		this.Altlabel2ref=v;
	}
	this.setAltlabel2ref=setAltlabel2ref;

	this.Altlabel3=null;


	function getAltlabel3() {
		return this.Altlabel3;
	}
	this.getAltlabel3=getAltlabel3;


	function setAltlabel3(v){
		this.Altlabel3=v;
	}
	this.setAltlabel3=setAltlabel3;

	this.Altlabel3ref=null;


	function getAltlabel3ref() {
		return this.Altlabel3ref;
	}
	this.getAltlabel3ref=getAltlabel3ref;


	function setAltlabel3ref(v){
		this.Altlabel3ref=v;
	}
	this.setAltlabel3ref=setAltlabel3ref;

	this.Tisscollid=null;


	function getTisscollid() {
		return this.Tisscollid;
	}
	this.getTisscollid=getTisscollid;


	function setTisscollid(v){
		this.Tisscollid=v;
	}
	this.setTisscollid=setTisscollid;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="contLabel"){
				return this.Contlabel ;
			} else 
			if(xmlPath=="collTime"){
				return this.Colltime ;
			} else 
			if(xmlPath=="status"){
				return this.Status ;
			} else 
			if(xmlPath=="file"){
				return this.File ;
			} else 
			if(xmlPath.startsWith("file")){
				xmlPath=xmlPath.substring(4);
				if(xmlPath=="")return this.File ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.File!=undefined)return this.File.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="volume"){
				return this.Volume ;
			} else 
			if(xmlPath=="unitsVolume"){
				return this.Unitsvolume ;
			} else 
			if(xmlPath=="mass"){
				return this.Mass ;
			} else 
			if(xmlPath=="unitsMass"){
				return this.Unitsmass ;
			} else 
			if(xmlPath=="typeCont"){
				return this.Typecont ;
			} else 
			if(xmlPath=="altLabel1"){
				return this.Altlabel1 ;
			} else 
			if(xmlPath=="altLabel1Ref"){
				return this.Altlabel1ref ;
			} else 
			if(xmlPath=="altLabel2"){
				return this.Altlabel2 ;
			} else 
			if(xmlPath=="altLabel2Ref"){
				return this.Altlabel2ref ;
			} else 
			if(xmlPath=="altLabel3"){
				return this.Altlabel3 ;
			} else 
			if(xmlPath=="altLabel3Ref"){
				return this.Altlabel3ref ;
			} else 
			if(xmlPath=="tissCollID"){
				return this.Tisscollid ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="contLabel"){
				this.Contlabel=value;
			} else 
			if(xmlPath=="collTime"){
				this.Colltime=value;
			} else 
			if(xmlPath=="status"){
				this.Status=value;
			} else 
			if(xmlPath=="file"){
				this.File=value;
			} else 
			if(xmlPath.startsWith("file")){
				xmlPath=xmlPath.substring(4);
				if(xmlPath=="")return this.File ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.File!=undefined){
					this.File.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.File= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.File= instanciateObject("xnat:resourceCatalog");//omUtils.js
						}
						if(options && options.where)this.File.setProperty(options.where.field,options.where.value);
						this.File.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="volume"){
				this.Volume=value;
			} else 
			if(xmlPath=="unitsVolume"){
				this.Unitsvolume=value;
			} else 
			if(xmlPath=="mass"){
				this.Mass=value;
			} else 
			if(xmlPath=="unitsMass"){
				this.Unitsmass=value;
			} else 
			if(xmlPath=="typeCont"){
				this.Typecont=value;
			} else 
			if(xmlPath=="altLabel1"){
				this.Altlabel1=value;
			} else 
			if(xmlPath=="altLabel1Ref"){
				this.Altlabel1ref=value;
			} else 
			if(xmlPath=="altLabel2"){
				this.Altlabel2=value;
			} else 
			if(xmlPath=="altLabel2Ref"){
				this.Altlabel2ref=value;
			} else 
			if(xmlPath=="altLabel3"){
				this.Altlabel3=value;
			} else 
			if(xmlPath=="altLabel3Ref"){
				this.Altlabel3ref=value;
			} else 
			if(xmlPath=="tissCollID"){
				this.Tisscollid=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="file"){
			this.setFile(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="file"){
			return "http://nrg.wustl.edu/xnat:resourceCatalog";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="contLabel"){
			return "field_data";
		}else if (xmlPath=="collTime"){
			return "field_data";
		}else if (xmlPath=="status"){
			return "field_data";
		}else if (xmlPath=="file"){
			return "field_single_reference";
		}else if (xmlPath=="volume"){
			return "field_data";
		}else if (xmlPath=="unitsVolume"){
			return "field_data";
		}else if (xmlPath=="mass"){
			return "field_data";
		}else if (xmlPath=="unitsMass"){
			return "field_data";
		}else if (xmlPath=="typeCont"){
			return "field_data";
		}else if (xmlPath=="altLabel1"){
			return "field_data";
		}else if (xmlPath=="altLabel1Ref"){
			return "field_data";
		}else if (xmlPath=="altLabel2"){
			return "field_data";
		}else if (xmlPath=="altLabel2Ref"){
			return "field_data";
		}else if (xmlPath=="altLabel3"){
			return "field_data";
		}else if (xmlPath=="altLabel3Ref"){
			return "field_data";
		}else if (xmlPath=="tissCollID"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<tissue:specData";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</tissue:specData>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		if (this.Tisscollid!=null)
			attTxt+=" tissCollID=\"" +this.Tisscollid +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Contlabel!=null){
			xmlTxt+="\n<tissue:contLabel";
			xmlTxt+=">";
			xmlTxt+=this.Contlabel.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:contLabel>";
		}
		if (this.Colltime!=null){
			xmlTxt+="\n<tissue:collTime";
			xmlTxt+=">";
			xmlTxt+=this.Colltime;
			xmlTxt+="</tissue:collTime>";
		}
		if (this.Status!=null){
			xmlTxt+="\n<tissue:status";
			xmlTxt+=">";
			xmlTxt+=this.Status.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:status>";
		}
		if (this.File!=null){
			xmlTxt+="\n<tissue:file";
			xmlTxt+=this.File.getXMLAtts();
			if(this.File.xsiType!="xnat:resourceCatalog"){
				xmlTxt+=" xsi:type=\"" + this.File.xsiType + "\"";
			}
			if (this.File.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.File.getXMLBody(preventComments);
				xmlTxt+="</tissue:file>";
			}else {xmlTxt+="/>";}
		}
		//NOT REQUIRED

		if (this.Volume!=null){
			xmlTxt+="\n<tissue:volume";
			xmlTxt+=">";
			xmlTxt+=this.Volume;
			xmlTxt+="</tissue:volume>";
		}
		if (this.Unitsvolume!=null){
			xmlTxt+="\n<tissue:unitsVolume";
			xmlTxt+=">";
			xmlTxt+=this.Unitsvolume.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:unitsVolume>";
		}
		if (this.Mass!=null){
			xmlTxt+="\n<tissue:mass";
			xmlTxt+=">";
			xmlTxt+=this.Mass;
			xmlTxt+="</tissue:mass>";
		}
		if (this.Unitsmass!=null){
			xmlTxt+="\n<tissue:unitsMass";
			xmlTxt+=">";
			xmlTxt+=this.Unitsmass.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:unitsMass>";
		}
		if (this.Typecont!=null){
			xmlTxt+="\n<tissue:typeCont";
			xmlTxt+=">";
			xmlTxt+=this.Typecont.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:typeCont>";
		}
		if (this.Altlabel1!=null){
			xmlTxt+="\n<tissue:altLabel1";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel1.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel1>";
		}
		if (this.Altlabel1ref!=null){
			xmlTxt+="\n<tissue:altLabel1Ref";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel1ref.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel1Ref>";
		}
		if (this.Altlabel2!=null){
			xmlTxt+="\n<tissue:altLabel2";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel2>";
		}
		if (this.Altlabel2ref!=null){
			xmlTxt+="\n<tissue:altLabel2Ref";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel2ref.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel2Ref>";
		}
		if (this.Altlabel3!=null){
			xmlTxt+="\n<tissue:altLabel3";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel3.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel3>";
		}
		if (this.Altlabel3ref!=null){
			xmlTxt+="\n<tissue:altLabel3Ref";
			xmlTxt+=">";
			xmlTxt+=this.Altlabel3ref.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tissue:altLabel3Ref>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Contlabel!=null) return true;
		if (this.Colltime!=null) return true;
		if (this.Status!=null) return true;
		if (this.File!=null){
			if (this.File.hasXMLBodyContent()) return true;
		}
		//NOT REQUIRED

		if (this.Volume!=null) return true;
		if (this.Unitsvolume!=null) return true;
		if (this.Mass!=null) return true;
		if (this.Unitsmass!=null) return true;
		if (this.Typecont!=null) return true;
		if (this.Altlabel1!=null) return true;
		if (this.Altlabel1ref!=null) return true;
		if (this.Altlabel2!=null) return true;
		if (this.Altlabel2ref!=null) return true;
		if (this.Altlabel3!=null) return true;
		if (this.Altlabel3ref!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
