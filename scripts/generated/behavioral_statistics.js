/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function behavioral_statistics(){
this.xsiType="behavioral:statistics";

	this.getSchemaElementName=function(){
		return "statistics";
	}

	this.getFullSchemaElementName=function(){
		return "behavioral:statistics";
	}

	this.Correct=null;


	function getCorrect() {
		return this.Correct;
	}
	this.getCorrect=getCorrect;


	function setCorrect(v){
		this.Correct=v;
	}
	this.setCorrect=setCorrect;

	this.Missed=null;


	function getMissed() {
		return this.Missed;
	}
	this.getMissed=getMissed;


	function setMissed(v){
		this.Missed=v;
	}
	this.setMissed=setMissed;

	this.Wrong=null;


	function getWrong() {
		return this.Wrong;
	}
	this.getWrong=getWrong;


	function setWrong(v){
		this.Wrong=v;
	}
	this.setWrong=setWrong;

	this.ResponseTime=null;


	function getResponseTime() {
		return this.ResponseTime;
	}
	this.getResponseTime=getResponseTime;


	function setResponseTime(v){
		this.ResponseTime=v;
	}
	this.setResponseTime=setResponseTime;

	this.Mean=null;


	function getMean() {
		return this.Mean;
	}
	this.getMean=getMean;


	function setMean(v){
		this.Mean=v;
	}
	this.setMean=setMean;

	this.Median=null;


	function getMedian() {
		return this.Median;
	}
	this.getMedian=getMedian;


	function setMedian(v){
		this.Median=v;
	}
	this.setMedian=setMedian;

	this.Mode=null;


	function getMode() {
		return this.Mode;
	}
	this.getMode=getMode;


	function setMode(v){
		this.Mode=v;
	}
	this.setMode=setMode;

	this.StdDeviation=null;


	function getStdDeviation() {
		return this.StdDeviation;
	}
	this.getStdDeviation=getStdDeviation;


	function setStdDeviation(v){
		this.StdDeviation=v;
	}
	this.setStdDeviation=setStdDeviation;

	this.StdError=null;


	function getStdError() {
		return this.StdError;
	}
	this.getStdError=getStdError;


	function setStdError(v){
		this.StdError=v;
	}
	this.setStdError=setStdError;

	this.WeightedAverage=null;


	function getWeightedAverage() {
		return this.WeightedAverage;
	}
	this.getWeightedAverage=getWeightedAverage;


	function setWeightedAverage(v){
		this.WeightedAverage=v;
	}
	this.setWeightedAverage=setWeightedAverage;

	this.Maximum=null;


	function getMaximum() {
		return this.Maximum;
	}
	this.getMaximum=getMaximum;


	function setMaximum(v){
		this.Maximum=v;
	}
	this.setMaximum=setMaximum;

	this.Minimum=null;


	function getMinimum() {
		return this.Minimum;
	}
	this.getMinimum=getMinimum;


	function setMinimum(v){
		this.Minimum=v;
	}
	this.setMinimum=setMinimum;
	this.Additional =new Array();

	function getAdditional() {
		return this.Additional;
	}
	this.getAdditional=getAdditional;


	function addAdditional(v){
		this.Additional.push(v);
	}
	this.addAdditional=addAdditional;

	this.BehavioralStatisticsId=null;


	function getBehavioralStatisticsId() {
		return this.BehavioralStatisticsId;
	}
	this.getBehavioralStatisticsId=getBehavioralStatisticsId;


	function setBehavioralStatisticsId(v){
		this.BehavioralStatisticsId=v;
	}
	this.setBehavioralStatisticsId=setBehavioralStatisticsId;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="correct"){
				return this.Correct ;
			} else 
			if(xmlPath=="missed"){
				return this.Missed ;
			} else 
			if(xmlPath=="wrong"){
				return this.Wrong ;
			} else 
			if(xmlPath=="response_time"){
				return this.ResponseTime ;
			} else 
			if(xmlPath=="mean"){
				return this.Mean ;
			} else 
			if(xmlPath=="median"){
				return this.Median ;
			} else 
			if(xmlPath=="mode"){
				return this.Mode ;
			} else 
			if(xmlPath=="std_deviation"){
				return this.StdDeviation ;
			} else 
			if(xmlPath=="std_error"){
				return this.StdError ;
			} else 
			if(xmlPath=="weighted_average"){
				return this.WeightedAverage ;
			} else 
			if(xmlPath=="maximum"){
				return this.Maximum ;
			} else 
			if(xmlPath=="minimum"){
				return this.Minimum ;
			} else 
			if(xmlPath=="additional"){
				return this.Additional ;
			} else 
			if(xmlPath.startsWith("additional")){
				xmlPath=xmlPath.substring(10);
				if(xmlPath=="")return this.Additional ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Additional.length;whereCount++){

					var tempValue=this.Additional[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Additional[whereCount]);

					}

				}
				}else{

				whereArray=this.Additional;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="behavioral_statistics_id"){
				return this.BehavioralStatisticsId ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="correct"){
				this.Correct=value;
			} else 
			if(xmlPath=="missed"){
				this.Missed=value;
			} else 
			if(xmlPath=="wrong"){
				this.Wrong=value;
			} else 
			if(xmlPath=="response_time"){
				this.ResponseTime=value;
			} else 
			if(xmlPath=="mean"){
				this.Mean=value;
			} else 
			if(xmlPath=="median"){
				this.Median=value;
			} else 
			if(xmlPath=="mode"){
				this.Mode=value;
			} else 
			if(xmlPath=="std_deviation"){
				this.StdDeviation=value;
			} else 
			if(xmlPath=="std_error"){
				this.StdError=value;
			} else 
			if(xmlPath=="weighted_average"){
				this.WeightedAverage=value;
			} else 
			if(xmlPath=="maximum"){
				this.Maximum=value;
			} else 
			if(xmlPath=="minimum"){
				this.Minimum=value;
			} else 
			if(xmlPath=="additional"){
				this.Additional=value;
			} else 
			if(xmlPath.startsWith("additional")){
				xmlPath=xmlPath.substring(10);
				if(xmlPath=="")return this.Additional ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Additional.length;whereCount++){

					var tempValue=this.Additional[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Additional[whereCount]);

					}

				}
				}else{

				whereArray=this.Additional;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("behavioral:statistics_additional");//omUtils.js
					}
					this.addAdditional(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="behavioral_statistics_id"){
				this.BehavioralStatisticsId=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="additional"){
			this.addAdditional(v);
		}
		else{
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="additional"){
			return "http://nrg.wustl.edu/behavioral:statistics_additional";
		}
		else{
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="correct"){
			return "field_data";
		}else if (xmlPath=="missed"){
			return "field_data";
		}else if (xmlPath=="wrong"){
			return "field_data";
		}else if (xmlPath=="response_time"){
			return "field_data";
		}else if (xmlPath=="mean"){
			return "field_data";
		}else if (xmlPath=="median"){
			return "field_data";
		}else if (xmlPath=="mode"){
			return "field_data";
		}else if (xmlPath=="std_deviation"){
			return "field_data";
		}else if (xmlPath=="std_error"){
			return "field_data";
		}else if (xmlPath=="weighted_average"){
			return "field_data";
		}else if (xmlPath=="maximum"){
			return "field_data";
		}else if (xmlPath=="minimum"){
			return "field_data";
		}else if (xmlPath=="additional"){
			return "field_NO_CHILD";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<behavioral:statistics";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</behavioral:statistics>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.BehavioralStatisticsId!=null){
				if(hiddenCount++>0)str+=",";
				str+="behavioral_statistics_id=\"" + this.BehavioralStatisticsId + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Correct!=null){
			xmlTxt+="\n<behavioral:correct";
			xmlTxt+=">";
			xmlTxt+=this.Correct;
			xmlTxt+="</behavioral:correct>";
		}
		if (this.Missed!=null){
			xmlTxt+="\n<behavioral:missed";
			xmlTxt+=">";
			xmlTxt+=this.Missed;
			xmlTxt+="</behavioral:missed>";
		}
		if (this.Wrong!=null){
			xmlTxt+="\n<behavioral:wrong";
			xmlTxt+=">";
			xmlTxt+=this.Wrong;
			xmlTxt+="</behavioral:wrong>";
		}
		if (this.ResponseTime!=null){
			xmlTxt+="\n<behavioral:response_time";
			xmlTxt+=">";
			xmlTxt+=this.ResponseTime;
			xmlTxt+="</behavioral:response_time>";
		}
		if (this.Mean!=null){
			xmlTxt+="\n<behavioral:mean";
			xmlTxt+=">";
			xmlTxt+=this.Mean;
			xmlTxt+="</behavioral:mean>";
		}
		if (this.Median!=null){
			xmlTxt+="\n<behavioral:median";
			xmlTxt+=">";
			xmlTxt+=this.Median;
			xmlTxt+="</behavioral:median>";
		}
		if (this.Mode!=null){
			xmlTxt+="\n<behavioral:mode";
			xmlTxt+=">";
			xmlTxt+=this.Mode;
			xmlTxt+="</behavioral:mode>";
		}
		if (this.StdDeviation!=null){
			xmlTxt+="\n<behavioral:std_deviation";
			xmlTxt+=">";
			xmlTxt+=this.StdDeviation;
			xmlTxt+="</behavioral:std_deviation>";
		}
		if (this.StdError!=null){
			xmlTxt+="\n<behavioral:std_error";
			xmlTxt+=">";
			xmlTxt+=this.StdError;
			xmlTxt+="</behavioral:std_error>";
		}
		if (this.WeightedAverage!=null){
			xmlTxt+="\n<behavioral:weighted_average";
			xmlTxt+=">";
			xmlTxt+=this.WeightedAverage;
			xmlTxt+="</behavioral:weighted_average>";
		}
		if (this.Maximum!=null){
			xmlTxt+="\n<behavioral:maximum";
			xmlTxt+=">";
			xmlTxt+=this.Maximum.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</behavioral:maximum>";
		}
		if (this.Minimum!=null){
			xmlTxt+="\n<behavioral:minimum";
			xmlTxt+=">";
			xmlTxt+=this.Minimum;
			xmlTxt+="</behavioral:minimum>";
		}
		for(var AdditionalCOUNT=0;AdditionalCOUNT<this.Additional.length;AdditionalCOUNT++){
			xmlTxt +="\n<behavioral:additional";
			xmlTxt +=this.Additional[AdditionalCOUNT].getXMLAtts();
			if(this.Additional[AdditionalCOUNT].xsiType!="behavioral:statistics_additional"){
				xmlTxt+=" xsi:type=\"" + this.Additional[AdditionalCOUNT].xsiType + "\"";
			}
			if (this.Additional[AdditionalCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Additional[AdditionalCOUNT].getXMLBody(preventComments);
					xmlTxt+="</behavioral:additional>";
			}else {xmlTxt+="/>";}
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.BehavioralStatisticsId!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Correct!=null) return true;
		if (this.Missed!=null) return true;
		if (this.Wrong!=null) return true;
		if (this.ResponseTime!=null) return true;
		if (this.Mean!=null) return true;
		if (this.Median!=null) return true;
		if (this.Mode!=null) return true;
		if (this.StdDeviation!=null) return true;
		if (this.StdError!=null) return true;
		if (this.WeightedAverage!=null) return true;
		if (this.Maximum!=null) return true;
		if (this.Minimum!=null) return true;
		if(this.Additional.length>0) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
