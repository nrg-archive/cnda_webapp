/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function pup_pupTimeCourseData(){
this.xsiType="pup:pupTimeCourseData";

	this.getSchemaElementName=function(){
		return "pupTimeCourseData";
	}

	this.getFullSchemaElementName=function(){
		return "pup:pupTimeCourseData";
	}
this.extension=dynamicJSLoad('xnat_petAssessorData','generated/xnat_petAssessorData.js');

	this.Proctype=null;


	function getProctype() {
		return this.Proctype;
	}
	this.getProctype=getProctype;


	function setProctype(v){
		this.Proctype=v;
	}
	this.setProctype=setProctype;

	this.Model=null;


	function getModel() {
		return this.Model;
	}
	this.getModel=getModel;


	function setModel(v){
		this.Model=v;
	}
	this.setModel=setModel;

	this.Tracer=null;


	function getTracer() {
		return this.Tracer;
	}
	this.getTracer=getTracer;


	function setTracer(v){
		this.Tracer=v;
	}
	this.setTracer=setTracer;

	this.Templatetype=null;


	function getTemplatetype() {
		return this.Templatetype;
	}
	this.getTemplatetype=getTemplatetype;


	function setTemplatetype(v){
		this.Templatetype=v;
	}
	this.setTemplatetype=setTemplatetype;

	this.Fsid=null;


	function getFsid() {
		return this.Fsid;
	}
	this.getFsid=getFsid;


	function setFsid(v){
		this.Fsid=v;
	}
	this.setFsid=setFsid;

	this.Mrid=null;


	function getMrid() {
		return this.Mrid;
	}
	this.getMrid=getMrid;


	function setMrid(v){
		this.Mrid=v;
	}
	this.setMrid=setMrid;

	this.Mocoerror=null;


	function getMocoerror() {
		return this.Mocoerror;
	}
	this.getMocoerror=getMocoerror;


	function setMocoerror(v){
		this.Mocoerror=v;
	}
	this.setMocoerror=setMocoerror;

	this.Regerror=null;


	function getRegerror() {
		return this.Regerror;
	}
	this.getRegerror=getRegerror;


	function setRegerror(v){
		this.Regerror=v;
	}
	this.setRegerror=setRegerror;

	this.Suvrflag=null;


	function getSuvrflag() {
		return this.Suvrflag;
	}
	this.getSuvrflag=getSuvrflag;


	function setSuvrflag(v){
		this.Suvrflag=v;
	}
	this.setSuvrflag=setSuvrflag;
	this.Rois_roi =new Array();

	function getRois_roi() {
		return this.Rois_roi;
	}
	this.getRois_roi=getRois_roi;


	function addRois_roi(v){
		this.Rois_roi.push(v);
	}
	this.addRois_roi=addRois_roi;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="petAssessorData"){
				return this.Petassessordata ;
			} else 
			if(xmlPath.startsWith("petAssessorData")){
				xmlPath=xmlPath.substring(15);
				if(xmlPath=="")return this.Petassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Petassessordata!=undefined)return this.Petassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="procType"){
				return this.Proctype ;
			} else 
			if(xmlPath=="model"){
				return this.Model ;
			} else 
			if(xmlPath=="tracer"){
				return this.Tracer ;
			} else 
			if(xmlPath=="templateType"){
				return this.Templatetype ;
			} else 
			if(xmlPath=="FSId"){
				return this.Fsid ;
			} else 
			if(xmlPath=="MRId"){
				return this.Mrid ;
			} else 
			if(xmlPath=="mocoError"){
				return this.Mocoerror ;
			} else 
			if(xmlPath=="regError"){
				return this.Regerror ;
			} else 
			if(xmlPath=="suvrFlag"){
				return this.Suvrflag ;
			} else 
			if(xmlPath=="rois/roi"){
				return this.Rois_roi ;
			} else 
			if(xmlPath.startsWith("rois/roi")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Rois_roi ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Rois_roi.length;whereCount++){

					var tempValue=this.Rois_roi[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Rois_roi[whereCount]);

					}

				}
				}else{

				whereArray=this.Rois_roi;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="petAssessorData"){
				this.Petassessordata=value;
			} else 
			if(xmlPath.startsWith("petAssessorData")){
				xmlPath=xmlPath.substring(15);
				if(xmlPath=="")return this.Petassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Petassessordata!=undefined){
					this.Petassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Petassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Petassessordata= instanciateObject("xnat:petAssessorData");//omUtils.js
						}
						if(options && options.where)this.Petassessordata.setProperty(options.where.field,options.where.value);
						this.Petassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="procType"){
				this.Proctype=value;
			} else 
			if(xmlPath=="model"){
				this.Model=value;
			} else 
			if(xmlPath=="tracer"){
				this.Tracer=value;
			} else 
			if(xmlPath=="templateType"){
				this.Templatetype=value;
			} else 
			if(xmlPath=="FSId"){
				this.Fsid=value;
			} else 
			if(xmlPath=="MRId"){
				this.Mrid=value;
			} else 
			if(xmlPath=="mocoError"){
				this.Mocoerror=value;
			} else 
			if(xmlPath=="regError"){
				this.Regerror=value;
			} else 
			if(xmlPath=="suvrFlag"){
				this.Suvrflag=value;
			} else 
			if(xmlPath=="rois/roi"){
				this.Rois_roi=value;
			} else 
			if(xmlPath.startsWith("rois/roi")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Rois_roi ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Rois_roi.length;whereCount++){

					var tempValue=this.Rois_roi[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Rois_roi[whereCount]);

					}

				}
				}else{

				whereArray=this.Rois_roi;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("pup:pupTimeCourseData_roi");//omUtils.js
					}
					this.addRois_roi(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="rois/roi"){
			this.addRois_roi(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="rois/roi"){
			return "http://nrg.wustl.edu/pup:pupTimeCourseData_roi";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="procType"){
			return "field_data";
		}else if (xmlPath=="model"){
			return "field_data";
		}else if (xmlPath=="tracer"){
			return "field_data";
		}else if (xmlPath=="templateType"){
			return "field_data";
		}else if (xmlPath=="FSId"){
			return "field_data";
		}else if (xmlPath=="MRId"){
			return "field_data";
		}else if (xmlPath=="mocoError"){
			return "field_data";
		}else if (xmlPath=="regError"){
			return "field_data";
		}else if (xmlPath=="suvrFlag"){
			return "field_data";
		}else if (xmlPath=="rois/roi"){
			return "field_multi_reference";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<pup:PUPTimeCourse";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</pup:PUPTimeCourse>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Proctype!=null){
			xmlTxt+="\n<pup:procType";
			xmlTxt+=">";
			xmlTxt+=this.Proctype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:procType>";
		}
		if (this.Model!=null){
			xmlTxt+="\n<pup:model";
			xmlTxt+=">";
			xmlTxt+=this.Model.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:model>";
		}
		if (this.Tracer!=null){
			xmlTxt+="\n<pup:tracer";
			xmlTxt+=">";
			xmlTxt+=this.Tracer.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:tracer>";
		}
		if (this.Templatetype!=null){
			xmlTxt+="\n<pup:templateType";
			xmlTxt+=">";
			xmlTxt+=this.Templatetype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:templateType>";
		}
		if (this.Fsid!=null){
			xmlTxt+="\n<pup:FSId";
			xmlTxt+=">";
			xmlTxt+=this.Fsid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:FSId>";
		}
		if (this.Mrid!=null){
			xmlTxt+="\n<pup:MRId";
			xmlTxt+=">";
			xmlTxt+=this.Mrid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:MRId>";
		}
		if (this.Mocoerror!=null){
			xmlTxt+="\n<pup:mocoError";
			xmlTxt+=">";
			xmlTxt+=this.Mocoerror;
			xmlTxt+="</pup:mocoError>";
		}
		if (this.Regerror!=null){
			xmlTxt+="\n<pup:regError";
			xmlTxt+=">";
			xmlTxt+=this.Regerror;
			xmlTxt+="</pup:regError>";
		}
		if (this.Suvrflag!=null){
			xmlTxt+="\n<pup:suvrFlag";
			xmlTxt+=">";
			xmlTxt+=this.Suvrflag;
			xmlTxt+="</pup:suvrFlag>";
		}
			var child0=0;
			var att0=0;
			child0+=this.Rois_roi.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<pup:rois";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Rois_roiCOUNT=0;Rois_roiCOUNT<this.Rois_roi.length;Rois_roiCOUNT++){
			xmlTxt +="\n<pup:roi";
			xmlTxt +=this.Rois_roi[Rois_roiCOUNT].getXMLAtts();
			if(this.Rois_roi[Rois_roiCOUNT].xsiType!="pup:pupTimeCourseData_roi"){
				xmlTxt+=" xsi:type=\"" + this.Rois_roi[Rois_roiCOUNT].xsiType + "\"";
			}
			if (this.Rois_roi[Rois_roiCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Rois_roi[Rois_roiCOUNT].getXMLBody(preventComments);
					xmlTxt+="</pup:roi>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</pup:rois>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Proctype!=null) return true;
		if (this.Model!=null) return true;
		if (this.Tracer!=null) return true;
		if (this.Templatetype!=null) return true;
		if (this.Fsid!=null) return true;
		if (this.Mrid!=null) return true;
		if (this.Mocoerror!=null) return true;
		if (this.Regerror!=null) return true;
		if (this.Suvrflag!=null) return true;
			if(this.Rois_roi.length>0)return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
