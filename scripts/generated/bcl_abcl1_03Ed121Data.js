/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function bcl_abcl1_03Ed121Data(){
this.xsiType="bcl:abcl1-03Ed121Data";

	this.getSchemaElementName=function(){
		return "abcl1-03Ed121Data";
	}

	this.getFullSchemaElementName=function(){
		return "bcl:abcl1-03Ed121Data";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.AnxdpRaw=null;


	function getAnxdpRaw() {
		return this.AnxdpRaw;
	}
	this.getAnxdpRaw=getAnxdpRaw;


	function setAnxdpRaw(v){
		this.AnxdpRaw=v;
	}
	this.setAnxdpRaw=setAnxdpRaw;

	this.AnxdpT=null;


	function getAnxdpT() {
		return this.AnxdpT;
	}
	this.getAnxdpT=getAnxdpT;


	function setAnxdpT(v){
		this.AnxdpT=v;
	}
	this.setAnxdpT=setAnxdpT;

	this.AnxdpNote=null;


	function getAnxdpNote() {
		return this.AnxdpNote;
	}
	this.getAnxdpNote=getAnxdpNote;


	function setAnxdpNote(v){
		this.AnxdpNote=v;
	}
	this.setAnxdpNote=setAnxdpNote;

	this.WthdpRaw=null;


	function getWthdpRaw() {
		return this.WthdpRaw;
	}
	this.getWthdpRaw=getWthdpRaw;


	function setWthdpRaw(v){
		this.WthdpRaw=v;
	}
	this.setWthdpRaw=setWthdpRaw;

	this.WthdpT=null;


	function getWthdpT() {
		return this.WthdpT;
	}
	this.getWthdpT=getWthdpT;


	function setWthdpT(v){
		this.WthdpT=v;
	}
	this.setWthdpT=setWthdpT;

	this.WthdpNote=null;


	function getWthdpNote() {
		return this.WthdpNote;
	}
	this.getWthdpNote=getWthdpNote;


	function setWthdpNote(v){
		this.WthdpNote=v;
	}
	this.setWthdpNote=setWthdpNote;

	this.SomRaw=null;


	function getSomRaw() {
		return this.SomRaw;
	}
	this.getSomRaw=getSomRaw;


	function setSomRaw(v){
		this.SomRaw=v;
	}
	this.setSomRaw=setSomRaw;

	this.SomT=null;


	function getSomT() {
		return this.SomT;
	}
	this.getSomT=getSomT;


	function setSomT(v){
		this.SomT=v;
	}
	this.setSomT=setSomT;

	this.SomNote=null;


	function getSomNote() {
		return this.SomNote;
	}
	this.getSomNote=getSomNote;


	function setSomNote(v){
		this.SomNote=v;
	}
	this.setSomNote=setSomNote;

	this.ThoRaw=null;


	function getThoRaw() {
		return this.ThoRaw;
	}
	this.getThoRaw=getThoRaw;


	function setThoRaw(v){
		this.ThoRaw=v;
	}
	this.setThoRaw=setThoRaw;

	this.ThoT=null;


	function getThoT() {
		return this.ThoT;
	}
	this.getThoT=getThoT;


	function setThoT(v){
		this.ThoT=v;
	}
	this.setThoT=setThoT;

	this.ThoNote=null;


	function getThoNote() {
		return this.ThoNote;
	}
	this.getThoNote=getThoNote;


	function setThoNote(v){
		this.ThoNote=v;
	}
	this.setThoNote=setThoNote;

	this.AttRaw=null;


	function getAttRaw() {
		return this.AttRaw;
	}
	this.getAttRaw=getAttRaw;


	function setAttRaw(v){
		this.AttRaw=v;
	}
	this.setAttRaw=setAttRaw;

	this.AttT=null;


	function getAttT() {
		return this.AttT;
	}
	this.getAttT=getAttT;


	function setAttT(v){
		this.AttT=v;
	}
	this.setAttT=setAttT;

	this.AttNote=null;


	function getAttNote() {
		return this.AttNote;
	}
	this.getAttNote=getAttNote;


	function setAttNote(v){
		this.AttNote=v;
	}
	this.setAttNote=setAttNote;

	this.RuleRaw=null;


	function getRuleRaw() {
		return this.RuleRaw;
	}
	this.getRuleRaw=getRuleRaw;


	function setRuleRaw(v){
		this.RuleRaw=v;
	}
	this.setRuleRaw=setRuleRaw;

	this.RuleT=null;


	function getRuleT() {
		return this.RuleT;
	}
	this.getRuleT=getRuleT;


	function setRuleT(v){
		this.RuleT=v;
	}
	this.setRuleT=setRuleT;

	this.RuleNote=null;


	function getRuleNote() {
		return this.RuleNote;
	}
	this.getRuleNote=getRuleNote;


	function setRuleNote(v){
		this.RuleNote=v;
	}
	this.setRuleNote=setRuleNote;

	this.AggRaw=null;


	function getAggRaw() {
		return this.AggRaw;
	}
	this.getAggRaw=getAggRaw;


	function setAggRaw(v){
		this.AggRaw=v;
	}
	this.setAggRaw=setAggRaw;

	this.AggT=null;


	function getAggT() {
		return this.AggT;
	}
	this.getAggT=getAggT;


	function setAggT(v){
		this.AggT=v;
	}
	this.setAggT=setAggT;

	this.AggNote=null;


	function getAggNote() {
		return this.AggNote;
	}
	this.getAggNote=getAggNote;


	function setAggNote(v){
		this.AggNote=v;
	}
	this.setAggNote=setAggNote;

	this.IntRaw=null;


	function getIntRaw() {
		return this.IntRaw;
	}
	this.getIntRaw=getIntRaw;


	function setIntRaw(v){
		this.IntRaw=v;
	}
	this.setIntRaw=setIntRaw;

	this.IntT=null;


	function getIntT() {
		return this.IntT;
	}
	this.getIntT=getIntT;


	function setIntT(v){
		this.IntT=v;
	}
	this.setIntT=setIntT;

	this.IntNote=null;


	function getIntNote() {
		return this.IntNote;
	}
	this.getIntNote=getIntNote;


	function setIntNote(v){
		this.IntNote=v;
	}
	this.setIntNote=setIntNote;

	this.Scoreform_resprltnsubj=null;


	function getScoreform_resprltnsubj() {
		return this.Scoreform_resprltnsubj;
	}
	this.getScoreform_resprltnsubj=getScoreform_resprltnsubj;


	function setScoreform_resprltnsubj(v){
		this.Scoreform_resprltnsubj=v;
	}
	this.setScoreform_resprltnsubj=setScoreform_resprltnsubj;

	this.Scoreform_sectioni_friendsa=null;


	function getScoreform_sectioni_friendsa() {
		return this.Scoreform_sectioni_friendsa;
	}
	this.getScoreform_sectioni_friendsa=getScoreform_sectioni_friendsa;


	function setScoreform_sectioni_friendsa(v){
		this.Scoreform_sectioni_friendsa=v;
	}
	this.setScoreform_sectioni_friendsa=setScoreform_sectioni_friendsa;

	this.Scoreform_sectioni_friendsb=null;


	function getScoreform_sectioni_friendsb() {
		return this.Scoreform_sectioni_friendsb;
	}
	this.getScoreform_sectioni_friendsb=getScoreform_sectioni_friendsb;


	function setScoreform_sectioni_friendsb(v){
		this.Scoreform_sectioni_friendsb=v;
	}
	this.setScoreform_sectioni_friendsb=setScoreform_sectioni_friendsb;

	this.Scoreform_sectioni_friendsc=null;


	function getScoreform_sectioni_friendsc() {
		return this.Scoreform_sectioni_friendsc;
	}
	this.getScoreform_sectioni_friendsc=getScoreform_sectioni_friendsc;


	function setScoreform_sectioni_friendsc(v){
		this.Scoreform_sectioni_friendsc=v;
	}
	this.setScoreform_sectioni_friendsc=setScoreform_sectioni_friendsc;

	this.Scoreform_sectioni_friendsd=null;


	function getScoreform_sectioni_friendsd() {
		return this.Scoreform_sectioni_friendsd;
	}
	this.getScoreform_sectioni_friendsd=getScoreform_sectioni_friendsd;


	function setScoreform_sectioni_friendsd(v){
		this.Scoreform_sectioni_friendsd=v;
	}
	this.setScoreform_sectioni_friendsd=setScoreform_sectioni_friendsd;

	this.Scoreform_sectionii_maritalstatus=null;


	function getScoreform_sectionii_maritalstatus() {
		return this.Scoreform_sectionii_maritalstatus;
	}
	this.getScoreform_sectionii_maritalstatus=getScoreform_sectionii_maritalstatus;


	function setScoreform_sectionii_maritalstatus(v){
		this.Scoreform_sectionii_maritalstatus=v;
	}
	this.setScoreform_sectionii_maritalstatus=setScoreform_sectionii_maritalstatus;

	this.Scoreform_sectionii_maritalstatusother=null;


	function getScoreform_sectionii_maritalstatusother() {
		return this.Scoreform_sectionii_maritalstatusother;
	}
	this.getScoreform_sectionii_maritalstatusother=getScoreform_sectionii_maritalstatusother;


	function setScoreform_sectionii_maritalstatusother(v){
		this.Scoreform_sectionii_maritalstatusother=v;
	}
	this.setScoreform_sectionii_maritalstatusother=setScoreform_sectionii_maritalstatusother;

	this.Scoreform_sectionii_maritallast6mnths=null;


	function getScoreform_sectionii_maritallast6mnths() {
		return this.Scoreform_sectionii_maritallast6mnths;
	}
	this.getScoreform_sectionii_maritallast6mnths=getScoreform_sectionii_maritallast6mnths;


	function setScoreform_sectionii_maritallast6mnths(v){
		this.Scoreform_sectionii_maritallast6mnths=v;
	}
	this.setScoreform_sectionii_maritallast6mnths=setScoreform_sectionii_maritallast6mnths;

	this.Scoreform_sectionii_maritala=null;


	function getScoreform_sectionii_maritala() {
		return this.Scoreform_sectionii_maritala;
	}
	this.getScoreform_sectionii_maritala=getScoreform_sectionii_maritala;


	function setScoreform_sectionii_maritala(v){
		this.Scoreform_sectionii_maritala=v;
	}
	this.setScoreform_sectionii_maritala=setScoreform_sectionii_maritala;

	this.Scoreform_sectionii_maritalb=null;


	function getScoreform_sectionii_maritalb() {
		return this.Scoreform_sectionii_maritalb;
	}
	this.getScoreform_sectionii_maritalb=getScoreform_sectionii_maritalb;


	function setScoreform_sectionii_maritalb(v){
		this.Scoreform_sectionii_maritalb=v;
	}
	this.setScoreform_sectionii_maritalb=setScoreform_sectionii_maritalb;

	this.Scoreform_sectionii_maritalc=null;


	function getScoreform_sectionii_maritalc() {
		return this.Scoreform_sectionii_maritalc;
	}
	this.getScoreform_sectionii_maritalc=getScoreform_sectionii_maritalc;


	function setScoreform_sectionii_maritalc(v){
		this.Scoreform_sectionii_maritalc=v;
	}
	this.setScoreform_sectionii_maritalc=setScoreform_sectionii_maritalc;

	this.Scoreform_sectionii_maritald=null;


	function getScoreform_sectionii_maritald() {
		return this.Scoreform_sectionii_maritald;
	}
	this.getScoreform_sectionii_maritald=getScoreform_sectionii_maritald;


	function setScoreform_sectionii_maritald(v){
		this.Scoreform_sectionii_maritald=v;
	}
	this.setScoreform_sectionii_maritald=setScoreform_sectionii_maritald;

	this.Scoreform_sectionii_maritale=null;


	function getScoreform_sectionii_maritale() {
		return this.Scoreform_sectionii_maritale;
	}
	this.getScoreform_sectionii_maritale=getScoreform_sectionii_maritale;


	function setScoreform_sectionii_maritale(v){
		this.Scoreform_sectionii_maritale=v;
	}
	this.setScoreform_sectionii_maritale=setScoreform_sectionii_maritale;

	this.Scoreform_sectionii_maritalf=null;


	function getScoreform_sectionii_maritalf() {
		return this.Scoreform_sectionii_maritalf;
	}
	this.getScoreform_sectionii_maritalf=getScoreform_sectionii_maritalf;


	function setScoreform_sectionii_maritalf(v){
		this.Scoreform_sectionii_maritalf=v;
	}
	this.setScoreform_sectionii_maritalf=setScoreform_sectionii_maritalf;

	this.Scoreform_sectionii_maritalg=null;


	function getScoreform_sectionii_maritalg() {
		return this.Scoreform_sectionii_maritalg;
	}
	this.getScoreform_sectionii_maritalg=getScoreform_sectionii_maritalg;


	function setScoreform_sectionii_maritalg(v){
		this.Scoreform_sectionii_maritalg=v;
	}
	this.setScoreform_sectionii_maritalg=setScoreform_sectionii_maritalg;

	this.Scoreform_sectionii_maritalh=null;


	function getScoreform_sectionii_maritalh() {
		return this.Scoreform_sectionii_maritalh;
	}
	this.getScoreform_sectionii_maritalh=getScoreform_sectionii_maritalh;


	function setScoreform_sectionii_maritalh(v){
		this.Scoreform_sectionii_maritalh=v;
	}
	this.setScoreform_sectionii_maritalh=setScoreform_sectionii_maritalh;

	this.Scoreform_sectioniii_handicap=null;


	function getScoreform_sectioniii_handicap() {
		return this.Scoreform_sectioniii_handicap;
	}
	this.getScoreform_sectioniii_handicap=getScoreform_sectioniii_handicap;


	function setScoreform_sectioniii_handicap(v){
		this.Scoreform_sectioniii_handicap=v;
	}
	this.setScoreform_sectioniii_handicap=setScoreform_sectioniii_handicap;

	this.Scoreform_sectioniii_handicapexplain=null;


	function getScoreform_sectioniii_handicapexplain() {
		return this.Scoreform_sectioniii_handicapexplain;
	}
	this.getScoreform_sectioniii_handicapexplain=getScoreform_sectioniii_handicapexplain;


	function setScoreform_sectioniii_handicapexplain(v){
		this.Scoreform_sectioniii_handicapexplain=v;
	}
	this.setScoreform_sectioniii_handicapexplain=setScoreform_sectioniii_handicapexplain;

	this.Scoreform_sectioniv_concerns=null;


	function getScoreform_sectioniv_concerns() {
		return this.Scoreform_sectioniv_concerns;
	}
	this.getScoreform_sectioniv_concerns=getScoreform_sectioniv_concerns;


	function setScoreform_sectioniv_concerns(v){
		this.Scoreform_sectioniv_concerns=v;
	}
	this.setScoreform_sectioniv_concerns=setScoreform_sectioniv_concerns;

	this.Scoreform_sectionv_describe=null;


	function getScoreform_sectionv_describe() {
		return this.Scoreform_sectionv_describe;
	}
	this.getScoreform_sectionv_describe=getScoreform_sectionv_describe;


	function setScoreform_sectionv_describe(v){
		this.Scoreform_sectionv_describe=v;
	}
	this.setScoreform_sectionv_describe=setScoreform_sectionv_describe;

	this.Scoreform_sectionvi_question1=null;


	function getScoreform_sectionvi_question1() {
		return this.Scoreform_sectionvi_question1;
	}
	this.getScoreform_sectionvi_question1=getScoreform_sectionvi_question1;


	function setScoreform_sectionvi_question1(v){
		this.Scoreform_sectionvi_question1=v;
	}
	this.setScoreform_sectionvi_question1=setScoreform_sectionvi_question1;

	this.Scoreform_sectionvi_question2=null;


	function getScoreform_sectionvi_question2() {
		return this.Scoreform_sectionvi_question2;
	}
	this.getScoreform_sectionvi_question2=getScoreform_sectionvi_question2;


	function setScoreform_sectionvi_question2(v){
		this.Scoreform_sectionvi_question2=v;
	}
	this.setScoreform_sectionvi_question2=setScoreform_sectionvi_question2;

	this.Scoreform_sectionvi_question3=null;


	function getScoreform_sectionvi_question3() {
		return this.Scoreform_sectionvi_question3;
	}
	this.getScoreform_sectionvi_question3=getScoreform_sectionvi_question3;


	function setScoreform_sectionvi_question3(v){
		this.Scoreform_sectionvi_question3=v;
	}
	this.setScoreform_sectionvi_question3=setScoreform_sectionvi_question3;

	this.Scoreform_sectionvi_question4=null;


	function getScoreform_sectionvi_question4() {
		return this.Scoreform_sectionvi_question4;
	}
	this.getScoreform_sectionvi_question4=getScoreform_sectionvi_question4;


	function setScoreform_sectionvi_question4(v){
		this.Scoreform_sectionvi_question4=v;
	}
	this.setScoreform_sectionvi_question4=setScoreform_sectionvi_question4;

	this.Scoreform_sectionvi_question5=null;


	function getScoreform_sectionvi_question5() {
		return this.Scoreform_sectionvi_question5;
	}
	this.getScoreform_sectionvi_question5=getScoreform_sectionvi_question5;


	function setScoreform_sectionvi_question5(v){
		this.Scoreform_sectionvi_question5=v;
	}
	this.setScoreform_sectionvi_question5=setScoreform_sectionvi_question5;

	this.Scoreform_sectionvi_question6=null;


	function getScoreform_sectionvi_question6() {
		return this.Scoreform_sectionvi_question6;
	}
	this.getScoreform_sectionvi_question6=getScoreform_sectionvi_question6;


	function setScoreform_sectionvi_question6(v){
		this.Scoreform_sectionvi_question6=v;
	}
	this.setScoreform_sectionvi_question6=setScoreform_sectionvi_question6;

	this.Scoreform_sectionvi_question6desc=null;


	function getScoreform_sectionvi_question6desc() {
		return this.Scoreform_sectionvi_question6desc;
	}
	this.getScoreform_sectionvi_question6desc=getScoreform_sectionvi_question6desc;


	function setScoreform_sectionvi_question6desc(v){
		this.Scoreform_sectionvi_question6desc=v;
	}
	this.setScoreform_sectionvi_question6desc=setScoreform_sectionvi_question6desc;

	this.Scoreform_sectionvi_question7=null;


	function getScoreform_sectionvi_question7() {
		return this.Scoreform_sectionvi_question7;
	}
	this.getScoreform_sectionvi_question7=getScoreform_sectionvi_question7;


	function setScoreform_sectionvi_question7(v){
		this.Scoreform_sectionvi_question7=v;
	}
	this.setScoreform_sectionvi_question7=setScoreform_sectionvi_question7;

	this.Scoreform_sectionvi_question8=null;


	function getScoreform_sectionvi_question8() {
		return this.Scoreform_sectionvi_question8;
	}
	this.getScoreform_sectionvi_question8=getScoreform_sectionvi_question8;


	function setScoreform_sectionvi_question8(v){
		this.Scoreform_sectionvi_question8=v;
	}
	this.setScoreform_sectionvi_question8=setScoreform_sectionvi_question8;

	this.Scoreform_sectionvi_question9=null;


	function getScoreform_sectionvi_question9() {
		return this.Scoreform_sectionvi_question9;
	}
	this.getScoreform_sectionvi_question9=getScoreform_sectionvi_question9;


	function setScoreform_sectionvi_question9(v){
		this.Scoreform_sectionvi_question9=v;
	}
	this.setScoreform_sectionvi_question9=setScoreform_sectionvi_question9;

	this.Scoreform_sectionvi_question9desc=null;


	function getScoreform_sectionvi_question9desc() {
		return this.Scoreform_sectionvi_question9desc;
	}
	this.getScoreform_sectionvi_question9desc=getScoreform_sectionvi_question9desc;


	function setScoreform_sectionvi_question9desc(v){
		this.Scoreform_sectionvi_question9desc=v;
	}
	this.setScoreform_sectionvi_question9desc=setScoreform_sectionvi_question9desc;

	this.Scoreform_sectionvi_question10=null;


	function getScoreform_sectionvi_question10() {
		return this.Scoreform_sectionvi_question10;
	}
	this.getScoreform_sectionvi_question10=getScoreform_sectionvi_question10;


	function setScoreform_sectionvi_question10(v){
		this.Scoreform_sectionvi_question10=v;
	}
	this.setScoreform_sectionvi_question10=setScoreform_sectionvi_question10;

	this.Scoreform_sectionvi_question11=null;


	function getScoreform_sectionvi_question11() {
		return this.Scoreform_sectionvi_question11;
	}
	this.getScoreform_sectionvi_question11=getScoreform_sectionvi_question11;


	function setScoreform_sectionvi_question11(v){
		this.Scoreform_sectionvi_question11=v;
	}
	this.setScoreform_sectionvi_question11=setScoreform_sectionvi_question11;

	this.Scoreform_sectionvi_question12=null;


	function getScoreform_sectionvi_question12() {
		return this.Scoreform_sectionvi_question12;
	}
	this.getScoreform_sectionvi_question12=getScoreform_sectionvi_question12;


	function setScoreform_sectionvi_question12(v){
		this.Scoreform_sectionvi_question12=v;
	}
	this.setScoreform_sectionvi_question12=setScoreform_sectionvi_question12;

	this.Scoreform_sectionvi_question13=null;


	function getScoreform_sectionvi_question13() {
		return this.Scoreform_sectionvi_question13;
	}
	this.getScoreform_sectionvi_question13=getScoreform_sectionvi_question13;


	function setScoreform_sectionvi_question13(v){
		this.Scoreform_sectionvi_question13=v;
	}
	this.setScoreform_sectionvi_question13=setScoreform_sectionvi_question13;

	this.Scoreform_sectionvi_question14=null;


	function getScoreform_sectionvi_question14() {
		return this.Scoreform_sectionvi_question14;
	}
	this.getScoreform_sectionvi_question14=getScoreform_sectionvi_question14;


	function setScoreform_sectionvi_question14(v){
		this.Scoreform_sectionvi_question14=v;
	}
	this.setScoreform_sectionvi_question14=setScoreform_sectionvi_question14;

	this.Scoreform_sectionvi_question15=null;


	function getScoreform_sectionvi_question15() {
		return this.Scoreform_sectionvi_question15;
	}
	this.getScoreform_sectionvi_question15=getScoreform_sectionvi_question15;


	function setScoreform_sectionvi_question15(v){
		this.Scoreform_sectionvi_question15=v;
	}
	this.setScoreform_sectionvi_question15=setScoreform_sectionvi_question15;

	this.Scoreform_sectionvi_question16=null;


	function getScoreform_sectionvi_question16() {
		return this.Scoreform_sectionvi_question16;
	}
	this.getScoreform_sectionvi_question16=getScoreform_sectionvi_question16;


	function setScoreform_sectionvi_question16(v){
		this.Scoreform_sectionvi_question16=v;
	}
	this.setScoreform_sectionvi_question16=setScoreform_sectionvi_question16;

	this.Scoreform_sectionvi_question17=null;


	function getScoreform_sectionvi_question17() {
		return this.Scoreform_sectionvi_question17;
	}
	this.getScoreform_sectionvi_question17=getScoreform_sectionvi_question17;


	function setScoreform_sectionvi_question17(v){
		this.Scoreform_sectionvi_question17=v;
	}
	this.setScoreform_sectionvi_question17=setScoreform_sectionvi_question17;

	this.Scoreform_sectionvi_question18=null;


	function getScoreform_sectionvi_question18() {
		return this.Scoreform_sectionvi_question18;
	}
	this.getScoreform_sectionvi_question18=getScoreform_sectionvi_question18;


	function setScoreform_sectionvi_question18(v){
		this.Scoreform_sectionvi_question18=v;
	}
	this.setScoreform_sectionvi_question18=setScoreform_sectionvi_question18;

	this.Scoreform_sectionvi_question19=null;


	function getScoreform_sectionvi_question19() {
		return this.Scoreform_sectionvi_question19;
	}
	this.getScoreform_sectionvi_question19=getScoreform_sectionvi_question19;


	function setScoreform_sectionvi_question19(v){
		this.Scoreform_sectionvi_question19=v;
	}
	this.setScoreform_sectionvi_question19=setScoreform_sectionvi_question19;

	this.Scoreform_sectionvi_question20=null;


	function getScoreform_sectionvi_question20() {
		return this.Scoreform_sectionvi_question20;
	}
	this.getScoreform_sectionvi_question20=getScoreform_sectionvi_question20;


	function setScoreform_sectionvi_question20(v){
		this.Scoreform_sectionvi_question20=v;
	}
	this.setScoreform_sectionvi_question20=setScoreform_sectionvi_question20;

	this.Scoreform_sectionvi_question21=null;


	function getScoreform_sectionvi_question21() {
		return this.Scoreform_sectionvi_question21;
	}
	this.getScoreform_sectionvi_question21=getScoreform_sectionvi_question21;


	function setScoreform_sectionvi_question21(v){
		this.Scoreform_sectionvi_question21=v;
	}
	this.setScoreform_sectionvi_question21=setScoreform_sectionvi_question21;

	this.Scoreform_sectionvi_question22=null;


	function getScoreform_sectionvi_question22() {
		return this.Scoreform_sectionvi_question22;
	}
	this.getScoreform_sectionvi_question22=getScoreform_sectionvi_question22;


	function setScoreform_sectionvi_question22(v){
		this.Scoreform_sectionvi_question22=v;
	}
	this.setScoreform_sectionvi_question22=setScoreform_sectionvi_question22;

	this.Scoreform_sectionvi_question23=null;


	function getScoreform_sectionvi_question23() {
		return this.Scoreform_sectionvi_question23;
	}
	this.getScoreform_sectionvi_question23=getScoreform_sectionvi_question23;


	function setScoreform_sectionvi_question23(v){
		this.Scoreform_sectionvi_question23=v;
	}
	this.setScoreform_sectionvi_question23=setScoreform_sectionvi_question23;

	this.Scoreform_sectionvi_question24=null;


	function getScoreform_sectionvi_question24() {
		return this.Scoreform_sectionvi_question24;
	}
	this.getScoreform_sectionvi_question24=getScoreform_sectionvi_question24;


	function setScoreform_sectionvi_question24(v){
		this.Scoreform_sectionvi_question24=v;
	}
	this.setScoreform_sectionvi_question24=setScoreform_sectionvi_question24;

	this.Scoreform_sectionvi_question25=null;


	function getScoreform_sectionvi_question25() {
		return this.Scoreform_sectionvi_question25;
	}
	this.getScoreform_sectionvi_question25=getScoreform_sectionvi_question25;


	function setScoreform_sectionvi_question25(v){
		this.Scoreform_sectionvi_question25=v;
	}
	this.setScoreform_sectionvi_question25=setScoreform_sectionvi_question25;

	this.Scoreform_sectionvi_question26=null;


	function getScoreform_sectionvi_question26() {
		return this.Scoreform_sectionvi_question26;
	}
	this.getScoreform_sectionvi_question26=getScoreform_sectionvi_question26;


	function setScoreform_sectionvi_question26(v){
		this.Scoreform_sectionvi_question26=v;
	}
	this.setScoreform_sectionvi_question26=setScoreform_sectionvi_question26;

	this.Scoreform_sectionvi_question27=null;


	function getScoreform_sectionvi_question27() {
		return this.Scoreform_sectionvi_question27;
	}
	this.getScoreform_sectionvi_question27=getScoreform_sectionvi_question27;


	function setScoreform_sectionvi_question27(v){
		this.Scoreform_sectionvi_question27=v;
	}
	this.setScoreform_sectionvi_question27=setScoreform_sectionvi_question27;

	this.Scoreform_sectionvi_question28=null;


	function getScoreform_sectionvi_question28() {
		return this.Scoreform_sectionvi_question28;
	}
	this.getScoreform_sectionvi_question28=getScoreform_sectionvi_question28;


	function setScoreform_sectionvi_question28(v){
		this.Scoreform_sectionvi_question28=v;
	}
	this.setScoreform_sectionvi_question28=setScoreform_sectionvi_question28;

	this.Scoreform_sectionvi_question29=null;


	function getScoreform_sectionvi_question29() {
		return this.Scoreform_sectionvi_question29;
	}
	this.getScoreform_sectionvi_question29=getScoreform_sectionvi_question29;


	function setScoreform_sectionvi_question29(v){
		this.Scoreform_sectionvi_question29=v;
	}
	this.setScoreform_sectionvi_question29=setScoreform_sectionvi_question29;

	this.Scoreform_sectionvi_question29desc=null;


	function getScoreform_sectionvi_question29desc() {
		return this.Scoreform_sectionvi_question29desc;
	}
	this.getScoreform_sectionvi_question29desc=getScoreform_sectionvi_question29desc;


	function setScoreform_sectionvi_question29desc(v){
		this.Scoreform_sectionvi_question29desc=v;
	}
	this.setScoreform_sectionvi_question29desc=setScoreform_sectionvi_question29desc;

	this.Scoreform_sectionvi_question30=null;


	function getScoreform_sectionvi_question30() {
		return this.Scoreform_sectionvi_question30;
	}
	this.getScoreform_sectionvi_question30=getScoreform_sectionvi_question30;


	function setScoreform_sectionvi_question30(v){
		this.Scoreform_sectionvi_question30=v;
	}
	this.setScoreform_sectionvi_question30=setScoreform_sectionvi_question30;

	this.Scoreform_sectionvi_question31=null;


	function getScoreform_sectionvi_question31() {
		return this.Scoreform_sectionvi_question31;
	}
	this.getScoreform_sectionvi_question31=getScoreform_sectionvi_question31;


	function setScoreform_sectionvi_question31(v){
		this.Scoreform_sectionvi_question31=v;
	}
	this.setScoreform_sectionvi_question31=setScoreform_sectionvi_question31;

	this.Scoreform_sectionvi_question32=null;


	function getScoreform_sectionvi_question32() {
		return this.Scoreform_sectionvi_question32;
	}
	this.getScoreform_sectionvi_question32=getScoreform_sectionvi_question32;


	function setScoreform_sectionvi_question32(v){
		this.Scoreform_sectionvi_question32=v;
	}
	this.setScoreform_sectionvi_question32=setScoreform_sectionvi_question32;

	this.Scoreform_sectionvi_question33=null;


	function getScoreform_sectionvi_question33() {
		return this.Scoreform_sectionvi_question33;
	}
	this.getScoreform_sectionvi_question33=getScoreform_sectionvi_question33;


	function setScoreform_sectionvi_question33(v){
		this.Scoreform_sectionvi_question33=v;
	}
	this.setScoreform_sectionvi_question33=setScoreform_sectionvi_question33;

	this.Scoreform_sectionvi_question34=null;


	function getScoreform_sectionvi_question34() {
		return this.Scoreform_sectionvi_question34;
	}
	this.getScoreform_sectionvi_question34=getScoreform_sectionvi_question34;


	function setScoreform_sectionvi_question34(v){
		this.Scoreform_sectionvi_question34=v;
	}
	this.setScoreform_sectionvi_question34=setScoreform_sectionvi_question34;

	this.Scoreform_sectionvi_question35=null;


	function getScoreform_sectionvi_question35() {
		return this.Scoreform_sectionvi_question35;
	}
	this.getScoreform_sectionvi_question35=getScoreform_sectionvi_question35;


	function setScoreform_sectionvi_question35(v){
		this.Scoreform_sectionvi_question35=v;
	}
	this.setScoreform_sectionvi_question35=setScoreform_sectionvi_question35;

	this.Scoreform_sectionvi_question36=null;


	function getScoreform_sectionvi_question36() {
		return this.Scoreform_sectionvi_question36;
	}
	this.getScoreform_sectionvi_question36=getScoreform_sectionvi_question36;


	function setScoreform_sectionvi_question36(v){
		this.Scoreform_sectionvi_question36=v;
	}
	this.setScoreform_sectionvi_question36=setScoreform_sectionvi_question36;

	this.Scoreform_sectionvi_question37=null;


	function getScoreform_sectionvi_question37() {
		return this.Scoreform_sectionvi_question37;
	}
	this.getScoreform_sectionvi_question37=getScoreform_sectionvi_question37;


	function setScoreform_sectionvi_question37(v){
		this.Scoreform_sectionvi_question37=v;
	}
	this.setScoreform_sectionvi_question37=setScoreform_sectionvi_question37;

	this.Scoreform_sectionvi_question38=null;


	function getScoreform_sectionvi_question38() {
		return this.Scoreform_sectionvi_question38;
	}
	this.getScoreform_sectionvi_question38=getScoreform_sectionvi_question38;


	function setScoreform_sectionvi_question38(v){
		this.Scoreform_sectionvi_question38=v;
	}
	this.setScoreform_sectionvi_question38=setScoreform_sectionvi_question38;

	this.Scoreform_sectionvi_question39=null;


	function getScoreform_sectionvi_question39() {
		return this.Scoreform_sectionvi_question39;
	}
	this.getScoreform_sectionvi_question39=getScoreform_sectionvi_question39;


	function setScoreform_sectionvi_question39(v){
		this.Scoreform_sectionvi_question39=v;
	}
	this.setScoreform_sectionvi_question39=setScoreform_sectionvi_question39;

	this.Scoreform_sectionvi_question40=null;


	function getScoreform_sectionvi_question40() {
		return this.Scoreform_sectionvi_question40;
	}
	this.getScoreform_sectionvi_question40=getScoreform_sectionvi_question40;


	function setScoreform_sectionvi_question40(v){
		this.Scoreform_sectionvi_question40=v;
	}
	this.setScoreform_sectionvi_question40=setScoreform_sectionvi_question40;

	this.Scoreform_sectionvi_question40desc=null;


	function getScoreform_sectionvi_question40desc() {
		return this.Scoreform_sectionvi_question40desc;
	}
	this.getScoreform_sectionvi_question40desc=getScoreform_sectionvi_question40desc;


	function setScoreform_sectionvi_question40desc(v){
		this.Scoreform_sectionvi_question40desc=v;
	}
	this.setScoreform_sectionvi_question40desc=setScoreform_sectionvi_question40desc;

	this.Scoreform_sectionvi_question41=null;


	function getScoreform_sectionvi_question41() {
		return this.Scoreform_sectionvi_question41;
	}
	this.getScoreform_sectionvi_question41=getScoreform_sectionvi_question41;


	function setScoreform_sectionvi_question41(v){
		this.Scoreform_sectionvi_question41=v;
	}
	this.setScoreform_sectionvi_question41=setScoreform_sectionvi_question41;

	this.Scoreform_sectionvi_question42=null;


	function getScoreform_sectionvi_question42() {
		return this.Scoreform_sectionvi_question42;
	}
	this.getScoreform_sectionvi_question42=getScoreform_sectionvi_question42;


	function setScoreform_sectionvi_question42(v){
		this.Scoreform_sectionvi_question42=v;
	}
	this.setScoreform_sectionvi_question42=setScoreform_sectionvi_question42;

	this.Scoreform_sectionvi_question43=null;


	function getScoreform_sectionvi_question43() {
		return this.Scoreform_sectionvi_question43;
	}
	this.getScoreform_sectionvi_question43=getScoreform_sectionvi_question43;


	function setScoreform_sectionvi_question43(v){
		this.Scoreform_sectionvi_question43=v;
	}
	this.setScoreform_sectionvi_question43=setScoreform_sectionvi_question43;

	this.Scoreform_sectionvi_question44=null;


	function getScoreform_sectionvi_question44() {
		return this.Scoreform_sectionvi_question44;
	}
	this.getScoreform_sectionvi_question44=getScoreform_sectionvi_question44;


	function setScoreform_sectionvi_question44(v){
		this.Scoreform_sectionvi_question44=v;
	}
	this.setScoreform_sectionvi_question44=setScoreform_sectionvi_question44;

	this.Scoreform_sectionvi_question45=null;


	function getScoreform_sectionvi_question45() {
		return this.Scoreform_sectionvi_question45;
	}
	this.getScoreform_sectionvi_question45=getScoreform_sectionvi_question45;


	function setScoreform_sectionvi_question45(v){
		this.Scoreform_sectionvi_question45=v;
	}
	this.setScoreform_sectionvi_question45=setScoreform_sectionvi_question45;

	this.Scoreform_sectionvi_question46=null;


	function getScoreform_sectionvi_question46() {
		return this.Scoreform_sectionvi_question46;
	}
	this.getScoreform_sectionvi_question46=getScoreform_sectionvi_question46;


	function setScoreform_sectionvi_question46(v){
		this.Scoreform_sectionvi_question46=v;
	}
	this.setScoreform_sectionvi_question46=setScoreform_sectionvi_question46;

	this.Scoreform_sectionvi_question46desc=null;


	function getScoreform_sectionvi_question46desc() {
		return this.Scoreform_sectionvi_question46desc;
	}
	this.getScoreform_sectionvi_question46desc=getScoreform_sectionvi_question46desc;


	function setScoreform_sectionvi_question46desc(v){
		this.Scoreform_sectionvi_question46desc=v;
	}
	this.setScoreform_sectionvi_question46desc=setScoreform_sectionvi_question46desc;

	this.Scoreform_sectionvi_question47=null;


	function getScoreform_sectionvi_question47() {
		return this.Scoreform_sectionvi_question47;
	}
	this.getScoreform_sectionvi_question47=getScoreform_sectionvi_question47;


	function setScoreform_sectionvi_question47(v){
		this.Scoreform_sectionvi_question47=v;
	}
	this.setScoreform_sectionvi_question47=setScoreform_sectionvi_question47;

	this.Scoreform_sectionvi_question48=null;


	function getScoreform_sectionvi_question48() {
		return this.Scoreform_sectionvi_question48;
	}
	this.getScoreform_sectionvi_question48=getScoreform_sectionvi_question48;


	function setScoreform_sectionvi_question48(v){
		this.Scoreform_sectionvi_question48=v;
	}
	this.setScoreform_sectionvi_question48=setScoreform_sectionvi_question48;

	this.Scoreform_sectionvi_question49=null;


	function getScoreform_sectionvi_question49() {
		return this.Scoreform_sectionvi_question49;
	}
	this.getScoreform_sectionvi_question49=getScoreform_sectionvi_question49;


	function setScoreform_sectionvi_question49(v){
		this.Scoreform_sectionvi_question49=v;
	}
	this.setScoreform_sectionvi_question49=setScoreform_sectionvi_question49;

	this.Scoreform_sectionvi_question50=null;


	function getScoreform_sectionvi_question50() {
		return this.Scoreform_sectionvi_question50;
	}
	this.getScoreform_sectionvi_question50=getScoreform_sectionvi_question50;


	function setScoreform_sectionvi_question50(v){
		this.Scoreform_sectionvi_question50=v;
	}
	this.setScoreform_sectionvi_question50=setScoreform_sectionvi_question50;

	this.Scoreform_sectionvi_question51=null;


	function getScoreform_sectionvi_question51() {
		return this.Scoreform_sectionvi_question51;
	}
	this.getScoreform_sectionvi_question51=getScoreform_sectionvi_question51;


	function setScoreform_sectionvi_question51(v){
		this.Scoreform_sectionvi_question51=v;
	}
	this.setScoreform_sectionvi_question51=setScoreform_sectionvi_question51;

	this.Scoreform_sectionvi_question52=null;


	function getScoreform_sectionvi_question52() {
		return this.Scoreform_sectionvi_question52;
	}
	this.getScoreform_sectionvi_question52=getScoreform_sectionvi_question52;


	function setScoreform_sectionvi_question52(v){
		this.Scoreform_sectionvi_question52=v;
	}
	this.setScoreform_sectionvi_question52=setScoreform_sectionvi_question52;

	this.Scoreform_sectionvi_question53=null;


	function getScoreform_sectionvi_question53() {
		return this.Scoreform_sectionvi_question53;
	}
	this.getScoreform_sectionvi_question53=getScoreform_sectionvi_question53;


	function setScoreform_sectionvi_question53(v){
		this.Scoreform_sectionvi_question53=v;
	}
	this.setScoreform_sectionvi_question53=setScoreform_sectionvi_question53;

	this.Scoreform_sectionvi_question54=null;


	function getScoreform_sectionvi_question54() {
		return this.Scoreform_sectionvi_question54;
	}
	this.getScoreform_sectionvi_question54=getScoreform_sectionvi_question54;


	function setScoreform_sectionvi_question54(v){
		this.Scoreform_sectionvi_question54=v;
	}
	this.setScoreform_sectionvi_question54=setScoreform_sectionvi_question54;

	this.Scoreform_sectionvi_question55=null;


	function getScoreform_sectionvi_question55() {
		return this.Scoreform_sectionvi_question55;
	}
	this.getScoreform_sectionvi_question55=getScoreform_sectionvi_question55;


	function setScoreform_sectionvi_question55(v){
		this.Scoreform_sectionvi_question55=v;
	}
	this.setScoreform_sectionvi_question55=setScoreform_sectionvi_question55;

	this.Scoreform_sectionvi_question56a=null;


	function getScoreform_sectionvi_question56a() {
		return this.Scoreform_sectionvi_question56a;
	}
	this.getScoreform_sectionvi_question56a=getScoreform_sectionvi_question56a;


	function setScoreform_sectionvi_question56a(v){
		this.Scoreform_sectionvi_question56a=v;
	}
	this.setScoreform_sectionvi_question56a=setScoreform_sectionvi_question56a;

	this.Scoreform_sectionvi_question56b=null;


	function getScoreform_sectionvi_question56b() {
		return this.Scoreform_sectionvi_question56b;
	}
	this.getScoreform_sectionvi_question56b=getScoreform_sectionvi_question56b;


	function setScoreform_sectionvi_question56b(v){
		this.Scoreform_sectionvi_question56b=v;
	}
	this.setScoreform_sectionvi_question56b=setScoreform_sectionvi_question56b;

	this.Scoreform_sectionvi_question56c=null;


	function getScoreform_sectionvi_question56c() {
		return this.Scoreform_sectionvi_question56c;
	}
	this.getScoreform_sectionvi_question56c=getScoreform_sectionvi_question56c;


	function setScoreform_sectionvi_question56c(v){
		this.Scoreform_sectionvi_question56c=v;
	}
	this.setScoreform_sectionvi_question56c=setScoreform_sectionvi_question56c;

	this.Scoreform_sectionvi_question56d=null;


	function getScoreform_sectionvi_question56d() {
		return this.Scoreform_sectionvi_question56d;
	}
	this.getScoreform_sectionvi_question56d=getScoreform_sectionvi_question56d;


	function setScoreform_sectionvi_question56d(v){
		this.Scoreform_sectionvi_question56d=v;
	}
	this.setScoreform_sectionvi_question56d=setScoreform_sectionvi_question56d;

	this.Scoreform_sectionvi_question56ddesc=null;


	function getScoreform_sectionvi_question56ddesc() {
		return this.Scoreform_sectionvi_question56ddesc;
	}
	this.getScoreform_sectionvi_question56ddesc=getScoreform_sectionvi_question56ddesc;


	function setScoreform_sectionvi_question56ddesc(v){
		this.Scoreform_sectionvi_question56ddesc=v;
	}
	this.setScoreform_sectionvi_question56ddesc=setScoreform_sectionvi_question56ddesc;

	this.Scoreform_sectionvi_question56e=null;


	function getScoreform_sectionvi_question56e() {
		return this.Scoreform_sectionvi_question56e;
	}
	this.getScoreform_sectionvi_question56e=getScoreform_sectionvi_question56e;


	function setScoreform_sectionvi_question56e(v){
		this.Scoreform_sectionvi_question56e=v;
	}
	this.setScoreform_sectionvi_question56e=setScoreform_sectionvi_question56e;

	this.Scoreform_sectionvi_question56f=null;


	function getScoreform_sectionvi_question56f() {
		return this.Scoreform_sectionvi_question56f;
	}
	this.getScoreform_sectionvi_question56f=getScoreform_sectionvi_question56f;


	function setScoreform_sectionvi_question56f(v){
		this.Scoreform_sectionvi_question56f=v;
	}
	this.setScoreform_sectionvi_question56f=setScoreform_sectionvi_question56f;

	this.Scoreform_sectionvi_question56g=null;


	function getScoreform_sectionvi_question56g() {
		return this.Scoreform_sectionvi_question56g;
	}
	this.getScoreform_sectionvi_question56g=getScoreform_sectionvi_question56g;


	function setScoreform_sectionvi_question56g(v){
		this.Scoreform_sectionvi_question56g=v;
	}
	this.setScoreform_sectionvi_question56g=setScoreform_sectionvi_question56g;

	this.Scoreform_sectionvi_question57=null;


	function getScoreform_sectionvi_question57() {
		return this.Scoreform_sectionvi_question57;
	}
	this.getScoreform_sectionvi_question57=getScoreform_sectionvi_question57;


	function setScoreform_sectionvi_question57(v){
		this.Scoreform_sectionvi_question57=v;
	}
	this.setScoreform_sectionvi_question57=setScoreform_sectionvi_question57;

	this.Scoreform_sectionvi_question58=null;


	function getScoreform_sectionvi_question58() {
		return this.Scoreform_sectionvi_question58;
	}
	this.getScoreform_sectionvi_question58=getScoreform_sectionvi_question58;


	function setScoreform_sectionvi_question58(v){
		this.Scoreform_sectionvi_question58=v;
	}
	this.setScoreform_sectionvi_question58=setScoreform_sectionvi_question58;

	this.Scoreform_sectionvi_question58desc=null;


	function getScoreform_sectionvi_question58desc() {
		return this.Scoreform_sectionvi_question58desc;
	}
	this.getScoreform_sectionvi_question58desc=getScoreform_sectionvi_question58desc;


	function setScoreform_sectionvi_question58desc(v){
		this.Scoreform_sectionvi_question58desc=v;
	}
	this.setScoreform_sectionvi_question58desc=setScoreform_sectionvi_question58desc;

	this.Scoreform_sectionvi_question59=null;


	function getScoreform_sectionvi_question59() {
		return this.Scoreform_sectionvi_question59;
	}
	this.getScoreform_sectionvi_question59=getScoreform_sectionvi_question59;


	function setScoreform_sectionvi_question59(v){
		this.Scoreform_sectionvi_question59=v;
	}
	this.setScoreform_sectionvi_question59=setScoreform_sectionvi_question59;

	this.Scoreform_sectionvi_question60=null;


	function getScoreform_sectionvi_question60() {
		return this.Scoreform_sectionvi_question60;
	}
	this.getScoreform_sectionvi_question60=getScoreform_sectionvi_question60;


	function setScoreform_sectionvi_question60(v){
		this.Scoreform_sectionvi_question60=v;
	}
	this.setScoreform_sectionvi_question60=setScoreform_sectionvi_question60;

	this.Scoreform_sectionvi_question61=null;


	function getScoreform_sectionvi_question61() {
		return this.Scoreform_sectionvi_question61;
	}
	this.getScoreform_sectionvi_question61=getScoreform_sectionvi_question61;


	function setScoreform_sectionvi_question61(v){
		this.Scoreform_sectionvi_question61=v;
	}
	this.setScoreform_sectionvi_question61=setScoreform_sectionvi_question61;

	this.Scoreform_sectionvi_question62=null;


	function getScoreform_sectionvi_question62() {
		return this.Scoreform_sectionvi_question62;
	}
	this.getScoreform_sectionvi_question62=getScoreform_sectionvi_question62;


	function setScoreform_sectionvi_question62(v){
		this.Scoreform_sectionvi_question62=v;
	}
	this.setScoreform_sectionvi_question62=setScoreform_sectionvi_question62;

	this.Scoreform_sectionvi_question63=null;


	function getScoreform_sectionvi_question63() {
		return this.Scoreform_sectionvi_question63;
	}
	this.getScoreform_sectionvi_question63=getScoreform_sectionvi_question63;


	function setScoreform_sectionvi_question63(v){
		this.Scoreform_sectionvi_question63=v;
	}
	this.setScoreform_sectionvi_question63=setScoreform_sectionvi_question63;

	this.Scoreform_sectionvi_question64=null;


	function getScoreform_sectionvi_question64() {
		return this.Scoreform_sectionvi_question64;
	}
	this.getScoreform_sectionvi_question64=getScoreform_sectionvi_question64;


	function setScoreform_sectionvi_question64(v){
		this.Scoreform_sectionvi_question64=v;
	}
	this.setScoreform_sectionvi_question64=setScoreform_sectionvi_question64;

	this.Scoreform_sectionvi_question65=null;


	function getScoreform_sectionvi_question65() {
		return this.Scoreform_sectionvi_question65;
	}
	this.getScoreform_sectionvi_question65=getScoreform_sectionvi_question65;


	function setScoreform_sectionvi_question65(v){
		this.Scoreform_sectionvi_question65=v;
	}
	this.setScoreform_sectionvi_question65=setScoreform_sectionvi_question65;

	this.Scoreform_sectionvi_question66=null;


	function getScoreform_sectionvi_question66() {
		return this.Scoreform_sectionvi_question66;
	}
	this.getScoreform_sectionvi_question66=getScoreform_sectionvi_question66;


	function setScoreform_sectionvi_question66(v){
		this.Scoreform_sectionvi_question66=v;
	}
	this.setScoreform_sectionvi_question66=setScoreform_sectionvi_question66;

	this.Scoreform_sectionvi_question66desc=null;


	function getScoreform_sectionvi_question66desc() {
		return this.Scoreform_sectionvi_question66desc;
	}
	this.getScoreform_sectionvi_question66desc=getScoreform_sectionvi_question66desc;


	function setScoreform_sectionvi_question66desc(v){
		this.Scoreform_sectionvi_question66desc=v;
	}
	this.setScoreform_sectionvi_question66desc=setScoreform_sectionvi_question66desc;

	this.Scoreform_sectionvi_question67=null;


	function getScoreform_sectionvi_question67() {
		return this.Scoreform_sectionvi_question67;
	}
	this.getScoreform_sectionvi_question67=getScoreform_sectionvi_question67;


	function setScoreform_sectionvi_question67(v){
		this.Scoreform_sectionvi_question67=v;
	}
	this.setScoreform_sectionvi_question67=setScoreform_sectionvi_question67;

	this.Scoreform_sectionvi_question68=null;


	function getScoreform_sectionvi_question68() {
		return this.Scoreform_sectionvi_question68;
	}
	this.getScoreform_sectionvi_question68=getScoreform_sectionvi_question68;


	function setScoreform_sectionvi_question68(v){
		this.Scoreform_sectionvi_question68=v;
	}
	this.setScoreform_sectionvi_question68=setScoreform_sectionvi_question68;

	this.Scoreform_sectionvi_question69=null;


	function getScoreform_sectionvi_question69() {
		return this.Scoreform_sectionvi_question69;
	}
	this.getScoreform_sectionvi_question69=getScoreform_sectionvi_question69;


	function setScoreform_sectionvi_question69(v){
		this.Scoreform_sectionvi_question69=v;
	}
	this.setScoreform_sectionvi_question69=setScoreform_sectionvi_question69;

	this.Scoreform_sectionvi_question70=null;


	function getScoreform_sectionvi_question70() {
		return this.Scoreform_sectionvi_question70;
	}
	this.getScoreform_sectionvi_question70=getScoreform_sectionvi_question70;


	function setScoreform_sectionvi_question70(v){
		this.Scoreform_sectionvi_question70=v;
	}
	this.setScoreform_sectionvi_question70=setScoreform_sectionvi_question70;

	this.Scoreform_sectionvi_question70desc=null;


	function getScoreform_sectionvi_question70desc() {
		return this.Scoreform_sectionvi_question70desc;
	}
	this.getScoreform_sectionvi_question70desc=getScoreform_sectionvi_question70desc;


	function setScoreform_sectionvi_question70desc(v){
		this.Scoreform_sectionvi_question70desc=v;
	}
	this.setScoreform_sectionvi_question70desc=setScoreform_sectionvi_question70desc;

	this.Scoreform_sectionvi_question71=null;


	function getScoreform_sectionvi_question71() {
		return this.Scoreform_sectionvi_question71;
	}
	this.getScoreform_sectionvi_question71=getScoreform_sectionvi_question71;


	function setScoreform_sectionvi_question71(v){
		this.Scoreform_sectionvi_question71=v;
	}
	this.setScoreform_sectionvi_question71=setScoreform_sectionvi_question71;

	this.Scoreform_sectionvi_question72=null;


	function getScoreform_sectionvi_question72() {
		return this.Scoreform_sectionvi_question72;
	}
	this.getScoreform_sectionvi_question72=getScoreform_sectionvi_question72;


	function setScoreform_sectionvi_question72(v){
		this.Scoreform_sectionvi_question72=v;
	}
	this.setScoreform_sectionvi_question72=setScoreform_sectionvi_question72;

	this.Scoreform_sectionvi_question73=null;


	function getScoreform_sectionvi_question73() {
		return this.Scoreform_sectionvi_question73;
	}
	this.getScoreform_sectionvi_question73=getScoreform_sectionvi_question73;


	function setScoreform_sectionvi_question73(v){
		this.Scoreform_sectionvi_question73=v;
	}
	this.setScoreform_sectionvi_question73=setScoreform_sectionvi_question73;

	this.Scoreform_sectionvi_question74=null;


	function getScoreform_sectionvi_question74() {
		return this.Scoreform_sectionvi_question74;
	}
	this.getScoreform_sectionvi_question74=getScoreform_sectionvi_question74;


	function setScoreform_sectionvi_question74(v){
		this.Scoreform_sectionvi_question74=v;
	}
	this.setScoreform_sectionvi_question74=setScoreform_sectionvi_question74;

	this.Scoreform_sectionvi_question75=null;


	function getScoreform_sectionvi_question75() {
		return this.Scoreform_sectionvi_question75;
	}
	this.getScoreform_sectionvi_question75=getScoreform_sectionvi_question75;


	function setScoreform_sectionvi_question75(v){
		this.Scoreform_sectionvi_question75=v;
	}
	this.setScoreform_sectionvi_question75=setScoreform_sectionvi_question75;

	this.Scoreform_sectionvi_question76=null;


	function getScoreform_sectionvi_question76() {
		return this.Scoreform_sectionvi_question76;
	}
	this.getScoreform_sectionvi_question76=getScoreform_sectionvi_question76;


	function setScoreform_sectionvi_question76(v){
		this.Scoreform_sectionvi_question76=v;
	}
	this.setScoreform_sectionvi_question76=setScoreform_sectionvi_question76;

	this.Scoreform_sectionvi_question77=null;


	function getScoreform_sectionvi_question77() {
		return this.Scoreform_sectionvi_question77;
	}
	this.getScoreform_sectionvi_question77=getScoreform_sectionvi_question77;


	function setScoreform_sectionvi_question77(v){
		this.Scoreform_sectionvi_question77=v;
	}
	this.setScoreform_sectionvi_question77=setScoreform_sectionvi_question77;

	this.Scoreform_sectionvi_question77desc=null;


	function getScoreform_sectionvi_question77desc() {
		return this.Scoreform_sectionvi_question77desc;
	}
	this.getScoreform_sectionvi_question77desc=getScoreform_sectionvi_question77desc;


	function setScoreform_sectionvi_question77desc(v){
		this.Scoreform_sectionvi_question77desc=v;
	}
	this.setScoreform_sectionvi_question77desc=setScoreform_sectionvi_question77desc;

	this.Scoreform_sectionvi_question78=null;


	function getScoreform_sectionvi_question78() {
		return this.Scoreform_sectionvi_question78;
	}
	this.getScoreform_sectionvi_question78=getScoreform_sectionvi_question78;


	function setScoreform_sectionvi_question78(v){
		this.Scoreform_sectionvi_question78=v;
	}
	this.setScoreform_sectionvi_question78=setScoreform_sectionvi_question78;

	this.Scoreform_sectionvi_question79=null;


	function getScoreform_sectionvi_question79() {
		return this.Scoreform_sectionvi_question79;
	}
	this.getScoreform_sectionvi_question79=getScoreform_sectionvi_question79;


	function setScoreform_sectionvi_question79(v){
		this.Scoreform_sectionvi_question79=v;
	}
	this.setScoreform_sectionvi_question79=setScoreform_sectionvi_question79;

	this.Scoreform_sectionvi_question79desc=null;


	function getScoreform_sectionvi_question79desc() {
		return this.Scoreform_sectionvi_question79desc;
	}
	this.getScoreform_sectionvi_question79desc=getScoreform_sectionvi_question79desc;


	function setScoreform_sectionvi_question79desc(v){
		this.Scoreform_sectionvi_question79desc=v;
	}
	this.setScoreform_sectionvi_question79desc=setScoreform_sectionvi_question79desc;

	this.Scoreform_sectionvi_question80=null;


	function getScoreform_sectionvi_question80() {
		return this.Scoreform_sectionvi_question80;
	}
	this.getScoreform_sectionvi_question80=getScoreform_sectionvi_question80;


	function setScoreform_sectionvi_question80(v){
		this.Scoreform_sectionvi_question80=v;
	}
	this.setScoreform_sectionvi_question80=setScoreform_sectionvi_question80;

	this.Scoreform_sectionvi_question81=null;


	function getScoreform_sectionvi_question81() {
		return this.Scoreform_sectionvi_question81;
	}
	this.getScoreform_sectionvi_question81=getScoreform_sectionvi_question81;


	function setScoreform_sectionvi_question81(v){
		this.Scoreform_sectionvi_question81=v;
	}
	this.setScoreform_sectionvi_question81=setScoreform_sectionvi_question81;

	this.Scoreform_sectionvi_question82=null;


	function getScoreform_sectionvi_question82() {
		return this.Scoreform_sectionvi_question82;
	}
	this.getScoreform_sectionvi_question82=getScoreform_sectionvi_question82;


	function setScoreform_sectionvi_question82(v){
		this.Scoreform_sectionvi_question82=v;
	}
	this.setScoreform_sectionvi_question82=setScoreform_sectionvi_question82;

	this.Scoreform_sectionvi_question83=null;


	function getScoreform_sectionvi_question83() {
		return this.Scoreform_sectionvi_question83;
	}
	this.getScoreform_sectionvi_question83=getScoreform_sectionvi_question83;


	function setScoreform_sectionvi_question83(v){
		this.Scoreform_sectionvi_question83=v;
	}
	this.setScoreform_sectionvi_question83=setScoreform_sectionvi_question83;

	this.Scoreform_sectionvi_question84=null;


	function getScoreform_sectionvi_question84() {
		return this.Scoreform_sectionvi_question84;
	}
	this.getScoreform_sectionvi_question84=getScoreform_sectionvi_question84;


	function setScoreform_sectionvi_question84(v){
		this.Scoreform_sectionvi_question84=v;
	}
	this.setScoreform_sectionvi_question84=setScoreform_sectionvi_question84;

	this.Scoreform_sectionvi_question84desc=null;


	function getScoreform_sectionvi_question84desc() {
		return this.Scoreform_sectionvi_question84desc;
	}
	this.getScoreform_sectionvi_question84desc=getScoreform_sectionvi_question84desc;


	function setScoreform_sectionvi_question84desc(v){
		this.Scoreform_sectionvi_question84desc=v;
	}
	this.setScoreform_sectionvi_question84desc=setScoreform_sectionvi_question84desc;

	this.Scoreform_sectionvi_question85=null;


	function getScoreform_sectionvi_question85() {
		return this.Scoreform_sectionvi_question85;
	}
	this.getScoreform_sectionvi_question85=getScoreform_sectionvi_question85;


	function setScoreform_sectionvi_question85(v){
		this.Scoreform_sectionvi_question85=v;
	}
	this.setScoreform_sectionvi_question85=setScoreform_sectionvi_question85;

	this.Scoreform_sectionvi_question85desc=null;


	function getScoreform_sectionvi_question85desc() {
		return this.Scoreform_sectionvi_question85desc;
	}
	this.getScoreform_sectionvi_question85desc=getScoreform_sectionvi_question85desc;


	function setScoreform_sectionvi_question85desc(v){
		this.Scoreform_sectionvi_question85desc=v;
	}
	this.setScoreform_sectionvi_question85desc=setScoreform_sectionvi_question85desc;

	this.Scoreform_sectionvi_question86=null;


	function getScoreform_sectionvi_question86() {
		return this.Scoreform_sectionvi_question86;
	}
	this.getScoreform_sectionvi_question86=getScoreform_sectionvi_question86;


	function setScoreform_sectionvi_question86(v){
		this.Scoreform_sectionvi_question86=v;
	}
	this.setScoreform_sectionvi_question86=setScoreform_sectionvi_question86;

	this.Scoreform_sectionvi_question87=null;


	function getScoreform_sectionvi_question87() {
		return this.Scoreform_sectionvi_question87;
	}
	this.getScoreform_sectionvi_question87=getScoreform_sectionvi_question87;


	function setScoreform_sectionvi_question87(v){
		this.Scoreform_sectionvi_question87=v;
	}
	this.setScoreform_sectionvi_question87=setScoreform_sectionvi_question87;

	this.Scoreform_sectionvi_question88=null;


	function getScoreform_sectionvi_question88() {
		return this.Scoreform_sectionvi_question88;
	}
	this.getScoreform_sectionvi_question88=getScoreform_sectionvi_question88;


	function setScoreform_sectionvi_question88(v){
		this.Scoreform_sectionvi_question88=v;
	}
	this.setScoreform_sectionvi_question88=setScoreform_sectionvi_question88;

	this.Scoreform_sectionvi_question89=null;


	function getScoreform_sectionvi_question89() {
		return this.Scoreform_sectionvi_question89;
	}
	this.getScoreform_sectionvi_question89=getScoreform_sectionvi_question89;


	function setScoreform_sectionvi_question89(v){
		this.Scoreform_sectionvi_question89=v;
	}
	this.setScoreform_sectionvi_question89=setScoreform_sectionvi_question89;

	this.Scoreform_sectionvi_question90=null;


	function getScoreform_sectionvi_question90() {
		return this.Scoreform_sectionvi_question90;
	}
	this.getScoreform_sectionvi_question90=getScoreform_sectionvi_question90;


	function setScoreform_sectionvi_question90(v){
		this.Scoreform_sectionvi_question90=v;
	}
	this.setScoreform_sectionvi_question90=setScoreform_sectionvi_question90;

	this.Scoreform_sectionvi_question91=null;


	function getScoreform_sectionvi_question91() {
		return this.Scoreform_sectionvi_question91;
	}
	this.getScoreform_sectionvi_question91=getScoreform_sectionvi_question91;


	function setScoreform_sectionvi_question91(v){
		this.Scoreform_sectionvi_question91=v;
	}
	this.setScoreform_sectionvi_question91=setScoreform_sectionvi_question91;

	this.Scoreform_sectionvi_question92=null;


	function getScoreform_sectionvi_question92() {
		return this.Scoreform_sectionvi_question92;
	}
	this.getScoreform_sectionvi_question92=getScoreform_sectionvi_question92;


	function setScoreform_sectionvi_question92(v){
		this.Scoreform_sectionvi_question92=v;
	}
	this.setScoreform_sectionvi_question92=setScoreform_sectionvi_question92;

	this.Scoreform_sectionvi_question92desc=null;


	function getScoreform_sectionvi_question92desc() {
		return this.Scoreform_sectionvi_question92desc;
	}
	this.getScoreform_sectionvi_question92desc=getScoreform_sectionvi_question92desc;


	function setScoreform_sectionvi_question92desc(v){
		this.Scoreform_sectionvi_question92desc=v;
	}
	this.setScoreform_sectionvi_question92desc=setScoreform_sectionvi_question92desc;

	this.Scoreform_sectionvi_question93=null;


	function getScoreform_sectionvi_question93() {
		return this.Scoreform_sectionvi_question93;
	}
	this.getScoreform_sectionvi_question93=getScoreform_sectionvi_question93;


	function setScoreform_sectionvi_question93(v){
		this.Scoreform_sectionvi_question93=v;
	}
	this.setScoreform_sectionvi_question93=setScoreform_sectionvi_question93;

	this.Scoreform_sectionvi_question94=null;


	function getScoreform_sectionvi_question94() {
		return this.Scoreform_sectionvi_question94;
	}
	this.getScoreform_sectionvi_question94=getScoreform_sectionvi_question94;


	function setScoreform_sectionvi_question94(v){
		this.Scoreform_sectionvi_question94=v;
	}
	this.setScoreform_sectionvi_question94=setScoreform_sectionvi_question94;

	this.Scoreform_sectionvi_question95=null;


	function getScoreform_sectionvi_question95() {
		return this.Scoreform_sectionvi_question95;
	}
	this.getScoreform_sectionvi_question95=getScoreform_sectionvi_question95;


	function setScoreform_sectionvi_question95(v){
		this.Scoreform_sectionvi_question95=v;
	}
	this.setScoreform_sectionvi_question95=setScoreform_sectionvi_question95;

	this.Scoreform_sectionvi_question96=null;


	function getScoreform_sectionvi_question96() {
		return this.Scoreform_sectionvi_question96;
	}
	this.getScoreform_sectionvi_question96=getScoreform_sectionvi_question96;


	function setScoreform_sectionvi_question96(v){
		this.Scoreform_sectionvi_question96=v;
	}
	this.setScoreform_sectionvi_question96=setScoreform_sectionvi_question96;

	this.Scoreform_sectionvi_question97=null;


	function getScoreform_sectionvi_question97() {
		return this.Scoreform_sectionvi_question97;
	}
	this.getScoreform_sectionvi_question97=getScoreform_sectionvi_question97;


	function setScoreform_sectionvi_question97(v){
		this.Scoreform_sectionvi_question97=v;
	}
	this.setScoreform_sectionvi_question97=setScoreform_sectionvi_question97;

	this.Scoreform_sectionvi_question98=null;


	function getScoreform_sectionvi_question98() {
		return this.Scoreform_sectionvi_question98;
	}
	this.getScoreform_sectionvi_question98=getScoreform_sectionvi_question98;


	function setScoreform_sectionvi_question98(v){
		this.Scoreform_sectionvi_question98=v;
	}
	this.setScoreform_sectionvi_question98=setScoreform_sectionvi_question98;

	this.Scoreform_sectionvi_question99=null;


	function getScoreform_sectionvi_question99() {
		return this.Scoreform_sectionvi_question99;
	}
	this.getScoreform_sectionvi_question99=getScoreform_sectionvi_question99;


	function setScoreform_sectionvi_question99(v){
		this.Scoreform_sectionvi_question99=v;
	}
	this.setScoreform_sectionvi_question99=setScoreform_sectionvi_question99;

	this.Scoreform_sectionvi_question100=null;


	function getScoreform_sectionvi_question100() {
		return this.Scoreform_sectionvi_question100;
	}
	this.getScoreform_sectionvi_question100=getScoreform_sectionvi_question100;


	function setScoreform_sectionvi_question100(v){
		this.Scoreform_sectionvi_question100=v;
	}
	this.setScoreform_sectionvi_question100=setScoreform_sectionvi_question100;

	this.Scoreform_sectionvi_question101=null;


	function getScoreform_sectionvi_question101() {
		return this.Scoreform_sectionvi_question101;
	}
	this.getScoreform_sectionvi_question101=getScoreform_sectionvi_question101;


	function setScoreform_sectionvi_question101(v){
		this.Scoreform_sectionvi_question101=v;
	}
	this.setScoreform_sectionvi_question101=setScoreform_sectionvi_question101;

	this.Scoreform_sectionvi_question102=null;


	function getScoreform_sectionvi_question102() {
		return this.Scoreform_sectionvi_question102;
	}
	this.getScoreform_sectionvi_question102=getScoreform_sectionvi_question102;


	function setScoreform_sectionvi_question102(v){
		this.Scoreform_sectionvi_question102=v;
	}
	this.setScoreform_sectionvi_question102=setScoreform_sectionvi_question102;

	this.Scoreform_sectionvi_question103=null;


	function getScoreform_sectionvi_question103() {
		return this.Scoreform_sectionvi_question103;
	}
	this.getScoreform_sectionvi_question103=getScoreform_sectionvi_question103;


	function setScoreform_sectionvi_question103(v){
		this.Scoreform_sectionvi_question103=v;
	}
	this.setScoreform_sectionvi_question103=setScoreform_sectionvi_question103;

	this.Scoreform_sectionvi_question104=null;


	function getScoreform_sectionvi_question104() {
		return this.Scoreform_sectionvi_question104;
	}
	this.getScoreform_sectionvi_question104=getScoreform_sectionvi_question104;


	function setScoreform_sectionvi_question104(v){
		this.Scoreform_sectionvi_question104=v;
	}
	this.setScoreform_sectionvi_question104=setScoreform_sectionvi_question104;

	this.Scoreform_sectionvi_question105=null;


	function getScoreform_sectionvi_question105() {
		return this.Scoreform_sectionvi_question105;
	}
	this.getScoreform_sectionvi_question105=getScoreform_sectionvi_question105;


	function setScoreform_sectionvi_question105(v){
		this.Scoreform_sectionvi_question105=v;
	}
	this.setScoreform_sectionvi_question105=setScoreform_sectionvi_question105;

	this.Scoreform_sectionvi_question106=null;


	function getScoreform_sectionvi_question106() {
		return this.Scoreform_sectionvi_question106;
	}
	this.getScoreform_sectionvi_question106=getScoreform_sectionvi_question106;


	function setScoreform_sectionvi_question106(v){
		this.Scoreform_sectionvi_question106=v;
	}
	this.setScoreform_sectionvi_question106=setScoreform_sectionvi_question106;

	this.Scoreform_sectionvi_question107=null;


	function getScoreform_sectionvi_question107() {
		return this.Scoreform_sectionvi_question107;
	}
	this.getScoreform_sectionvi_question107=getScoreform_sectionvi_question107;


	function setScoreform_sectionvi_question107(v){
		this.Scoreform_sectionvi_question107=v;
	}
	this.setScoreform_sectionvi_question107=setScoreform_sectionvi_question107;

	this.Scoreform_sectionvi_question108=null;


	function getScoreform_sectionvi_question108() {
		return this.Scoreform_sectionvi_question108;
	}
	this.getScoreform_sectionvi_question108=getScoreform_sectionvi_question108;


	function setScoreform_sectionvi_question108(v){
		this.Scoreform_sectionvi_question108=v;
	}
	this.setScoreform_sectionvi_question108=setScoreform_sectionvi_question108;

	this.Scoreform_sectionvi_question109=null;


	function getScoreform_sectionvi_question109() {
		return this.Scoreform_sectionvi_question109;
	}
	this.getScoreform_sectionvi_question109=getScoreform_sectionvi_question109;


	function setScoreform_sectionvi_question109(v){
		this.Scoreform_sectionvi_question109=v;
	}
	this.setScoreform_sectionvi_question109=setScoreform_sectionvi_question109;

	this.Scoreform_sectionvi_question110=null;


	function getScoreform_sectionvi_question110() {
		return this.Scoreform_sectionvi_question110;
	}
	this.getScoreform_sectionvi_question110=getScoreform_sectionvi_question110;


	function setScoreform_sectionvi_question110(v){
		this.Scoreform_sectionvi_question110=v;
	}
	this.setScoreform_sectionvi_question110=setScoreform_sectionvi_question110;

	this.Scoreform_sectionvi_question111=null;


	function getScoreform_sectionvi_question111() {
		return this.Scoreform_sectionvi_question111;
	}
	this.getScoreform_sectionvi_question111=getScoreform_sectionvi_question111;


	function setScoreform_sectionvi_question111(v){
		this.Scoreform_sectionvi_question111=v;
	}
	this.setScoreform_sectionvi_question111=setScoreform_sectionvi_question111;

	this.Scoreform_sectionvi_question112=null;


	function getScoreform_sectionvi_question112() {
		return this.Scoreform_sectionvi_question112;
	}
	this.getScoreform_sectionvi_question112=getScoreform_sectionvi_question112;


	function setScoreform_sectionvi_question112(v){
		this.Scoreform_sectionvi_question112=v;
	}
	this.setScoreform_sectionvi_question112=setScoreform_sectionvi_question112;

	this.Scoreform_sectionvi_question113=null;


	function getScoreform_sectionvi_question113() {
		return this.Scoreform_sectionvi_question113;
	}
	this.getScoreform_sectionvi_question113=getScoreform_sectionvi_question113;


	function setScoreform_sectionvi_question113(v){
		this.Scoreform_sectionvi_question113=v;
	}
	this.setScoreform_sectionvi_question113=setScoreform_sectionvi_question113;

	this.Scoreform_sectionvi_question114=null;


	function getScoreform_sectionvi_question114() {
		return this.Scoreform_sectionvi_question114;
	}
	this.getScoreform_sectionvi_question114=getScoreform_sectionvi_question114;


	function setScoreform_sectionvi_question114(v){
		this.Scoreform_sectionvi_question114=v;
	}
	this.setScoreform_sectionvi_question114=setScoreform_sectionvi_question114;

	this.Scoreform_sectionvi_question115=null;


	function getScoreform_sectionvi_question115() {
		return this.Scoreform_sectionvi_question115;
	}
	this.getScoreform_sectionvi_question115=getScoreform_sectionvi_question115;


	function setScoreform_sectionvi_question115(v){
		this.Scoreform_sectionvi_question115=v;
	}
	this.setScoreform_sectionvi_question115=setScoreform_sectionvi_question115;

	this.Scoreform_sectionvi_question116=null;


	function getScoreform_sectionvi_question116() {
		return this.Scoreform_sectionvi_question116;
	}
	this.getScoreform_sectionvi_question116=getScoreform_sectionvi_question116;


	function setScoreform_sectionvi_question116(v){
		this.Scoreform_sectionvi_question116=v;
	}
	this.setScoreform_sectionvi_question116=setScoreform_sectionvi_question116;

	this.Scoreform_sectionvi_question117=null;


	function getScoreform_sectionvi_question117() {
		return this.Scoreform_sectionvi_question117;
	}
	this.getScoreform_sectionvi_question117=getScoreform_sectionvi_question117;


	function setScoreform_sectionvi_question117(v){
		this.Scoreform_sectionvi_question117=v;
	}
	this.setScoreform_sectionvi_question117=setScoreform_sectionvi_question117;

	this.Scoreform_sectionvi_question118=null;


	function getScoreform_sectionvi_question118() {
		return this.Scoreform_sectionvi_question118;
	}
	this.getScoreform_sectionvi_question118=getScoreform_sectionvi_question118;


	function setScoreform_sectionvi_question118(v){
		this.Scoreform_sectionvi_question118=v;
	}
	this.setScoreform_sectionvi_question118=setScoreform_sectionvi_question118;

	this.Scoreform_sectionvi_question119=null;


	function getScoreform_sectionvi_question119() {
		return this.Scoreform_sectionvi_question119;
	}
	this.getScoreform_sectionvi_question119=getScoreform_sectionvi_question119;


	function setScoreform_sectionvi_question119(v){
		this.Scoreform_sectionvi_question119=v;
	}
	this.setScoreform_sectionvi_question119=setScoreform_sectionvi_question119;

	this.Scoreform_sectionvi_question120=null;


	function getScoreform_sectionvi_question120() {
		return this.Scoreform_sectionvi_question120;
	}
	this.getScoreform_sectionvi_question120=getScoreform_sectionvi_question120;


	function setScoreform_sectionvi_question120(v){
		this.Scoreform_sectionvi_question120=v;
	}
	this.setScoreform_sectionvi_question120=setScoreform_sectionvi_question120;

	this.Scoreform_sectionvi_question121=null;


	function getScoreform_sectionvi_question121() {
		return this.Scoreform_sectionvi_question121;
	}
	this.getScoreform_sectionvi_question121=getScoreform_sectionvi_question121;


	function setScoreform_sectionvi_question121(v){
		this.Scoreform_sectionvi_question121=v;
	}
	this.setScoreform_sectionvi_question121=setScoreform_sectionvi_question121;

	this.Scoreform_sectionvi_question122=null;


	function getScoreform_sectionvi_question122() {
		return this.Scoreform_sectionvi_question122;
	}
	this.getScoreform_sectionvi_question122=getScoreform_sectionvi_question122;


	function setScoreform_sectionvi_question122(v){
		this.Scoreform_sectionvi_question122=v;
	}
	this.setScoreform_sectionvi_question122=setScoreform_sectionvi_question122;

	this.Scoreform_sectionvi_question123=null;


	function getScoreform_sectionvi_question123() {
		return this.Scoreform_sectionvi_question123;
	}
	this.getScoreform_sectionvi_question123=getScoreform_sectionvi_question123;


	function setScoreform_sectionvi_question123(v){
		this.Scoreform_sectionvi_question123=v;
	}
	this.setScoreform_sectionvi_question123=setScoreform_sectionvi_question123;

	this.Scoreform_sectionvi_question124=null;


	function getScoreform_sectionvi_question124() {
		return this.Scoreform_sectionvi_question124;
	}
	this.getScoreform_sectionvi_question124=getScoreform_sectionvi_question124;


	function setScoreform_sectionvi_question124(v){
		this.Scoreform_sectionvi_question124=v;
	}
	this.setScoreform_sectionvi_question124=setScoreform_sectionvi_question124;

	this.Scoreform_sectionvi_question125=null;


	function getScoreform_sectionvi_question125() {
		return this.Scoreform_sectionvi_question125;
	}
	this.getScoreform_sectionvi_question125=getScoreform_sectionvi_question125;


	function setScoreform_sectionvi_question125(v){
		this.Scoreform_sectionvi_question125=v;
	}
	this.setScoreform_sectionvi_question125=setScoreform_sectionvi_question125;

	this.Scoreform_sectionvi_question126=null;


	function getScoreform_sectionvi_question126() {
		return this.Scoreform_sectionvi_question126;
	}
	this.getScoreform_sectionvi_question126=getScoreform_sectionvi_question126;


	function setScoreform_sectionvi_question126(v){
		this.Scoreform_sectionvi_question126=v;
	}
	this.setScoreform_sectionvi_question126=setScoreform_sectionvi_question126;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="anxdp_raw"){
				return this.AnxdpRaw ;
			} else 
			if(xmlPath=="anxdp_t"){
				return this.AnxdpT ;
			} else 
			if(xmlPath=="anxdp_note"){
				return this.AnxdpNote ;
			} else 
			if(xmlPath=="wthdp_raw"){
				return this.WthdpRaw ;
			} else 
			if(xmlPath=="wthdp_t"){
				return this.WthdpT ;
			} else 
			if(xmlPath=="wthdp_note"){
				return this.WthdpNote ;
			} else 
			if(xmlPath=="som_raw"){
				return this.SomRaw ;
			} else 
			if(xmlPath=="som_t"){
				return this.SomT ;
			} else 
			if(xmlPath=="som_note"){
				return this.SomNote ;
			} else 
			if(xmlPath=="tho_raw"){
				return this.ThoRaw ;
			} else 
			if(xmlPath=="tho_t"){
				return this.ThoT ;
			} else 
			if(xmlPath=="tho_note"){
				return this.ThoNote ;
			} else 
			if(xmlPath=="att_raw"){
				return this.AttRaw ;
			} else 
			if(xmlPath=="att_t"){
				return this.AttT ;
			} else 
			if(xmlPath=="att_note"){
				return this.AttNote ;
			} else 
			if(xmlPath=="rule_raw"){
				return this.RuleRaw ;
			} else 
			if(xmlPath=="rule_t"){
				return this.RuleT ;
			} else 
			if(xmlPath=="rule_note"){
				return this.RuleNote ;
			} else 
			if(xmlPath=="agg_raw"){
				return this.AggRaw ;
			} else 
			if(xmlPath=="agg_t"){
				return this.AggT ;
			} else 
			if(xmlPath=="agg_note"){
				return this.AggNote ;
			} else 
			if(xmlPath=="int_raw"){
				return this.IntRaw ;
			} else 
			if(xmlPath=="int_t"){
				return this.IntT ;
			} else 
			if(xmlPath=="int_note"){
				return this.IntNote ;
			} else 
			if(xmlPath=="ScoreForm/respRltnSubj"){
				return this.Scoreform_resprltnsubj ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/friendsA"){
				return this.Scoreform_sectioni_friendsa ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/friendsB"){
				return this.Scoreform_sectioni_friendsb ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/friendsC"){
				return this.Scoreform_sectioni_friendsc ;
			} else 
			if(xmlPath=="ScoreForm/sectionI/friendsD"){
				return this.Scoreform_sectioni_friendsd ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalStatus"){
				return this.Scoreform_sectionii_maritalstatus ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalStatusOther"){
				return this.Scoreform_sectionii_maritalstatusother ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalLast6Mnths"){
				return this.Scoreform_sectionii_maritallast6mnths ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalA"){
				return this.Scoreform_sectionii_maritala ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalB"){
				return this.Scoreform_sectionii_maritalb ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalC"){
				return this.Scoreform_sectionii_maritalc ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalD"){
				return this.Scoreform_sectionii_maritald ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalE"){
				return this.Scoreform_sectionii_maritale ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalF"){
				return this.Scoreform_sectionii_maritalf ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalG"){
				return this.Scoreform_sectionii_maritalg ;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalH"){
				return this.Scoreform_sectionii_maritalh ;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/handicap"){
				return this.Scoreform_sectioniii_handicap ;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/handicapExplain"){
				return this.Scoreform_sectioniii_handicapexplain ;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/concerns"){
				return this.Scoreform_sectioniv_concerns ;
			} else 
			if(xmlPath=="ScoreForm/sectionV/describe"){
				return this.Scoreform_sectionv_describe ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question1"){
				return this.Scoreform_sectionvi_question1 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question2"){
				return this.Scoreform_sectionvi_question2 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question3"){
				return this.Scoreform_sectionvi_question3 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question4"){
				return this.Scoreform_sectionvi_question4 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question5"){
				return this.Scoreform_sectionvi_question5 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question6"){
				return this.Scoreform_sectionvi_question6 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question6Desc"){
				return this.Scoreform_sectionvi_question6desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question7"){
				return this.Scoreform_sectionvi_question7 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question8"){
				return this.Scoreform_sectionvi_question8 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question9"){
				return this.Scoreform_sectionvi_question9 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question9Desc"){
				return this.Scoreform_sectionvi_question9desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question10"){
				return this.Scoreform_sectionvi_question10 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question11"){
				return this.Scoreform_sectionvi_question11 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question12"){
				return this.Scoreform_sectionvi_question12 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question13"){
				return this.Scoreform_sectionvi_question13 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question14"){
				return this.Scoreform_sectionvi_question14 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question15"){
				return this.Scoreform_sectionvi_question15 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question16"){
				return this.Scoreform_sectionvi_question16 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question17"){
				return this.Scoreform_sectionvi_question17 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question18"){
				return this.Scoreform_sectionvi_question18 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question19"){
				return this.Scoreform_sectionvi_question19 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question20"){
				return this.Scoreform_sectionvi_question20 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question21"){
				return this.Scoreform_sectionvi_question21 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question22"){
				return this.Scoreform_sectionvi_question22 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question23"){
				return this.Scoreform_sectionvi_question23 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question24"){
				return this.Scoreform_sectionvi_question24 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question25"){
				return this.Scoreform_sectionvi_question25 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question26"){
				return this.Scoreform_sectionvi_question26 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question27"){
				return this.Scoreform_sectionvi_question27 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question28"){
				return this.Scoreform_sectionvi_question28 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question29"){
				return this.Scoreform_sectionvi_question29 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question29Desc"){
				return this.Scoreform_sectionvi_question29desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question30"){
				return this.Scoreform_sectionvi_question30 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question31"){
				return this.Scoreform_sectionvi_question31 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question32"){
				return this.Scoreform_sectionvi_question32 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question33"){
				return this.Scoreform_sectionvi_question33 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question34"){
				return this.Scoreform_sectionvi_question34 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question35"){
				return this.Scoreform_sectionvi_question35 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question36"){
				return this.Scoreform_sectionvi_question36 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question37"){
				return this.Scoreform_sectionvi_question37 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question38"){
				return this.Scoreform_sectionvi_question38 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question39"){
				return this.Scoreform_sectionvi_question39 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question40"){
				return this.Scoreform_sectionvi_question40 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question40Desc"){
				return this.Scoreform_sectionvi_question40desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question41"){
				return this.Scoreform_sectionvi_question41 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question42"){
				return this.Scoreform_sectionvi_question42 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question43"){
				return this.Scoreform_sectionvi_question43 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question44"){
				return this.Scoreform_sectionvi_question44 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question45"){
				return this.Scoreform_sectionvi_question45 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question46"){
				return this.Scoreform_sectionvi_question46 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question46Desc"){
				return this.Scoreform_sectionvi_question46desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question47"){
				return this.Scoreform_sectionvi_question47 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question48"){
				return this.Scoreform_sectionvi_question48 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question49"){
				return this.Scoreform_sectionvi_question49 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question50"){
				return this.Scoreform_sectionvi_question50 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question51"){
				return this.Scoreform_sectionvi_question51 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question52"){
				return this.Scoreform_sectionvi_question52 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question53"){
				return this.Scoreform_sectionvi_question53 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question54"){
				return this.Scoreform_sectionvi_question54 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question55"){
				return this.Scoreform_sectionvi_question55 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56a"){
				return this.Scoreform_sectionvi_question56a ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56b"){
				return this.Scoreform_sectionvi_question56b ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56c"){
				return this.Scoreform_sectionvi_question56c ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56d"){
				return this.Scoreform_sectionvi_question56d ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56dDesc"){
				return this.Scoreform_sectionvi_question56ddesc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56e"){
				return this.Scoreform_sectionvi_question56e ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56f"){
				return this.Scoreform_sectionvi_question56f ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56g"){
				return this.Scoreform_sectionvi_question56g ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question57"){
				return this.Scoreform_sectionvi_question57 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question58"){
				return this.Scoreform_sectionvi_question58 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question58Desc"){
				return this.Scoreform_sectionvi_question58desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question59"){
				return this.Scoreform_sectionvi_question59 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question60"){
				return this.Scoreform_sectionvi_question60 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question61"){
				return this.Scoreform_sectionvi_question61 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question62"){
				return this.Scoreform_sectionvi_question62 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question63"){
				return this.Scoreform_sectionvi_question63 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question64"){
				return this.Scoreform_sectionvi_question64 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question65"){
				return this.Scoreform_sectionvi_question65 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question66"){
				return this.Scoreform_sectionvi_question66 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question66Desc"){
				return this.Scoreform_sectionvi_question66desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question67"){
				return this.Scoreform_sectionvi_question67 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question68"){
				return this.Scoreform_sectionvi_question68 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question69"){
				return this.Scoreform_sectionvi_question69 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question70"){
				return this.Scoreform_sectionvi_question70 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question70Desc"){
				return this.Scoreform_sectionvi_question70desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question71"){
				return this.Scoreform_sectionvi_question71 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question72"){
				return this.Scoreform_sectionvi_question72 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question73"){
				return this.Scoreform_sectionvi_question73 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question74"){
				return this.Scoreform_sectionvi_question74 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question75"){
				return this.Scoreform_sectionvi_question75 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question76"){
				return this.Scoreform_sectionvi_question76 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question77"){
				return this.Scoreform_sectionvi_question77 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question77Desc"){
				return this.Scoreform_sectionvi_question77desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question78"){
				return this.Scoreform_sectionvi_question78 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question79"){
				return this.Scoreform_sectionvi_question79 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question79Desc"){
				return this.Scoreform_sectionvi_question79desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question80"){
				return this.Scoreform_sectionvi_question80 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question81"){
				return this.Scoreform_sectionvi_question81 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question82"){
				return this.Scoreform_sectionvi_question82 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question83"){
				return this.Scoreform_sectionvi_question83 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question84"){
				return this.Scoreform_sectionvi_question84 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question84Desc"){
				return this.Scoreform_sectionvi_question84desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question85"){
				return this.Scoreform_sectionvi_question85 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question85Desc"){
				return this.Scoreform_sectionvi_question85desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question86"){
				return this.Scoreform_sectionvi_question86 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question87"){
				return this.Scoreform_sectionvi_question87 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question88"){
				return this.Scoreform_sectionvi_question88 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question89"){
				return this.Scoreform_sectionvi_question89 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question90"){
				return this.Scoreform_sectionvi_question90 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question91"){
				return this.Scoreform_sectionvi_question91 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question92"){
				return this.Scoreform_sectionvi_question92 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question92Desc"){
				return this.Scoreform_sectionvi_question92desc ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question93"){
				return this.Scoreform_sectionvi_question93 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question94"){
				return this.Scoreform_sectionvi_question94 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question95"){
				return this.Scoreform_sectionvi_question95 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question96"){
				return this.Scoreform_sectionvi_question96 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question97"){
				return this.Scoreform_sectionvi_question97 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question98"){
				return this.Scoreform_sectionvi_question98 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question99"){
				return this.Scoreform_sectionvi_question99 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question100"){
				return this.Scoreform_sectionvi_question100 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question101"){
				return this.Scoreform_sectionvi_question101 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question102"){
				return this.Scoreform_sectionvi_question102 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question103"){
				return this.Scoreform_sectionvi_question103 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question104"){
				return this.Scoreform_sectionvi_question104 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question105"){
				return this.Scoreform_sectionvi_question105 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question106"){
				return this.Scoreform_sectionvi_question106 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question107"){
				return this.Scoreform_sectionvi_question107 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question108"){
				return this.Scoreform_sectionvi_question108 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question109"){
				return this.Scoreform_sectionvi_question109 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question110"){
				return this.Scoreform_sectionvi_question110 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question111"){
				return this.Scoreform_sectionvi_question111 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question112"){
				return this.Scoreform_sectionvi_question112 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question113"){
				return this.Scoreform_sectionvi_question113 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question114"){
				return this.Scoreform_sectionvi_question114 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question115"){
				return this.Scoreform_sectionvi_question115 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question116"){
				return this.Scoreform_sectionvi_question116 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question117"){
				return this.Scoreform_sectionvi_question117 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question118"){
				return this.Scoreform_sectionvi_question118 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question119"){
				return this.Scoreform_sectionvi_question119 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question120"){
				return this.Scoreform_sectionvi_question120 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question121"){
				return this.Scoreform_sectionvi_question121 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question122"){
				return this.Scoreform_sectionvi_question122 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question123"){
				return this.Scoreform_sectionvi_question123 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question124"){
				return this.Scoreform_sectionvi_question124 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question125"){
				return this.Scoreform_sectionvi_question125 ;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question126"){
				return this.Scoreform_sectionvi_question126 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="anxdp_raw"){
				this.AnxdpRaw=value;
			} else 
			if(xmlPath=="anxdp_t"){
				this.AnxdpT=value;
			} else 
			if(xmlPath=="anxdp_note"){
				this.AnxdpNote=value;
			} else 
			if(xmlPath=="wthdp_raw"){
				this.WthdpRaw=value;
			} else 
			if(xmlPath=="wthdp_t"){
				this.WthdpT=value;
			} else 
			if(xmlPath=="wthdp_note"){
				this.WthdpNote=value;
			} else 
			if(xmlPath=="som_raw"){
				this.SomRaw=value;
			} else 
			if(xmlPath=="som_t"){
				this.SomT=value;
			} else 
			if(xmlPath=="som_note"){
				this.SomNote=value;
			} else 
			if(xmlPath=="tho_raw"){
				this.ThoRaw=value;
			} else 
			if(xmlPath=="tho_t"){
				this.ThoT=value;
			} else 
			if(xmlPath=="tho_note"){
				this.ThoNote=value;
			} else 
			if(xmlPath=="att_raw"){
				this.AttRaw=value;
			} else 
			if(xmlPath=="att_t"){
				this.AttT=value;
			} else 
			if(xmlPath=="att_note"){
				this.AttNote=value;
			} else 
			if(xmlPath=="rule_raw"){
				this.RuleRaw=value;
			} else 
			if(xmlPath=="rule_t"){
				this.RuleT=value;
			} else 
			if(xmlPath=="rule_note"){
				this.RuleNote=value;
			} else 
			if(xmlPath=="agg_raw"){
				this.AggRaw=value;
			} else 
			if(xmlPath=="agg_t"){
				this.AggT=value;
			} else 
			if(xmlPath=="agg_note"){
				this.AggNote=value;
			} else 
			if(xmlPath=="int_raw"){
				this.IntRaw=value;
			} else 
			if(xmlPath=="int_t"){
				this.IntT=value;
			} else 
			if(xmlPath=="int_note"){
				this.IntNote=value;
			} else 
			if(xmlPath=="ScoreForm/respRltnSubj"){
				this.Scoreform_resprltnsubj=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/friendsA"){
				this.Scoreform_sectioni_friendsa=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/friendsB"){
				this.Scoreform_sectioni_friendsb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/friendsC"){
				this.Scoreform_sectioni_friendsc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionI/friendsD"){
				this.Scoreform_sectioni_friendsd=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalStatus"){
				this.Scoreform_sectionii_maritalstatus=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalStatusOther"){
				this.Scoreform_sectionii_maritalstatusother=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalLast6Mnths"){
				this.Scoreform_sectionii_maritallast6mnths=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalA"){
				this.Scoreform_sectionii_maritala=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalB"){
				this.Scoreform_sectionii_maritalb=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalC"){
				this.Scoreform_sectionii_maritalc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalD"){
				this.Scoreform_sectionii_maritald=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalE"){
				this.Scoreform_sectionii_maritale=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalF"){
				this.Scoreform_sectionii_maritalf=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalG"){
				this.Scoreform_sectionii_maritalg=value;
			} else 
			if(xmlPath=="ScoreForm/sectionII/maritalH"){
				this.Scoreform_sectionii_maritalh=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/handicap"){
				this.Scoreform_sectioniii_handicap=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIII/handicapExplain"){
				this.Scoreform_sectioniii_handicapexplain=value;
			} else 
			if(xmlPath=="ScoreForm/sectionIV/concerns"){
				this.Scoreform_sectioniv_concerns=value;
			} else 
			if(xmlPath=="ScoreForm/sectionV/describe"){
				this.Scoreform_sectionv_describe=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question1"){
				this.Scoreform_sectionvi_question1=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question2"){
				this.Scoreform_sectionvi_question2=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question3"){
				this.Scoreform_sectionvi_question3=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question4"){
				this.Scoreform_sectionvi_question4=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question5"){
				this.Scoreform_sectionvi_question5=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question6"){
				this.Scoreform_sectionvi_question6=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question6Desc"){
				this.Scoreform_sectionvi_question6desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question7"){
				this.Scoreform_sectionvi_question7=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question8"){
				this.Scoreform_sectionvi_question8=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question9"){
				this.Scoreform_sectionvi_question9=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question9Desc"){
				this.Scoreform_sectionvi_question9desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question10"){
				this.Scoreform_sectionvi_question10=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question11"){
				this.Scoreform_sectionvi_question11=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question12"){
				this.Scoreform_sectionvi_question12=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question13"){
				this.Scoreform_sectionvi_question13=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question14"){
				this.Scoreform_sectionvi_question14=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question15"){
				this.Scoreform_sectionvi_question15=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question16"){
				this.Scoreform_sectionvi_question16=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question17"){
				this.Scoreform_sectionvi_question17=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question18"){
				this.Scoreform_sectionvi_question18=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question19"){
				this.Scoreform_sectionvi_question19=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question20"){
				this.Scoreform_sectionvi_question20=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question21"){
				this.Scoreform_sectionvi_question21=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question22"){
				this.Scoreform_sectionvi_question22=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question23"){
				this.Scoreform_sectionvi_question23=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question24"){
				this.Scoreform_sectionvi_question24=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question25"){
				this.Scoreform_sectionvi_question25=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question26"){
				this.Scoreform_sectionvi_question26=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question27"){
				this.Scoreform_sectionvi_question27=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question28"){
				this.Scoreform_sectionvi_question28=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question29"){
				this.Scoreform_sectionvi_question29=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question29Desc"){
				this.Scoreform_sectionvi_question29desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question30"){
				this.Scoreform_sectionvi_question30=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question31"){
				this.Scoreform_sectionvi_question31=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question32"){
				this.Scoreform_sectionvi_question32=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question33"){
				this.Scoreform_sectionvi_question33=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question34"){
				this.Scoreform_sectionvi_question34=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question35"){
				this.Scoreform_sectionvi_question35=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question36"){
				this.Scoreform_sectionvi_question36=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question37"){
				this.Scoreform_sectionvi_question37=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question38"){
				this.Scoreform_sectionvi_question38=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question39"){
				this.Scoreform_sectionvi_question39=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question40"){
				this.Scoreform_sectionvi_question40=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question40Desc"){
				this.Scoreform_sectionvi_question40desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question41"){
				this.Scoreform_sectionvi_question41=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question42"){
				this.Scoreform_sectionvi_question42=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question43"){
				this.Scoreform_sectionvi_question43=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question44"){
				this.Scoreform_sectionvi_question44=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question45"){
				this.Scoreform_sectionvi_question45=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question46"){
				this.Scoreform_sectionvi_question46=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question46Desc"){
				this.Scoreform_sectionvi_question46desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question47"){
				this.Scoreform_sectionvi_question47=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question48"){
				this.Scoreform_sectionvi_question48=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question49"){
				this.Scoreform_sectionvi_question49=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question50"){
				this.Scoreform_sectionvi_question50=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question51"){
				this.Scoreform_sectionvi_question51=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question52"){
				this.Scoreform_sectionvi_question52=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question53"){
				this.Scoreform_sectionvi_question53=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question54"){
				this.Scoreform_sectionvi_question54=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question55"){
				this.Scoreform_sectionvi_question55=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56a"){
				this.Scoreform_sectionvi_question56a=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56b"){
				this.Scoreform_sectionvi_question56b=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56c"){
				this.Scoreform_sectionvi_question56c=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56d"){
				this.Scoreform_sectionvi_question56d=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56dDesc"){
				this.Scoreform_sectionvi_question56ddesc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56e"){
				this.Scoreform_sectionvi_question56e=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56f"){
				this.Scoreform_sectionvi_question56f=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question56g"){
				this.Scoreform_sectionvi_question56g=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question57"){
				this.Scoreform_sectionvi_question57=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question58"){
				this.Scoreform_sectionvi_question58=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question58Desc"){
				this.Scoreform_sectionvi_question58desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question59"){
				this.Scoreform_sectionvi_question59=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question60"){
				this.Scoreform_sectionvi_question60=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question61"){
				this.Scoreform_sectionvi_question61=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question62"){
				this.Scoreform_sectionvi_question62=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question63"){
				this.Scoreform_sectionvi_question63=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question64"){
				this.Scoreform_sectionvi_question64=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question65"){
				this.Scoreform_sectionvi_question65=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question66"){
				this.Scoreform_sectionvi_question66=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question66Desc"){
				this.Scoreform_sectionvi_question66desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question67"){
				this.Scoreform_sectionvi_question67=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question68"){
				this.Scoreform_sectionvi_question68=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question69"){
				this.Scoreform_sectionvi_question69=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question70"){
				this.Scoreform_sectionvi_question70=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question70Desc"){
				this.Scoreform_sectionvi_question70desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question71"){
				this.Scoreform_sectionvi_question71=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question72"){
				this.Scoreform_sectionvi_question72=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question73"){
				this.Scoreform_sectionvi_question73=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question74"){
				this.Scoreform_sectionvi_question74=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question75"){
				this.Scoreform_sectionvi_question75=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question76"){
				this.Scoreform_sectionvi_question76=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question77"){
				this.Scoreform_sectionvi_question77=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question77Desc"){
				this.Scoreform_sectionvi_question77desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question78"){
				this.Scoreform_sectionvi_question78=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question79"){
				this.Scoreform_sectionvi_question79=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question79Desc"){
				this.Scoreform_sectionvi_question79desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question80"){
				this.Scoreform_sectionvi_question80=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question81"){
				this.Scoreform_sectionvi_question81=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question82"){
				this.Scoreform_sectionvi_question82=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question83"){
				this.Scoreform_sectionvi_question83=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question84"){
				this.Scoreform_sectionvi_question84=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question84Desc"){
				this.Scoreform_sectionvi_question84desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question85"){
				this.Scoreform_sectionvi_question85=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question85Desc"){
				this.Scoreform_sectionvi_question85desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question86"){
				this.Scoreform_sectionvi_question86=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question87"){
				this.Scoreform_sectionvi_question87=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question88"){
				this.Scoreform_sectionvi_question88=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question89"){
				this.Scoreform_sectionvi_question89=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question90"){
				this.Scoreform_sectionvi_question90=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question91"){
				this.Scoreform_sectionvi_question91=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question92"){
				this.Scoreform_sectionvi_question92=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question92Desc"){
				this.Scoreform_sectionvi_question92desc=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question93"){
				this.Scoreform_sectionvi_question93=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question94"){
				this.Scoreform_sectionvi_question94=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question95"){
				this.Scoreform_sectionvi_question95=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question96"){
				this.Scoreform_sectionvi_question96=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question97"){
				this.Scoreform_sectionvi_question97=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question98"){
				this.Scoreform_sectionvi_question98=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question99"){
				this.Scoreform_sectionvi_question99=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question100"){
				this.Scoreform_sectionvi_question100=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question101"){
				this.Scoreform_sectionvi_question101=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question102"){
				this.Scoreform_sectionvi_question102=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question103"){
				this.Scoreform_sectionvi_question103=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question104"){
				this.Scoreform_sectionvi_question104=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question105"){
				this.Scoreform_sectionvi_question105=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question106"){
				this.Scoreform_sectionvi_question106=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question107"){
				this.Scoreform_sectionvi_question107=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question108"){
				this.Scoreform_sectionvi_question108=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question109"){
				this.Scoreform_sectionvi_question109=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question110"){
				this.Scoreform_sectionvi_question110=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question111"){
				this.Scoreform_sectionvi_question111=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question112"){
				this.Scoreform_sectionvi_question112=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question113"){
				this.Scoreform_sectionvi_question113=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question114"){
				this.Scoreform_sectionvi_question114=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question115"){
				this.Scoreform_sectionvi_question115=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question116"){
				this.Scoreform_sectionvi_question116=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question117"){
				this.Scoreform_sectionvi_question117=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question118"){
				this.Scoreform_sectionvi_question118=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question119"){
				this.Scoreform_sectionvi_question119=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question120"){
				this.Scoreform_sectionvi_question120=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question121"){
				this.Scoreform_sectionvi_question121=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question122"){
				this.Scoreform_sectionvi_question122=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question123"){
				this.Scoreform_sectionvi_question123=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question124"){
				this.Scoreform_sectionvi_question124=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question125"){
				this.Scoreform_sectionvi_question125=value;
			} else 
			if(xmlPath=="ScoreForm/sectionVI/question126"){
				this.Scoreform_sectionvi_question126=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="anxdp_raw"){
			return "field_data";
		}else if (xmlPath=="anxdp_t"){
			return "field_data";
		}else if (xmlPath=="anxdp_note"){
			return "field_data";
		}else if (xmlPath=="wthdp_raw"){
			return "field_data";
		}else if (xmlPath=="wthdp_t"){
			return "field_data";
		}else if (xmlPath=="wthdp_note"){
			return "field_data";
		}else if (xmlPath=="som_raw"){
			return "field_data";
		}else if (xmlPath=="som_t"){
			return "field_data";
		}else if (xmlPath=="som_note"){
			return "field_data";
		}else if (xmlPath=="tho_raw"){
			return "field_data";
		}else if (xmlPath=="tho_t"){
			return "field_data";
		}else if (xmlPath=="tho_note"){
			return "field_data";
		}else if (xmlPath=="att_raw"){
			return "field_data";
		}else if (xmlPath=="att_t"){
			return "field_data";
		}else if (xmlPath=="att_note"){
			return "field_data";
		}else if (xmlPath=="rule_raw"){
			return "field_data";
		}else if (xmlPath=="rule_t"){
			return "field_data";
		}else if (xmlPath=="rule_note"){
			return "field_data";
		}else if (xmlPath=="agg_raw"){
			return "field_data";
		}else if (xmlPath=="agg_t"){
			return "field_data";
		}else if (xmlPath=="agg_note"){
			return "field_data";
		}else if (xmlPath=="int_raw"){
			return "field_data";
		}else if (xmlPath=="int_t"){
			return "field_data";
		}else if (xmlPath=="int_note"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/respRltnSubj"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/friendsA"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/friendsB"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/friendsC"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionI/friendsD"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/maritalStatus"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/maritalStatusOther"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionII/maritalLast6Mnths"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/maritalA"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/maritalB"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/maritalC"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/maritalD"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/maritalE"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/maritalF"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/maritalG"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionII/maritalH"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIII/handicap"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionIII/handicapExplain"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionIV/concerns"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionV/describe"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question1"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question2"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question3"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question4"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question5"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question6"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question6Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question7"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question8"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question9"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question9Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question10"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question11"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question12"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question13"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question14"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question15"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question16"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question17"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question18"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question19"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question20"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question21"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question22"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question23"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question24"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question25"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question26"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question27"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question28"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question29"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question29Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question30"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question31"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question32"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question33"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question34"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question35"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question36"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question37"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question38"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question39"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question40"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question40Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question41"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question42"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question43"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question44"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question45"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question46"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question46Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question47"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question48"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question49"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question50"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question51"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question52"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question53"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question54"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question55"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question56a"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question56b"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question56c"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question56d"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question56dDesc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question56e"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question56f"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question56g"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question57"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question58"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question58Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question59"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question60"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question61"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question62"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question63"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question64"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question65"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question66"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question66Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question67"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question68"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question69"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question70"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question70Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question71"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question72"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question73"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question74"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question75"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question76"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question77"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question77Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question78"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question79"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question79Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question80"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question81"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question82"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question83"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question84"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question84Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question85"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question85Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question86"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question87"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question88"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question89"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question90"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question91"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question92"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question92Desc"){
			return "field_LONG_DATA";
		}else if (xmlPath=="ScoreForm/sectionVI/question93"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question94"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question95"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question96"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question97"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question98"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question99"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question100"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question101"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question102"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question103"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question104"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question105"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question106"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question107"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question108"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question109"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question110"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question111"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question112"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question113"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question114"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question115"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question116"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question117"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question118"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question119"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question120"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question121"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question122"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question123"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question124"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question125"){
			return "field_data";
		}else if (xmlPath=="ScoreForm/sectionVI/question126"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<bcl:ABCL1-03Ed121";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</bcl:ABCL1-03Ed121>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.AnxdpRaw!=null){
			xmlTxt+="\n<bcl:anxdp_raw";
			xmlTxt+=">";
			xmlTxt+=this.AnxdpRaw;
			xmlTxt+="</bcl:anxdp_raw>";
		}
		if (this.AnxdpT!=null){
			xmlTxt+="\n<bcl:anxdp_t";
			xmlTxt+=">";
			xmlTxt+=this.AnxdpT;
			xmlTxt+="</bcl:anxdp_t>";
		}
		if (this.AnxdpNote!=null){
			xmlTxt+="\n<bcl:anxdp_note";
			xmlTxt+=">";
			xmlTxt+=this.AnxdpNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:anxdp_note>";
		}
		if (this.WthdpRaw!=null){
			xmlTxt+="\n<bcl:wthdp_raw";
			xmlTxt+=">";
			xmlTxt+=this.WthdpRaw;
			xmlTxt+="</bcl:wthdp_raw>";
		}
		if (this.WthdpT!=null){
			xmlTxt+="\n<bcl:wthdp_t";
			xmlTxt+=">";
			xmlTxt+=this.WthdpT;
			xmlTxt+="</bcl:wthdp_t>";
		}
		if (this.WthdpNote!=null){
			xmlTxt+="\n<bcl:wthdp_note";
			xmlTxt+=">";
			xmlTxt+=this.WthdpNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:wthdp_note>";
		}
		if (this.SomRaw!=null){
			xmlTxt+="\n<bcl:som_raw";
			xmlTxt+=">";
			xmlTxt+=this.SomRaw;
			xmlTxt+="</bcl:som_raw>";
		}
		if (this.SomT!=null){
			xmlTxt+="\n<bcl:som_t";
			xmlTxt+=">";
			xmlTxt+=this.SomT;
			xmlTxt+="</bcl:som_t>";
		}
		if (this.SomNote!=null){
			xmlTxt+="\n<bcl:som_note";
			xmlTxt+=">";
			xmlTxt+=this.SomNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:som_note>";
		}
		if (this.ThoRaw!=null){
			xmlTxt+="\n<bcl:tho_raw";
			xmlTxt+=">";
			xmlTxt+=this.ThoRaw;
			xmlTxt+="</bcl:tho_raw>";
		}
		if (this.ThoT!=null){
			xmlTxt+="\n<bcl:tho_t";
			xmlTxt+=">";
			xmlTxt+=this.ThoT;
			xmlTxt+="</bcl:tho_t>";
		}
		if (this.ThoNote!=null){
			xmlTxt+="\n<bcl:tho_note";
			xmlTxt+=">";
			xmlTxt+=this.ThoNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:tho_note>";
		}
		if (this.AttRaw!=null){
			xmlTxt+="\n<bcl:att_raw";
			xmlTxt+=">";
			xmlTxt+=this.AttRaw;
			xmlTxt+="</bcl:att_raw>";
		}
		if (this.AttT!=null){
			xmlTxt+="\n<bcl:att_t";
			xmlTxt+=">";
			xmlTxt+=this.AttT;
			xmlTxt+="</bcl:att_t>";
		}
		if (this.AttNote!=null){
			xmlTxt+="\n<bcl:att_note";
			xmlTxt+=">";
			xmlTxt+=this.AttNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:att_note>";
		}
		if (this.RuleRaw!=null){
			xmlTxt+="\n<bcl:rule_raw";
			xmlTxt+=">";
			xmlTxt+=this.RuleRaw;
			xmlTxt+="</bcl:rule_raw>";
		}
		if (this.RuleT!=null){
			xmlTxt+="\n<bcl:rule_t";
			xmlTxt+=">";
			xmlTxt+=this.RuleT;
			xmlTxt+="</bcl:rule_t>";
		}
		if (this.RuleNote!=null){
			xmlTxt+="\n<bcl:rule_note";
			xmlTxt+=">";
			xmlTxt+=this.RuleNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:rule_note>";
		}
		if (this.AggRaw!=null){
			xmlTxt+="\n<bcl:agg_raw";
			xmlTxt+=">";
			xmlTxt+=this.AggRaw;
			xmlTxt+="</bcl:agg_raw>";
		}
		if (this.AggT!=null){
			xmlTxt+="\n<bcl:agg_t";
			xmlTxt+=">";
			xmlTxt+=this.AggT;
			xmlTxt+="</bcl:agg_t>";
		}
		if (this.AggNote!=null){
			xmlTxt+="\n<bcl:agg_note";
			xmlTxt+=">";
			xmlTxt+=this.AggNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:agg_note>";
		}
		if (this.IntRaw!=null){
			xmlTxt+="\n<bcl:int_raw";
			xmlTxt+=">";
			xmlTxt+=this.IntRaw;
			xmlTxt+="</bcl:int_raw>";
		}
		if (this.IntT!=null){
			xmlTxt+="\n<bcl:int_t";
			xmlTxt+=">";
			xmlTxt+=this.IntT;
			xmlTxt+="</bcl:int_t>";
		}
		if (this.IntNote!=null){
			xmlTxt+="\n<bcl:int_note";
			xmlTxt+=">";
			xmlTxt+=this.IntNote.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:int_note>";
		}
			var child0=0;
			var att0=0;
			if(this.Scoreform_sectionii_maritallast6mnths!=null)
			child0++;
			if(this.Scoreform_sectioniii_handicap!=null)
			child0++;
			if(this.Scoreform_sectionvi_question99!=null)
			child0++;
			if(this.Scoreform_sectionvi_question98!=null)
			child0++;
			if(this.Scoreform_sectionvi_question97!=null)
			child0++;
			if(this.Scoreform_sectionvi_question96!=null)
			child0++;
			if(this.Scoreform_sectionvi_question95!=null)
			child0++;
			if(this.Scoreform_sectionvi_question94!=null)
			child0++;
			if(this.Scoreform_sectionvi_question93!=null)
			child0++;
			if(this.Scoreform_sectionvi_question92!=null)
			child0++;
			if(this.Scoreform_sectionvi_question91!=null)
			child0++;
			if(this.Scoreform_sectionvi_question90!=null)
			child0++;
			if(this.Scoreform_sectionvi_question89!=null)
			child0++;
			if(this.Scoreform_sectionvi_question88!=null)
			child0++;
			if(this.Scoreform_sectionvi_question87!=null)
			child0++;
			if(this.Scoreform_sectionvi_question86!=null)
			child0++;
			if(this.Scoreform_sectionvi_question85!=null)
			child0++;
			if(this.Scoreform_sectionvi_question84!=null)
			child0++;
			if(this.Scoreform_sectionvi_question83!=null)
			child0++;
			if(this.Scoreform_sectionvi_question82!=null)
			child0++;
			if(this.Scoreform_sectionvi_question81!=null)
			child0++;
			if(this.Scoreform_sectionvi_question80!=null)
			child0++;
			if(this.Scoreform_sectionvi_question79desc!=null)
			child0++;
			if(this.Scoreform_sectionvi_question56ddesc!=null)
			child0++;
			if(this.Scoreform_sectionvi_question79!=null)
			child0++;
			if(this.Scoreform_sectionvi_question78!=null)
			child0++;
			if(this.Scoreform_sectionvi_question77!=null)
			child0++;
			if(this.Scoreform_sectionvi_question76!=null)
			child0++;
			if(this.Scoreform_sectionvi_question75!=null)
			child0++;
			if(this.Scoreform_sectionvi_question74!=null)
			child0++;
			if(this.Scoreform_sectionvi_question73!=null)
			child0++;
			if(this.Scoreform_sectionvi_question6desc!=null)
			child0++;
			if(this.Scoreform_sectionvi_question72!=null)
			child0++;
			if(this.Scoreform_sectionvi_question71!=null)
			child0++;
			if(this.Scoreform_sectionvi_question46desc!=null)
			child0++;
			if(this.Scoreform_sectionvi_question70!=null)
			child0++;
			if(this.Scoreform_sectionvi_question69!=null)
			child0++;
			if(this.Scoreform_sectionvi_question68!=null)
			child0++;
			if(this.Scoreform_sectionvi_question67!=null)
			child0++;
			if(this.Scoreform_sectionvi_question66!=null)
			child0++;
			if(this.Scoreform_sectionvi_question65!=null)
			child0++;
			if(this.Scoreform_sectionvi_question64!=null)
			child0++;
			if(this.Scoreform_sectionvi_question63!=null)
			child0++;
			if(this.Scoreform_sectionvi_question62!=null)
			child0++;
			if(this.Scoreform_sectionvi_question61!=null)
			child0++;
			if(this.Scoreform_sectionvi_question60!=null)
			child0++;
			if(this.Scoreform_sectioniii_handicapexplain!=null)
			child0++;
			if(this.Scoreform_sectionvi_question59!=null)
			child0++;
			if(this.Scoreform_sectionvi_question58!=null)
			child0++;
			if(this.Scoreform_sectionvi_question57!=null)
			child0++;
			if(this.Scoreform_sectionvi_question9desc!=null)
			child0++;
			if(this.Scoreform_sectionvi_question55!=null)
			child0++;
			if(this.Scoreform_sectionvi_question54!=null)
			child0++;
			if(this.Scoreform_sectionvi_question53!=null)
			child0++;
			if(this.Scoreform_sectionvi_question52!=null)
			child0++;
			if(this.Scoreform_sectionvi_question51!=null)
			child0++;
			if(this.Scoreform_sectionvi_question50!=null)
			child0++;
			if(this.Scoreform_sectionvi_question126!=null)
			child0++;
			if(this.Scoreform_sectionvi_question125!=null)
			child0++;
			if(this.Scoreform_sectionvi_question124!=null)
			child0++;
			if(this.Scoreform_sectionvi_question123!=null)
			child0++;
			if(this.Scoreform_sectionvi_question122!=null)
			child0++;
			if(this.Scoreform_sectionvi_question121!=null)
			child0++;
			if(this.Scoreform_sectionvi_question120!=null)
			child0++;
			if(this.Scoreform_sectioniv_concerns!=null)
			child0++;
			if(this.Scoreform_sectionvi_question49!=null)
			child0++;
			if(this.Scoreform_sectionvi_question48!=null)
			child0++;
			if(this.Scoreform_sectionvi_question47!=null)
			child0++;
			if(this.Scoreform_sectionvi_question46!=null)
			child0++;
			if(this.Scoreform_sectionvi_question45!=null)
			child0++;
			if(this.Scoreform_sectionvi_question44!=null)
			child0++;
			if(this.Scoreform_sectionvi_question43!=null)
			child0++;
			if(this.Scoreform_sectionvi_question119!=null)
			child0++;
			if(this.Scoreform_sectionvi_question42!=null)
			child0++;
			if(this.Scoreform_sectionvi_question118!=null)
			child0++;
			if(this.Scoreform_sectionvi_question41!=null)
			child0++;
			if(this.Scoreform_sectionvi_question117!=null)
			child0++;
			if(this.Scoreform_sectionvi_question40!=null)
			child0++;
			if(this.Scoreform_sectionvi_question116!=null)
			child0++;
			if(this.Scoreform_sectionii_maritalstatus!=null)
			child0++;
			if(this.Scoreform_sectionvi_question115!=null)
			child0++;
			if(this.Scoreform_sectionvi_question114!=null)
			child0++;
			if(this.Scoreform_sectionvi_question113!=null)
			child0++;
			if(this.Scoreform_sectionvi_question112!=null)
			child0++;
			if(this.Scoreform_sectionvi_question111!=null)
			child0++;
			if(this.Scoreform_sectionvi_question110!=null)
			child0++;
			if(this.Scoreform_sectionvi_question39!=null)
			child0++;
			if(this.Scoreform_sectionvi_question38!=null)
			child0++;
			if(this.Scoreform_sectionvi_question37!=null)
			child0++;
			if(this.Scoreform_sectionvi_question36!=null)
			child0++;
			if(this.Scoreform_sectionvi_question35!=null)
			child0++;
			if(this.Scoreform_sectionvi_question34!=null)
			child0++;
			if(this.Scoreform_sectionvi_question85desc!=null)
			child0++;
			if(this.Scoreform_sectionvi_question33!=null)
			child0++;
			if(this.Scoreform_sectionvi_question109!=null)
			child0++;
			if(this.Scoreform_sectionvi_question32!=null)
			child0++;
			if(this.Scoreform_sectionvi_question108!=null)
			child0++;
			if(this.Scoreform_sectionvi_question31!=null)
			child0++;
			if(this.Scoreform_sectionvi_question107!=null)
			child0++;
			if(this.Scoreform_sectionvi_question30!=null)
			child0++;
			if(this.Scoreform_sectionvi_question106!=null)
			child0++;
			if(this.Scoreform_sectionvi_question105!=null)
			child0++;
			if(this.Scoreform_sectionvi_question104!=null)
			child0++;
			if(this.Scoreform_sectionvi_question103!=null)
			child0++;
			if(this.Scoreform_sectionvi_question102!=null)
			child0++;
			if(this.Scoreform_sectionvi_question101!=null)
			child0++;
			if(this.Scoreform_sectionvi_question100!=null)
			child0++;
			if(this.Scoreform_sectionvi_question29!=null)
			child0++;
			if(this.Scoreform_sectionvi_question28!=null)
			child0++;
			if(this.Scoreform_sectionvi_question9!=null)
			child0++;
			if(this.Scoreform_sectionvi_question27!=null)
			child0++;
			if(this.Scoreform_sectionvi_question8!=null)
			child0++;
			if(this.Scoreform_sectionvi_question26!=null)
			child0++;
			if(this.Scoreform_sectionvi_question7!=null)
			child0++;
			if(this.Scoreform_sectionv_describe!=null)
			child0++;
			if(this.Scoreform_sectionvi_question25!=null)
			child0++;
			if(this.Scoreform_sectionvi_question6!=null)
			child0++;
			if(this.Scoreform_sectionvi_question24!=null)
			child0++;
			if(this.Scoreform_sectionvi_question5!=null)
			child0++;
			if(this.Scoreform_sectionvi_question23!=null)
			child0++;
			if(this.Scoreform_sectionvi_question4!=null)
			child0++;
			if(this.Scoreform_sectionvi_question22!=null)
			child0++;
			if(this.Scoreform_sectionvi_question3!=null)
			child0++;
			if(this.Scoreform_sectionvi_question21!=null)
			child0++;
			if(this.Scoreform_sectionvi_question2!=null)
			child0++;
			if(this.Scoreform_sectionvi_question20!=null)
			child0++;
			if(this.Scoreform_sectionvi_question1!=null)
			child0++;
			if(this.Scoreform_sectionvi_question92desc!=null)
			child0++;
			if(this.Scoreform_sectionvi_question19!=null)
			child0++;
			if(this.Scoreform_sectionvi_question70desc!=null)
			child0++;
			if(this.Scoreform_sectionvi_question18!=null)
			child0++;
			if(this.Scoreform_sectionvi_question17!=null)
			child0++;
			if(this.Scoreform_sectionvi_question16!=null)
			child0++;
			if(this.Scoreform_sectionvi_question15!=null)
			child0++;
			if(this.Scoreform_sectionvi_question14!=null)
			child0++;
			if(this.Scoreform_sectionvi_question13!=null)
			child0++;
			if(this.Scoreform_sectionvi_question12!=null)
			child0++;
			if(this.Scoreform_sectionvi_question11!=null)
			child0++;
			if(this.Scoreform_sectionvi_question10!=null)
			child0++;
			if(this.Scoreform_sectionvi_question77desc!=null)
			child0++;
			if(this.Scoreform_sectionii_maritalstatusother!=null)
			child0++;
			if(this.Scoreform_sectionvi_question66desc!=null)
			child0++;
			if(this.Scoreform_sectioni_friendsd!=null)
			child0++;
			if(this.Scoreform_sectioni_friendsc!=null)
			child0++;
			if(this.Scoreform_sectioni_friendsb!=null)
			child0++;
			if(this.Scoreform_sectioni_friendsa!=null)
			child0++;
			if(this.Scoreform_sectionvi_question84desc!=null)
			child0++;
			if(this.Scoreform_sectionii_maritalh!=null)
			child0++;
			if(this.Scoreform_sectionii_maritalg!=null)
			child0++;
			if(this.Scoreform_sectionii_maritalf!=null)
			child0++;
			if(this.Scoreform_sectionii_maritale!=null)
			child0++;
			if(this.Scoreform_sectionii_maritald!=null)
			child0++;
			if(this.Scoreform_sectionii_maritalc!=null)
			child0++;
			if(this.Scoreform_sectionii_maritalb!=null)
			child0++;
			if(this.Scoreform_sectionii_maritala!=null)
			child0++;
			if(this.Scoreform_sectionvi_question29desc!=null)
			child0++;
			if(this.Scoreform_sectionvi_question56g!=null)
			child0++;
			if(this.Scoreform_sectionvi_question56f!=null)
			child0++;
			if(this.Scoreform_sectionvi_question56e!=null)
			child0++;
			if(this.Scoreform_sectionvi_question56d!=null)
			child0++;
			if(this.Scoreform_sectionvi_question56c!=null)
			child0++;
			if(this.Scoreform_sectionvi_question56b!=null)
			child0++;
			if(this.Scoreform_sectionvi_question56a!=null)
			child0++;
			if(this.Scoreform_sectionvi_question40desc!=null)
			child0++;
			if(this.Scoreform_sectionvi_question58desc!=null)
			child0++;
			if(this.Scoreform_resprltnsubj!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<bcl:ScoreForm";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_resprltnsubj!=null){
			xmlTxt+="\n<bcl:respRltnSubj";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_resprltnsubj.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:respRltnSubj>";
		}
			var child1=0;
			var att1=0;
			if(this.Scoreform_sectioni_friendsd!=null)
			child1++;
			if(this.Scoreform_sectioni_friendsc!=null)
			child1++;
			if(this.Scoreform_sectioni_friendsb!=null)
			child1++;
			if(this.Scoreform_sectioni_friendsa!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<bcl:sectionI";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectioni_friendsa!=null){
			xmlTxt+="\n<bcl:friendsA";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_friendsa;
			xmlTxt+="</bcl:friendsA>";
		}
		if (this.Scoreform_sectioni_friendsb!=null){
			xmlTxt+="\n<bcl:friendsB";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_friendsb;
			xmlTxt+="</bcl:friendsB>";
		}
		if (this.Scoreform_sectioni_friendsc!=null){
			xmlTxt+="\n<bcl:friendsC";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_friendsc;
			xmlTxt+="</bcl:friendsC>";
		}
		if (this.Scoreform_sectioni_friendsd!=null){
			xmlTxt+="\n<bcl:friendsD";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioni_friendsd;
			xmlTxt+="</bcl:friendsD>";
		}
				xmlTxt+="\n</bcl:sectionI>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Scoreform_sectionii_maritalh!=null)
			child2++;
			if(this.Scoreform_sectionii_maritalg!=null)
			child2++;
			if(this.Scoreform_sectionii_maritallast6mnths!=null)
			child2++;
			if(this.Scoreform_sectionii_maritalf!=null)
			child2++;
			if(this.Scoreform_sectionii_maritale!=null)
			child2++;
			if(this.Scoreform_sectionii_maritald!=null)
			child2++;
			if(this.Scoreform_sectionii_maritalc!=null)
			child2++;
			if(this.Scoreform_sectionii_maritalb!=null)
			child2++;
			if(this.Scoreform_sectionii_maritala!=null)
			child2++;
			if(this.Scoreform_sectionii_maritalstatusother!=null)
			child2++;
			if(this.Scoreform_sectionii_maritalstatus!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<bcl:sectionII";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionii_maritalstatus!=null){
			xmlTxt+="\n<bcl:maritalStatus";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritalstatus;
			xmlTxt+="</bcl:maritalStatus>";
		}
		if (this.Scoreform_sectionii_maritalstatusother!=null){
			xmlTxt+="\n<bcl:maritalStatusOther";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritalstatusother.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:maritalStatusOther>";
		}
		if (this.Scoreform_sectionii_maritallast6mnths!=null){
			xmlTxt+="\n<bcl:maritalLast6Mnths";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritallast6mnths;
			xmlTxt+="</bcl:maritalLast6Mnths>";
		}
		if (this.Scoreform_sectionii_maritala!=null){
			xmlTxt+="\n<bcl:maritalA";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritala;
			xmlTxt+="</bcl:maritalA>";
		}
		if (this.Scoreform_sectionii_maritalb!=null){
			xmlTxt+="\n<bcl:maritalB";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritalb;
			xmlTxt+="</bcl:maritalB>";
		}
		if (this.Scoreform_sectionii_maritalc!=null){
			xmlTxt+="\n<bcl:maritalC";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritalc;
			xmlTxt+="</bcl:maritalC>";
		}
		if (this.Scoreform_sectionii_maritald!=null){
			xmlTxt+="\n<bcl:maritalD";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritald;
			xmlTxt+="</bcl:maritalD>";
		}
		if (this.Scoreform_sectionii_maritale!=null){
			xmlTxt+="\n<bcl:maritalE";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritale;
			xmlTxt+="</bcl:maritalE>";
		}
		if (this.Scoreform_sectionii_maritalf!=null){
			xmlTxt+="\n<bcl:maritalF";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritalf;
			xmlTxt+="</bcl:maritalF>";
		}
		if (this.Scoreform_sectionii_maritalg!=null){
			xmlTxt+="\n<bcl:maritalG";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritalg;
			xmlTxt+="</bcl:maritalG>";
		}
		if (this.Scoreform_sectionii_maritalh!=null){
			xmlTxt+="\n<bcl:maritalH";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionii_maritalh;
			xmlTxt+="</bcl:maritalH>";
		}
				xmlTxt+="\n</bcl:sectionII>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Scoreform_sectioniii_handicapexplain!=null)
			child3++;
			if(this.Scoreform_sectioniii_handicap!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<bcl:sectionIII";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectioniii_handicap!=null){
			xmlTxt+="\n<bcl:handicap";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniii_handicap;
			xmlTxt+="</bcl:handicap>";
		}
		if (this.Scoreform_sectioniii_handicapexplain!=null){
			xmlTxt+="\n<bcl:handicapExplain";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniii_handicapexplain.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:handicapExplain>";
		}
				xmlTxt+="\n</bcl:sectionIII>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Scoreform_sectioniv_concerns!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<bcl:sectionIV";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectioniv_concerns!=null){
			xmlTxt+="\n<bcl:concerns";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectioniv_concerns.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:concerns>";
		}
				xmlTxt+="\n</bcl:sectionIV>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Scoreform_sectionv_describe!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<bcl:sectionV";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionv_describe!=null){
			xmlTxt+="\n<bcl:describe";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionv_describe.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:describe>";
		}
				xmlTxt+="\n</bcl:sectionV>";
			}
			}

			var child6=0;
			var att6=0;
			if(this.Scoreform_sectionvi_question99!=null)
			child6++;
			if(this.Scoreform_sectionvi_question98!=null)
			child6++;
			if(this.Scoreform_sectionvi_question97!=null)
			child6++;
			if(this.Scoreform_sectionvi_question96!=null)
			child6++;
			if(this.Scoreform_sectionvi_question95!=null)
			child6++;
			if(this.Scoreform_sectionvi_question94!=null)
			child6++;
			if(this.Scoreform_sectionvi_question93!=null)
			child6++;
			if(this.Scoreform_sectionvi_question92!=null)
			child6++;
			if(this.Scoreform_sectionvi_question91!=null)
			child6++;
			if(this.Scoreform_sectionvi_question90!=null)
			child6++;
			if(this.Scoreform_sectionvi_question89!=null)
			child6++;
			if(this.Scoreform_sectionvi_question88!=null)
			child6++;
			if(this.Scoreform_sectionvi_question87!=null)
			child6++;
			if(this.Scoreform_sectionvi_question86!=null)
			child6++;
			if(this.Scoreform_sectionvi_question85!=null)
			child6++;
			if(this.Scoreform_sectionvi_question84!=null)
			child6++;
			if(this.Scoreform_sectionvi_question83!=null)
			child6++;
			if(this.Scoreform_sectionvi_question82!=null)
			child6++;
			if(this.Scoreform_sectionvi_question81!=null)
			child6++;
			if(this.Scoreform_sectionvi_question80!=null)
			child6++;
			if(this.Scoreform_sectionvi_question79desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question56ddesc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question79!=null)
			child6++;
			if(this.Scoreform_sectionvi_question78!=null)
			child6++;
			if(this.Scoreform_sectionvi_question77!=null)
			child6++;
			if(this.Scoreform_sectionvi_question76!=null)
			child6++;
			if(this.Scoreform_sectionvi_question75!=null)
			child6++;
			if(this.Scoreform_sectionvi_question74!=null)
			child6++;
			if(this.Scoreform_sectionvi_question73!=null)
			child6++;
			if(this.Scoreform_sectionvi_question6desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question72!=null)
			child6++;
			if(this.Scoreform_sectionvi_question71!=null)
			child6++;
			if(this.Scoreform_sectionvi_question46desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question70!=null)
			child6++;
			if(this.Scoreform_sectionvi_question69!=null)
			child6++;
			if(this.Scoreform_sectionvi_question68!=null)
			child6++;
			if(this.Scoreform_sectionvi_question67!=null)
			child6++;
			if(this.Scoreform_sectionvi_question66!=null)
			child6++;
			if(this.Scoreform_sectionvi_question65!=null)
			child6++;
			if(this.Scoreform_sectionvi_question64!=null)
			child6++;
			if(this.Scoreform_sectionvi_question63!=null)
			child6++;
			if(this.Scoreform_sectionvi_question62!=null)
			child6++;
			if(this.Scoreform_sectionvi_question61!=null)
			child6++;
			if(this.Scoreform_sectionvi_question60!=null)
			child6++;
			if(this.Scoreform_sectionvi_question59!=null)
			child6++;
			if(this.Scoreform_sectionvi_question58!=null)
			child6++;
			if(this.Scoreform_sectionvi_question57!=null)
			child6++;
			if(this.Scoreform_sectionvi_question9desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question55!=null)
			child6++;
			if(this.Scoreform_sectionvi_question54!=null)
			child6++;
			if(this.Scoreform_sectionvi_question53!=null)
			child6++;
			if(this.Scoreform_sectionvi_question52!=null)
			child6++;
			if(this.Scoreform_sectionvi_question51!=null)
			child6++;
			if(this.Scoreform_sectionvi_question126!=null)
			child6++;
			if(this.Scoreform_sectionvi_question50!=null)
			child6++;
			if(this.Scoreform_sectionvi_question125!=null)
			child6++;
			if(this.Scoreform_sectionvi_question124!=null)
			child6++;
			if(this.Scoreform_sectionvi_question123!=null)
			child6++;
			if(this.Scoreform_sectionvi_question122!=null)
			child6++;
			if(this.Scoreform_sectionvi_question121!=null)
			child6++;
			if(this.Scoreform_sectionvi_question120!=null)
			child6++;
			if(this.Scoreform_sectionvi_question49!=null)
			child6++;
			if(this.Scoreform_sectionvi_question48!=null)
			child6++;
			if(this.Scoreform_sectionvi_question47!=null)
			child6++;
			if(this.Scoreform_sectionvi_question46!=null)
			child6++;
			if(this.Scoreform_sectionvi_question45!=null)
			child6++;
			if(this.Scoreform_sectionvi_question44!=null)
			child6++;
			if(this.Scoreform_sectionvi_question43!=null)
			child6++;
			if(this.Scoreform_sectionvi_question119!=null)
			child6++;
			if(this.Scoreform_sectionvi_question42!=null)
			child6++;
			if(this.Scoreform_sectionvi_question118!=null)
			child6++;
			if(this.Scoreform_sectionvi_question41!=null)
			child6++;
			if(this.Scoreform_sectionvi_question117!=null)
			child6++;
			if(this.Scoreform_sectionvi_question40!=null)
			child6++;
			if(this.Scoreform_sectionvi_question116!=null)
			child6++;
			if(this.Scoreform_sectionvi_question115!=null)
			child6++;
			if(this.Scoreform_sectionvi_question114!=null)
			child6++;
			if(this.Scoreform_sectionvi_question113!=null)
			child6++;
			if(this.Scoreform_sectionvi_question112!=null)
			child6++;
			if(this.Scoreform_sectionvi_question111!=null)
			child6++;
			if(this.Scoreform_sectionvi_question110!=null)
			child6++;
			if(this.Scoreform_sectionvi_question39!=null)
			child6++;
			if(this.Scoreform_sectionvi_question38!=null)
			child6++;
			if(this.Scoreform_sectionvi_question37!=null)
			child6++;
			if(this.Scoreform_sectionvi_question36!=null)
			child6++;
			if(this.Scoreform_sectionvi_question35!=null)
			child6++;
			if(this.Scoreform_sectionvi_question34!=null)
			child6++;
			if(this.Scoreform_sectionvi_question85desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question33!=null)
			child6++;
			if(this.Scoreform_sectionvi_question109!=null)
			child6++;
			if(this.Scoreform_sectionvi_question32!=null)
			child6++;
			if(this.Scoreform_sectionvi_question108!=null)
			child6++;
			if(this.Scoreform_sectionvi_question31!=null)
			child6++;
			if(this.Scoreform_sectionvi_question107!=null)
			child6++;
			if(this.Scoreform_sectionvi_question30!=null)
			child6++;
			if(this.Scoreform_sectionvi_question106!=null)
			child6++;
			if(this.Scoreform_sectionvi_question105!=null)
			child6++;
			if(this.Scoreform_sectionvi_question104!=null)
			child6++;
			if(this.Scoreform_sectionvi_question103!=null)
			child6++;
			if(this.Scoreform_sectionvi_question102!=null)
			child6++;
			if(this.Scoreform_sectionvi_question101!=null)
			child6++;
			if(this.Scoreform_sectionvi_question100!=null)
			child6++;
			if(this.Scoreform_sectionvi_question29!=null)
			child6++;
			if(this.Scoreform_sectionvi_question28!=null)
			child6++;
			if(this.Scoreform_sectionvi_question9!=null)
			child6++;
			if(this.Scoreform_sectionvi_question27!=null)
			child6++;
			if(this.Scoreform_sectionvi_question8!=null)
			child6++;
			if(this.Scoreform_sectionvi_question26!=null)
			child6++;
			if(this.Scoreform_sectionvi_question7!=null)
			child6++;
			if(this.Scoreform_sectionvi_question25!=null)
			child6++;
			if(this.Scoreform_sectionvi_question6!=null)
			child6++;
			if(this.Scoreform_sectionvi_question24!=null)
			child6++;
			if(this.Scoreform_sectionvi_question5!=null)
			child6++;
			if(this.Scoreform_sectionvi_question23!=null)
			child6++;
			if(this.Scoreform_sectionvi_question4!=null)
			child6++;
			if(this.Scoreform_sectionvi_question22!=null)
			child6++;
			if(this.Scoreform_sectionvi_question3!=null)
			child6++;
			if(this.Scoreform_sectionvi_question21!=null)
			child6++;
			if(this.Scoreform_sectionvi_question2!=null)
			child6++;
			if(this.Scoreform_sectionvi_question20!=null)
			child6++;
			if(this.Scoreform_sectionvi_question1!=null)
			child6++;
			if(this.Scoreform_sectionvi_question92desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question19!=null)
			child6++;
			if(this.Scoreform_sectionvi_question18!=null)
			child6++;
			if(this.Scoreform_sectionvi_question70desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question17!=null)
			child6++;
			if(this.Scoreform_sectionvi_question16!=null)
			child6++;
			if(this.Scoreform_sectionvi_question15!=null)
			child6++;
			if(this.Scoreform_sectionvi_question14!=null)
			child6++;
			if(this.Scoreform_sectionvi_question13!=null)
			child6++;
			if(this.Scoreform_sectionvi_question12!=null)
			child6++;
			if(this.Scoreform_sectionvi_question11!=null)
			child6++;
			if(this.Scoreform_sectionvi_question10!=null)
			child6++;
			if(this.Scoreform_sectionvi_question77desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question66desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question84desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question29desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question56g!=null)
			child6++;
			if(this.Scoreform_sectionvi_question56f!=null)
			child6++;
			if(this.Scoreform_sectionvi_question56e!=null)
			child6++;
			if(this.Scoreform_sectionvi_question56d!=null)
			child6++;
			if(this.Scoreform_sectionvi_question56c!=null)
			child6++;
			if(this.Scoreform_sectionvi_question56b!=null)
			child6++;
			if(this.Scoreform_sectionvi_question40desc!=null)
			child6++;
			if(this.Scoreform_sectionvi_question56a!=null)
			child6++;
			if(this.Scoreform_sectionvi_question58desc!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<bcl:sectionVI";
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Scoreform_sectionvi_question1!=null){
			xmlTxt+="\n<bcl:question1";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question1;
			xmlTxt+="</bcl:question1>";
		}
		if (this.Scoreform_sectionvi_question2!=null){
			xmlTxt+="\n<bcl:question2";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question2;
			xmlTxt+="</bcl:question2>";
		}
		if (this.Scoreform_sectionvi_question3!=null){
			xmlTxt+="\n<bcl:question3";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question3;
			xmlTxt+="</bcl:question3>";
		}
		if (this.Scoreform_sectionvi_question4!=null){
			xmlTxt+="\n<bcl:question4";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question4;
			xmlTxt+="</bcl:question4>";
		}
		if (this.Scoreform_sectionvi_question5!=null){
			xmlTxt+="\n<bcl:question5";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question5;
			xmlTxt+="</bcl:question5>";
		}
		if (this.Scoreform_sectionvi_question6!=null){
			xmlTxt+="\n<bcl:question6";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question6;
			xmlTxt+="</bcl:question6>";
		}
		if (this.Scoreform_sectionvi_question6desc!=null){
			xmlTxt+="\n<bcl:question6Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question6desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question6Desc>";
		}
		if (this.Scoreform_sectionvi_question7!=null){
			xmlTxt+="\n<bcl:question7";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question7;
			xmlTxt+="</bcl:question7>";
		}
		if (this.Scoreform_sectionvi_question8!=null){
			xmlTxt+="\n<bcl:question8";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question8;
			xmlTxt+="</bcl:question8>";
		}
		if (this.Scoreform_sectionvi_question9!=null){
			xmlTxt+="\n<bcl:question9";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question9;
			xmlTxt+="</bcl:question9>";
		}
		if (this.Scoreform_sectionvi_question9desc!=null){
			xmlTxt+="\n<bcl:question9Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question9desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question9Desc>";
		}
		if (this.Scoreform_sectionvi_question10!=null){
			xmlTxt+="\n<bcl:question10";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question10;
			xmlTxt+="</bcl:question10>";
		}
		if (this.Scoreform_sectionvi_question11!=null){
			xmlTxt+="\n<bcl:question11";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question11;
			xmlTxt+="</bcl:question11>";
		}
		if (this.Scoreform_sectionvi_question12!=null){
			xmlTxt+="\n<bcl:question12";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question12;
			xmlTxt+="</bcl:question12>";
		}
		if (this.Scoreform_sectionvi_question13!=null){
			xmlTxt+="\n<bcl:question13";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question13;
			xmlTxt+="</bcl:question13>";
		}
		if (this.Scoreform_sectionvi_question14!=null){
			xmlTxt+="\n<bcl:question14";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question14;
			xmlTxt+="</bcl:question14>";
		}
		if (this.Scoreform_sectionvi_question15!=null){
			xmlTxt+="\n<bcl:question15";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question15;
			xmlTxt+="</bcl:question15>";
		}
		if (this.Scoreform_sectionvi_question16!=null){
			xmlTxt+="\n<bcl:question16";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question16;
			xmlTxt+="</bcl:question16>";
		}
		if (this.Scoreform_sectionvi_question17!=null){
			xmlTxt+="\n<bcl:question17";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question17;
			xmlTxt+="</bcl:question17>";
		}
		if (this.Scoreform_sectionvi_question18!=null){
			xmlTxt+="\n<bcl:question18";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question18;
			xmlTxt+="</bcl:question18>";
		}
		if (this.Scoreform_sectionvi_question19!=null){
			xmlTxt+="\n<bcl:question19";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question19;
			xmlTxt+="</bcl:question19>";
		}
		if (this.Scoreform_sectionvi_question20!=null){
			xmlTxt+="\n<bcl:question20";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question20;
			xmlTxt+="</bcl:question20>";
		}
		if (this.Scoreform_sectionvi_question21!=null){
			xmlTxt+="\n<bcl:question21";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question21;
			xmlTxt+="</bcl:question21>";
		}
		if (this.Scoreform_sectionvi_question22!=null){
			xmlTxt+="\n<bcl:question22";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question22;
			xmlTxt+="</bcl:question22>";
		}
		if (this.Scoreform_sectionvi_question23!=null){
			xmlTxt+="\n<bcl:question23";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question23;
			xmlTxt+="</bcl:question23>";
		}
		if (this.Scoreform_sectionvi_question24!=null){
			xmlTxt+="\n<bcl:question24";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question24;
			xmlTxt+="</bcl:question24>";
		}
		if (this.Scoreform_sectionvi_question25!=null){
			xmlTxt+="\n<bcl:question25";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question25;
			xmlTxt+="</bcl:question25>";
		}
		if (this.Scoreform_sectionvi_question26!=null){
			xmlTxt+="\n<bcl:question26";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question26;
			xmlTxt+="</bcl:question26>";
		}
		if (this.Scoreform_sectionvi_question27!=null){
			xmlTxt+="\n<bcl:question27";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question27;
			xmlTxt+="</bcl:question27>";
		}
		if (this.Scoreform_sectionvi_question28!=null){
			xmlTxt+="\n<bcl:question28";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question28;
			xmlTxt+="</bcl:question28>";
		}
		if (this.Scoreform_sectionvi_question29!=null){
			xmlTxt+="\n<bcl:question29";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question29;
			xmlTxt+="</bcl:question29>";
		}
		if (this.Scoreform_sectionvi_question29desc!=null){
			xmlTxt+="\n<bcl:question29Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question29desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question29Desc>";
		}
		if (this.Scoreform_sectionvi_question30!=null){
			xmlTxt+="\n<bcl:question30";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question30;
			xmlTxt+="</bcl:question30>";
		}
		if (this.Scoreform_sectionvi_question31!=null){
			xmlTxt+="\n<bcl:question31";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question31;
			xmlTxt+="</bcl:question31>";
		}
		if (this.Scoreform_sectionvi_question32!=null){
			xmlTxt+="\n<bcl:question32";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question32;
			xmlTxt+="</bcl:question32>";
		}
		if (this.Scoreform_sectionvi_question33!=null){
			xmlTxt+="\n<bcl:question33";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question33;
			xmlTxt+="</bcl:question33>";
		}
		if (this.Scoreform_sectionvi_question34!=null){
			xmlTxt+="\n<bcl:question34";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question34;
			xmlTxt+="</bcl:question34>";
		}
		if (this.Scoreform_sectionvi_question35!=null){
			xmlTxt+="\n<bcl:question35";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question35;
			xmlTxt+="</bcl:question35>";
		}
		if (this.Scoreform_sectionvi_question36!=null){
			xmlTxt+="\n<bcl:question36";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question36;
			xmlTxt+="</bcl:question36>";
		}
		if (this.Scoreform_sectionvi_question37!=null){
			xmlTxt+="\n<bcl:question37";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question37;
			xmlTxt+="</bcl:question37>";
		}
		if (this.Scoreform_sectionvi_question38!=null){
			xmlTxt+="\n<bcl:question38";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question38;
			xmlTxt+="</bcl:question38>";
		}
		if (this.Scoreform_sectionvi_question39!=null){
			xmlTxt+="\n<bcl:question39";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question39;
			xmlTxt+="</bcl:question39>";
		}
		if (this.Scoreform_sectionvi_question40!=null){
			xmlTxt+="\n<bcl:question40";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question40;
			xmlTxt+="</bcl:question40>";
		}
		if (this.Scoreform_sectionvi_question40desc!=null){
			xmlTxt+="\n<bcl:question40Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question40desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question40Desc>";
		}
		if (this.Scoreform_sectionvi_question41!=null){
			xmlTxt+="\n<bcl:question41";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question41;
			xmlTxt+="</bcl:question41>";
		}
		if (this.Scoreform_sectionvi_question42!=null){
			xmlTxt+="\n<bcl:question42";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question42;
			xmlTxt+="</bcl:question42>";
		}
		if (this.Scoreform_sectionvi_question43!=null){
			xmlTxt+="\n<bcl:question43";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question43;
			xmlTxt+="</bcl:question43>";
		}
		if (this.Scoreform_sectionvi_question44!=null){
			xmlTxt+="\n<bcl:question44";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question44;
			xmlTxt+="</bcl:question44>";
		}
		if (this.Scoreform_sectionvi_question45!=null){
			xmlTxt+="\n<bcl:question45";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question45;
			xmlTxt+="</bcl:question45>";
		}
		if (this.Scoreform_sectionvi_question46!=null){
			xmlTxt+="\n<bcl:question46";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question46;
			xmlTxt+="</bcl:question46>";
		}
		if (this.Scoreform_sectionvi_question46desc!=null){
			xmlTxt+="\n<bcl:question46Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question46desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question46Desc>";
		}
		if (this.Scoreform_sectionvi_question47!=null){
			xmlTxt+="\n<bcl:question47";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question47;
			xmlTxt+="</bcl:question47>";
		}
		if (this.Scoreform_sectionvi_question48!=null){
			xmlTxt+="\n<bcl:question48";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question48;
			xmlTxt+="</bcl:question48>";
		}
		if (this.Scoreform_sectionvi_question49!=null){
			xmlTxt+="\n<bcl:question49";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question49;
			xmlTxt+="</bcl:question49>";
		}
		if (this.Scoreform_sectionvi_question50!=null){
			xmlTxt+="\n<bcl:question50";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question50;
			xmlTxt+="</bcl:question50>";
		}
		if (this.Scoreform_sectionvi_question51!=null){
			xmlTxt+="\n<bcl:question51";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question51;
			xmlTxt+="</bcl:question51>";
		}
		if (this.Scoreform_sectionvi_question52!=null){
			xmlTxt+="\n<bcl:question52";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question52;
			xmlTxt+="</bcl:question52>";
		}
		if (this.Scoreform_sectionvi_question53!=null){
			xmlTxt+="\n<bcl:question53";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question53;
			xmlTxt+="</bcl:question53>";
		}
		if (this.Scoreform_sectionvi_question54!=null){
			xmlTxt+="\n<bcl:question54";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question54;
			xmlTxt+="</bcl:question54>";
		}
		if (this.Scoreform_sectionvi_question55!=null){
			xmlTxt+="\n<bcl:question55";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question55;
			xmlTxt+="</bcl:question55>";
		}
		if (this.Scoreform_sectionvi_question56a!=null){
			xmlTxt+="\n<bcl:question56a";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question56a;
			xmlTxt+="</bcl:question56a>";
		}
		if (this.Scoreform_sectionvi_question56b!=null){
			xmlTxt+="\n<bcl:question56b";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question56b;
			xmlTxt+="</bcl:question56b>";
		}
		if (this.Scoreform_sectionvi_question56c!=null){
			xmlTxt+="\n<bcl:question56c";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question56c;
			xmlTxt+="</bcl:question56c>";
		}
		if (this.Scoreform_sectionvi_question56d!=null){
			xmlTxt+="\n<bcl:question56d";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question56d;
			xmlTxt+="</bcl:question56d>";
		}
		if (this.Scoreform_sectionvi_question56ddesc!=null){
			xmlTxt+="\n<bcl:question56dDesc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question56ddesc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question56dDesc>";
		}
		if (this.Scoreform_sectionvi_question56e!=null){
			xmlTxt+="\n<bcl:question56e";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question56e;
			xmlTxt+="</bcl:question56e>";
		}
		if (this.Scoreform_sectionvi_question56f!=null){
			xmlTxt+="\n<bcl:question56f";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question56f;
			xmlTxt+="</bcl:question56f>";
		}
		if (this.Scoreform_sectionvi_question56g!=null){
			xmlTxt+="\n<bcl:question56g";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question56g;
			xmlTxt+="</bcl:question56g>";
		}
		if (this.Scoreform_sectionvi_question57!=null){
			xmlTxt+="\n<bcl:question57";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question57;
			xmlTxt+="</bcl:question57>";
		}
		if (this.Scoreform_sectionvi_question58!=null){
			xmlTxt+="\n<bcl:question58";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question58;
			xmlTxt+="</bcl:question58>";
		}
		if (this.Scoreform_sectionvi_question58desc!=null){
			xmlTxt+="\n<bcl:question58Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question58desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question58Desc>";
		}
		if (this.Scoreform_sectionvi_question59!=null){
			xmlTxt+="\n<bcl:question59";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question59;
			xmlTxt+="</bcl:question59>";
		}
		if (this.Scoreform_sectionvi_question60!=null){
			xmlTxt+="\n<bcl:question60";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question60;
			xmlTxt+="</bcl:question60>";
		}
		if (this.Scoreform_sectionvi_question61!=null){
			xmlTxt+="\n<bcl:question61";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question61;
			xmlTxt+="</bcl:question61>";
		}
		if (this.Scoreform_sectionvi_question62!=null){
			xmlTxt+="\n<bcl:question62";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question62;
			xmlTxt+="</bcl:question62>";
		}
		if (this.Scoreform_sectionvi_question63!=null){
			xmlTxt+="\n<bcl:question63";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question63;
			xmlTxt+="</bcl:question63>";
		}
		if (this.Scoreform_sectionvi_question64!=null){
			xmlTxt+="\n<bcl:question64";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question64;
			xmlTxt+="</bcl:question64>";
		}
		if (this.Scoreform_sectionvi_question65!=null){
			xmlTxt+="\n<bcl:question65";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question65;
			xmlTxt+="</bcl:question65>";
		}
		if (this.Scoreform_sectionvi_question66!=null){
			xmlTxt+="\n<bcl:question66";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question66;
			xmlTxt+="</bcl:question66>";
		}
		if (this.Scoreform_sectionvi_question66desc!=null){
			xmlTxt+="\n<bcl:question66Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question66desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question66Desc>";
		}
		if (this.Scoreform_sectionvi_question67!=null){
			xmlTxt+="\n<bcl:question67";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question67;
			xmlTxt+="</bcl:question67>";
		}
		if (this.Scoreform_sectionvi_question68!=null){
			xmlTxt+="\n<bcl:question68";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question68;
			xmlTxt+="</bcl:question68>";
		}
		if (this.Scoreform_sectionvi_question69!=null){
			xmlTxt+="\n<bcl:question69";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question69;
			xmlTxt+="</bcl:question69>";
		}
		if (this.Scoreform_sectionvi_question70!=null){
			xmlTxt+="\n<bcl:question70";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question70;
			xmlTxt+="</bcl:question70>";
		}
		if (this.Scoreform_sectionvi_question70desc!=null){
			xmlTxt+="\n<bcl:question70Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question70desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question70Desc>";
		}
		if (this.Scoreform_sectionvi_question71!=null){
			xmlTxt+="\n<bcl:question71";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question71;
			xmlTxt+="</bcl:question71>";
		}
		if (this.Scoreform_sectionvi_question72!=null){
			xmlTxt+="\n<bcl:question72";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question72;
			xmlTxt+="</bcl:question72>";
		}
		if (this.Scoreform_sectionvi_question73!=null){
			xmlTxt+="\n<bcl:question73";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question73;
			xmlTxt+="</bcl:question73>";
		}
		if (this.Scoreform_sectionvi_question74!=null){
			xmlTxt+="\n<bcl:question74";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question74;
			xmlTxt+="</bcl:question74>";
		}
		if (this.Scoreform_sectionvi_question75!=null){
			xmlTxt+="\n<bcl:question75";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question75;
			xmlTxt+="</bcl:question75>";
		}
		if (this.Scoreform_sectionvi_question76!=null){
			xmlTxt+="\n<bcl:question76";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question76;
			xmlTxt+="</bcl:question76>";
		}
		if (this.Scoreform_sectionvi_question77!=null){
			xmlTxt+="\n<bcl:question77";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question77;
			xmlTxt+="</bcl:question77>";
		}
		if (this.Scoreform_sectionvi_question77desc!=null){
			xmlTxt+="\n<bcl:question77Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question77desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question77Desc>";
		}
		if (this.Scoreform_sectionvi_question78!=null){
			xmlTxt+="\n<bcl:question78";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question78;
			xmlTxt+="</bcl:question78>";
		}
		if (this.Scoreform_sectionvi_question79!=null){
			xmlTxt+="\n<bcl:question79";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question79;
			xmlTxt+="</bcl:question79>";
		}
		if (this.Scoreform_sectionvi_question79desc!=null){
			xmlTxt+="\n<bcl:question79Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question79desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question79Desc>";
		}
		if (this.Scoreform_sectionvi_question80!=null){
			xmlTxt+="\n<bcl:question80";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question80;
			xmlTxt+="</bcl:question80>";
		}
		if (this.Scoreform_sectionvi_question81!=null){
			xmlTxt+="\n<bcl:question81";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question81;
			xmlTxt+="</bcl:question81>";
		}
		if (this.Scoreform_sectionvi_question82!=null){
			xmlTxt+="\n<bcl:question82";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question82;
			xmlTxt+="</bcl:question82>";
		}
		if (this.Scoreform_sectionvi_question83!=null){
			xmlTxt+="\n<bcl:question83";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question83;
			xmlTxt+="</bcl:question83>";
		}
		if (this.Scoreform_sectionvi_question84!=null){
			xmlTxt+="\n<bcl:question84";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question84;
			xmlTxt+="</bcl:question84>";
		}
		if (this.Scoreform_sectionvi_question84desc!=null){
			xmlTxt+="\n<bcl:question84Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question84desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question84Desc>";
		}
		if (this.Scoreform_sectionvi_question85!=null){
			xmlTxt+="\n<bcl:question85";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question85;
			xmlTxt+="</bcl:question85>";
		}
		if (this.Scoreform_sectionvi_question85desc!=null){
			xmlTxt+="\n<bcl:question85Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question85desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question85Desc>";
		}
		if (this.Scoreform_sectionvi_question86!=null){
			xmlTxt+="\n<bcl:question86";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question86;
			xmlTxt+="</bcl:question86>";
		}
		if (this.Scoreform_sectionvi_question87!=null){
			xmlTxt+="\n<bcl:question87";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question87;
			xmlTxt+="</bcl:question87>";
		}
		if (this.Scoreform_sectionvi_question88!=null){
			xmlTxt+="\n<bcl:question88";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question88;
			xmlTxt+="</bcl:question88>";
		}
		if (this.Scoreform_sectionvi_question89!=null){
			xmlTxt+="\n<bcl:question89";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question89;
			xmlTxt+="</bcl:question89>";
		}
		if (this.Scoreform_sectionvi_question90!=null){
			xmlTxt+="\n<bcl:question90";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question90;
			xmlTxt+="</bcl:question90>";
		}
		if (this.Scoreform_sectionvi_question91!=null){
			xmlTxt+="\n<bcl:question91";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question91;
			xmlTxt+="</bcl:question91>";
		}
		if (this.Scoreform_sectionvi_question92!=null){
			xmlTxt+="\n<bcl:question92";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question92;
			xmlTxt+="</bcl:question92>";
		}
		if (this.Scoreform_sectionvi_question92desc!=null){
			xmlTxt+="\n<bcl:question92Desc";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question92desc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</bcl:question92Desc>";
		}
		if (this.Scoreform_sectionvi_question93!=null){
			xmlTxt+="\n<bcl:question93";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question93;
			xmlTxt+="</bcl:question93>";
		}
		if (this.Scoreform_sectionvi_question94!=null){
			xmlTxt+="\n<bcl:question94";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question94;
			xmlTxt+="</bcl:question94>";
		}
		if (this.Scoreform_sectionvi_question95!=null){
			xmlTxt+="\n<bcl:question95";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question95;
			xmlTxt+="</bcl:question95>";
		}
		if (this.Scoreform_sectionvi_question96!=null){
			xmlTxt+="\n<bcl:question96";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question96;
			xmlTxt+="</bcl:question96>";
		}
		if (this.Scoreform_sectionvi_question97!=null){
			xmlTxt+="\n<bcl:question97";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question97;
			xmlTxt+="</bcl:question97>";
		}
		if (this.Scoreform_sectionvi_question98!=null){
			xmlTxt+="\n<bcl:question98";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question98;
			xmlTxt+="</bcl:question98>";
		}
		if (this.Scoreform_sectionvi_question99!=null){
			xmlTxt+="\n<bcl:question99";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question99;
			xmlTxt+="</bcl:question99>";
		}
		if (this.Scoreform_sectionvi_question100!=null){
			xmlTxt+="\n<bcl:question100";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question100;
			xmlTxt+="</bcl:question100>";
		}
		if (this.Scoreform_sectionvi_question101!=null){
			xmlTxt+="\n<bcl:question101";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question101;
			xmlTxt+="</bcl:question101>";
		}
		if (this.Scoreform_sectionvi_question102!=null){
			xmlTxt+="\n<bcl:question102";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question102;
			xmlTxt+="</bcl:question102>";
		}
		if (this.Scoreform_sectionvi_question103!=null){
			xmlTxt+="\n<bcl:question103";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question103;
			xmlTxt+="</bcl:question103>";
		}
		if (this.Scoreform_sectionvi_question104!=null){
			xmlTxt+="\n<bcl:question104";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question104;
			xmlTxt+="</bcl:question104>";
		}
		if (this.Scoreform_sectionvi_question105!=null){
			xmlTxt+="\n<bcl:question105";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question105;
			xmlTxt+="</bcl:question105>";
		}
		if (this.Scoreform_sectionvi_question106!=null){
			xmlTxt+="\n<bcl:question106";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question106;
			xmlTxt+="</bcl:question106>";
		}
		if (this.Scoreform_sectionvi_question107!=null){
			xmlTxt+="\n<bcl:question107";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question107;
			xmlTxt+="</bcl:question107>";
		}
		if (this.Scoreform_sectionvi_question108!=null){
			xmlTxt+="\n<bcl:question108";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question108;
			xmlTxt+="</bcl:question108>";
		}
		if (this.Scoreform_sectionvi_question109!=null){
			xmlTxt+="\n<bcl:question109";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question109;
			xmlTxt+="</bcl:question109>";
		}
		if (this.Scoreform_sectionvi_question110!=null){
			xmlTxt+="\n<bcl:question110";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question110;
			xmlTxt+="</bcl:question110>";
		}
		if (this.Scoreform_sectionvi_question111!=null){
			xmlTxt+="\n<bcl:question111";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question111;
			xmlTxt+="</bcl:question111>";
		}
		if (this.Scoreform_sectionvi_question112!=null){
			xmlTxt+="\n<bcl:question112";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question112;
			xmlTxt+="</bcl:question112>";
		}
		if (this.Scoreform_sectionvi_question113!=null){
			xmlTxt+="\n<bcl:question113";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question113;
			xmlTxt+="</bcl:question113>";
		}
		if (this.Scoreform_sectionvi_question114!=null){
			xmlTxt+="\n<bcl:question114";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question114;
			xmlTxt+="</bcl:question114>";
		}
		if (this.Scoreform_sectionvi_question115!=null){
			xmlTxt+="\n<bcl:question115";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question115;
			xmlTxt+="</bcl:question115>";
		}
		if (this.Scoreform_sectionvi_question116!=null){
			xmlTxt+="\n<bcl:question116";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question116;
			xmlTxt+="</bcl:question116>";
		}
		if (this.Scoreform_sectionvi_question117!=null){
			xmlTxt+="\n<bcl:question117";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question117;
			xmlTxt+="</bcl:question117>";
		}
		if (this.Scoreform_sectionvi_question118!=null){
			xmlTxt+="\n<bcl:question118";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question118;
			xmlTxt+="</bcl:question118>";
		}
		if (this.Scoreform_sectionvi_question119!=null){
			xmlTxt+="\n<bcl:question119";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question119;
			xmlTxt+="</bcl:question119>";
		}
		if (this.Scoreform_sectionvi_question120!=null){
			xmlTxt+="\n<bcl:question120";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question120;
			xmlTxt+="</bcl:question120>";
		}
		if (this.Scoreform_sectionvi_question121!=null){
			xmlTxt+="\n<bcl:question121";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question121;
			xmlTxt+="</bcl:question121>";
		}
		if (this.Scoreform_sectionvi_question122!=null){
			xmlTxt+="\n<bcl:question122";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question122;
			xmlTxt+="</bcl:question122>";
		}
		if (this.Scoreform_sectionvi_question123!=null){
			xmlTxt+="\n<bcl:question123";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question123;
			xmlTxt+="</bcl:question123>";
		}
		if (this.Scoreform_sectionvi_question124!=null){
			xmlTxt+="\n<bcl:question124";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question124;
			xmlTxt+="</bcl:question124>";
		}
		if (this.Scoreform_sectionvi_question125!=null){
			xmlTxt+="\n<bcl:question125";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question125;
			xmlTxt+="</bcl:question125>";
		}
		if (this.Scoreform_sectionvi_question126!=null){
			xmlTxt+="\n<bcl:question126";
			xmlTxt+=">";
			xmlTxt+=this.Scoreform_sectionvi_question126;
			xmlTxt+="</bcl:question126>";
		}
				xmlTxt+="\n</bcl:sectionVI>";
			}
			}

				xmlTxt+="\n</bcl:ScoreForm>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.AnxdpRaw!=null) return true;
		if (this.AnxdpT!=null) return true;
		if (this.AnxdpNote!=null) return true;
		if (this.WthdpRaw!=null) return true;
		if (this.WthdpT!=null) return true;
		if (this.WthdpNote!=null) return true;
		if (this.SomRaw!=null) return true;
		if (this.SomT!=null) return true;
		if (this.SomNote!=null) return true;
		if (this.ThoRaw!=null) return true;
		if (this.ThoT!=null) return true;
		if (this.ThoNote!=null) return true;
		if (this.AttRaw!=null) return true;
		if (this.AttT!=null) return true;
		if (this.AttNote!=null) return true;
		if (this.RuleRaw!=null) return true;
		if (this.RuleT!=null) return true;
		if (this.RuleNote!=null) return true;
		if (this.AggRaw!=null) return true;
		if (this.AggT!=null) return true;
		if (this.AggNote!=null) return true;
		if (this.IntRaw!=null) return true;
		if (this.IntT!=null) return true;
		if (this.IntNote!=null) return true;
			if(this.Scoreform_sectionii_maritallast6mnths!=null) return true;
			if(this.Scoreform_sectioniii_handicap!=null) return true;
			if(this.Scoreform_sectionvi_question99!=null) return true;
			if(this.Scoreform_sectionvi_question98!=null) return true;
			if(this.Scoreform_sectionvi_question97!=null) return true;
			if(this.Scoreform_sectionvi_question96!=null) return true;
			if(this.Scoreform_sectionvi_question95!=null) return true;
			if(this.Scoreform_sectionvi_question94!=null) return true;
			if(this.Scoreform_sectionvi_question93!=null) return true;
			if(this.Scoreform_sectionvi_question92!=null) return true;
			if(this.Scoreform_sectionvi_question91!=null) return true;
			if(this.Scoreform_sectionvi_question90!=null) return true;
			if(this.Scoreform_sectionvi_question89!=null) return true;
			if(this.Scoreform_sectionvi_question88!=null) return true;
			if(this.Scoreform_sectionvi_question87!=null) return true;
			if(this.Scoreform_sectionvi_question86!=null) return true;
			if(this.Scoreform_sectionvi_question85!=null) return true;
			if(this.Scoreform_sectionvi_question84!=null) return true;
			if(this.Scoreform_sectionvi_question83!=null) return true;
			if(this.Scoreform_sectionvi_question82!=null) return true;
			if(this.Scoreform_sectionvi_question81!=null) return true;
			if(this.Scoreform_sectionvi_question80!=null) return true;
			if(this.Scoreform_sectionvi_question79desc!=null) return true;
			if(this.Scoreform_sectionvi_question56ddesc!=null) return true;
			if(this.Scoreform_sectionvi_question79!=null) return true;
			if(this.Scoreform_sectionvi_question78!=null) return true;
			if(this.Scoreform_sectionvi_question77!=null) return true;
			if(this.Scoreform_sectionvi_question76!=null) return true;
			if(this.Scoreform_sectionvi_question75!=null) return true;
			if(this.Scoreform_sectionvi_question74!=null) return true;
			if(this.Scoreform_sectionvi_question73!=null) return true;
			if(this.Scoreform_sectionvi_question6desc!=null) return true;
			if(this.Scoreform_sectionvi_question72!=null) return true;
			if(this.Scoreform_sectionvi_question71!=null) return true;
			if(this.Scoreform_sectionvi_question46desc!=null) return true;
			if(this.Scoreform_sectionvi_question70!=null) return true;
			if(this.Scoreform_sectionvi_question69!=null) return true;
			if(this.Scoreform_sectionvi_question68!=null) return true;
			if(this.Scoreform_sectionvi_question67!=null) return true;
			if(this.Scoreform_sectionvi_question66!=null) return true;
			if(this.Scoreform_sectionvi_question65!=null) return true;
			if(this.Scoreform_sectionvi_question64!=null) return true;
			if(this.Scoreform_sectionvi_question63!=null) return true;
			if(this.Scoreform_sectionvi_question62!=null) return true;
			if(this.Scoreform_sectionvi_question61!=null) return true;
			if(this.Scoreform_sectionvi_question60!=null) return true;
			if(this.Scoreform_sectioniii_handicapexplain!=null) return true;
			if(this.Scoreform_sectionvi_question59!=null) return true;
			if(this.Scoreform_sectionvi_question58!=null) return true;
			if(this.Scoreform_sectionvi_question57!=null) return true;
			if(this.Scoreform_sectionvi_question9desc!=null) return true;
			if(this.Scoreform_sectionvi_question55!=null) return true;
			if(this.Scoreform_sectionvi_question54!=null) return true;
			if(this.Scoreform_sectionvi_question53!=null) return true;
			if(this.Scoreform_sectionvi_question52!=null) return true;
			if(this.Scoreform_sectionvi_question51!=null) return true;
			if(this.Scoreform_sectionvi_question50!=null) return true;
			if(this.Scoreform_sectionvi_question126!=null) return true;
			if(this.Scoreform_sectionvi_question125!=null) return true;
			if(this.Scoreform_sectionvi_question124!=null) return true;
			if(this.Scoreform_sectionvi_question123!=null) return true;
			if(this.Scoreform_sectionvi_question122!=null) return true;
			if(this.Scoreform_sectionvi_question121!=null) return true;
			if(this.Scoreform_sectionvi_question120!=null) return true;
			if(this.Scoreform_sectioniv_concerns!=null) return true;
			if(this.Scoreform_sectionvi_question49!=null) return true;
			if(this.Scoreform_sectionvi_question48!=null) return true;
			if(this.Scoreform_sectionvi_question47!=null) return true;
			if(this.Scoreform_sectionvi_question46!=null) return true;
			if(this.Scoreform_sectionvi_question45!=null) return true;
			if(this.Scoreform_sectionvi_question44!=null) return true;
			if(this.Scoreform_sectionvi_question43!=null) return true;
			if(this.Scoreform_sectionvi_question119!=null) return true;
			if(this.Scoreform_sectionvi_question42!=null) return true;
			if(this.Scoreform_sectionvi_question118!=null) return true;
			if(this.Scoreform_sectionvi_question41!=null) return true;
			if(this.Scoreform_sectionvi_question117!=null) return true;
			if(this.Scoreform_sectionvi_question40!=null) return true;
			if(this.Scoreform_sectionvi_question116!=null) return true;
			if(this.Scoreform_sectionii_maritalstatus!=null) return true;
			if(this.Scoreform_sectionvi_question115!=null) return true;
			if(this.Scoreform_sectionvi_question114!=null) return true;
			if(this.Scoreform_sectionvi_question113!=null) return true;
			if(this.Scoreform_sectionvi_question112!=null) return true;
			if(this.Scoreform_sectionvi_question111!=null) return true;
			if(this.Scoreform_sectionvi_question110!=null) return true;
			if(this.Scoreform_sectionvi_question39!=null) return true;
			if(this.Scoreform_sectionvi_question38!=null) return true;
			if(this.Scoreform_sectionvi_question37!=null) return true;
			if(this.Scoreform_sectionvi_question36!=null) return true;
			if(this.Scoreform_sectionvi_question35!=null) return true;
			if(this.Scoreform_sectionvi_question34!=null) return true;
			if(this.Scoreform_sectionvi_question85desc!=null) return true;
			if(this.Scoreform_sectionvi_question33!=null) return true;
			if(this.Scoreform_sectionvi_question109!=null) return true;
			if(this.Scoreform_sectionvi_question32!=null) return true;
			if(this.Scoreform_sectionvi_question108!=null) return true;
			if(this.Scoreform_sectionvi_question31!=null) return true;
			if(this.Scoreform_sectionvi_question107!=null) return true;
			if(this.Scoreform_sectionvi_question30!=null) return true;
			if(this.Scoreform_sectionvi_question106!=null) return true;
			if(this.Scoreform_sectionvi_question105!=null) return true;
			if(this.Scoreform_sectionvi_question104!=null) return true;
			if(this.Scoreform_sectionvi_question103!=null) return true;
			if(this.Scoreform_sectionvi_question102!=null) return true;
			if(this.Scoreform_sectionvi_question101!=null) return true;
			if(this.Scoreform_sectionvi_question100!=null) return true;
			if(this.Scoreform_sectionvi_question29!=null) return true;
			if(this.Scoreform_sectionvi_question28!=null) return true;
			if(this.Scoreform_sectionvi_question9!=null) return true;
			if(this.Scoreform_sectionvi_question27!=null) return true;
			if(this.Scoreform_sectionvi_question8!=null) return true;
			if(this.Scoreform_sectionvi_question26!=null) return true;
			if(this.Scoreform_sectionvi_question7!=null) return true;
			if(this.Scoreform_sectionv_describe!=null) return true;
			if(this.Scoreform_sectionvi_question25!=null) return true;
			if(this.Scoreform_sectionvi_question6!=null) return true;
			if(this.Scoreform_sectionvi_question24!=null) return true;
			if(this.Scoreform_sectionvi_question5!=null) return true;
			if(this.Scoreform_sectionvi_question23!=null) return true;
			if(this.Scoreform_sectionvi_question4!=null) return true;
			if(this.Scoreform_sectionvi_question22!=null) return true;
			if(this.Scoreform_sectionvi_question3!=null) return true;
			if(this.Scoreform_sectionvi_question21!=null) return true;
			if(this.Scoreform_sectionvi_question2!=null) return true;
			if(this.Scoreform_sectionvi_question20!=null) return true;
			if(this.Scoreform_sectionvi_question1!=null) return true;
			if(this.Scoreform_sectionvi_question92desc!=null) return true;
			if(this.Scoreform_sectionvi_question19!=null) return true;
			if(this.Scoreform_sectionvi_question70desc!=null) return true;
			if(this.Scoreform_sectionvi_question18!=null) return true;
			if(this.Scoreform_sectionvi_question17!=null) return true;
			if(this.Scoreform_sectionvi_question16!=null) return true;
			if(this.Scoreform_sectionvi_question15!=null) return true;
			if(this.Scoreform_sectionvi_question14!=null) return true;
			if(this.Scoreform_sectionvi_question13!=null) return true;
			if(this.Scoreform_sectionvi_question12!=null) return true;
			if(this.Scoreform_sectionvi_question11!=null) return true;
			if(this.Scoreform_sectionvi_question10!=null) return true;
			if(this.Scoreform_sectionvi_question77desc!=null) return true;
			if(this.Scoreform_sectionii_maritalstatusother!=null) return true;
			if(this.Scoreform_sectionvi_question66desc!=null) return true;
			if(this.Scoreform_sectioni_friendsd!=null) return true;
			if(this.Scoreform_sectioni_friendsc!=null) return true;
			if(this.Scoreform_sectioni_friendsb!=null) return true;
			if(this.Scoreform_sectioni_friendsa!=null) return true;
			if(this.Scoreform_sectionvi_question84desc!=null) return true;
			if(this.Scoreform_sectionii_maritalh!=null) return true;
			if(this.Scoreform_sectionii_maritalg!=null) return true;
			if(this.Scoreform_sectionii_maritalf!=null) return true;
			if(this.Scoreform_sectionii_maritale!=null) return true;
			if(this.Scoreform_sectionii_maritald!=null) return true;
			if(this.Scoreform_sectionii_maritalc!=null) return true;
			if(this.Scoreform_sectionii_maritalb!=null) return true;
			if(this.Scoreform_sectionii_maritala!=null) return true;
			if(this.Scoreform_sectionvi_question29desc!=null) return true;
			if(this.Scoreform_sectionvi_question56g!=null) return true;
			if(this.Scoreform_sectionvi_question56f!=null) return true;
			if(this.Scoreform_sectionvi_question56e!=null) return true;
			if(this.Scoreform_sectionvi_question56d!=null) return true;
			if(this.Scoreform_sectionvi_question56c!=null) return true;
			if(this.Scoreform_sectionvi_question56b!=null) return true;
			if(this.Scoreform_sectionvi_question56a!=null) return true;
			if(this.Scoreform_sectionvi_question40desc!=null) return true;
			if(this.Scoreform_sectionvi_question58desc!=null) return true;
			if(this.Scoreform_resprltnsubj!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
