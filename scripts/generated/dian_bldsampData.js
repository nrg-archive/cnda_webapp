/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function dian_bldsampData(){
this.xsiType="dian:bldsampData";

	this.getSchemaElementName=function(){
		return "bldsampData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:bldsampData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Gencoll=null;


	function getGencoll() {
		return this.Gencoll;
	}
	this.getGencoll=getGencoll;


	function setGencoll(v){
		this.Gencoll=v;
	}
	this.setGencoll=setGencoll;

	this.Gendate=null;


	function getGendate() {
		return this.Gendate;
	}
	this.getGendate=getGendate;


	function setGendate(v){
		this.Gendate=v;
	}
	this.setGendate=setGendate;

	this.Gentime=null;


	function getGentime() {
		return this.Gentime;
	}
	this.getGentime=getGentime;


	function setGentime(v){
		this.Gentime=v;
	}
	this.setGentime=setGentime;

	this.Genship=null;


	function getGenship() {
		return this.Genship;
	}
	this.getGenship=getGenship;


	function setGenship(v){
		this.Genship=v;
	}
	this.setGenship=setGenship;

	this.Genshdt=null;


	function getGenshdt() {
		return this.Genshdt;
	}
	this.getGenshdt=getGenshdt;


	function setGenshdt(v){
		this.Genshdt=v;
	}
	this.setGenshdt=setGenshdt;

	this.Genshnum=null;


	function getGenshnum() {
		return this.Genshnum;
	}
	this.getGenshnum=getGenshnum;


	function setGenshnum(v){
		this.Genshnum=v;
	}
	this.setGenshnum=setGenshnum;

	this.Gendatervd=null;


	function getGendatervd() {
		return this.Gendatervd;
	}
	this.getGendatervd=getGendatervd;


	function setGendatervd(v){
		this.Gendatervd=v;
	}
	this.setGendatervd=setGendatervd;

	this.Genvolrvd=null;


	function getGenvolrvd() {
		return this.Genvolrvd;
	}
	this.getGenvolrvd=getGenvolrvd;


	function setGenvolrvd(v){
		this.Genvolrvd=v;
	}
	this.setGenvolrvd=setGenvolrvd;

	this.Genvialsrvd=null;


	function getGenvialsrvd() {
		return this.Genvialsrvd;
	}
	this.getGenvialsrvd=getGenvialsrvd;


	function setGenvialsrvd(v){
		this.Genvialsrvd=v;
	}
	this.setGenvialsrvd=setGenvialsrvd;

	this.Immcoll=null;


	function getImmcoll() {
		return this.Immcoll;
	}
	this.getImmcoll=getImmcoll;


	function setImmcoll(v){
		this.Immcoll=v;
	}
	this.setImmcoll=setImmcoll;

	this.Immdate=null;


	function getImmdate() {
		return this.Immdate;
	}
	this.getImmdate=getImmdate;


	function setImmdate(v){
		this.Immdate=v;
	}
	this.setImmdate=setImmdate;

	this.Immtime=null;


	function getImmtime() {
		return this.Immtime;
	}
	this.getImmtime=getImmtime;


	function setImmtime(v){
		this.Immtime=v;
	}
	this.setImmtime=setImmtime;

	this.Immship=null;


	function getImmship() {
		return this.Immship;
	}
	this.getImmship=getImmship;


	function setImmship(v){
		this.Immship=v;
	}
	this.setImmship=setImmship;

	this.Immshdt=null;


	function getImmshdt() {
		return this.Immshdt;
	}
	this.getImmshdt=getImmshdt;


	function setImmshdt(v){
		this.Immshdt=v;
	}
	this.setImmshdt=setImmshdt;

	this.Immshnum=null;


	function getImmshnum() {
		return this.Immshnum;
	}
	this.getImmshnum=getImmshnum;


	function setImmshnum(v){
		this.Immshnum=v;
	}
	this.setImmshnum=setImmshnum;

	this.Immdatervd=null;


	function getImmdatervd() {
		return this.Immdatervd;
	}
	this.getImmdatervd=getImmdatervd;


	function setImmdatervd(v){
		this.Immdatervd=v;
	}
	this.setImmdatervd=setImmdatervd;

	this.Immvolrvd=null;


	function getImmvolrvd() {
		return this.Immvolrvd;
	}
	this.getImmvolrvd=getImmvolrvd;


	function setImmvolrvd(v){
		this.Immvolrvd=v;
	}
	this.setImmvolrvd=setImmvolrvd;

	this.Immvialsrvd=null;


	function getImmvialsrvd() {
		return this.Immvialsrvd;
	}
	this.getImmvialsrvd=getImmvialsrvd;


	function setImmvialsrvd(v){
		this.Immvialsrvd=v;
	}
	this.setImmvialsrvd=setImmvialsrvd;

	this.Biocoll=null;


	function getBiocoll() {
		return this.Biocoll;
	}
	this.getBiocoll=getBiocoll;


	function setBiocoll(v){
		this.Biocoll=v;
	}
	this.setBiocoll=setBiocoll;

	this.Biodate=null;


	function getBiodate() {
		return this.Biodate;
	}
	this.getBiodate=getBiodate;


	function setBiodate(v){
		this.Biodate=v;
	}
	this.setBiodate=setBiodate;

	this.Biotime=null;


	function getBiotime() {
		return this.Biotime;
	}
	this.getBiotime=getBiotime;


	function setBiotime(v){
		this.Biotime=v;
	}
	this.setBiotime=setBiotime;

	this.Biofast=null;


	function getBiofast() {
		return this.Biofast;
	}
	this.getBiofast=getBiofast;


	function setBiofast(v){
		this.Biofast=v;
	}
	this.setBiofast=setBiofast;

	this.Plasvol=null;


	function getPlasvol() {
		return this.Plasvol;
	}
	this.getPlasvol=getPlasvol;


	function setPlasvol(v){
		this.Plasvol=v;
	}
	this.setPlasvol=setPlasvol;

	this.Plasvolrvd=null;


	function getPlasvolrvd() {
		return this.Plasvolrvd;
	}
	this.getPlasvolrvd=getPlasvolrvd;


	function setPlasvolrvd(v){
		this.Plasvolrvd=v;
	}
	this.setPlasvolrvd=setPlasvolrvd;

	this.Plasvialsrvd=null;


	function getPlasvialsrvd() {
		return this.Plasvialsrvd;
	}
	this.getPlasvialsrvd=getPlasvialsrvd;


	function setPlasvialsrvd(v){
		this.Plasvialsrvd=v;
	}
	this.setPlasvialsrvd=setPlasvialsrvd;

	this.Plasfroz=null;


	function getPlasfroz() {
		return this.Plasfroz;
	}
	this.getPlasfroz=getPlasfroz;


	function setPlasfroz(v){
		this.Plasfroz=v;
	}
	this.setPlasfroz=setPlasfroz;

	this.Servol=null;


	function getServol() {
		return this.Servol;
	}
	this.getServol=getServol;


	function setServol(v){
		this.Servol=v;
	}
	this.setServol=setServol;

	this.Servolrvd=null;


	function getServolrvd() {
		return this.Servolrvd;
	}
	this.getServolrvd=getServolrvd;


	function setServolrvd(v){
		this.Servolrvd=v;
	}
	this.setServolrvd=setServolrvd;

	this.Servialsrvd=null;


	function getServialsrvd() {
		return this.Servialsrvd;
	}
	this.getServialsrvd=getServialsrvd;


	function setServialsrvd(v){
		this.Servialsrvd=v;
	}
	this.setServialsrvd=setServialsrvd;

	this.Serfroz=null;


	function getSerfroz() {
		return this.Serfroz;
	}
	this.getSerfroz=getSerfroz;


	function setSerfroz(v){
		this.Serfroz=v;
	}
	this.setSerfroz=setSerfroz;

	this.Bioship=null;


	function getBioship() {
		return this.Bioship;
	}
	this.getBioship=getBioship;


	function setBioship(v){
		this.Bioship=v;
	}
	this.setBioship=setBioship;

	this.Bioshdt=null;


	function getBioshdt() {
		return this.Bioshdt;
	}
	this.getBioshdt=getBioshdt;


	function setBioshdt(v){
		this.Bioshdt=v;
	}
	this.setBioshdt=setBioshdt;

	this.Bioshnum=null;


	function getBioshnum() {
		return this.Bioshnum;
	}
	this.getBioshnum=getBioshnum;


	function setBioshnum(v){
		this.Bioshnum=v;
	}
	this.setBioshnum=setBioshnum;

	this.Biodatervd=null;


	function getBiodatervd() {
		return this.Biodatervd;
	}
	this.getBiodatervd=getBiodatervd;


	function setBiodatervd(v){
		this.Biodatervd=v;
	}
	this.setBiodatervd=setBiodatervd;

	this.Bufvol=null;


	function getBufvol() {
		return this.Bufvol;
	}
	this.getBufvol=getBufvol;


	function setBufvol(v){
		this.Bufvol=v;
	}
	this.setBufvol=setBufvol;

	this.Buffroz=null;


	function getBuffroz() {
		return this.Buffroz;
	}
	this.getBuffroz=getBuffroz;


	function setBuffroz(v){
		this.Buffroz=v;
	}
	this.setBuffroz=setBuffroz;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="GENCOLL"){
				return this.Gencoll ;
			} else 
			if(xmlPath=="GENDATE"){
				return this.Gendate ;
			} else 
			if(xmlPath=="GENTIME"){
				return this.Gentime ;
			} else 
			if(xmlPath=="GENSHIP"){
				return this.Genship ;
			} else 
			if(xmlPath=="GENSHDT"){
				return this.Genshdt ;
			} else 
			if(xmlPath=="GENSHNUM"){
				return this.Genshnum ;
			} else 
			if(xmlPath=="GENDATERVD"){
				return this.Gendatervd ;
			} else 
			if(xmlPath=="GENVOLRVD"){
				return this.Genvolrvd ;
			} else 
			if(xmlPath=="GENVIALSRVD"){
				return this.Genvialsrvd ;
			} else 
			if(xmlPath=="IMMCOLL"){
				return this.Immcoll ;
			} else 
			if(xmlPath=="IMMDATE"){
				return this.Immdate ;
			} else 
			if(xmlPath=="IMMTIME"){
				return this.Immtime ;
			} else 
			if(xmlPath=="IMMSHIP"){
				return this.Immship ;
			} else 
			if(xmlPath=="IMMSHDT"){
				return this.Immshdt ;
			} else 
			if(xmlPath=="IMMSHNUM"){
				return this.Immshnum ;
			} else 
			if(xmlPath=="IMMDATERVD"){
				return this.Immdatervd ;
			} else 
			if(xmlPath=="IMMVOLRVD"){
				return this.Immvolrvd ;
			} else 
			if(xmlPath=="IMMVIALSRVD"){
				return this.Immvialsrvd ;
			} else 
			if(xmlPath=="BIOCOLL"){
				return this.Biocoll ;
			} else 
			if(xmlPath=="BIODATE"){
				return this.Biodate ;
			} else 
			if(xmlPath=="BIOTIME"){
				return this.Biotime ;
			} else 
			if(xmlPath=="BIOFAST"){
				return this.Biofast ;
			} else 
			if(xmlPath=="PLASVOL"){
				return this.Plasvol ;
			} else 
			if(xmlPath=="PLASVOLRVD"){
				return this.Plasvolrvd ;
			} else 
			if(xmlPath=="PLASVIALSRVD"){
				return this.Plasvialsrvd ;
			} else 
			if(xmlPath=="PLASFROZ"){
				return this.Plasfroz ;
			} else 
			if(xmlPath=="SERVOL"){
				return this.Servol ;
			} else 
			if(xmlPath=="SERVOLRVD"){
				return this.Servolrvd ;
			} else 
			if(xmlPath=="SERVIALSRVD"){
				return this.Servialsrvd ;
			} else 
			if(xmlPath=="SERFROZ"){
				return this.Serfroz ;
			} else 
			if(xmlPath=="BIOSHIP"){
				return this.Bioship ;
			} else 
			if(xmlPath=="BIOSHDT"){
				return this.Bioshdt ;
			} else 
			if(xmlPath=="BIOSHNUM"){
				return this.Bioshnum ;
			} else 
			if(xmlPath=="BIODATERVD"){
				return this.Biodatervd ;
			} else 
			if(xmlPath=="BUFVOL"){
				return this.Bufvol ;
			} else 
			if(xmlPath=="BUFFROZ"){
				return this.Buffroz ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="GENCOLL"){
				this.Gencoll=value;
			} else 
			if(xmlPath=="GENDATE"){
				this.Gendate=value;
			} else 
			if(xmlPath=="GENTIME"){
				this.Gentime=value;
			} else 
			if(xmlPath=="GENSHIP"){
				this.Genship=value;
			} else 
			if(xmlPath=="GENSHDT"){
				this.Genshdt=value;
			} else 
			if(xmlPath=="GENSHNUM"){
				this.Genshnum=value;
			} else 
			if(xmlPath=="GENDATERVD"){
				this.Gendatervd=value;
			} else 
			if(xmlPath=="GENVOLRVD"){
				this.Genvolrvd=value;
			} else 
			if(xmlPath=="GENVIALSRVD"){
				this.Genvialsrvd=value;
			} else 
			if(xmlPath=="IMMCOLL"){
				this.Immcoll=value;
			} else 
			if(xmlPath=="IMMDATE"){
				this.Immdate=value;
			} else 
			if(xmlPath=="IMMTIME"){
				this.Immtime=value;
			} else 
			if(xmlPath=="IMMSHIP"){
				this.Immship=value;
			} else 
			if(xmlPath=="IMMSHDT"){
				this.Immshdt=value;
			} else 
			if(xmlPath=="IMMSHNUM"){
				this.Immshnum=value;
			} else 
			if(xmlPath=="IMMDATERVD"){
				this.Immdatervd=value;
			} else 
			if(xmlPath=="IMMVOLRVD"){
				this.Immvolrvd=value;
			} else 
			if(xmlPath=="IMMVIALSRVD"){
				this.Immvialsrvd=value;
			} else 
			if(xmlPath=="BIOCOLL"){
				this.Biocoll=value;
			} else 
			if(xmlPath=="BIODATE"){
				this.Biodate=value;
			} else 
			if(xmlPath=="BIOTIME"){
				this.Biotime=value;
			} else 
			if(xmlPath=="BIOFAST"){
				this.Biofast=value;
			} else 
			if(xmlPath=="PLASVOL"){
				this.Plasvol=value;
			} else 
			if(xmlPath=="PLASVOLRVD"){
				this.Plasvolrvd=value;
			} else 
			if(xmlPath=="PLASVIALSRVD"){
				this.Plasvialsrvd=value;
			} else 
			if(xmlPath=="PLASFROZ"){
				this.Plasfroz=value;
			} else 
			if(xmlPath=="SERVOL"){
				this.Servol=value;
			} else 
			if(xmlPath=="SERVOLRVD"){
				this.Servolrvd=value;
			} else 
			if(xmlPath=="SERVIALSRVD"){
				this.Servialsrvd=value;
			} else 
			if(xmlPath=="SERFROZ"){
				this.Serfroz=value;
			} else 
			if(xmlPath=="BIOSHIP"){
				this.Bioship=value;
			} else 
			if(xmlPath=="BIOSHDT"){
				this.Bioshdt=value;
			} else 
			if(xmlPath=="BIOSHNUM"){
				this.Bioshnum=value;
			} else 
			if(xmlPath=="BIODATERVD"){
				this.Biodatervd=value;
			} else 
			if(xmlPath=="BUFVOL"){
				this.Bufvol=value;
			} else 
			if(xmlPath=="BUFFROZ"){
				this.Buffroz=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="GENCOLL"){
			return "field_data";
		}else if (xmlPath=="GENDATE"){
			return "field_data";
		}else if (xmlPath=="GENTIME"){
			return "field_data";
		}else if (xmlPath=="GENSHIP"){
			return "field_data";
		}else if (xmlPath=="GENSHDT"){
			return "field_data";
		}else if (xmlPath=="GENSHNUM"){
			return "field_data";
		}else if (xmlPath=="GENDATERVD"){
			return "field_data";
		}else if (xmlPath=="GENVOLRVD"){
			return "field_data";
		}else if (xmlPath=="GENVIALSRVD"){
			return "field_data";
		}else if (xmlPath=="IMMCOLL"){
			return "field_data";
		}else if (xmlPath=="IMMDATE"){
			return "field_data";
		}else if (xmlPath=="IMMTIME"){
			return "field_data";
		}else if (xmlPath=="IMMSHIP"){
			return "field_data";
		}else if (xmlPath=="IMMSHDT"){
			return "field_data";
		}else if (xmlPath=="IMMSHNUM"){
			return "field_data";
		}else if (xmlPath=="IMMDATERVD"){
			return "field_data";
		}else if (xmlPath=="IMMVOLRVD"){
			return "field_data";
		}else if (xmlPath=="IMMVIALSRVD"){
			return "field_data";
		}else if (xmlPath=="BIOCOLL"){
			return "field_data";
		}else if (xmlPath=="BIODATE"){
			return "field_data";
		}else if (xmlPath=="BIOTIME"){
			return "field_data";
		}else if (xmlPath=="BIOFAST"){
			return "field_data";
		}else if (xmlPath=="PLASVOL"){
			return "field_data";
		}else if (xmlPath=="PLASVOLRVD"){
			return "field_data";
		}else if (xmlPath=="PLASVIALSRVD"){
			return "field_data";
		}else if (xmlPath=="PLASFROZ"){
			return "field_data";
		}else if (xmlPath=="SERVOL"){
			return "field_data";
		}else if (xmlPath=="SERVOLRVD"){
			return "field_data";
		}else if (xmlPath=="SERVIALSRVD"){
			return "field_data";
		}else if (xmlPath=="SERFROZ"){
			return "field_data";
		}else if (xmlPath=="BIOSHIP"){
			return "field_data";
		}else if (xmlPath=="BIOSHDT"){
			return "field_data";
		}else if (xmlPath=="BIOSHNUM"){
			return "field_data";
		}else if (xmlPath=="BIODATERVD"){
			return "field_data";
		}else if (xmlPath=="BUFVOL"){
			return "field_data";
		}else if (xmlPath=="BUFFROZ"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:BLDSAMP";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:BLDSAMP>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Gencoll!=null){
			xmlTxt+="\n<dian:GENCOLL";
			xmlTxt+=">";
			xmlTxt+=this.Gencoll;
			xmlTxt+="</dian:GENCOLL>";
		}
		if (this.Gendate!=null){
			xmlTxt+="\n<dian:GENDATE";
			xmlTxt+=">";
			xmlTxt+=this.Gendate;
			xmlTxt+="</dian:GENDATE>";
		}
		if (this.Gentime!=null){
			xmlTxt+="\n<dian:GENTIME";
			xmlTxt+=">";
			xmlTxt+=this.Gentime;
			xmlTxt+="</dian:GENTIME>";
		}
		if (this.Genship!=null){
			xmlTxt+="\n<dian:GENSHIP";
			xmlTxt+=">";
			xmlTxt+=this.Genship;
			xmlTxt+="</dian:GENSHIP>";
		}
		if (this.Genshdt!=null){
			xmlTxt+="\n<dian:GENSHDT";
			xmlTxt+=">";
			xmlTxt+=this.Genshdt;
			xmlTxt+="</dian:GENSHDT>";
		}
		if (this.Genshnum!=null){
			xmlTxt+="\n<dian:GENSHNUM";
			xmlTxt+=">";
			xmlTxt+=this.Genshnum.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:GENSHNUM>";
		}
		if (this.Gendatervd!=null){
			xmlTxt+="\n<dian:GENDATERVD";
			xmlTxt+=">";
			xmlTxt+=this.Gendatervd;
			xmlTxt+="</dian:GENDATERVD>";
		}
		if (this.Genvolrvd!=null){
			xmlTxt+="\n<dian:GENVOLRVD";
			xmlTxt+=">";
			xmlTxt+=this.Genvolrvd;
			xmlTxt+="</dian:GENVOLRVD>";
		}
		if (this.Genvialsrvd!=null){
			xmlTxt+="\n<dian:GENVIALSRVD";
			xmlTxt+=">";
			xmlTxt+=this.Genvialsrvd;
			xmlTxt+="</dian:GENVIALSRVD>";
		}
		if (this.Immcoll!=null){
			xmlTxt+="\n<dian:IMMCOLL";
			xmlTxt+=">";
			xmlTxt+=this.Immcoll;
			xmlTxt+="</dian:IMMCOLL>";
		}
		if (this.Immdate!=null){
			xmlTxt+="\n<dian:IMMDATE";
			xmlTxt+=">";
			xmlTxt+=this.Immdate;
			xmlTxt+="</dian:IMMDATE>";
		}
		if (this.Immtime!=null){
			xmlTxt+="\n<dian:IMMTIME";
			xmlTxt+=">";
			xmlTxt+=this.Immtime;
			xmlTxt+="</dian:IMMTIME>";
		}
		if (this.Immship!=null){
			xmlTxt+="\n<dian:IMMSHIP";
			xmlTxt+=">";
			xmlTxt+=this.Immship;
			xmlTxt+="</dian:IMMSHIP>";
		}
		if (this.Immshdt!=null){
			xmlTxt+="\n<dian:IMMSHDT";
			xmlTxt+=">";
			xmlTxt+=this.Immshdt;
			xmlTxt+="</dian:IMMSHDT>";
		}
		if (this.Immshnum!=null){
			xmlTxt+="\n<dian:IMMSHNUM";
			xmlTxt+=">";
			xmlTxt+=this.Immshnum.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:IMMSHNUM>";
		}
		if (this.Immdatervd!=null){
			xmlTxt+="\n<dian:IMMDATERVD";
			xmlTxt+=">";
			xmlTxt+=this.Immdatervd;
			xmlTxt+="</dian:IMMDATERVD>";
		}
		if (this.Immvolrvd!=null){
			xmlTxt+="\n<dian:IMMVOLRVD";
			xmlTxt+=">";
			xmlTxt+=this.Immvolrvd;
			xmlTxt+="</dian:IMMVOLRVD>";
		}
		if (this.Immvialsrvd!=null){
			xmlTxt+="\n<dian:IMMVIALSRVD";
			xmlTxt+=">";
			xmlTxt+=this.Immvialsrvd;
			xmlTxt+="</dian:IMMVIALSRVD>";
		}
		if (this.Biocoll!=null){
			xmlTxt+="\n<dian:BIOCOLL";
			xmlTxt+=">";
			xmlTxt+=this.Biocoll;
			xmlTxt+="</dian:BIOCOLL>";
		}
		if (this.Biodate!=null){
			xmlTxt+="\n<dian:BIODATE";
			xmlTxt+=">";
			xmlTxt+=this.Biodate;
			xmlTxt+="</dian:BIODATE>";
		}
		if (this.Biotime!=null){
			xmlTxt+="\n<dian:BIOTIME";
			xmlTxt+=">";
			xmlTxt+=this.Biotime;
			xmlTxt+="</dian:BIOTIME>";
		}
		if (this.Biofast!=null){
			xmlTxt+="\n<dian:BIOFAST";
			xmlTxt+=">";
			xmlTxt+=this.Biofast;
			xmlTxt+="</dian:BIOFAST>";
		}
		if (this.Plasvol!=null){
			xmlTxt+="\n<dian:PLASVOL";
			xmlTxt+=">";
			xmlTxt+=this.Plasvol;
			xmlTxt+="</dian:PLASVOL>";
		}
		if (this.Plasvolrvd!=null){
			xmlTxt+="\n<dian:PLASVOLRVD";
			xmlTxt+=">";
			xmlTxt+=this.Plasvolrvd;
			xmlTxt+="</dian:PLASVOLRVD>";
		}
		if (this.Plasvialsrvd!=null){
			xmlTxt+="\n<dian:PLASVIALSRVD";
			xmlTxt+=">";
			xmlTxt+=this.Plasvialsrvd;
			xmlTxt+="</dian:PLASVIALSRVD>";
		}
		if (this.Plasfroz!=null){
			xmlTxt+="\n<dian:PLASFROZ";
			xmlTxt+=">";
			xmlTxt+=this.Plasfroz;
			xmlTxt+="</dian:PLASFROZ>";
		}
		if (this.Servol!=null){
			xmlTxt+="\n<dian:SERVOL";
			xmlTxt+=">";
			xmlTxt+=this.Servol;
			xmlTxt+="</dian:SERVOL>";
		}
		if (this.Servolrvd!=null){
			xmlTxt+="\n<dian:SERVOLRVD";
			xmlTxt+=">";
			xmlTxt+=this.Servolrvd;
			xmlTxt+="</dian:SERVOLRVD>";
		}
		if (this.Servialsrvd!=null){
			xmlTxt+="\n<dian:SERVIALSRVD";
			xmlTxt+=">";
			xmlTxt+=this.Servialsrvd;
			xmlTxt+="</dian:SERVIALSRVD>";
		}
		if (this.Serfroz!=null){
			xmlTxt+="\n<dian:SERFROZ";
			xmlTxt+=">";
			xmlTxt+=this.Serfroz;
			xmlTxt+="</dian:SERFROZ>";
		}
		if (this.Bioship!=null){
			xmlTxt+="\n<dian:BIOSHIP";
			xmlTxt+=">";
			xmlTxt+=this.Bioship;
			xmlTxt+="</dian:BIOSHIP>";
		}
		if (this.Bioshdt!=null){
			xmlTxt+="\n<dian:BIOSHDT";
			xmlTxt+=">";
			xmlTxt+=this.Bioshdt;
			xmlTxt+="</dian:BIOSHDT>";
		}
		if (this.Bioshnum!=null){
			xmlTxt+="\n<dian:BIOSHNUM";
			xmlTxt+=">";
			xmlTxt+=this.Bioshnum.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:BIOSHNUM>";
		}
		if (this.Biodatervd!=null){
			xmlTxt+="\n<dian:BIODATERVD";
			xmlTxt+=">";
			xmlTxt+=this.Biodatervd;
			xmlTxt+="</dian:BIODATERVD>";
		}
		if (this.Bufvol!=null){
			xmlTxt+="\n<dian:BUFVOL";
			xmlTxt+=">";
			xmlTxt+=this.Bufvol;
			xmlTxt+="</dian:BUFVOL>";
		}
		if (this.Buffroz!=null){
			xmlTxt+="\n<dian:BUFFROZ";
			xmlTxt+=">";
			xmlTxt+=this.Buffroz;
			xmlTxt+="</dian:BUFFROZ>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Gencoll!=null) return true;
		if (this.Gendate!=null) return true;
		if (this.Gentime!=null) return true;
		if (this.Genship!=null) return true;
		if (this.Genshdt!=null) return true;
		if (this.Genshnum!=null) return true;
		if (this.Gendatervd!=null) return true;
		if (this.Genvolrvd!=null) return true;
		if (this.Genvialsrvd!=null) return true;
		if (this.Immcoll!=null) return true;
		if (this.Immdate!=null) return true;
		if (this.Immtime!=null) return true;
		if (this.Immship!=null) return true;
		if (this.Immshdt!=null) return true;
		if (this.Immshnum!=null) return true;
		if (this.Immdatervd!=null) return true;
		if (this.Immvolrvd!=null) return true;
		if (this.Immvialsrvd!=null) return true;
		if (this.Biocoll!=null) return true;
		if (this.Biodate!=null) return true;
		if (this.Biotime!=null) return true;
		if (this.Biofast!=null) return true;
		if (this.Plasvol!=null) return true;
		if (this.Plasvolrvd!=null) return true;
		if (this.Plasvialsrvd!=null) return true;
		if (this.Plasfroz!=null) return true;
		if (this.Servol!=null) return true;
		if (this.Servolrvd!=null) return true;
		if (this.Servialsrvd!=null) return true;
		if (this.Serfroz!=null) return true;
		if (this.Bioship!=null) return true;
		if (this.Bioshdt!=null) return true;
		if (this.Bioshnum!=null) return true;
		if (this.Biodatervd!=null) return true;
		if (this.Bufvol!=null) return true;
		if (this.Buffroz!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
