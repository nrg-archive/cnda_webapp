/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function dian_adverseData_adverse(){
this.xsiType="dian:adverseData_adverse";

	this.getSchemaElementName=function(){
		return "adverseData_adverse";
	}

	this.getFullSchemaElementName=function(){
		return "dian:adverseData_adverse";
	}

	this.Recno=null;


	function getRecno() {
		return this.Recno;
	}
	this.getRecno=getRecno;


	function setRecno(v){
		this.Recno=v;
	}
	this.setRecno=setRecno;

	this.Aeevent=null;


	function getAeevent() {
		return this.Aeevent;
	}
	this.getAeevent=getAeevent;


	function setAeevent(v){
		this.Aeevent=v;
	}
	this.setAeevent=setAeevent;

	this.Aeondate=null;


	function getAeondate() {
		return this.Aeondate;
	}
	this.getAeondate=getAeondate;


	function setAeondate(v){
		this.Aeondate=v;
	}
	this.setAeondate=setAeondate;

	this.Aeonyear=null;


	function getAeonyear() {
		return this.Aeonyear;
	}
	this.getAeonyear=getAeonyear;


	function setAeonyear(v){
		this.Aeonyear=v;
	}
	this.setAeonyear=setAeonyear;

	this.Aeonmonth=null;


	function getAeonmonth() {
		return this.Aeonmonth;
	}
	this.getAeonmonth=getAeonmonth;


	function setAeonmonth(v){
		this.Aeonmonth=v;
	}
	this.setAeonmonth=setAeonmonth;

	this.Aeonday=null;


	function getAeonday() {
		return this.Aeonday;
	}
	this.getAeonday=getAeonday;


	function setAeonday(v){
		this.Aeonday=v;
	}
	this.setAeonday=setAeonday;

	this.Aeongo=null;


	function getAeongo() {
		return this.Aeongo;
	}
	this.getAeongo=getAeongo;


	function setAeongo(v){
		this.Aeongo=v;
	}
	this.setAeongo=setAeongo;

	this.Aeoffdate=null;


	function getAeoffdate() {
		return this.Aeoffdate;
	}
	this.getAeoffdate=getAeoffdate;


	function setAeoffdate(v){
		this.Aeoffdate=v;
	}
	this.setAeoffdate=setAeoffdate;

	this.Aeoffyear=null;


	function getAeoffyear() {
		return this.Aeoffyear;
	}
	this.getAeoffyear=getAeoffyear;


	function setAeoffyear(v){
		this.Aeoffyear=v;
	}
	this.setAeoffyear=setAeoffyear;

	this.Aeoffmonth=null;


	function getAeoffmonth() {
		return this.Aeoffmonth;
	}
	this.getAeoffmonth=getAeoffmonth;


	function setAeoffmonth(v){
		this.Aeoffmonth=v;
	}
	this.setAeoffmonth=setAeoffmonth;

	this.Aeoffday=null;


	function getAeoffday() {
		return this.Aeoffday;
	}
	this.getAeoffday=getAeoffday;


	function setAeoffday(v){
		this.Aeoffday=v;
	}
	this.setAeoffday=setAeoffday;

	this.Aesevr=null;


	function getAesevr() {
		return this.Aesevr;
	}
	this.getAesevr=getAesevr;


	function setAesevr(v){
		this.Aesevr=v;
	}
	this.setAesevr=setAesevr;

	this.Aelp=null;


	function getAelp() {
		return this.Aelp;
	}
	this.getAelp=getAelp;


	function setAelp(v){
		this.Aelp=v;
	}
	this.setAelp=setAelp;

	this.Aerelat=null;


	function getAerelat() {
		return this.Aerelat;
	}
	this.getAerelat=getAerelat;


	function setAerelat(v){
		this.Aerelat=v;
	}
	this.setAerelat=setAerelat;

	this.Aerelato=null;


	function getAerelato() {
		return this.Aerelato;
	}
	this.getAerelato=getAerelato;


	function setAerelato(v){
		this.Aerelato=v;
	}
	this.setAerelato=setAerelato;

	this.Aecmed=null;


	function getAecmed() {
		return this.Aecmed;
	}
	this.getAecmed=getAecmed;


	function setAecmed(v){
		this.Aecmed=v;
	}
	this.setAecmed=setAecmed;

	this.Sae=null;


	function getSae() {
		return this.Sae;
	}
	this.getSae=getSae;


	function setSae(v){
		this.Sae=v;
	}
	this.setSae=setSae;

	this.Saereason=null;


	function getSaereason() {
		return this.Saereason;
	}
	this.getSaereason=getSaereason;


	function setSaereason(v){
		this.Saereason=v;
	}
	this.setSaereason=setSaereason;

	this.Aeinpt=null;


	function getAeinpt() {
		return this.Aeinpt;
	}
	this.getAeinpt=getAeinpt;


	function setAeinpt(v){
		this.Aeinpt=v;
	}
	this.setAeinpt=setAeinpt;

	this.Aedisdate=null;


	function getAedisdate() {
		return this.Aedisdate;
	}
	this.getAedisdate=getAedisdate;


	function setAedisdate(v){
		this.Aedisdate=v;
	}
	this.setAedisdate=setAedisdate;

	this.Aedeadt=null;


	function getAedeadt() {
		return this.Aedeadt;
	}
	this.getAedeadt=getAedeadt;


	function setAedeadt(v){
		this.Aedeadt=v;
	}
	this.setAedeadt=setAedeadt;

	this.Aedears=null;


	function getAedears() {
		return this.Aedears;
	}
	this.getAedears=getAedears;


	function setAedears(v){
		this.Aedears=v;
	}
	this.setAedears=setAedears;

	this.Aeautop=null;


	function getAeautop() {
		return this.Aeautop;
	}
	this.getAeautop=getAeautop;


	function setAeautop(v){
		this.Aeautop=v;
	}
	this.setAeautop=setAeautop;

	this.Aecomm=null;


	function getAecomm() {
		return this.Aecomm;
	}
	this.getAecomm=getAecomm;


	function setAecomm(v){
		this.Aecomm=v;
	}
	this.setAecomm=setAecomm;

	this.DianAdversedataAdverseId=null;


	function getDianAdversedataAdverseId() {
		return this.DianAdversedataAdverseId;
	}
	this.getDianAdversedataAdverseId=getDianAdversedataAdverseId;


	function setDianAdversedataAdverseId(v){
		this.DianAdversedataAdverseId=v;
	}
	this.setDianAdversedataAdverseId=setDianAdversedataAdverseId;

	this.adverselist_adverse_dian_advers_id_fk=null;


	this.getadverselist_adverse_dian_advers_id=function() {
		return this.adverselist_adverse_dian_advers_id_fk;
	}


	this.setadverselist_adverse_dian_advers_id=function(v){
		this.adverselist_adverse_dian_advers_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				return this.Recno ;
			} else 
			if(xmlPath=="AEEVENT"){
				return this.Aeevent ;
			} else 
			if(xmlPath=="AEONDATE"){
				return this.Aeondate ;
			} else 
			if(xmlPath=="AEONYEAR"){
				return this.Aeonyear ;
			} else 
			if(xmlPath=="AEONMONTH"){
				return this.Aeonmonth ;
			} else 
			if(xmlPath=="AEONDAY"){
				return this.Aeonday ;
			} else 
			if(xmlPath=="AEONGO"){
				return this.Aeongo ;
			} else 
			if(xmlPath=="AEOFFDATE"){
				return this.Aeoffdate ;
			} else 
			if(xmlPath=="AEOFFYEAR"){
				return this.Aeoffyear ;
			} else 
			if(xmlPath=="AEOFFMONTH"){
				return this.Aeoffmonth ;
			} else 
			if(xmlPath=="AEOFFDAY"){
				return this.Aeoffday ;
			} else 
			if(xmlPath=="AESEVR"){
				return this.Aesevr ;
			} else 
			if(xmlPath=="AELP"){
				return this.Aelp ;
			} else 
			if(xmlPath=="AERELAT"){
				return this.Aerelat ;
			} else 
			if(xmlPath=="AERELATO"){
				return this.Aerelato ;
			} else 
			if(xmlPath=="AECMED"){
				return this.Aecmed ;
			} else 
			if(xmlPath=="SAE"){
				return this.Sae ;
			} else 
			if(xmlPath=="SAEREASON"){
				return this.Saereason ;
			} else 
			if(xmlPath=="AEINPT"){
				return this.Aeinpt ;
			} else 
			if(xmlPath=="AEDISDATE"){
				return this.Aedisdate ;
			} else 
			if(xmlPath=="AEDEADT"){
				return this.Aedeadt ;
			} else 
			if(xmlPath=="AEDEARS"){
				return this.Aedears ;
			} else 
			if(xmlPath=="AEAUTOP"){
				return this.Aeautop ;
			} else 
			if(xmlPath=="AECOMM"){
				return this.Aecomm ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="dian_adverseData_adverse_id"){
				return this.DianAdversedataAdverseId ;
			} else 
			if(xmlPath=="adverselist_adverse_dian_advers_id"){
				return this.adverselist_adverse_dian_advers_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				this.Recno=value;
			} else 
			if(xmlPath=="AEEVENT"){
				this.Aeevent=value;
			} else 
			if(xmlPath=="AEONDATE"){
				this.Aeondate=value;
			} else 
			if(xmlPath=="AEONYEAR"){
				this.Aeonyear=value;
			} else 
			if(xmlPath=="AEONMONTH"){
				this.Aeonmonth=value;
			} else 
			if(xmlPath=="AEONDAY"){
				this.Aeonday=value;
			} else 
			if(xmlPath=="AEONGO"){
				this.Aeongo=value;
			} else 
			if(xmlPath=="AEOFFDATE"){
				this.Aeoffdate=value;
			} else 
			if(xmlPath=="AEOFFYEAR"){
				this.Aeoffyear=value;
			} else 
			if(xmlPath=="AEOFFMONTH"){
				this.Aeoffmonth=value;
			} else 
			if(xmlPath=="AEOFFDAY"){
				this.Aeoffday=value;
			} else 
			if(xmlPath=="AESEVR"){
				this.Aesevr=value;
			} else 
			if(xmlPath=="AELP"){
				this.Aelp=value;
			} else 
			if(xmlPath=="AERELAT"){
				this.Aerelat=value;
			} else 
			if(xmlPath=="AERELATO"){
				this.Aerelato=value;
			} else 
			if(xmlPath=="AECMED"){
				this.Aecmed=value;
			} else 
			if(xmlPath=="SAE"){
				this.Sae=value;
			} else 
			if(xmlPath=="SAEREASON"){
				this.Saereason=value;
			} else 
			if(xmlPath=="AEINPT"){
				this.Aeinpt=value;
			} else 
			if(xmlPath=="AEDISDATE"){
				this.Aedisdate=value;
			} else 
			if(xmlPath=="AEDEADT"){
				this.Aedeadt=value;
			} else 
			if(xmlPath=="AEDEARS"){
				this.Aedears=value;
			} else 
			if(xmlPath=="AEAUTOP"){
				this.Aeautop=value;
			} else 
			if(xmlPath=="AECOMM"){
				this.Aecomm=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="dian_adverseData_adverse_id"){
				this.DianAdversedataAdverseId=value;
			} else 
			if(xmlPath=="adverselist_adverse_dian_advers_id"){
				this.adverselist_adverse_dian_advers_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="RECNO"){
			return "field_data";
		}else if (xmlPath=="AEEVENT"){
			return "field_data";
		}else if (xmlPath=="AEONDATE"){
			return "field_data";
		}else if (xmlPath=="AEONYEAR"){
			return "field_data";
		}else if (xmlPath=="AEONMONTH"){
			return "field_data";
		}else if (xmlPath=="AEONDAY"){
			return "field_data";
		}else if (xmlPath=="AEONGO"){
			return "field_data";
		}else if (xmlPath=="AEOFFDATE"){
			return "field_data";
		}else if (xmlPath=="AEOFFYEAR"){
			return "field_data";
		}else if (xmlPath=="AEOFFMONTH"){
			return "field_data";
		}else if (xmlPath=="AEOFFDAY"){
			return "field_data";
		}else if (xmlPath=="AESEVR"){
			return "field_data";
		}else if (xmlPath=="AELP"){
			return "field_data";
		}else if (xmlPath=="AERELAT"){
			return "field_data";
		}else if (xmlPath=="AERELATO"){
			return "field_data";
		}else if (xmlPath=="AECMED"){
			return "field_data";
		}else if (xmlPath=="SAE"){
			return "field_data";
		}else if (xmlPath=="SAEREASON"){
			return "field_data";
		}else if (xmlPath=="AEINPT"){
			return "field_data";
		}else if (xmlPath=="AEDISDATE"){
			return "field_data";
		}else if (xmlPath=="AEDEADT"){
			return "field_data";
		}else if (xmlPath=="AEDEARS"){
			return "field_data";
		}else if (xmlPath=="AEAUTOP"){
			return "field_data";
		}else if (xmlPath=="AECOMM"){
			return "field_LONG_DATA";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:adverseData_adverse";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:adverseData_adverse>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.DianAdversedataAdverseId!=null){
				if(hiddenCount++>0)str+=",";
				str+="dian_adverseData_adverse_id=\"" + this.DianAdversedataAdverseId + "\"";
			}
			if(this.adverselist_adverse_dian_advers_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="adverselist_adverse_dian_advers_id=\"" + this.adverselist_adverse_dian_advers_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Recno!=null){
			xmlTxt+="\n<dian:RECNO";
			xmlTxt+=">";
			xmlTxt+=this.Recno;
			xmlTxt+="</dian:RECNO>";
		}
		if (this.Aeevent!=null){
			xmlTxt+="\n<dian:AEEVENT";
			xmlTxt+=">";
			xmlTxt+=this.Aeevent.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:AEEVENT>";
		}
		if (this.Aeondate!=null){
			xmlTxt+="\n<dian:AEONDATE";
			xmlTxt+=">";
			xmlTxt+=this.Aeondate;
			xmlTxt+="</dian:AEONDATE>";
		}
		if (this.Aeonyear!=null){
			xmlTxt+="\n<dian:AEONYEAR";
			xmlTxt+=">";
			xmlTxt+=this.Aeonyear;
			xmlTxt+="</dian:AEONYEAR>";
		}
		if (this.Aeonmonth!=null){
			xmlTxt+="\n<dian:AEONMONTH";
			xmlTxt+=">";
			xmlTxt+=this.Aeonmonth;
			xmlTxt+="</dian:AEONMONTH>";
		}
		if (this.Aeonday!=null){
			xmlTxt+="\n<dian:AEONDAY";
			xmlTxt+=">";
			xmlTxt+=this.Aeonday;
			xmlTxt+="</dian:AEONDAY>";
		}
		if (this.Aeongo!=null){
			xmlTxt+="\n<dian:AEONGO";
			xmlTxt+=">";
			xmlTxt+=this.Aeongo;
			xmlTxt+="</dian:AEONGO>";
		}
		if (this.Aeoffdate!=null){
			xmlTxt+="\n<dian:AEOFFDATE";
			xmlTxt+=">";
			xmlTxt+=this.Aeoffdate;
			xmlTxt+="</dian:AEOFFDATE>";
		}
		if (this.Aeoffyear!=null){
			xmlTxt+="\n<dian:AEOFFYEAR";
			xmlTxt+=">";
			xmlTxt+=this.Aeoffyear;
			xmlTxt+="</dian:AEOFFYEAR>";
		}
		if (this.Aeoffmonth!=null){
			xmlTxt+="\n<dian:AEOFFMONTH";
			xmlTxt+=">";
			xmlTxt+=this.Aeoffmonth;
			xmlTxt+="</dian:AEOFFMONTH>";
		}
		if (this.Aeoffday!=null){
			xmlTxt+="\n<dian:AEOFFDAY";
			xmlTxt+=">";
			xmlTxt+=this.Aeoffday;
			xmlTxt+="</dian:AEOFFDAY>";
		}
		if (this.Aesevr!=null){
			xmlTxt+="\n<dian:AESEVR";
			xmlTxt+=">";
			xmlTxt+=this.Aesevr;
			xmlTxt+="</dian:AESEVR>";
		}
		if (this.Aelp!=null){
			xmlTxt+="\n<dian:AELP";
			xmlTxt+=">";
			xmlTxt+=this.Aelp;
			xmlTxt+="</dian:AELP>";
		}
		if (this.Aerelat!=null){
			xmlTxt+="\n<dian:AERELAT";
			xmlTxt+=">";
			xmlTxt+=this.Aerelat;
			xmlTxt+="</dian:AERELAT>";
		}
		if (this.Aerelato!=null){
			xmlTxt+="\n<dian:AERELATO";
			xmlTxt+=">";
			xmlTxt+=this.Aerelato.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:AERELATO>";
		}
		if (this.Aecmed!=null){
			xmlTxt+="\n<dian:AECMED";
			xmlTxt+=">";
			xmlTxt+=this.Aecmed;
			xmlTxt+="</dian:AECMED>";
		}
		if (this.Sae!=null){
			xmlTxt+="\n<dian:SAE";
			xmlTxt+=">";
			xmlTxt+=this.Sae;
			xmlTxt+="</dian:SAE>";
		}
		if (this.Saereason!=null){
			xmlTxt+="\n<dian:SAEREASON";
			xmlTxt+=">";
			xmlTxt+=this.Saereason.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:SAEREASON>";
		}
		if (this.Aeinpt!=null){
			xmlTxt+="\n<dian:AEINPT";
			xmlTxt+=">";
			xmlTxt+=this.Aeinpt;
			xmlTxt+="</dian:AEINPT>";
		}
		if (this.Aedisdate!=null){
			xmlTxt+="\n<dian:AEDISDATE";
			xmlTxt+=">";
			xmlTxt+=this.Aedisdate;
			xmlTxt+="</dian:AEDISDATE>";
		}
		if (this.Aedeadt!=null){
			xmlTxt+="\n<dian:AEDEADT";
			xmlTxt+=">";
			xmlTxt+=this.Aedeadt;
			xmlTxt+="</dian:AEDEADT>";
		}
		if (this.Aedears!=null){
			xmlTxt+="\n<dian:AEDEARS";
			xmlTxt+=">";
			xmlTxt+=this.Aedears.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:AEDEARS>";
		}
		if (this.Aeautop!=null){
			xmlTxt+="\n<dian:AEAUTOP";
			xmlTxt+=">";
			xmlTxt+=this.Aeautop;
			xmlTxt+="</dian:AEAUTOP>";
		}
		if (this.Aecomm!=null){
			xmlTxt+="\n<dian:AECOMM";
			xmlTxt+=">";
			xmlTxt+=this.Aecomm.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:AECOMM>";
		}
		else{
			xmlTxt+="\n<dian:AECOMM";
			xmlTxt+="/>";
		}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.DianAdversedataAdverseId!=null) return true;
			if (this.adverselist_adverse_dian_advers_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Recno!=null) return true;
		if (this.Aeevent!=null) return true;
		if (this.Aeondate!=null) return true;
		if (this.Aeonyear!=null) return true;
		if (this.Aeonmonth!=null) return true;
		if (this.Aeonday!=null) return true;
		if (this.Aeongo!=null) return true;
		if (this.Aeoffdate!=null) return true;
		if (this.Aeoffyear!=null) return true;
		if (this.Aeoffmonth!=null) return true;
		if (this.Aeoffday!=null) return true;
		if (this.Aesevr!=null) return true;
		if (this.Aelp!=null) return true;
		if (this.Aerelat!=null) return true;
		if (this.Aerelato!=null) return true;
		if (this.Aecmed!=null) return true;
		if (this.Sae!=null) return true;
		if (this.Saereason!=null) return true;
		if (this.Aeinpt!=null) return true;
		if (this.Aedisdate!=null) return true;
		if (this.Aedeadt!=null) return true;
		if (this.Aedears!=null) return true;
		if (this.Aeautop!=null) return true;
		if (this.Aecomm!=null) return true;
		return true;//REQUIRED AECOMM
	}
}
