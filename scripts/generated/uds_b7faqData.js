/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function uds_b7faqData(){
this.xsiType="uds:b7faqData";

	this.getSchemaElementName=function(){
		return "b7faqData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:b7faqData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Bills=null;


	function getBills() {
		return this.Bills;
	}
	this.getBills=getBills;


	function setBills(v){
		this.Bills=v;
	}
	this.setBills=setBills;

	this.Taxes=null;


	function getTaxes() {
		return this.Taxes;
	}
	this.getTaxes=getTaxes;


	function setTaxes(v){
		this.Taxes=v;
	}
	this.setTaxes=setTaxes;

	this.Shopping=null;


	function getShopping() {
		return this.Shopping;
	}
	this.getShopping=getShopping;


	function setShopping(v){
		this.Shopping=v;
	}
	this.setShopping=setShopping;

	this.Games=null;


	function getGames() {
		return this.Games;
	}
	this.getGames=getGames;


	function setGames(v){
		this.Games=v;
	}
	this.setGames=setGames;

	this.Stove=null;


	function getStove() {
		return this.Stove;
	}
	this.getStove=getStove;


	function setStove(v){
		this.Stove=v;
	}
	this.setStove=setStove;

	this.Mealprep=null;


	function getMealprep() {
		return this.Mealprep;
	}
	this.getMealprep=getMealprep;


	function setMealprep(v){
		this.Mealprep=v;
	}
	this.setMealprep=setMealprep;

	this.Events=null;


	function getEvents() {
		return this.Events;
	}
	this.getEvents=getEvents;


	function setEvents(v){
		this.Events=v;
	}
	this.setEvents=setEvents;

	this.Payattn=null;


	function getPayattn() {
		return this.Payattn;
	}
	this.getPayattn=getPayattn;


	function setPayattn(v){
		this.Payattn=v;
	}
	this.setPayattn=setPayattn;

	this.Remdates=null;


	function getRemdates() {
		return this.Remdates;
	}
	this.getRemdates=getRemdates;


	function setRemdates(v){
		this.Remdates=v;
	}
	this.setRemdates=setRemdates;

	this.Travel=null;


	function getTravel() {
		return this.Travel;
	}
	this.getTravel=getTravel;


	function setTravel(v){
		this.Travel=v;
	}
	this.setTravel=setTravel;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="BILLS"){
				return this.Bills ;
			} else 
			if(xmlPath=="TAXES"){
				return this.Taxes ;
			} else 
			if(xmlPath=="SHOPPING"){
				return this.Shopping ;
			} else 
			if(xmlPath=="GAMES"){
				return this.Games ;
			} else 
			if(xmlPath=="STOVE"){
				return this.Stove ;
			} else 
			if(xmlPath=="MEALPREP"){
				return this.Mealprep ;
			} else 
			if(xmlPath=="EVENTS"){
				return this.Events ;
			} else 
			if(xmlPath=="PAYATTN"){
				return this.Payattn ;
			} else 
			if(xmlPath=="REMDATES"){
				return this.Remdates ;
			} else 
			if(xmlPath=="TRAVEL"){
				return this.Travel ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="BILLS"){
				this.Bills=value;
			} else 
			if(xmlPath=="TAXES"){
				this.Taxes=value;
			} else 
			if(xmlPath=="SHOPPING"){
				this.Shopping=value;
			} else 
			if(xmlPath=="GAMES"){
				this.Games=value;
			} else 
			if(xmlPath=="STOVE"){
				this.Stove=value;
			} else 
			if(xmlPath=="MEALPREP"){
				this.Mealprep=value;
			} else 
			if(xmlPath=="EVENTS"){
				this.Events=value;
			} else 
			if(xmlPath=="PAYATTN"){
				this.Payattn=value;
			} else 
			if(xmlPath=="REMDATES"){
				this.Remdates=value;
			} else 
			if(xmlPath=="TRAVEL"){
				this.Travel=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="BILLS"){
			return "field_data";
		}else if (xmlPath=="TAXES"){
			return "field_data";
		}else if (xmlPath=="SHOPPING"){
			return "field_data";
		}else if (xmlPath=="GAMES"){
			return "field_data";
		}else if (xmlPath=="STOVE"){
			return "field_data";
		}else if (xmlPath=="MEALPREP"){
			return "field_data";
		}else if (xmlPath=="EVENTS"){
			return "field_data";
		}else if (xmlPath=="PAYATTN"){
			return "field_data";
		}else if (xmlPath=="REMDATES"){
			return "field_data";
		}else if (xmlPath=="TRAVEL"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:B7FAQ";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:B7FAQ>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Bills!=null){
			xmlTxt+="\n<uds:BILLS";
			xmlTxt+=">";
			xmlTxt+=this.Bills;
			xmlTxt+="</uds:BILLS>";
		}
		if (this.Taxes!=null){
			xmlTxt+="\n<uds:TAXES";
			xmlTxt+=">";
			xmlTxt+=this.Taxes;
			xmlTxt+="</uds:TAXES>";
		}
		if (this.Shopping!=null){
			xmlTxt+="\n<uds:SHOPPING";
			xmlTxt+=">";
			xmlTxt+=this.Shopping;
			xmlTxt+="</uds:SHOPPING>";
		}
		if (this.Games!=null){
			xmlTxt+="\n<uds:GAMES";
			xmlTxt+=">";
			xmlTxt+=this.Games;
			xmlTxt+="</uds:GAMES>";
		}
		if (this.Stove!=null){
			xmlTxt+="\n<uds:STOVE";
			xmlTxt+=">";
			xmlTxt+=this.Stove;
			xmlTxt+="</uds:STOVE>";
		}
		if (this.Mealprep!=null){
			xmlTxt+="\n<uds:MEALPREP";
			xmlTxt+=">";
			xmlTxt+=this.Mealprep;
			xmlTxt+="</uds:MEALPREP>";
		}
		if (this.Events!=null){
			xmlTxt+="\n<uds:EVENTS";
			xmlTxt+=">";
			xmlTxt+=this.Events;
			xmlTxt+="</uds:EVENTS>";
		}
		if (this.Payattn!=null){
			xmlTxt+="\n<uds:PAYATTN";
			xmlTxt+=">";
			xmlTxt+=this.Payattn;
			xmlTxt+="</uds:PAYATTN>";
		}
		if (this.Remdates!=null){
			xmlTxt+="\n<uds:REMDATES";
			xmlTxt+=">";
			xmlTxt+=this.Remdates;
			xmlTxt+="</uds:REMDATES>";
		}
		if (this.Travel!=null){
			xmlTxt+="\n<uds:TRAVEL";
			xmlTxt+=">";
			xmlTxt+=this.Travel;
			xmlTxt+="</uds:TRAVEL>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Bills!=null) return true;
		if (this.Taxes!=null) return true;
		if (this.Shopping!=null) return true;
		if (this.Games!=null) return true;
		if (this.Stove!=null) return true;
		if (this.Mealprep!=null) return true;
		if (this.Events!=null) return true;
		if (this.Payattn!=null) return true;
		if (this.Remdates!=null) return true;
		if (this.Travel!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
