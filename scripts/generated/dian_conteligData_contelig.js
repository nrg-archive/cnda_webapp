/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function dian_conteligData_contelig(){
this.xsiType="dian:conteligData_contelig";

	this.getSchemaElementName=function(){
		return "conteligData_contelig";
	}

	this.getFullSchemaElementName=function(){
		return "dian:conteligData_contelig";
	}

	this.Recno=null;


	function getRecno() {
		return this.Recno;
	}
	this.getRecno=getRecno;


	function setRecno(v){
		this.Recno=v;
	}
	this.setRecno=setRecno;

	this.Eligmed=null;


	function getEligmed() {
		return this.Eligmed;
	}
	this.getEligmed=getEligmed;


	function setEligmed(v){
		this.Eligmed=v;
	}
	this.setEligmed=setEligmed;

	this.Elignurs=null;


	function getElignurs() {
		return this.Elignurs;
	}
	this.getElignurs=getElignurs;


	function setElignurs(v){
		this.Elignurs=v;
	}
	this.setElignurs=setElignurs;

	this.Eligmri=null;


	function getEligmri() {
		return this.Eligmri;
	}
	this.getEligmri=getEligmri;


	function setEligmri(v){
		this.Eligmri=v;
	}
	this.setEligmri=setEligmri;

	this.Eligpet=null;


	function getEligpet() {
		return this.Eligpet;
	}
	this.getEligpet=getEligpet;


	function setEligpet(v){
		this.Eligpet=v;
	}
	this.setEligpet=setEligpet;

	this.Eligpet2=null;


	function getEligpet2() {
		return this.Eligpet2;
	}
	this.getEligpet2=getEligpet2;


	function setEligpet2(v){
		this.Eligpet2=v;
	}
	this.setEligpet2=setEligpet2;

	this.Eliglp=null;


	function getEliglp() {
		return this.Eliglp;
	}
	this.getEliglp=getEliglp;


	function setEliglp(v){
		this.Eliglp=v;
	}
	this.setEliglp=setEliglp;

	this.Eligmut=null;


	function getEligmut() {
		return this.Eligmut;
	}
	this.getEligmut=getEligmut;


	function setEligmut(v){
		this.Eligmut=v;
	}
	this.setEligmut=setEligmut;

	this.Eligneur=null;


	function getEligneur() {
		return this.Eligneur;
	}
	this.getEligneur=getEligneur;


	function setEligneur(v){
		this.Eligneur=v;
	}
	this.setEligneur=setEligneur;

	this.DianConteligdataConteligId=null;


	function getDianConteligdataConteligId() {
		return this.DianConteligdataConteligId;
	}
	this.getDianConteligdataConteligId=getDianConteligdataConteligId;


	function setDianConteligdataConteligId(v){
		this.DianConteligdataConteligId=v;
	}
	this.setDianConteligdataConteligId=setDianConteligdataConteligId;

	this.conteliglist_contelig_dian_cont_id_fk=null;


	this.getconteliglist_contelig_dian_cont_id=function() {
		return this.conteliglist_contelig_dian_cont_id_fk;
	}


	this.setconteliglist_contelig_dian_cont_id=function(v){
		this.conteliglist_contelig_dian_cont_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				return this.Recno ;
			} else 
			if(xmlPath=="ELIGMED"){
				return this.Eligmed ;
			} else 
			if(xmlPath=="ELIGNURS"){
				return this.Elignurs ;
			} else 
			if(xmlPath=="ELIGMRI"){
				return this.Eligmri ;
			} else 
			if(xmlPath=="ELIGPET"){
				return this.Eligpet ;
			} else 
			if(xmlPath=="ELIGPET2"){
				return this.Eligpet2 ;
			} else 
			if(xmlPath=="ELIGLP"){
				return this.Eliglp ;
			} else 
			if(xmlPath=="ELIGMUT"){
				return this.Eligmut ;
			} else 
			if(xmlPath=="ELIGNEUR"){
				return this.Eligneur ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="dian_conteligData_contelig_id"){
				return this.DianConteligdataConteligId ;
			} else 
			if(xmlPath=="conteliglist_contelig_dian_cont_id"){
				return this.conteliglist_contelig_dian_cont_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="RECNO"){
				this.Recno=value;
			} else 
			if(xmlPath=="ELIGMED"){
				this.Eligmed=value;
			} else 
			if(xmlPath=="ELIGNURS"){
				this.Elignurs=value;
			} else 
			if(xmlPath=="ELIGMRI"){
				this.Eligmri=value;
			} else 
			if(xmlPath=="ELIGPET"){
				this.Eligpet=value;
			} else 
			if(xmlPath=="ELIGPET2"){
				this.Eligpet2=value;
			} else 
			if(xmlPath=="ELIGLP"){
				this.Eliglp=value;
			} else 
			if(xmlPath=="ELIGMUT"){
				this.Eligmut=value;
			} else 
			if(xmlPath=="ELIGNEUR"){
				this.Eligneur=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="dian_conteligData_contelig_id"){
				this.DianConteligdataConteligId=value;
			} else 
			if(xmlPath=="conteliglist_contelig_dian_cont_id"){
				this.conteliglist_contelig_dian_cont_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="RECNO"){
			return "field_data";
		}else if (xmlPath=="ELIGMED"){
			return "field_data";
		}else if (xmlPath=="ELIGNURS"){
			return "field_data";
		}else if (xmlPath=="ELIGMRI"){
			return "field_data";
		}else if (xmlPath=="ELIGPET"){
			return "field_data";
		}else if (xmlPath=="ELIGPET2"){
			return "field_data";
		}else if (xmlPath=="ELIGLP"){
			return "field_data";
		}else if (xmlPath=="ELIGMUT"){
			return "field_data";
		}else if (xmlPath=="ELIGNEUR"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:conteligData_contelig";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:conteligData_contelig>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.DianConteligdataConteligId!=null){
				if(hiddenCount++>0)str+=",";
				str+="dian_conteligData_contelig_id=\"" + this.DianConteligdataConteligId + "\"";
			}
			if(this.conteliglist_contelig_dian_cont_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="conteliglist_contelig_dian_cont_id=\"" + this.conteliglist_contelig_dian_cont_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Recno!=null){
			xmlTxt+="\n<dian:RECNO";
			xmlTxt+=">";
			xmlTxt+=this.Recno;
			xmlTxt+="</dian:RECNO>";
		}
		if (this.Eligmed!=null){
			xmlTxt+="\n<dian:ELIGMED";
			xmlTxt+=">";
			xmlTxt+=this.Eligmed;
			xmlTxt+="</dian:ELIGMED>";
		}
		if (this.Elignurs!=null){
			xmlTxt+="\n<dian:ELIGNURS";
			xmlTxt+=">";
			xmlTxt+=this.Elignurs;
			xmlTxt+="</dian:ELIGNURS>";
		}
		if (this.Eligmri!=null){
			xmlTxt+="\n<dian:ELIGMRI";
			xmlTxt+=">";
			xmlTxt+=this.Eligmri;
			xmlTxt+="</dian:ELIGMRI>";
		}
		if (this.Eligpet!=null){
			xmlTxt+="\n<dian:ELIGPET";
			xmlTxt+=">";
			xmlTxt+=this.Eligpet;
			xmlTxt+="</dian:ELIGPET>";
		}
		if (this.Eligpet2!=null){
			xmlTxt+="\n<dian:ELIGPET2";
			xmlTxt+=">";
			xmlTxt+=this.Eligpet2;
			xmlTxt+="</dian:ELIGPET2>";
		}
		if (this.Eliglp!=null){
			xmlTxt+="\n<dian:ELIGLP";
			xmlTxt+=">";
			xmlTxt+=this.Eliglp;
			xmlTxt+="</dian:ELIGLP>";
		}
		if (this.Eligmut!=null){
			xmlTxt+="\n<dian:ELIGMUT";
			xmlTxt+=">";
			xmlTxt+=this.Eligmut;
			xmlTxt+="</dian:ELIGMUT>";
		}
		if (this.Eligneur!=null){
			xmlTxt+="\n<dian:ELIGNEUR";
			xmlTxt+=">";
			xmlTxt+=this.Eligneur;
			xmlTxt+="</dian:ELIGNEUR>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.DianConteligdataConteligId!=null) return true;
			if (this.conteliglist_contelig_dian_cont_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Recno!=null) return true;
		if (this.Eligmed!=null) return true;
		if (this.Elignurs!=null) return true;
		if (this.Eligmri!=null) return true;
		if (this.Eligpet!=null) return true;
		if (this.Eligpet2!=null) return true;
		if (this.Eliglp!=null) return true;
		if (this.Eligmut!=null) return true;
		if (this.Eligneur!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
