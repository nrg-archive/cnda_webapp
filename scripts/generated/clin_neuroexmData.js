/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:01 CST 2014
 *
 */

/**
 * @author XDAT
 *
 */

function clin_neuroexmData(){
this.xsiType="clin:neuroexmData";

	this.getSchemaElementName=function(){
		return "neuroexmData";
	}

	this.getFullSchemaElementName=function(){
		return "clin:neuroexmData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Nxvisual=null;


	function getNxvisual() {
		return this.Nxvisual;
	}
	this.getNxvisual=getNxvisual;


	function setNxvisual(v){
		this.Nxvisual=v;
	}
	this.setNxvisual=setNxvisual;

	this.Nxvisdes=null;


	function getNxvisdes() {
		return this.Nxvisdes;
	}
	this.getNxvisdes=getNxvisdes;


	function setNxvisdes(v){
		this.Nxvisdes=v;
	}
	this.setNxvisdes=setNxvisdes;

	this.Nxaudito=null;


	function getNxaudito() {
		return this.Nxaudito;
	}
	this.getNxaudito=getNxaudito;


	function setNxaudito(v){
		this.Nxaudito=v;
	}
	this.setNxaudito=setNxaudito;

	this.Nxauddes=null;


	function getNxauddes() {
		return this.Nxauddes;
	}
	this.getNxauddes=getNxauddes;


	function setNxauddes(v){
		this.Nxauddes=v;
	}
	this.setNxauddes=setNxauddes;

	this.Nxtremor=null;


	function getNxtremor() {
		return this.Nxtremor;
	}
	this.getNxtremor=getNxtremor;


	function setNxtremor(v){
		this.Nxtremor=v;
	}
	this.setNxtremor=setNxtremor;

	this.Nxtredes=null;


	function getNxtredes() {
		return this.Nxtredes;
	}
	this.getNxtredes=getNxtredes;


	function setNxtredes(v){
		this.Nxtredes=v;
	}
	this.setNxtredes=setNxtredes;

	this.Nxconsci=null;


	function getNxconsci() {
		return this.Nxconsci;
	}
	this.getNxconsci=getNxconsci;


	function setNxconsci(v){
		this.Nxconsci=v;
	}
	this.setNxconsci=setNxconsci;

	this.Nxcondes=null;


	function getNxcondes() {
		return this.Nxcondes;
	}
	this.getNxcondes=getNxcondes;


	function setNxcondes(v){
		this.Nxcondes=v;
	}
	this.setNxcondes=setNxcondes;

	this.Nxnerve=null;


	function getNxnerve() {
		return this.Nxnerve;
	}
	this.getNxnerve=getNxnerve;


	function setNxnerve(v){
		this.Nxnerve=v;
	}
	this.setNxnerve=setNxnerve;

	this.Nxnerdes=null;


	function getNxnerdes() {
		return this.Nxnerdes;
	}
	this.getNxnerdes=getNxnerdes;


	function setNxnerdes(v){
		this.Nxnerdes=v;
	}
	this.setNxnerdes=setNxnerdes;

	this.Nxmotor=null;


	function getNxmotor() {
		return this.Nxmotor;
	}
	this.getNxmotor=getNxmotor;


	function setNxmotor(v){
		this.Nxmotor=v;
	}
	this.setNxmotor=setNxmotor;

	this.Nxmotdes=null;


	function getNxmotdes() {
		return this.Nxmotdes;
	}
	this.getNxmotdes=getNxmotdes;


	function setNxmotdes(v){
		this.Nxmotdes=v;
	}
	this.setNxmotdes=setNxmotdes;

	this.Nxfinger=null;


	function getNxfinger() {
		return this.Nxfinger;
	}
	this.getNxfinger=getNxfinger;


	function setNxfinger(v){
		this.Nxfinger=v;
	}
	this.setNxfinger=setNxfinger;

	this.Nxfindes=null;


	function getNxfindes() {
		return this.Nxfindes;
	}
	this.getNxfindes=getNxfindes;


	function setNxfindes(v){
		this.Nxfindes=v;
	}
	this.setNxfindes=setNxfindes;

	this.Nxheel=null;


	function getNxheel() {
		return this.Nxheel;
	}
	this.getNxheel=getNxheel;


	function setNxheel(v){
		this.Nxheel=v;
	}
	this.setNxheel=setNxheel;

	this.Nxheedes=null;


	function getNxheedes() {
		return this.Nxheedes;
	}
	this.getNxheedes=getNxheedes;


	function setNxheedes(v){
		this.Nxheedes=v;
	}
	this.setNxheedes=setNxheedes;

	this.Nxsensor=null;


	function getNxsensor() {
		return this.Nxsensor;
	}
	this.getNxsensor=getNxsensor;


	function setNxsensor(v){
		this.Nxsensor=v;
	}
	this.setNxsensor=setNxsensor;

	this.Nxsendes=null;


	function getNxsendes() {
		return this.Nxsendes;
	}
	this.getNxsendes=getNxsendes;


	function setNxsendes(v){
		this.Nxsendes=v;
	}
	this.setNxsendes=setNxsendes;

	this.Nxtendon=null;


	function getNxtendon() {
		return this.Nxtendon;
	}
	this.getNxtendon=getNxtendon;


	function setNxtendon(v){
		this.Nxtendon=v;
	}
	this.setNxtendon=setNxtendon;

	this.Nxtendes=null;


	function getNxtendes() {
		return this.Nxtendes;
	}
	this.getNxtendes=getNxtendes;


	function setNxtendes(v){
		this.Nxtendes=v;
	}
	this.setNxtendes=setNxtendes;

	this.Nxplanta=null;


	function getNxplanta() {
		return this.Nxplanta;
	}
	this.getNxplanta=getNxplanta;


	function setNxplanta(v){
		this.Nxplanta=v;
	}
	this.setNxplanta=setNxplanta;

	this.Nxplades=null;


	function getNxplades() {
		return this.Nxplades;
	}
	this.getNxplades=getNxplades;


	function setNxplades(v){
		this.Nxplades=v;
	}
	this.setNxplades=setNxplades;

	this.Nxgait=null;


	function getNxgait() {
		return this.Nxgait;
	}
	this.getNxgait=getNxgait;


	function setNxgait(v){
		this.Nxgait=v;
	}
	this.setNxgait=setNxgait;

	this.Nxgaides=null;


	function getNxgaides() {
		return this.Nxgaides;
	}
	this.getNxgaides=getNxgaides;


	function setNxgaides(v){
		this.Nxgaides=v;
	}
	this.setNxgaides=setNxgaides;

	this.Nxother=null;


	function getNxother() {
		return this.Nxother;
	}
	this.getNxother=getNxother;


	function setNxother(v){
		this.Nxother=v;
	}
	this.setNxother=setNxother;

	this.Nxothspe=null;


	function getNxothspe() {
		return this.Nxothspe;
	}
	this.getNxothspe=getNxothspe;


	function setNxothspe(v){
		this.Nxothspe=v;
	}
	this.setNxothspe=setNxothspe;

	this.Nxgencom=null;


	function getNxgencom() {
		return this.Nxgencom;
	}
	this.getNxgencom=getNxgencom;


	function setNxgencom(v){
		this.Nxgencom=v;
	}
	this.setNxgencom=setNxgencom;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="NXVISUAL"){
				return this.Nxvisual ;
			} else 
			if(xmlPath=="NXVISDES"){
				return this.Nxvisdes ;
			} else 
			if(xmlPath=="NXAUDITO"){
				return this.Nxaudito ;
			} else 
			if(xmlPath=="NXAUDDES"){
				return this.Nxauddes ;
			} else 
			if(xmlPath=="NXTREMOR"){
				return this.Nxtremor ;
			} else 
			if(xmlPath=="NXTREDES"){
				return this.Nxtredes ;
			} else 
			if(xmlPath=="NXCONSCI"){
				return this.Nxconsci ;
			} else 
			if(xmlPath=="NXCONDES"){
				return this.Nxcondes ;
			} else 
			if(xmlPath=="NXNERVE"){
				return this.Nxnerve ;
			} else 
			if(xmlPath=="NXNERDES"){
				return this.Nxnerdes ;
			} else 
			if(xmlPath=="NXMOTOR"){
				return this.Nxmotor ;
			} else 
			if(xmlPath=="NXMOTDES"){
				return this.Nxmotdes ;
			} else 
			if(xmlPath=="NXFINGER"){
				return this.Nxfinger ;
			} else 
			if(xmlPath=="NXFINDES"){
				return this.Nxfindes ;
			} else 
			if(xmlPath=="NXHEEL"){
				return this.Nxheel ;
			} else 
			if(xmlPath=="NXHEEDES"){
				return this.Nxheedes ;
			} else 
			if(xmlPath=="NXSENSOR"){
				return this.Nxsensor ;
			} else 
			if(xmlPath=="NXSENDES"){
				return this.Nxsendes ;
			} else 
			if(xmlPath=="NXTENDON"){
				return this.Nxtendon ;
			} else 
			if(xmlPath=="NXTENDES"){
				return this.Nxtendes ;
			} else 
			if(xmlPath=="NXPLANTA"){
				return this.Nxplanta ;
			} else 
			if(xmlPath=="NXPLADES"){
				return this.Nxplades ;
			} else 
			if(xmlPath=="NXGAIT"){
				return this.Nxgait ;
			} else 
			if(xmlPath=="NXGAIDES"){
				return this.Nxgaides ;
			} else 
			if(xmlPath=="NXOTHER"){
				return this.Nxother ;
			} else 
			if(xmlPath=="NXOTHSPE"){
				return this.Nxothspe ;
			} else 
			if(xmlPath=="NXGENCOM"){
				return this.Nxgencom ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="NXVISUAL"){
				this.Nxvisual=value;
			} else 
			if(xmlPath=="NXVISDES"){
				this.Nxvisdes=value;
			} else 
			if(xmlPath=="NXAUDITO"){
				this.Nxaudito=value;
			} else 
			if(xmlPath=="NXAUDDES"){
				this.Nxauddes=value;
			} else 
			if(xmlPath=="NXTREMOR"){
				this.Nxtremor=value;
			} else 
			if(xmlPath=="NXTREDES"){
				this.Nxtredes=value;
			} else 
			if(xmlPath=="NXCONSCI"){
				this.Nxconsci=value;
			} else 
			if(xmlPath=="NXCONDES"){
				this.Nxcondes=value;
			} else 
			if(xmlPath=="NXNERVE"){
				this.Nxnerve=value;
			} else 
			if(xmlPath=="NXNERDES"){
				this.Nxnerdes=value;
			} else 
			if(xmlPath=="NXMOTOR"){
				this.Nxmotor=value;
			} else 
			if(xmlPath=="NXMOTDES"){
				this.Nxmotdes=value;
			} else 
			if(xmlPath=="NXFINGER"){
				this.Nxfinger=value;
			} else 
			if(xmlPath=="NXFINDES"){
				this.Nxfindes=value;
			} else 
			if(xmlPath=="NXHEEL"){
				this.Nxheel=value;
			} else 
			if(xmlPath=="NXHEEDES"){
				this.Nxheedes=value;
			} else 
			if(xmlPath=="NXSENSOR"){
				this.Nxsensor=value;
			} else 
			if(xmlPath=="NXSENDES"){
				this.Nxsendes=value;
			} else 
			if(xmlPath=="NXTENDON"){
				this.Nxtendon=value;
			} else 
			if(xmlPath=="NXTENDES"){
				this.Nxtendes=value;
			} else 
			if(xmlPath=="NXPLANTA"){
				this.Nxplanta=value;
			} else 
			if(xmlPath=="NXPLADES"){
				this.Nxplades=value;
			} else 
			if(xmlPath=="NXGAIT"){
				this.Nxgait=value;
			} else 
			if(xmlPath=="NXGAIDES"){
				this.Nxgaides=value;
			} else 
			if(xmlPath=="NXOTHER"){
				this.Nxother=value;
			} else 
			if(xmlPath=="NXOTHSPE"){
				this.Nxothspe=value;
			} else 
			if(xmlPath=="NXGENCOM"){
				this.Nxgencom=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="NXVISUAL"){
			return "field_data";
		}else if (xmlPath=="NXVISDES"){
			return "field_data";
		}else if (xmlPath=="NXAUDITO"){
			return "field_data";
		}else if (xmlPath=="NXAUDDES"){
			return "field_data";
		}else if (xmlPath=="NXTREMOR"){
			return "field_data";
		}else if (xmlPath=="NXTREDES"){
			return "field_data";
		}else if (xmlPath=="NXCONSCI"){
			return "field_data";
		}else if (xmlPath=="NXCONDES"){
			return "field_data";
		}else if (xmlPath=="NXNERVE"){
			return "field_data";
		}else if (xmlPath=="NXNERDES"){
			return "field_data";
		}else if (xmlPath=="NXMOTOR"){
			return "field_data";
		}else if (xmlPath=="NXMOTDES"){
			return "field_data";
		}else if (xmlPath=="NXFINGER"){
			return "field_data";
		}else if (xmlPath=="NXFINDES"){
			return "field_data";
		}else if (xmlPath=="NXHEEL"){
			return "field_data";
		}else if (xmlPath=="NXHEEDES"){
			return "field_data";
		}else if (xmlPath=="NXSENSOR"){
			return "field_data";
		}else if (xmlPath=="NXSENDES"){
			return "field_data";
		}else if (xmlPath=="NXTENDON"){
			return "field_data";
		}else if (xmlPath=="NXTENDES"){
			return "field_data";
		}else if (xmlPath=="NXPLANTA"){
			return "field_data";
		}else if (xmlPath=="NXPLADES"){
			return "field_data";
		}else if (xmlPath=="NXGAIT"){
			return "field_data";
		}else if (xmlPath=="NXGAIDES"){
			return "field_data";
		}else if (xmlPath=="NXOTHER"){
			return "field_data";
		}else if (xmlPath=="NXOTHSPE"){
			return "field_data";
		}else if (xmlPath=="NXGENCOM"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<clin:NEUROEXM";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</clin:NEUROEXM>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Nxvisual!=null){
			xmlTxt+="\n<clin:NXVISUAL";
			xmlTxt+=">";
			xmlTxt+=this.Nxvisual;
			xmlTxt+="</clin:NXVISUAL>";
		}
		if (this.Nxvisdes!=null){
			xmlTxt+="\n<clin:NXVISDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxvisdes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXVISDES>";
		}
		if (this.Nxaudito!=null){
			xmlTxt+="\n<clin:NXAUDITO";
			xmlTxt+=">";
			xmlTxt+=this.Nxaudito;
			xmlTxt+="</clin:NXAUDITO>";
		}
		if (this.Nxauddes!=null){
			xmlTxt+="\n<clin:NXAUDDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxauddes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXAUDDES>";
		}
		if (this.Nxtremor!=null){
			xmlTxt+="\n<clin:NXTREMOR";
			xmlTxt+=">";
			xmlTxt+=this.Nxtremor;
			xmlTxt+="</clin:NXTREMOR>";
		}
		if (this.Nxtredes!=null){
			xmlTxt+="\n<clin:NXTREDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxtredes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXTREDES>";
		}
		if (this.Nxconsci!=null){
			xmlTxt+="\n<clin:NXCONSCI";
			xmlTxt+=">";
			xmlTxt+=this.Nxconsci;
			xmlTxt+="</clin:NXCONSCI>";
		}
		if (this.Nxcondes!=null){
			xmlTxt+="\n<clin:NXCONDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxcondes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXCONDES>";
		}
		if (this.Nxnerve!=null){
			xmlTxt+="\n<clin:NXNERVE";
			xmlTxt+=">";
			xmlTxt+=this.Nxnerve;
			xmlTxt+="</clin:NXNERVE>";
		}
		if (this.Nxnerdes!=null){
			xmlTxt+="\n<clin:NXNERDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxnerdes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXNERDES>";
		}
		if (this.Nxmotor!=null){
			xmlTxt+="\n<clin:NXMOTOR";
			xmlTxt+=">";
			xmlTxt+=this.Nxmotor;
			xmlTxt+="</clin:NXMOTOR>";
		}
		if (this.Nxmotdes!=null){
			xmlTxt+="\n<clin:NXMOTDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxmotdes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXMOTDES>";
		}
		if (this.Nxfinger!=null){
			xmlTxt+="\n<clin:NXFINGER";
			xmlTxt+=">";
			xmlTxt+=this.Nxfinger;
			xmlTxt+="</clin:NXFINGER>";
		}
		if (this.Nxfindes!=null){
			xmlTxt+="\n<clin:NXFINDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxfindes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXFINDES>";
		}
		if (this.Nxheel!=null){
			xmlTxt+="\n<clin:NXHEEL";
			xmlTxt+=">";
			xmlTxt+=this.Nxheel;
			xmlTxt+="</clin:NXHEEL>";
		}
		if (this.Nxheedes!=null){
			xmlTxt+="\n<clin:NXHEEDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxheedes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXHEEDES>";
		}
		if (this.Nxsensor!=null){
			xmlTxt+="\n<clin:NXSENSOR";
			xmlTxt+=">";
			xmlTxt+=this.Nxsensor;
			xmlTxt+="</clin:NXSENSOR>";
		}
		if (this.Nxsendes!=null){
			xmlTxt+="\n<clin:NXSENDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxsendes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXSENDES>";
		}
		if (this.Nxtendon!=null){
			xmlTxt+="\n<clin:NXTENDON";
			xmlTxt+=">";
			xmlTxt+=this.Nxtendon;
			xmlTxt+="</clin:NXTENDON>";
		}
		if (this.Nxtendes!=null){
			xmlTxt+="\n<clin:NXTENDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxtendes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXTENDES>";
		}
		if (this.Nxplanta!=null){
			xmlTxt+="\n<clin:NXPLANTA";
			xmlTxt+=">";
			xmlTxt+=this.Nxplanta;
			xmlTxt+="</clin:NXPLANTA>";
		}
		if (this.Nxplades!=null){
			xmlTxt+="\n<clin:NXPLADES";
			xmlTxt+=">";
			xmlTxt+=this.Nxplades.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXPLADES>";
		}
		if (this.Nxgait!=null){
			xmlTxt+="\n<clin:NXGAIT";
			xmlTxt+=">";
			xmlTxt+=this.Nxgait;
			xmlTxt+="</clin:NXGAIT>";
		}
		if (this.Nxgaides!=null){
			xmlTxt+="\n<clin:NXGAIDES";
			xmlTxt+=">";
			xmlTxt+=this.Nxgaides.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXGAIDES>";
		}
		if (this.Nxother!=null){
			xmlTxt+="\n<clin:NXOTHER";
			xmlTxt+=">";
			xmlTxt+=this.Nxother;
			xmlTxt+="</clin:NXOTHER>";
		}
		if (this.Nxothspe!=null){
			xmlTxt+="\n<clin:NXOTHSPE";
			xmlTxt+=">";
			xmlTxt+=this.Nxothspe.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXOTHSPE>";
		}
		if (this.Nxgencom!=null){
			xmlTxt+="\n<clin:NXGENCOM";
			xmlTxt+=">";
			xmlTxt+=this.Nxgencom.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</clin:NXGENCOM>";
		}
		else{
			xmlTxt+="\n<clin:NXGENCOM";
			xmlTxt+="/>";
		}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Nxvisual!=null) return true;
		if (this.Nxvisdes!=null) return true;
		if (this.Nxaudito!=null) return true;
		if (this.Nxauddes!=null) return true;
		if (this.Nxtremor!=null) return true;
		if (this.Nxtredes!=null) return true;
		if (this.Nxconsci!=null) return true;
		if (this.Nxcondes!=null) return true;
		if (this.Nxnerve!=null) return true;
		if (this.Nxnerdes!=null) return true;
		if (this.Nxmotor!=null) return true;
		if (this.Nxmotdes!=null) return true;
		if (this.Nxfinger!=null) return true;
		if (this.Nxfindes!=null) return true;
		if (this.Nxheel!=null) return true;
		if (this.Nxheedes!=null) return true;
		if (this.Nxsensor!=null) return true;
		if (this.Nxsendes!=null) return true;
		if (this.Nxtendon!=null) return true;
		if (this.Nxtendes!=null) return true;
		if (this.Nxplanta!=null) return true;
		if (this.Nxplades!=null) return true;
		if (this.Nxgait!=null) return true;
		if (this.Nxgaides!=null) return true;
		if (this.Nxother!=null) return true;
		if (this.Nxothspe!=null) return true;
		if (this.Nxgencom!=null) return true;
		return true;//REQUIRED NXGENCOM
	}
}
