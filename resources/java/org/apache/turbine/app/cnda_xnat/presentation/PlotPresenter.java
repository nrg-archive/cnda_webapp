//Copyright Washington University School of Medicine All Rights Reserved
/*
 * Created on Feb 26, 2007
 *
 */
package org.apache.turbine.app.cnda_xnat.presentation;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.nrg.xdat.collections.DisplayFieldCollection.DisplayFieldNotFoundException;
import org.nrg.xdat.display.DisplayFieldReferenceI;
import org.nrg.xdat.display.DisplayManager;
import org.nrg.xdat.display.ElementDisplay;
import org.nrg.xdat.presentation.CSVPresenter;
import org.nrg.xdat.presentation.PresentationA;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xft.XFTTable;
import org.nrg.xft.XFTTableI;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.utils.StringUtils;

public class PlotPresenter extends PresentationA {
    static org.apache.log4j.Logger logger = Logger.getLogger(PlotPresenter.class);

    public String getVersionExtension(){return "csv";}

    public XFTTableI formatTable(XFTTableI table,DisplaySearch search) throws XFTInitException,ElementNotFoundException
    {
        return formatTable(table,search,false);
    }

    @Override
    public XFTTableI formatTable(XFTTableI table, DisplaySearch search,
            boolean allowDiffs) throws XFTInitException,ElementNotFoundException {
        //CREATE THE NEW TABLE
        PlotTable plot = new PlotTable();
        
        //LOAD the element display for the root element of the search
        ElementDisplay ed = DisplayManager.GetElementDisplay(getRootElement().getFullXMLName());
        
        //LOAD the visible fields for that elemetn display
        ArrayList visibleFields = this.getVisibleFields(ed,search);

        ArrayList columnHeaders = new ArrayList();

        //ADDED FOR PlotTable
        ArrayList columnTypes = new ArrayList();
        
        //IN CLAUSE Search
        //When users submit a search using the big text area, they are allowed to include a list of ids 
        //to match to.  I call this an 'In Clause' search.  It is handled specially, because the listing 
        //should include all of the searched ids (even though they don't correspond to predefined dispaly fields).  
        //Becasue they don't correspond to display fields, they must be handled manually by the presenter.
        int counter = search.getInClauses().size();
        if (counter>0)
        {
            for(int i=0;i<counter;i++)
            {
                //FOR each in clause search, include a new column (without a header)
                columnHeaders.add("");
                columnTypes.add("string");
            }
        }
        
        //POPULATE HEADERS
        Iterator fields = visibleFields.iterator();
        while (fields.hasNext())
        {
            DisplayFieldReferenceI df = (DisplayFieldReferenceI)fields.next();
            
            if (!df.isHtmlContent())
            {
                columnHeaders.add(df.getHeader());
                try {
                    String type = df.getDisplayField().getDataType();
                    if (type!=null){
                        columnTypes.add(type);
                    }else{
                        columnTypes.add("");
                    }
                } catch (DisplayFieldNotFoundException e) {
                    logger.error("",e);
                    columnTypes.add("");
                }
            }
        }
        
        //initialize the table with the given headers.
        plot.initTable(columnHeaders);
        plot.setTypes(columnTypes);
        
        //POPULATE DATA
        table.resetRowCursor();

        
        while (table.hasMoreRows())
        {
            //FOR each row in the old table - create a new row in the new table
            Hashtable row = table.nextRowHash();
            Object[] newRow = new Object[columnHeaders.size()];

            counter = search.getInClauses().size();
            if (counter>0)
            {
                for(int i=0;i<counter;i++)
                {
                    //GET THE searched values 'search_field1','search_field2',etc
                    Object v = row.get("search_field"+i);
                    if (v!=null)
                    {
                        //INCLUDE THE value as the first column value
                        newRow[i] = v;
                    }else{
                        newRow[i] = "";
                    }
                }
            }
            
            
            fields = visibleFields.iterator();
            while (fields.hasNext())
            {
                //for each visible fields
                DisplayFieldReferenceI dfr = (DisplayFieldReferenceI)fields.next();
                if(!dfr.isHtmlContent())
                {
                    try {
                        //INCLUDE IF TESTING HERE TO FILTER OUT UNWANTED COLUMNS
                        //example would only include float and integer values
                        //if (dfr.getDisplayField().getDataType().equals("float") || dfr.getDisplayField().getDataType().equals("integer")){
                        
                        //Build Old header String
                        String oldHeader = "";
                        if (dfr.getElementName().equalsIgnoreCase(search.getRootElement().getFullXMLName()))
                        {
                            oldHeader = dfr.getRowID().toLowerCase();
                        }else{
                            oldHeader = dfr.getElementSQLName().toLowerCase() + "_" + dfr.getRowID().toLowerCase();
                        }
                        
                        //retrieve value from old row
                        Object v = row.get(oldHeader);
                        
                        if (v != null)
                        {
                            //place in new row
                            newRow[counter] = v;
                        }
                        
                        //INCLUDE FOR IF TEST
                        //}
                    } catch (XFTInitException e) {
                        logger.error("",e);
                    } catch (ElementNotFoundException e) {
                        logger.error("",e);
                    }

                    counter++;
                }
            }
            
            //insert new row
            plot.insertRow(newRow);
        }
        

        return plot;
    }

}
