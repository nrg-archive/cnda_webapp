/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.util.ArrayList;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;

public class ImageUploadSession extends SecureReport {
    
    public void finalProcessing(RunData data, Context context) {
        XnatPetsessiondata dbPet = new XnatPetsessiondata(item);
        ArrayList matchingFiles = dbPet.getReconstructedFileByContent("MPRAGE_222");
        if (matchingFiles == null || matchingFiles.size() == 0) {
            data.setMessage(dbPet.getId() + " hasnt yet been built. Please upload after session is built");
            data.setScreen("ClosePage.vm");
        }else {
            context.put("relative_path", data.getParameters().get("relative_path"));
        }
    }
}
