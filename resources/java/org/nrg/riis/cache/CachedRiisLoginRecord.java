package org.nrg.riis.cache;

import java.util.Date;
import org.nrg.riis.entities.RiisLoginRecord;

/**
 * Class for holding a RiisLoginRecord along with its associated 
 * DICOM Study Date/Time. 
 */
public final class CachedRiisLoginRecord{
   
   private final Date             _date;
   private final RiisLoginRecord  _loginRecord;
   
   /**
    * Constructor. Sets the date and login record we wish to hold. 
    * @param date - The DICOM study date string associated with the RiisLoginRecord
    * @param rec - The RiisLoginRecord we want to store. 
    */
   public CachedRiisLoginRecord(final Date date, final RiisLoginRecord rec){
      _date        = (Date)date.clone();
      _loginRecord = rec;
   }
   
   //Get the date associated with this object.
   public Date getDate(){ return _date; }
   
   //Get the loginRecord associated with this object.
   public RiisLoginRecord getLoginRecord(){ return _loginRecord; }
}