/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsA5subhstdata extends BaseUdsA5subhstdata {

	public UdsA5subhstdata(ItemI item)
	{
		super(item);
	}

	public UdsA5subhstdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA5subhstdata(UserI user)
	 **/
	public UdsA5subhstdata()
	{}

	public UdsA5subhstdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
