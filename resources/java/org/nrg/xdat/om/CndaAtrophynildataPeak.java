/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaAtrophynildataPeak extends BaseCndaAtrophynildataPeak {

	public CndaAtrophynildataPeak(ItemI item)
	{
		super(item);
	}

	public CndaAtrophynildataPeak(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaAtrophynildataPeak(UserI user)
	 **/
	public CndaAtrophynildataPeak()
	{}

	public CndaAtrophynildataPeak(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
