/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:47:27 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class ArcProjectDescendant extends BaseArcProjectDescendant {

	public ArcProjectDescendant(ItemI item)
	{
		super(item);
	}

	public ArcProjectDescendant(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseArcProjectDescendant(UserI user)
	 **/
	public ArcProjectDescendant()
	{}

	public ArcProjectDescendant(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
