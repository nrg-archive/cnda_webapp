/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaVitalsdata extends BaseCndaVitalsdata {

	public CndaVitalsdata(ItemI item)
	{
		super(item);
	}

	public CndaVitalsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaVitalsdata(UserI user)
	 **/
	public CndaVitalsdata()
	{}

	public CndaVitalsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
