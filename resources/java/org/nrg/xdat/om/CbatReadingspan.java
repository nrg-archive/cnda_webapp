/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CbatReadingspan extends BaseCbatReadingspan {

	public CbatReadingspan(ItemI item)
	{
		super(item);
	}

	public CbatReadingspan(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatReadingspan(UserI user)
	 **/
	public CbatReadingspan()
	{}

	public CbatReadingspan(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
