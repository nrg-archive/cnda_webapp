/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class ClinPregdata extends BaseClinPregdata {

	public ClinPregdata(ItemI item)
	{
		super(item);
	}

	public ClinPregdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseClinPregdata(UserI user)
	 **/
	public ClinPregdata()
	{}

	public ClinPregdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
