/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CondrBraincolldataSurgicalext extends BaseCondrBraincolldataSurgicalext {

	public CondrBraincolldataSurgicalext(ItemI item)
	{
		super(item);
	}

	public CondrBraincolldataSurgicalext(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrBraincolldataSurgicalext(UserI user)
	 **/
	public CondrBraincolldataSurgicalext()
	{}

	public CondrBraincolldataSurgicalext(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
