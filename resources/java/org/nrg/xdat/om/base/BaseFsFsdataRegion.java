/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseFsFsdataRegion extends AutoFsFsdataRegion {

	public BaseFsFsdataRegion(ItemI item)
	{
		super(item);
	}

	public BaseFsFsdataRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsFsdataRegion(UserI user)
	 **/
	public BaseFsFsdataRegion()
	{}

	public BaseFsFsdataRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
