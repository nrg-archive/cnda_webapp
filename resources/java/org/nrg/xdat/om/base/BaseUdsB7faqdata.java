/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsB7faqdata extends AutoUdsB7faqdata {

	public BaseUdsB7faqdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsB7faqdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB7faqdata(UserI user)
	 **/
	public BaseUdsB7faqdata()
	{}

	public BaseUdsB7faqdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
