/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaVitalsdataPulse extends AutoCndaVitalsdataPulse {

	public BaseCndaVitalsdataPulse(ItemI item)
	{
		super(item);
	}

	public BaseCndaVitalsdataPulse(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaVitalsdataPulse(UserI user)
	 **/
	public BaseCndaVitalsdataPulse()
	{}

	public BaseCndaVitalsdataPulse(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
