/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaVitalsdata extends AutoCndaVitalsdata {

	public BaseCndaVitalsdata(ItemI item)
	{
		super(item);
	}

	public BaseCndaVitalsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaVitalsdata(UserI user)
	 **/
	public BaseCndaVitalsdata()
	{}

	public BaseCndaVitalsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
