/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSfAdverseevent extends XnatSubjectassessordata implements org.nrg.xdat.model.SfAdverseeventI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSfAdverseevent.class);
	public static String SCHEMA_ELEMENT_NAME="sf:adverseEvent";

	public AutoSfAdverseevent(ItemI item)
	{
		super(item);
	}

	public AutoSfAdverseevent(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSfAdverseevent(UserI user)
	 **/
	public AutoSfAdverseevent(){}

	public AutoSfAdverseevent(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "sf:adverseEvent";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Eventdescription=null;

	/**
	 * @return Returns the eventDescription.
	 */
	public String getEventdescription(){
		try{
			if (_Eventdescription==null){
				_Eventdescription=getStringProperty("eventDescription");
				return _Eventdescription;
			}else {
				return _Eventdescription;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for eventDescription.
	 * @param v Value to Set.
	 */
	public void setEventdescription(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/eventDescription",v);
		_Eventdescription=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Severity=null;

	/**
	 * @return Returns the severity.
	 */
	public String getSeverity(){
		try{
			if (_Severity==null){
				_Severity=getStringProperty("severity");
				return _Severity;
			}else {
				return _Severity;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for severity.
	 * @param v Value to Set.
	 */
	public void setSeverity(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/severity",v);
		_Severity=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Attribution=null;

	/**
	 * @return Returns the attribution.
	 */
	public String getAttribution(){
		try{
			if (_Attribution==null){
				_Attribution=getStringProperty("attribution");
				return _Attribution;
			}else {
				return _Attribution;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for attribution.
	 * @param v Value to Set.
	 */
	public void setAttribution(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/attribution",v);
		_Attribution=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Attributionnotes=null;

	/**
	 * @return Returns the attributionNotes.
	 */
	public String getAttributionnotes(){
		try{
			if (_Attributionnotes==null){
				_Attributionnotes=getStringProperty("attributionNotes");
				return _Attributionnotes;
			}else {
				return _Attributionnotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for attributionNotes.
	 * @param v Value to Set.
	 */
	public void setAttributionnotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/attributionNotes",v);
		_Attributionnotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Expected=null;

	/**
	 * @return Returns the expected.
	 */
	public Boolean getExpected() {
		try{
			if (_Expected==null){
				_Expected=getBooleanProperty("expected");
				return _Expected;
			}else {
				return _Expected;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for expected.
	 * @param v Value to Set.
	 */
	public void setExpected(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/expected",v);
		_Expected=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Onsetdate=null;

	/**
	 * @return Returns the onsetDate.
	 */
	public Object getOnsetdate(){
		try{
			if (_Onsetdate==null){
				_Onsetdate=getProperty("onsetDate");
				return _Onsetdate;
			}else {
				return _Onsetdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for onsetDate.
	 * @param v Value to Set.
	 */
	public void setOnsetdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/onsetDate",v);
		_Onsetdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Resolutiondate=null;

	/**
	 * @return Returns the resolutionDate.
	 */
	public Object getResolutiondate(){
		try{
			if (_Resolutiondate==null){
				_Resolutiondate=getProperty("resolutionDate");
				return _Resolutiondate;
			}else {
				return _Resolutiondate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for resolutionDate.
	 * @param v Value to Set.
	 */
	public void setResolutiondate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/resolutionDate",v);
		_Resolutiondate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Ongoing=null;

	/**
	 * @return Returns the ongoing.
	 */
	public Boolean getOngoing() {
		try{
			if (_Ongoing==null){
				_Ongoing=getBooleanProperty("ongoing");
				return _Ongoing;
			}else {
				return _Ongoing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ongoing.
	 * @param v Value to Set.
	 */
	public void setOngoing(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/ongoing",v);
		_Ongoing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Actiontaken=null;

	/**
	 * @return Returns the actionTaken.
	 */
	public String getActiontaken(){
		try{
			if (_Actiontaken==null){
				_Actiontaken=getStringProperty("actionTaken");
				return _Actiontaken;
			}else {
				return _Actiontaken;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for actionTaken.
	 * @param v Value to Set.
	 */
	public void setActiontaken(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/actionTaken",v);
		_Actiontaken=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Outcome=null;

	/**
	 * @return Returns the outcome.
	 */
	public String getOutcome(){
		try{
			if (_Outcome==null){
				_Outcome=getStringProperty("outcome");
				return _Outcome;
			}else {
				return _Outcome;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for outcome.
	 * @param v Value to Set.
	 */
	public void setOutcome(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/outcome",v);
		_Outcome=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Serious=null;

	/**
	 * @return Returns the serious.
	 */
	public Boolean getSerious() {
		try{
			if (_Serious==null){
				_Serious=getBooleanProperty("serious");
				return _Serious;
			}else {
				return _Serious;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for serious.
	 * @param v Value to Set.
	 */
	public void setSerious(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/serious",v);
		_Serious=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Seriousreason=null;

	/**
	 * @return Returns the seriousReason.
	 */
	public String getSeriousreason(){
		try{
			if (_Seriousreason==null){
				_Seriousreason=getStringProperty("seriousReason");
				return _Seriousreason;
			}else {
				return _Seriousreason;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for seriousReason.
	 * @param v Value to Set.
	 */
	public void setSeriousreason(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/seriousReason",v);
		_Seriousreason=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Adverseeventnotes=null;

	/**
	 * @return Returns the adverseEventNotes.
	 */
	public String getAdverseeventnotes(){
		try{
			if (_Adverseeventnotes==null){
				_Adverseeventnotes=getStringProperty("adverseEventNotes");
				return _Adverseeventnotes;
			}else {
				return _Adverseeventnotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for adverseEventNotes.
	 * @param v Value to Set.
	 */
	public void setAdverseeventnotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/adverseEventNotes",v);
		_Adverseeventnotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SfAdverseevent> getAllSfAdverseevents(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfAdverseevent> al = new ArrayList<org.nrg.xdat.om.SfAdverseevent>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfAdverseevent> getSfAdverseeventsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfAdverseevent> al = new ArrayList<org.nrg.xdat.om.SfAdverseevent>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfAdverseevent> getSfAdverseeventsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfAdverseevent> al = new ArrayList<org.nrg.xdat.om.SfAdverseevent>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SfAdverseevent getSfAdverseeventsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("sf:adverseEvent/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SfAdverseevent) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
