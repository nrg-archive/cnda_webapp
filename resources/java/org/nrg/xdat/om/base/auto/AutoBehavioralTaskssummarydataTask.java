/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoBehavioralTaskssummarydataTask extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.BehavioralTaskssummarydataTaskI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoBehavioralTaskssummarydataTask.class);
	public static String SCHEMA_ELEMENT_NAME="behavioral:tasksSummaryData_task";

	public AutoBehavioralTaskssummarydataTask(ItemI item)
	{
		super(item);
	}

	public AutoBehavioralTaskssummarydataTask(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoBehavioralTaskssummarydataTask(UserI user)
	 **/
	public AutoBehavioralTaskssummarydataTask(){}

	public AutoBehavioralTaskssummarydataTask(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "behavioral:tasksSummaryData_task";
	}
	 private ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun> _Run =null;

	/**
	 * run
	 * @return Returns an List of org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun
	 */
	public <A extends org.nrg.xdat.model.BehavioralTaskssummarydataTaskRunI> List<A> getRun() {
		try{
			if (_Run==null){
				_Run=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("run"));
			}
			return (List<A>) _Run;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTaskRun>();}
	}

	/**
	 * Sets the value for run.
	 * @param v Value to Set.
	 */
	public void setRun(ItemI v) throws Exception{
		_Run =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/run",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/run",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * run
	 * Adds org.nrg.xdat.model.BehavioralTaskssummarydataTaskRunI
	 */
	public <A extends org.nrg.xdat.model.BehavioralTaskssummarydataTaskRunI> void addRun(A item) throws Exception{
	setRun((ItemI)item);
	}

	/**
	 * Removes the run of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeRun(int index) throws java.lang.IndexOutOfBoundsException {
		_Run =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/run",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.BehavioralStatistics _Statisticsacrossruns =null;

	/**
	 * StatisticsAcrossRuns
	 * @return org.nrg.xdat.om.BehavioralStatistics
	 */
	public org.nrg.xdat.om.BehavioralStatistics getStatisticsacrossruns() {
		try{
			if (_Statisticsacrossruns==null){
				_Statisticsacrossruns=((BehavioralStatistics)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("StatisticsAcrossRuns")));
				return _Statisticsacrossruns;
			}else {
				return _Statisticsacrossruns;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for StatisticsAcrossRuns.
	 * @param v Value to Set.
	 */
	public void setStatisticsacrossruns(ItemI v) throws Exception{
		_Statisticsacrossruns =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/StatisticsAcrossRuns",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/StatisticsAcrossRuns",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * StatisticsAcrossRuns
	 * set org.nrg.xdat.model.BehavioralStatisticsI
	 */
	public <A extends org.nrg.xdat.model.BehavioralStatisticsI> void setStatisticsacrossruns(A item) throws Exception{
	setStatisticsacrossruns((ItemI)item);
	}

	/**
	 * Removes the StatisticsAcrossRuns.
	 * */
	public void removeStatisticsacrossruns() {
		_Statisticsacrossruns =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/StatisticsAcrossRuns",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _StatisticsacrossrunsFK=null;

	/**
	 * @return Returns the behavioral:tasksSummaryData_task/statisticsacrossruns_behavioral_statistics_id.
	 */
	public Integer getStatisticsacrossrunsFK(){
		try{
			if (_StatisticsacrossrunsFK==null){
				_StatisticsacrossrunsFK=getIntegerProperty("behavioral:tasksSummaryData_task/statisticsacrossruns_behavioral_statistics_id");
				return _StatisticsacrossrunsFK;
			}else {
				return _StatisticsacrossrunsFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for behavioral:tasksSummaryData_task/statisticsacrossruns_behavioral_statistics_id.
	 * @param v Value to Set.
	 */
	public void setStatisticsacrossrunsFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/statisticsacrossruns_behavioral_statistics_id",v);
		_StatisticsacrossrunsFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _BehavioralTaskssummarydataTaskId=null;

	/**
	 * @return Returns the behavioral_tasksSummaryData_task_id.
	 */
	public Integer getBehavioralTaskssummarydataTaskId() {
		try{
			if (_BehavioralTaskssummarydataTaskId==null){
				_BehavioralTaskssummarydataTaskId=getIntegerProperty("behavioral_tasksSummaryData_task_id");
				return _BehavioralTaskssummarydataTaskId;
			}else {
				return _BehavioralTaskssummarydataTaskId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for behavioral_tasksSummaryData_task_id.
	 * @param v Value to Set.
	 */
	public void setBehavioralTaskssummarydataTaskId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/behavioral_tasksSummaryData_task_id",v);
		_BehavioralTaskssummarydataTaskId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask> getAllBehavioralTaskssummarydataTasks(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask> al = new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask> getBehavioralTaskssummarydataTasksByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask> al = new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask> getBehavioralTaskssummarydataTasksByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask> al = new ArrayList<org.nrg.xdat.om.BehavioralTaskssummarydataTask>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static BehavioralTaskssummarydataTask getBehavioralTaskssummarydataTasksByBehavioralTaskssummarydataTaskId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("behavioral:tasksSummaryData_task/behavioral_tasksSummaryData_task_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (BehavioralTaskssummarydataTask) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //run
	        for(org.nrg.xdat.model.BehavioralTaskssummarydataTaskRunI childRun : this.getRun()){
	            if (childRun!=null){
	              for(ResourceFile rf: ((BehavioralTaskssummarydataTaskRun)childRun).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("run[" + ((BehavioralTaskssummarydataTaskRun)childRun).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("run/" + ((BehavioralTaskssummarydataTaskRun)childRun).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //StatisticsAcrossRuns
	        BehavioralStatistics childStatisticsacrossruns = (BehavioralStatistics)this.getStatisticsacrossruns();
	            if (childStatisticsacrossruns!=null){
	              for(ResourceFile rf: ((BehavioralStatistics)childStatisticsacrossruns).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("StatisticsAcrossRuns[" + ((BehavioralStatistics)childStatisticsacrossruns).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("StatisticsAcrossRuns/" + ((BehavioralStatistics)childStatisticsacrossruns).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
