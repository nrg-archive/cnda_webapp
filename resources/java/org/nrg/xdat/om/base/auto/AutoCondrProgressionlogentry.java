/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrProgressionlogentry extends CndaExtSanode implements org.nrg.xdat.model.CondrProgressionlogentryI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrProgressionlogentry.class);
	public static String SCHEMA_ELEMENT_NAME="condr:progressionLogEntry";

	public AutoCondrProgressionlogentry(ItemI item)
	{
		super(item);
	}

	public AutoCondrProgressionlogentry(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrProgressionlogentry(UserI user)
	 **/
	public AutoCondrProgressionlogentry(){}

	public AutoCondrProgressionlogentry(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr:progressionLogEntry";
	}
	 private org.nrg.xdat.om.CndaExtSanode _Sanode =null;

	/**
	 * saNode
	 * @return org.nrg.xdat.om.CndaExtSanode
	 */
	public org.nrg.xdat.om.CndaExtSanode getSanode() {
		try{
			if (_Sanode==null){
				_Sanode=((CndaExtSanode)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("saNode")));
				return _Sanode;
			}else {
				return _Sanode;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for saNode.
	 * @param v Value to Set.
	 */
	public void setSanode(ItemI v) throws Exception{
		_Sanode =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/saNode",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/saNode",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * saNode
	 * set org.nrg.xdat.model.CndaExtSanodeI
	 */
	public <A extends org.nrg.xdat.model.CndaExtSanodeI> void setSanode(A item) throws Exception{
	setSanode((ItemI)item);
	}

	/**
	 * Removes the saNode.
	 * */
	public void removeSanode() {
		_Sanode =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/saNode",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Type=null;

	/**
	 * @return Returns the type.
	 */
	public String getType(){
		try{
			if (_Type==null){
				_Type=getStringProperty("type");
				return _Type;
			}else {
				return _Type;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for type.
	 * @param v Value to Set.
	 */
	public void setType(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/type",v);
		_Type=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kpsscore=null;

	/**
	 * @return Returns the kpsScore.
	 */
	public Integer getKpsscore() {
		try{
			if (_Kpsscore==null){
				_Kpsscore=getIntegerProperty("kpsScore");
				return _Kpsscore;
			}else {
				return _Kpsscore;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for kpsScore.
	 * @param v Value to Set.
	 */
	public void setKpsscore(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/kpsScore",v);
		_Kpsscore=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Treatmentchange=null;

	/**
	 * @return Returns the treatmentChange.
	 */
	public Boolean getTreatmentchange() {
		try{
			if (_Treatmentchange==null){
				_Treatmentchange=getBooleanProperty("treatmentChange");
				return _Treatmentchange;
			}else {
				return _Treatmentchange;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for treatmentChange.
	 * @param v Value to Set.
	 */
	public void setTreatmentchange(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/treatmentChange",v);
		_Treatmentchange=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Imaging=null;

	/**
	 * @return Returns the imaging.
	 */
	public Boolean getImaging() {
		try{
			if (_Imaging==null){
				_Imaging=getBooleanProperty("imaging");
				return _Imaging;
			}else {
				return _Imaging;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for imaging.
	 * @param v Value to Set.
	 */
	public void setImaging(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/imaging",v);
		_Imaging=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Steroiddose=null;

	/**
	 * @return Returns the steroidDose.
	 */
	public Boolean getSteroiddose() {
		try{
			if (_Steroiddose==null){
				_Steroiddose=getBooleanProperty("steroidDose");
				return _Steroiddose;
			}else {
				return _Steroiddose;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for steroidDose.
	 * @param v Value to Set.
	 */
	public void setSteroiddose(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/steroidDose",v);
		_Steroiddose=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Steroiddoseamount=null;

	/**
	 * @return Returns the steroidDoseAmount.
	 */
	public Double getSteroiddoseamount() {
		try{
			if (_Steroiddoseamount==null){
				_Steroiddoseamount=getDoubleProperty("steroidDoseAmount");
				return _Steroiddoseamount;
			}else {
				return _Steroiddoseamount;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for steroidDoseAmount.
	 * @param v Value to Set.
	 */
	public void setSteroiddoseamount(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/steroidDoseAmount",v);
		_Steroiddoseamount=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Functionalstatus=null;

	/**
	 * @return Returns the functionalStatus.
	 */
	public Boolean getFunctionalstatus() {
		try{
			if (_Functionalstatus==null){
				_Functionalstatus=getBooleanProperty("functionalStatus");
				return _Functionalstatus;
			}else {
				return _Functionalstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for functionalStatus.
	 * @param v Value to Set.
	 */
	public void setFunctionalstatus(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/functionalStatus",v);
		_Functionalstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Newneurologicaldefecit=null;

	/**
	 * @return Returns the newNeurologicalDefecit.
	 */
	public Boolean getNewneurologicaldefecit() {
		try{
			if (_Newneurologicaldefecit==null){
				_Newneurologicaldefecit=getBooleanProperty("newNeurologicalDefecit");
				return _Newneurologicaldefecit;
			}else {
				return _Newneurologicaldefecit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for newNeurologicalDefecit.
	 * @param v Value to Set.
	 */
	public void setNewneurologicaldefecit(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/newNeurologicalDefecit",v);
		_Newneurologicaldefecit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Motorweakness=null;

	/**
	 * @return Returns the motorWeakness.
	 */
	public Boolean getMotorweakness() {
		try{
			if (_Motorweakness==null){
				_Motorweakness=getBooleanProperty("motorWeakness");
				return _Motorweakness;
			}else {
				return _Motorweakness;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for motorWeakness.
	 * @param v Value to Set.
	 */
	public void setMotorweakness(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/motorWeakness",v);
		_Motorweakness=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Sensorychange=null;

	/**
	 * @return Returns the sensoryChange.
	 */
	public Boolean getSensorychange() {
		try{
			if (_Sensorychange==null){
				_Sensorychange=getBooleanProperty("sensoryChange");
				return _Sensorychange;
			}else {
				return _Sensorychange;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sensoryChange.
	 * @param v Value to Set.
	 */
	public void setSensorychange(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/sensoryChange",v);
		_Sensorychange=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Severeheadache=null;

	/**
	 * @return Returns the severeHeadache.
	 */
	public Boolean getSevereheadache() {
		try{
			if (_Severeheadache==null){
				_Severeheadache=getBooleanProperty("severeHeadache");
				return _Severeheadache;
			}else {
				return _Severeheadache;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for severeHeadache.
	 * @param v Value to Set.
	 */
	public void setSevereheadache(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/severeHeadache",v);
		_Severeheadache=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Nausea=null;

	/**
	 * @return Returns the nausea.
	 */
	public Boolean getNausea() {
		try{
			if (_Nausea==null){
				_Nausea=getBooleanProperty("nausea");
				return _Nausea;
			}else {
				return _Nausea;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for nausea.
	 * @param v Value to Set.
	 */
	public void setNausea(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/nausea",v);
		_Nausea=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Visualloss=null;

	/**
	 * @return Returns the visualLoss.
	 */
	public Boolean getVisualloss() {
		try{
			if (_Visualloss==null){
				_Visualloss=getBooleanProperty("visualLoss");
				return _Visualloss;
			}else {
				return _Visualloss;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for visualLoss.
	 * @param v Value to Set.
	 */
	public void setVisualloss(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/visualLoss",v);
		_Visualloss=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Cognitivedecline=null;

	/**
	 * @return Returns the cognitiveDecline.
	 */
	public Boolean getCognitivedecline() {
		try{
			if (_Cognitivedecline==null){
				_Cognitivedecline=getBooleanProperty("cognitiveDecline");
				return _Cognitivedecline;
			}else {
				return _Cognitivedecline;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cognitiveDecline.
	 * @param v Value to Set.
	 */
	public void setCognitivedecline(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/cognitiveDecline",v);
		_Cognitivedecline=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Clinicaldeterioration=null;

	/**
	 * @return Returns the clinicalDeterioration.
	 */
	public Boolean getClinicaldeterioration() {
		try{
			if (_Clinicaldeterioration==null){
				_Clinicaldeterioration=getBooleanProperty("clinicalDeterioration");
				return _Clinicaldeterioration;
			}else {
				return _Clinicaldeterioration;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for clinicalDeterioration.
	 * @param v Value to Set.
	 */
	public void setClinicaldeterioration(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/clinicalDeterioration",v);
		_Clinicaldeterioration=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Otherneurologicaldefecit=null;

	/**
	 * @return Returns the otherNeurologicalDefecit.
	 */
	public Boolean getOtherneurologicaldefecit() {
		try{
			if (_Otherneurologicaldefecit==null){
				_Otherneurologicaldefecit=getBooleanProperty("otherNeurologicalDefecit");
				return _Otherneurologicaldefecit;
			}else {
				return _Otherneurologicaldefecit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for otherNeurologicalDefecit.
	 * @param v Value to Set.
	 */
	public void setOtherneurologicaldefecit(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/otherNeurologicalDefecit",v);
		_Otherneurologicaldefecit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrProgressionlogentry> getAllCondrProgressionlogentrys(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrProgressionlogentry> al = new ArrayList<org.nrg.xdat.om.CondrProgressionlogentry>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrProgressionlogentry> getCondrProgressionlogentrysByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrProgressionlogentry> al = new ArrayList<org.nrg.xdat.om.CondrProgressionlogentry>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrProgressionlogentry> getCondrProgressionlogentrysByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrProgressionlogentry> al = new ArrayList<org.nrg.xdat.om.CondrProgressionlogentry>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrProgressionlogentry getCondrProgressionlogentrysById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr:progressionLogEntry/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrProgressionlogentry) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //saNode
	        CndaExtSanode childSanode = (CndaExtSanode)this.getSanode();
	            if (childSanode!=null){
	              for(ResourceFile rf: ((CndaExtSanode)childSanode).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("saNode[" + ((CndaExtSanode)childSanode).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("saNode/" + ((CndaExtSanode)childSanode).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
