/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsA3sbfmhstdata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsA3sbfmhstdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsA3sbfmhstdata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:a3sbfmhstData";

	public AutoUdsA3sbfmhstdata(ItemI item)
	{
		super(item);
	}

	public AutoUdsA3sbfmhstdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsA3sbfmhstdata(UserI user)
	 **/
	public AutoUdsA3sbfmhstdata(){}

	public AutoUdsA3sbfmhstdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:a3sbfmhstData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Examdate=null;

	/**
	 * @return Returns the EXAMDATE.
	 */
	public Object getExamdate(){
		try{
			if (_Examdate==null){
				_Examdate=getProperty("EXAMDATE");
				return _Examdate;
			}else {
				return _Examdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EXAMDATE.
	 * @param v Value to Set.
	 */
	public void setExamdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EXAMDATE",v);
		_Examdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _A3chg=null;

	/**
	 * @return Returns the A3CHG.
	 */
	public Integer getA3chg() {
		try{
			if (_A3chg==null){
				_A3chg=getIntegerProperty("A3CHG");
				return _A3chg;
			}else {
				return _A3chg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for A3CHG.
	 * @param v Value to Set.
	 */
	public void setA3chg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/A3CHG",v);
		_A3chg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Parchg=null;

	/**
	 * @return Returns the PARCHG.
	 */
	public Integer getParchg() {
		try{
			if (_Parchg==null){
				_Parchg=getIntegerProperty("PARCHG");
				return _Parchg;
			}else {
				return _Parchg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PARCHG.
	 * @param v Value to Set.
	 */
	public void setParchg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PARCHG",v);
		_Parchg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Familyid=null;

	/**
	 * @return Returns the FAMILYID.
	 */
	public Integer getFamilyid() {
		try{
			if (_Familyid==null){
				_Familyid=getIntegerProperty("FAMILYID");
				return _Familyid;
			}else {
				return _Familyid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FAMILYID.
	 * @param v Value to Set.
	 */
	public void setFamilyid(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FAMILYID",v);
		_Familyid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Gmomid=null;

	/**
	 * @return Returns the GMOMID.
	 */
	public String getGmomid(){
		try{
			if (_Gmomid==null){
				_Gmomid=getStringProperty("GMOMID");
				return _Gmomid;
			}else {
				return _Gmomid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GMOMID.
	 * @param v Value to Set.
	 */
	public void setGmomid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GMOMID",v);
		_Gmomid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gmomyob=null;

	/**
	 * @return Returns the GMOMYOB.
	 */
	public Integer getGmomyob() {
		try{
			if (_Gmomyob==null){
				_Gmomyob=getIntegerProperty("GMOMYOB");
				return _Gmomyob;
			}else {
				return _Gmomyob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GMOMYOB.
	 * @param v Value to Set.
	 */
	public void setGmomyob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GMOMYOB",v);
		_Gmomyob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gmomliv=null;

	/**
	 * @return Returns the GMOMLIV.
	 */
	public Integer getGmomliv() {
		try{
			if (_Gmomliv==null){
				_Gmomliv=getIntegerProperty("GMOMLIV");
				return _Gmomliv;
			}else {
				return _Gmomliv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GMOMLIV.
	 * @param v Value to Set.
	 */
	public void setGmomliv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GMOMLIV",v);
		_Gmomliv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gmomyod=null;

	/**
	 * @return Returns the GMOMYOD.
	 */
	public Integer getGmomyod() {
		try{
			if (_Gmomyod==null){
				_Gmomyod=getIntegerProperty("GMOMYOD");
				return _Gmomyod;
			}else {
				return _Gmomyod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GMOMYOD.
	 * @param v Value to Set.
	 */
	public void setGmomyod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GMOMYOD",v);
		_Gmomyod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gmomdem=null;

	/**
	 * @return Returns the GMOMDEM.
	 */
	public Integer getGmomdem() {
		try{
			if (_Gmomdem==null){
				_Gmomdem=getIntegerProperty("GMOMDEM");
				return _Gmomdem;
			}else {
				return _Gmomdem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GMOMDEM.
	 * @param v Value to Set.
	 */
	public void setGmomdem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GMOMDEM",v);
		_Gmomdem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Gmomonset=null;

	/**
	 * @return Returns the GMOMONSET.
	 */
	public String getGmomonset(){
		try{
			if (_Gmomonset==null){
				_Gmomonset=getStringProperty("GMOMONSET");
				return _Gmomonset;
			}else {
				return _Gmomonset;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GMOMONSET.
	 * @param v Value to Set.
	 */
	public void setGmomonset(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GMOMONSET",v);
		_Gmomonset=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gmomauto=null;

	/**
	 * @return Returns the GMOMAUTO.
	 */
	public Integer getGmomauto() {
		try{
			if (_Gmomauto==null){
				_Gmomauto=getIntegerProperty("GMOMAUTO");
				return _Gmomauto;
			}else {
				return _Gmomauto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GMOMAUTO.
	 * @param v Value to Set.
	 */
	public void setGmomauto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GMOMAUTO",v);
		_Gmomauto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Gdadid=null;

	/**
	 * @return Returns the GDADID.
	 */
	public String getGdadid(){
		try{
			if (_Gdadid==null){
				_Gdadid=getStringProperty("GDADID");
				return _Gdadid;
			}else {
				return _Gdadid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GDADID.
	 * @param v Value to Set.
	 */
	public void setGdadid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GDADID",v);
		_Gdadid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gdadyob=null;

	/**
	 * @return Returns the GDADYOB.
	 */
	public Integer getGdadyob() {
		try{
			if (_Gdadyob==null){
				_Gdadyob=getIntegerProperty("GDADYOB");
				return _Gdadyob;
			}else {
				return _Gdadyob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GDADYOB.
	 * @param v Value to Set.
	 */
	public void setGdadyob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GDADYOB",v);
		_Gdadyob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gdadliv=null;

	/**
	 * @return Returns the GDADLIV.
	 */
	public Integer getGdadliv() {
		try{
			if (_Gdadliv==null){
				_Gdadliv=getIntegerProperty("GDADLIV");
				return _Gdadliv;
			}else {
				return _Gdadliv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GDADLIV.
	 * @param v Value to Set.
	 */
	public void setGdadliv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GDADLIV",v);
		_Gdadliv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gdadyod=null;

	/**
	 * @return Returns the GDADYOD.
	 */
	public Integer getGdadyod() {
		try{
			if (_Gdadyod==null){
				_Gdadyod=getIntegerProperty("GDADYOD");
				return _Gdadyod;
			}else {
				return _Gdadyod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GDADYOD.
	 * @param v Value to Set.
	 */
	public void setGdadyod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GDADYOD",v);
		_Gdadyod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gdaddem=null;

	/**
	 * @return Returns the GDADDEM.
	 */
	public Integer getGdaddem() {
		try{
			if (_Gdaddem==null){
				_Gdaddem=getIntegerProperty("GDADDEM");
				return _Gdaddem;
			}else {
				return _Gdaddem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GDADDEM.
	 * @param v Value to Set.
	 */
	public void setGdaddem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GDADDEM",v);
		_Gdaddem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Gdadonset=null;

	/**
	 * @return Returns the GDADONSET.
	 */
	public String getGdadonset(){
		try{
			if (_Gdadonset==null){
				_Gdadonset=getStringProperty("GDADONSET");
				return _Gdadonset;
			}else {
				return _Gdadonset;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GDADONSET.
	 * @param v Value to Set.
	 */
	public void setGdadonset(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GDADONSET",v);
		_Gdadonset=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Gdadauto=null;

	/**
	 * @return Returns the GDADAUTO.
	 */
	public Integer getGdadauto() {
		try{
			if (_Gdadauto==null){
				_Gdadauto=getIntegerProperty("GDADAUTO");
				return _Gdadauto;
			}else {
				return _Gdadauto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GDADAUTO.
	 * @param v Value to Set.
	 */
	public void setGdadauto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GDADAUTO",v);
		_Gdadauto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Momid=null;

	/**
	 * @return Returns the MOMID.
	 */
	public String getMomid(){
		try{
			if (_Momid==null){
				_Momid=getStringProperty("MOMID");
				return _Momid;
			}else {
				return _Momid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOMID.
	 * @param v Value to Set.
	 */
	public void setMomid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOMID",v);
		_Momid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Momyob=null;

	/**
	 * @return Returns the MOMYOB.
	 */
	public Integer getMomyob() {
		try{
			if (_Momyob==null){
				_Momyob=getIntegerProperty("MOMYOB");
				return _Momyob;
			}else {
				return _Momyob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOMYOB.
	 * @param v Value to Set.
	 */
	public void setMomyob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOMYOB",v);
		_Momyob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Momliv=null;

	/**
	 * @return Returns the MOMLIV.
	 */
	public Integer getMomliv() {
		try{
			if (_Momliv==null){
				_Momliv=getIntegerProperty("MOMLIV");
				return _Momliv;
			}else {
				return _Momliv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOMLIV.
	 * @param v Value to Set.
	 */
	public void setMomliv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOMLIV",v);
		_Momliv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Momyod=null;

	/**
	 * @return Returns the MOMYOD.
	 */
	public Integer getMomyod() {
		try{
			if (_Momyod==null){
				_Momyod=getIntegerProperty("MOMYOD");
				return _Momyod;
			}else {
				return _Momyod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOMYOD.
	 * @param v Value to Set.
	 */
	public void setMomyod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOMYOD",v);
		_Momyod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Momdem=null;

	/**
	 * @return Returns the MOMDEM.
	 */
	public Integer getMomdem() {
		try{
			if (_Momdem==null){
				_Momdem=getIntegerProperty("MOMDEM");
				return _Momdem;
			}else {
				return _Momdem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOMDEM.
	 * @param v Value to Set.
	 */
	public void setMomdem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOMDEM",v);
		_Momdem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Momonset=null;

	/**
	 * @return Returns the MOMONSET.
	 */
	public String getMomonset(){
		try{
			if (_Momonset==null){
				_Momonset=getStringProperty("MOMONSET");
				return _Momonset;
			}else {
				return _Momonset;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOMONSET.
	 * @param v Value to Set.
	 */
	public void setMomonset(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOMONSET",v);
		_Momonset=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Momauto=null;

	/**
	 * @return Returns the MOMAUTO.
	 */
	public Integer getMomauto() {
		try{
			if (_Momauto==null){
				_Momauto=getIntegerProperty("MOMAUTO");
				return _Momauto;
			}else {
				return _Momauto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOMAUTO.
	 * @param v Value to Set.
	 */
	public void setMomauto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOMAUTO",v);
		_Momauto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dadid=null;

	/**
	 * @return Returns the DADID.
	 */
	public String getDadid(){
		try{
			if (_Dadid==null){
				_Dadid=getStringProperty("DADID");
				return _Dadid;
			}else {
				return _Dadid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DADID.
	 * @param v Value to Set.
	 */
	public void setDadid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DADID",v);
		_Dadid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dadyob=null;

	/**
	 * @return Returns the DADYOB.
	 */
	public Integer getDadyob() {
		try{
			if (_Dadyob==null){
				_Dadyob=getIntegerProperty("DADYOB");
				return _Dadyob;
			}else {
				return _Dadyob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DADYOB.
	 * @param v Value to Set.
	 */
	public void setDadyob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DADYOB",v);
		_Dadyob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dadliv=null;

	/**
	 * @return Returns the DADLIV.
	 */
	public Integer getDadliv() {
		try{
			if (_Dadliv==null){
				_Dadliv=getIntegerProperty("DADLIV");
				return _Dadliv;
			}else {
				return _Dadliv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DADLIV.
	 * @param v Value to Set.
	 */
	public void setDadliv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DADLIV",v);
		_Dadliv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dadyod=null;

	/**
	 * @return Returns the DADYOD.
	 */
	public Integer getDadyod() {
		try{
			if (_Dadyod==null){
				_Dadyod=getIntegerProperty("DADYOD");
				return _Dadyod;
			}else {
				return _Dadyod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DADYOD.
	 * @param v Value to Set.
	 */
	public void setDadyod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DADYOD",v);
		_Dadyod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Daddem=null;

	/**
	 * @return Returns the DADDEM.
	 */
	public Integer getDaddem() {
		try{
			if (_Daddem==null){
				_Daddem=getIntegerProperty("DADDEM");
				return _Daddem;
			}else {
				return _Daddem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DADDEM.
	 * @param v Value to Set.
	 */
	public void setDaddem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DADDEM",v);
		_Daddem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Dadonset=null;

	/**
	 * @return Returns the DADONSET.
	 */
	public String getDadonset(){
		try{
			if (_Dadonset==null){
				_Dadonset=getStringProperty("DADONSET");
				return _Dadonset;
			}else {
				return _Dadonset;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DADONSET.
	 * @param v Value to Set.
	 */
	public void setDadonset(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DADONSET",v);
		_Dadonset=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dadauto=null;

	/**
	 * @return Returns the DADAUTO.
	 */
	public Integer getDadauto() {
		try{
			if (_Dadauto==null){
				_Dadauto=getIntegerProperty("DADAUTO");
				return _Dadauto;
			}else {
				return _Dadauto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DADAUTO.
	 * @param v Value to Set.
	 */
	public void setDadauto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DADAUTO",v);
		_Dadauto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sibchg=null;

	/**
	 * @return Returns the SIBCHG.
	 */
	public Integer getSibchg() {
		try{
			if (_Sibchg==null){
				_Sibchg=getIntegerProperty("SIBCHG");
				return _Sibchg;
			}else {
				return _Sibchg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIBCHG.
	 * @param v Value to Set.
	 */
	public void setSibchg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIBCHG",v);
		_Sibchg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Twin=null;

	/**
	 * @return Returns the TWIN.
	 */
	public Integer getTwin() {
		try{
			if (_Twin==null){
				_Twin=getIntegerProperty("TWIN");
				return _Twin;
			}else {
				return _Twin;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TWIN.
	 * @param v Value to Set.
	 */
	public void setTwin(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TWIN",v);
		_Twin=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Twintype=null;

	/**
	 * @return Returns the TWINTYPE.
	 */
	public Integer getTwintype() {
		try{
			if (_Twintype==null){
				_Twintype=getIntegerProperty("TWINTYPE");
				return _Twintype;
			}else {
				return _Twintype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TWINTYPE.
	 * @param v Value to Set.
	 */
	public void setTwintype(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TWINTYPE",v);
		_Twintype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sibs=null;

	/**
	 * @return Returns the SIBS.
	 */
	public Integer getSibs() {
		try{
			if (_Sibs==null){
				_Sibs=getIntegerProperty("SIBS");
				return _Sibs;
			}else {
				return _Sibs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIBS.
	 * @param v Value to Set.
	 */
	public void setSibs(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIBS",v);
		_Sibs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib1id=null;

	/**
	 * @return Returns the SIB1ID.
	 */
	public String getSib1id(){
		try{
			if (_Sib1id==null){
				_Sib1id=getStringProperty("SIB1ID");
				return _Sib1id;
			}else {
				return _Sib1id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB1ID.
	 * @param v Value to Set.
	 */
	public void setSib1id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB1ID",v);
		_Sib1id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib1yob=null;

	/**
	 * @return Returns the SIB1YOB.
	 */
	public Integer getSib1yob() {
		try{
			if (_Sib1yob==null){
				_Sib1yob=getIntegerProperty("SIB1YOB");
				return _Sib1yob;
			}else {
				return _Sib1yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB1YOB.
	 * @param v Value to Set.
	 */
	public void setSib1yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB1YOB",v);
		_Sib1yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib1liv=null;

	/**
	 * @return Returns the SIB1LIV.
	 */
	public Integer getSib1liv() {
		try{
			if (_Sib1liv==null){
				_Sib1liv=getIntegerProperty("SIB1LIV");
				return _Sib1liv;
			}else {
				return _Sib1liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB1LIV.
	 * @param v Value to Set.
	 */
	public void setSib1liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB1LIV",v);
		_Sib1liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib1yod=null;

	/**
	 * @return Returns the SIB1YOD.
	 */
	public Integer getSib1yod() {
		try{
			if (_Sib1yod==null){
				_Sib1yod=getIntegerProperty("SIB1YOD");
				return _Sib1yod;
			}else {
				return _Sib1yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB1YOD.
	 * @param v Value to Set.
	 */
	public void setSib1yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB1YOD",v);
		_Sib1yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib1dem=null;

	/**
	 * @return Returns the SIB1DEM.
	 */
	public Integer getSib1dem() {
		try{
			if (_Sib1dem==null){
				_Sib1dem=getIntegerProperty("SIB1DEM");
				return _Sib1dem;
			}else {
				return _Sib1dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB1DEM.
	 * @param v Value to Set.
	 */
	public void setSib1dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB1DEM",v);
		_Sib1dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib1ons=null;

	/**
	 * @return Returns the SIB1ONS.
	 */
	public Integer getSib1ons() {
		try{
			if (_Sib1ons==null){
				_Sib1ons=getIntegerProperty("SIB1ONS");
				return _Sib1ons;
			}else {
				return _Sib1ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB1ONS.
	 * @param v Value to Set.
	 */
	public void setSib1ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB1ONS",v);
		_Sib1ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib1auto=null;

	/**
	 * @return Returns the SIB1AUTO.
	 */
	public Integer getSib1auto() {
		try{
			if (_Sib1auto==null){
				_Sib1auto=getIntegerProperty("SIB1AUTO");
				return _Sib1auto;
			}else {
				return _Sib1auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB1AUTO.
	 * @param v Value to Set.
	 */
	public void setSib1auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB1AUTO",v);
		_Sib1auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib1sex=null;

	/**
	 * @return Returns the SIB1SEX.
	 */
	public Integer getSib1sex() {
		try{
			if (_Sib1sex==null){
				_Sib1sex=getIntegerProperty("SIB1SEX");
				return _Sib1sex;
			}else {
				return _Sib1sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB1SEX.
	 * @param v Value to Set.
	 */
	public void setSib1sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB1SEX",v);
		_Sib1sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib2id=null;

	/**
	 * @return Returns the SIB2ID.
	 */
	public String getSib2id(){
		try{
			if (_Sib2id==null){
				_Sib2id=getStringProperty("SIB2ID");
				return _Sib2id;
			}else {
				return _Sib2id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB2ID.
	 * @param v Value to Set.
	 */
	public void setSib2id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB2ID",v);
		_Sib2id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib2yob=null;

	/**
	 * @return Returns the SIB2YOB.
	 */
	public Integer getSib2yob() {
		try{
			if (_Sib2yob==null){
				_Sib2yob=getIntegerProperty("SIB2YOB");
				return _Sib2yob;
			}else {
				return _Sib2yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB2YOB.
	 * @param v Value to Set.
	 */
	public void setSib2yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB2YOB",v);
		_Sib2yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib2liv=null;

	/**
	 * @return Returns the SIB2LIV.
	 */
	public Integer getSib2liv() {
		try{
			if (_Sib2liv==null){
				_Sib2liv=getIntegerProperty("SIB2LIV");
				return _Sib2liv;
			}else {
				return _Sib2liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB2LIV.
	 * @param v Value to Set.
	 */
	public void setSib2liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB2LIV",v);
		_Sib2liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib2yod=null;

	/**
	 * @return Returns the SIB2YOD.
	 */
	public Integer getSib2yod() {
		try{
			if (_Sib2yod==null){
				_Sib2yod=getIntegerProperty("SIB2YOD");
				return _Sib2yod;
			}else {
				return _Sib2yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB2YOD.
	 * @param v Value to Set.
	 */
	public void setSib2yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB2YOD",v);
		_Sib2yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib2dem=null;

	/**
	 * @return Returns the SIB2DEM.
	 */
	public Integer getSib2dem() {
		try{
			if (_Sib2dem==null){
				_Sib2dem=getIntegerProperty("SIB2DEM");
				return _Sib2dem;
			}else {
				return _Sib2dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB2DEM.
	 * @param v Value to Set.
	 */
	public void setSib2dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB2DEM",v);
		_Sib2dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib2ons=null;

	/**
	 * @return Returns the SIB2ONS.
	 */
	public Integer getSib2ons() {
		try{
			if (_Sib2ons==null){
				_Sib2ons=getIntegerProperty("SIB2ONS");
				return _Sib2ons;
			}else {
				return _Sib2ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB2ONS.
	 * @param v Value to Set.
	 */
	public void setSib2ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB2ONS",v);
		_Sib2ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib2auto=null;

	/**
	 * @return Returns the SIB2AUTO.
	 */
	public Integer getSib2auto() {
		try{
			if (_Sib2auto==null){
				_Sib2auto=getIntegerProperty("SIB2AUTO");
				return _Sib2auto;
			}else {
				return _Sib2auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB2AUTO.
	 * @param v Value to Set.
	 */
	public void setSib2auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB2AUTO",v);
		_Sib2auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib2sex=null;

	/**
	 * @return Returns the SIB2SEX.
	 */
	public Integer getSib2sex() {
		try{
			if (_Sib2sex==null){
				_Sib2sex=getIntegerProperty("SIB2SEX");
				return _Sib2sex;
			}else {
				return _Sib2sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB2SEX.
	 * @param v Value to Set.
	 */
	public void setSib2sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB2SEX",v);
		_Sib2sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib3id=null;

	/**
	 * @return Returns the SIB3ID.
	 */
	public String getSib3id(){
		try{
			if (_Sib3id==null){
				_Sib3id=getStringProperty("SIB3ID");
				return _Sib3id;
			}else {
				return _Sib3id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB3ID.
	 * @param v Value to Set.
	 */
	public void setSib3id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB3ID",v);
		_Sib3id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib3yob=null;

	/**
	 * @return Returns the SIB3YOB.
	 */
	public Integer getSib3yob() {
		try{
			if (_Sib3yob==null){
				_Sib3yob=getIntegerProperty("SIB3YOB");
				return _Sib3yob;
			}else {
				return _Sib3yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB3YOB.
	 * @param v Value to Set.
	 */
	public void setSib3yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB3YOB",v);
		_Sib3yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib3liv=null;

	/**
	 * @return Returns the SIB3LIV.
	 */
	public Integer getSib3liv() {
		try{
			if (_Sib3liv==null){
				_Sib3liv=getIntegerProperty("SIB3LIV");
				return _Sib3liv;
			}else {
				return _Sib3liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB3LIV.
	 * @param v Value to Set.
	 */
	public void setSib3liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB3LIV",v);
		_Sib3liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib3yod=null;

	/**
	 * @return Returns the SIB3YOD.
	 */
	public Integer getSib3yod() {
		try{
			if (_Sib3yod==null){
				_Sib3yod=getIntegerProperty("SIB3YOD");
				return _Sib3yod;
			}else {
				return _Sib3yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB3YOD.
	 * @param v Value to Set.
	 */
	public void setSib3yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB3YOD",v);
		_Sib3yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib3dem=null;

	/**
	 * @return Returns the SIB3DEM.
	 */
	public Integer getSib3dem() {
		try{
			if (_Sib3dem==null){
				_Sib3dem=getIntegerProperty("SIB3DEM");
				return _Sib3dem;
			}else {
				return _Sib3dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB3DEM.
	 * @param v Value to Set.
	 */
	public void setSib3dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB3DEM",v);
		_Sib3dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib3ons=null;

	/**
	 * @return Returns the SIB3ONS.
	 */
	public Integer getSib3ons() {
		try{
			if (_Sib3ons==null){
				_Sib3ons=getIntegerProperty("SIB3ONS");
				return _Sib3ons;
			}else {
				return _Sib3ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB3ONS.
	 * @param v Value to Set.
	 */
	public void setSib3ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB3ONS",v);
		_Sib3ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib3auto=null;

	/**
	 * @return Returns the SIB3AUTO.
	 */
	public Integer getSib3auto() {
		try{
			if (_Sib3auto==null){
				_Sib3auto=getIntegerProperty("SIB3AUTO");
				return _Sib3auto;
			}else {
				return _Sib3auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB3AUTO.
	 * @param v Value to Set.
	 */
	public void setSib3auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB3AUTO",v);
		_Sib3auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib3sex=null;

	/**
	 * @return Returns the SIB3SEX.
	 */
	public Integer getSib3sex() {
		try{
			if (_Sib3sex==null){
				_Sib3sex=getIntegerProperty("SIB3SEX");
				return _Sib3sex;
			}else {
				return _Sib3sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB3SEX.
	 * @param v Value to Set.
	 */
	public void setSib3sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB3SEX",v);
		_Sib3sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib4id=null;

	/**
	 * @return Returns the SIB4ID.
	 */
	public String getSib4id(){
		try{
			if (_Sib4id==null){
				_Sib4id=getStringProperty("SIB4ID");
				return _Sib4id;
			}else {
				return _Sib4id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB4ID.
	 * @param v Value to Set.
	 */
	public void setSib4id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB4ID",v);
		_Sib4id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib4yob=null;

	/**
	 * @return Returns the SIB4YOB.
	 */
	public Integer getSib4yob() {
		try{
			if (_Sib4yob==null){
				_Sib4yob=getIntegerProperty("SIB4YOB");
				return _Sib4yob;
			}else {
				return _Sib4yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB4YOB.
	 * @param v Value to Set.
	 */
	public void setSib4yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB4YOB",v);
		_Sib4yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib4liv=null;

	/**
	 * @return Returns the SIB4LIV.
	 */
	public Integer getSib4liv() {
		try{
			if (_Sib4liv==null){
				_Sib4liv=getIntegerProperty("SIB4LIV");
				return _Sib4liv;
			}else {
				return _Sib4liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB4LIV.
	 * @param v Value to Set.
	 */
	public void setSib4liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB4LIV",v);
		_Sib4liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib4yod=null;

	/**
	 * @return Returns the SIB4YOD.
	 */
	public Integer getSib4yod() {
		try{
			if (_Sib4yod==null){
				_Sib4yod=getIntegerProperty("SIB4YOD");
				return _Sib4yod;
			}else {
				return _Sib4yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB4YOD.
	 * @param v Value to Set.
	 */
	public void setSib4yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB4YOD",v);
		_Sib4yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib4dem=null;

	/**
	 * @return Returns the SIB4DEM.
	 */
	public Integer getSib4dem() {
		try{
			if (_Sib4dem==null){
				_Sib4dem=getIntegerProperty("SIB4DEM");
				return _Sib4dem;
			}else {
				return _Sib4dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB4DEM.
	 * @param v Value to Set.
	 */
	public void setSib4dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB4DEM",v);
		_Sib4dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib4ons=null;

	/**
	 * @return Returns the SIB4ONS.
	 */
	public Integer getSib4ons() {
		try{
			if (_Sib4ons==null){
				_Sib4ons=getIntegerProperty("SIB4ONS");
				return _Sib4ons;
			}else {
				return _Sib4ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB4ONS.
	 * @param v Value to Set.
	 */
	public void setSib4ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB4ONS",v);
		_Sib4ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib4auto=null;

	/**
	 * @return Returns the SIB4AUTO.
	 */
	public Integer getSib4auto() {
		try{
			if (_Sib4auto==null){
				_Sib4auto=getIntegerProperty("SIB4AUTO");
				return _Sib4auto;
			}else {
				return _Sib4auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB4AUTO.
	 * @param v Value to Set.
	 */
	public void setSib4auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB4AUTO",v);
		_Sib4auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib4sex=null;

	/**
	 * @return Returns the SIB4SEX.
	 */
	public Integer getSib4sex() {
		try{
			if (_Sib4sex==null){
				_Sib4sex=getIntegerProperty("SIB4SEX");
				return _Sib4sex;
			}else {
				return _Sib4sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB4SEX.
	 * @param v Value to Set.
	 */
	public void setSib4sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB4SEX",v);
		_Sib4sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib5id=null;

	/**
	 * @return Returns the SIB5ID.
	 */
	public String getSib5id(){
		try{
			if (_Sib5id==null){
				_Sib5id=getStringProperty("SIB5ID");
				return _Sib5id;
			}else {
				return _Sib5id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB5ID.
	 * @param v Value to Set.
	 */
	public void setSib5id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB5ID",v);
		_Sib5id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib5yob=null;

	/**
	 * @return Returns the SIB5YOB.
	 */
	public Integer getSib5yob() {
		try{
			if (_Sib5yob==null){
				_Sib5yob=getIntegerProperty("SIB5YOB");
				return _Sib5yob;
			}else {
				return _Sib5yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB5YOB.
	 * @param v Value to Set.
	 */
	public void setSib5yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB5YOB",v);
		_Sib5yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib5liv=null;

	/**
	 * @return Returns the SIB5LIV.
	 */
	public Integer getSib5liv() {
		try{
			if (_Sib5liv==null){
				_Sib5liv=getIntegerProperty("SIB5LIV");
				return _Sib5liv;
			}else {
				return _Sib5liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB5LIV.
	 * @param v Value to Set.
	 */
	public void setSib5liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB5LIV",v);
		_Sib5liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib5yod=null;

	/**
	 * @return Returns the SIB5YOD.
	 */
	public Integer getSib5yod() {
		try{
			if (_Sib5yod==null){
				_Sib5yod=getIntegerProperty("SIB5YOD");
				return _Sib5yod;
			}else {
				return _Sib5yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB5YOD.
	 * @param v Value to Set.
	 */
	public void setSib5yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB5YOD",v);
		_Sib5yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib5dem=null;

	/**
	 * @return Returns the SIB5DEM.
	 */
	public Integer getSib5dem() {
		try{
			if (_Sib5dem==null){
				_Sib5dem=getIntegerProperty("SIB5DEM");
				return _Sib5dem;
			}else {
				return _Sib5dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB5DEM.
	 * @param v Value to Set.
	 */
	public void setSib5dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB5DEM",v);
		_Sib5dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib5ons=null;

	/**
	 * @return Returns the SIB5ONS.
	 */
	public Integer getSib5ons() {
		try{
			if (_Sib5ons==null){
				_Sib5ons=getIntegerProperty("SIB5ONS");
				return _Sib5ons;
			}else {
				return _Sib5ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB5ONS.
	 * @param v Value to Set.
	 */
	public void setSib5ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB5ONS",v);
		_Sib5ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib5auto=null;

	/**
	 * @return Returns the SIB5AUTO.
	 */
	public Integer getSib5auto() {
		try{
			if (_Sib5auto==null){
				_Sib5auto=getIntegerProperty("SIB5AUTO");
				return _Sib5auto;
			}else {
				return _Sib5auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB5AUTO.
	 * @param v Value to Set.
	 */
	public void setSib5auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB5AUTO",v);
		_Sib5auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib5sex=null;

	/**
	 * @return Returns the SIB5SEX.
	 */
	public Integer getSib5sex() {
		try{
			if (_Sib5sex==null){
				_Sib5sex=getIntegerProperty("SIB5SEX");
				return _Sib5sex;
			}else {
				return _Sib5sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB5SEX.
	 * @param v Value to Set.
	 */
	public void setSib5sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB5SEX",v);
		_Sib5sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib6id=null;

	/**
	 * @return Returns the SIB6ID.
	 */
	public String getSib6id(){
		try{
			if (_Sib6id==null){
				_Sib6id=getStringProperty("SIB6ID");
				return _Sib6id;
			}else {
				return _Sib6id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB6ID.
	 * @param v Value to Set.
	 */
	public void setSib6id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB6ID",v);
		_Sib6id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib6yob=null;

	/**
	 * @return Returns the SIB6YOB.
	 */
	public Integer getSib6yob() {
		try{
			if (_Sib6yob==null){
				_Sib6yob=getIntegerProperty("SIB6YOB");
				return _Sib6yob;
			}else {
				return _Sib6yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB6YOB.
	 * @param v Value to Set.
	 */
	public void setSib6yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB6YOB",v);
		_Sib6yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib6liv=null;

	/**
	 * @return Returns the SIB6LIV.
	 */
	public Integer getSib6liv() {
		try{
			if (_Sib6liv==null){
				_Sib6liv=getIntegerProperty("SIB6LIV");
				return _Sib6liv;
			}else {
				return _Sib6liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB6LIV.
	 * @param v Value to Set.
	 */
	public void setSib6liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB6LIV",v);
		_Sib6liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib6yod=null;

	/**
	 * @return Returns the SIB6YOD.
	 */
	public Integer getSib6yod() {
		try{
			if (_Sib6yod==null){
				_Sib6yod=getIntegerProperty("SIB6YOD");
				return _Sib6yod;
			}else {
				return _Sib6yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB6YOD.
	 * @param v Value to Set.
	 */
	public void setSib6yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB6YOD",v);
		_Sib6yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib6dem=null;

	/**
	 * @return Returns the SIB6DEM.
	 */
	public Integer getSib6dem() {
		try{
			if (_Sib6dem==null){
				_Sib6dem=getIntegerProperty("SIB6DEM");
				return _Sib6dem;
			}else {
				return _Sib6dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB6DEM.
	 * @param v Value to Set.
	 */
	public void setSib6dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB6DEM",v);
		_Sib6dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib6ons=null;

	/**
	 * @return Returns the SIB6ONS.
	 */
	public Integer getSib6ons() {
		try{
			if (_Sib6ons==null){
				_Sib6ons=getIntegerProperty("SIB6ONS");
				return _Sib6ons;
			}else {
				return _Sib6ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB6ONS.
	 * @param v Value to Set.
	 */
	public void setSib6ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB6ONS",v);
		_Sib6ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib6auto=null;

	/**
	 * @return Returns the SIB6AUTO.
	 */
	public Integer getSib6auto() {
		try{
			if (_Sib6auto==null){
				_Sib6auto=getIntegerProperty("SIB6AUTO");
				return _Sib6auto;
			}else {
				return _Sib6auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB6AUTO.
	 * @param v Value to Set.
	 */
	public void setSib6auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB6AUTO",v);
		_Sib6auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib6sex=null;

	/**
	 * @return Returns the SIB6SEX.
	 */
	public Integer getSib6sex() {
		try{
			if (_Sib6sex==null){
				_Sib6sex=getIntegerProperty("SIB6SEX");
				return _Sib6sex;
			}else {
				return _Sib6sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB6SEX.
	 * @param v Value to Set.
	 */
	public void setSib6sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB6SEX",v);
		_Sib6sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib7id=null;

	/**
	 * @return Returns the SIB7ID.
	 */
	public String getSib7id(){
		try{
			if (_Sib7id==null){
				_Sib7id=getStringProperty("SIB7ID");
				return _Sib7id;
			}else {
				return _Sib7id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB7ID.
	 * @param v Value to Set.
	 */
	public void setSib7id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB7ID",v);
		_Sib7id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib7yob=null;

	/**
	 * @return Returns the SIB7YOB.
	 */
	public Integer getSib7yob() {
		try{
			if (_Sib7yob==null){
				_Sib7yob=getIntegerProperty("SIB7YOB");
				return _Sib7yob;
			}else {
				return _Sib7yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB7YOB.
	 * @param v Value to Set.
	 */
	public void setSib7yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB7YOB",v);
		_Sib7yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib7liv=null;

	/**
	 * @return Returns the SIB7LIV.
	 */
	public Integer getSib7liv() {
		try{
			if (_Sib7liv==null){
				_Sib7liv=getIntegerProperty("SIB7LIV");
				return _Sib7liv;
			}else {
				return _Sib7liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB7LIV.
	 * @param v Value to Set.
	 */
	public void setSib7liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB7LIV",v);
		_Sib7liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib7yod=null;

	/**
	 * @return Returns the SIB7YOD.
	 */
	public Integer getSib7yod() {
		try{
			if (_Sib7yod==null){
				_Sib7yod=getIntegerProperty("SIB7YOD");
				return _Sib7yod;
			}else {
				return _Sib7yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB7YOD.
	 * @param v Value to Set.
	 */
	public void setSib7yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB7YOD",v);
		_Sib7yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib7dem=null;

	/**
	 * @return Returns the SIB7DEM.
	 */
	public Integer getSib7dem() {
		try{
			if (_Sib7dem==null){
				_Sib7dem=getIntegerProperty("SIB7DEM");
				return _Sib7dem;
			}else {
				return _Sib7dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB7DEM.
	 * @param v Value to Set.
	 */
	public void setSib7dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB7DEM",v);
		_Sib7dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib7ons=null;

	/**
	 * @return Returns the SIB7ONS.
	 */
	public Integer getSib7ons() {
		try{
			if (_Sib7ons==null){
				_Sib7ons=getIntegerProperty("SIB7ONS");
				return _Sib7ons;
			}else {
				return _Sib7ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB7ONS.
	 * @param v Value to Set.
	 */
	public void setSib7ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB7ONS",v);
		_Sib7ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib7auto=null;

	/**
	 * @return Returns the SIB7AUTO.
	 */
	public Integer getSib7auto() {
		try{
			if (_Sib7auto==null){
				_Sib7auto=getIntegerProperty("SIB7AUTO");
				return _Sib7auto;
			}else {
				return _Sib7auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB7AUTO.
	 * @param v Value to Set.
	 */
	public void setSib7auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB7AUTO",v);
		_Sib7auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib7sex=null;

	/**
	 * @return Returns the SIB7SEX.
	 */
	public Integer getSib7sex() {
		try{
			if (_Sib7sex==null){
				_Sib7sex=getIntegerProperty("SIB7SEX");
				return _Sib7sex;
			}else {
				return _Sib7sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB7SEX.
	 * @param v Value to Set.
	 */
	public void setSib7sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB7SEX",v);
		_Sib7sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib8id=null;

	/**
	 * @return Returns the SIB8ID.
	 */
	public String getSib8id(){
		try{
			if (_Sib8id==null){
				_Sib8id=getStringProperty("SIB8ID");
				return _Sib8id;
			}else {
				return _Sib8id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB8ID.
	 * @param v Value to Set.
	 */
	public void setSib8id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB8ID",v);
		_Sib8id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib8yob=null;

	/**
	 * @return Returns the SIB8YOB.
	 */
	public Integer getSib8yob() {
		try{
			if (_Sib8yob==null){
				_Sib8yob=getIntegerProperty("SIB8YOB");
				return _Sib8yob;
			}else {
				return _Sib8yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB8YOB.
	 * @param v Value to Set.
	 */
	public void setSib8yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB8YOB",v);
		_Sib8yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib8liv=null;

	/**
	 * @return Returns the SIB8LIV.
	 */
	public Integer getSib8liv() {
		try{
			if (_Sib8liv==null){
				_Sib8liv=getIntegerProperty("SIB8LIV");
				return _Sib8liv;
			}else {
				return _Sib8liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB8LIV.
	 * @param v Value to Set.
	 */
	public void setSib8liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB8LIV",v);
		_Sib8liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib8yod=null;

	/**
	 * @return Returns the SIB8YOD.
	 */
	public Integer getSib8yod() {
		try{
			if (_Sib8yod==null){
				_Sib8yod=getIntegerProperty("SIB8YOD");
				return _Sib8yod;
			}else {
				return _Sib8yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB8YOD.
	 * @param v Value to Set.
	 */
	public void setSib8yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB8YOD",v);
		_Sib8yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib8dem=null;

	/**
	 * @return Returns the SIB8DEM.
	 */
	public Integer getSib8dem() {
		try{
			if (_Sib8dem==null){
				_Sib8dem=getIntegerProperty("SIB8DEM");
				return _Sib8dem;
			}else {
				return _Sib8dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB8DEM.
	 * @param v Value to Set.
	 */
	public void setSib8dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB8DEM",v);
		_Sib8dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib8ons=null;

	/**
	 * @return Returns the SIB8ONS.
	 */
	public Integer getSib8ons() {
		try{
			if (_Sib8ons==null){
				_Sib8ons=getIntegerProperty("SIB8ONS");
				return _Sib8ons;
			}else {
				return _Sib8ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB8ONS.
	 * @param v Value to Set.
	 */
	public void setSib8ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB8ONS",v);
		_Sib8ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib8auto=null;

	/**
	 * @return Returns the SIB8AUTO.
	 */
	public Integer getSib8auto() {
		try{
			if (_Sib8auto==null){
				_Sib8auto=getIntegerProperty("SIB8AUTO");
				return _Sib8auto;
			}else {
				return _Sib8auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB8AUTO.
	 * @param v Value to Set.
	 */
	public void setSib8auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB8AUTO",v);
		_Sib8auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib8sex=null;

	/**
	 * @return Returns the SIB8SEX.
	 */
	public Integer getSib8sex() {
		try{
			if (_Sib8sex==null){
				_Sib8sex=getIntegerProperty("SIB8SEX");
				return _Sib8sex;
			}else {
				return _Sib8sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB8SEX.
	 * @param v Value to Set.
	 */
	public void setSib8sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB8SEX",v);
		_Sib8sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib9id=null;

	/**
	 * @return Returns the SIB9ID.
	 */
	public String getSib9id(){
		try{
			if (_Sib9id==null){
				_Sib9id=getStringProperty("SIB9ID");
				return _Sib9id;
			}else {
				return _Sib9id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB9ID.
	 * @param v Value to Set.
	 */
	public void setSib9id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB9ID",v);
		_Sib9id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib9yob=null;

	/**
	 * @return Returns the SIB9YOB.
	 */
	public Integer getSib9yob() {
		try{
			if (_Sib9yob==null){
				_Sib9yob=getIntegerProperty("SIB9YOB");
				return _Sib9yob;
			}else {
				return _Sib9yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB9YOB.
	 * @param v Value to Set.
	 */
	public void setSib9yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB9YOB",v);
		_Sib9yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib9liv=null;

	/**
	 * @return Returns the SIB9LIV.
	 */
	public Integer getSib9liv() {
		try{
			if (_Sib9liv==null){
				_Sib9liv=getIntegerProperty("SIB9LIV");
				return _Sib9liv;
			}else {
				return _Sib9liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB9LIV.
	 * @param v Value to Set.
	 */
	public void setSib9liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB9LIV",v);
		_Sib9liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib9yod=null;

	/**
	 * @return Returns the SIB9YOD.
	 */
	public Integer getSib9yod() {
		try{
			if (_Sib9yod==null){
				_Sib9yod=getIntegerProperty("SIB9YOD");
				return _Sib9yod;
			}else {
				return _Sib9yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB9YOD.
	 * @param v Value to Set.
	 */
	public void setSib9yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB9YOD",v);
		_Sib9yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib9dem=null;

	/**
	 * @return Returns the SIB9DEM.
	 */
	public Integer getSib9dem() {
		try{
			if (_Sib9dem==null){
				_Sib9dem=getIntegerProperty("SIB9DEM");
				return _Sib9dem;
			}else {
				return _Sib9dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB9DEM.
	 * @param v Value to Set.
	 */
	public void setSib9dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB9DEM",v);
		_Sib9dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib9ons=null;

	/**
	 * @return Returns the SIB9ONS.
	 */
	public Integer getSib9ons() {
		try{
			if (_Sib9ons==null){
				_Sib9ons=getIntegerProperty("SIB9ONS");
				return _Sib9ons;
			}else {
				return _Sib9ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB9ONS.
	 * @param v Value to Set.
	 */
	public void setSib9ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB9ONS",v);
		_Sib9ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib9auto=null;

	/**
	 * @return Returns the SIB9AUTO.
	 */
	public Integer getSib9auto() {
		try{
			if (_Sib9auto==null){
				_Sib9auto=getIntegerProperty("SIB9AUTO");
				return _Sib9auto;
			}else {
				return _Sib9auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB9AUTO.
	 * @param v Value to Set.
	 */
	public void setSib9auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB9AUTO",v);
		_Sib9auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib9sex=null;

	/**
	 * @return Returns the SIB9SEX.
	 */
	public Integer getSib9sex() {
		try{
			if (_Sib9sex==null){
				_Sib9sex=getIntegerProperty("SIB9SEX");
				return _Sib9sex;
			}else {
				return _Sib9sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB9SEX.
	 * @param v Value to Set.
	 */
	public void setSib9sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB9SEX",v);
		_Sib9sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib10id=null;

	/**
	 * @return Returns the SIB10ID.
	 */
	public String getSib10id(){
		try{
			if (_Sib10id==null){
				_Sib10id=getStringProperty("SIB10ID");
				return _Sib10id;
			}else {
				return _Sib10id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB10ID.
	 * @param v Value to Set.
	 */
	public void setSib10id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB10ID",v);
		_Sib10id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib10yob=null;

	/**
	 * @return Returns the SIB10YOB.
	 */
	public Integer getSib10yob() {
		try{
			if (_Sib10yob==null){
				_Sib10yob=getIntegerProperty("SIB10YOB");
				return _Sib10yob;
			}else {
				return _Sib10yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB10YOB.
	 * @param v Value to Set.
	 */
	public void setSib10yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB10YOB",v);
		_Sib10yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib10liv=null;

	/**
	 * @return Returns the SIB10LIV.
	 */
	public Integer getSib10liv() {
		try{
			if (_Sib10liv==null){
				_Sib10liv=getIntegerProperty("SIB10LIV");
				return _Sib10liv;
			}else {
				return _Sib10liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB10LIV.
	 * @param v Value to Set.
	 */
	public void setSib10liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB10LIV",v);
		_Sib10liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib10yod=null;

	/**
	 * @return Returns the SIB10YOD.
	 */
	public Integer getSib10yod() {
		try{
			if (_Sib10yod==null){
				_Sib10yod=getIntegerProperty("SIB10YOD");
				return _Sib10yod;
			}else {
				return _Sib10yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB10YOD.
	 * @param v Value to Set.
	 */
	public void setSib10yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB10YOD",v);
		_Sib10yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib10dem=null;

	/**
	 * @return Returns the SIB10DEM.
	 */
	public Integer getSib10dem() {
		try{
			if (_Sib10dem==null){
				_Sib10dem=getIntegerProperty("SIB10DEM");
				return _Sib10dem;
			}else {
				return _Sib10dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB10DEM.
	 * @param v Value to Set.
	 */
	public void setSib10dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB10DEM",v);
		_Sib10dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib10ons=null;

	/**
	 * @return Returns the SIB10ONS.
	 */
	public Integer getSib10ons() {
		try{
			if (_Sib10ons==null){
				_Sib10ons=getIntegerProperty("SIB10ONS");
				return _Sib10ons;
			}else {
				return _Sib10ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB10ONS.
	 * @param v Value to Set.
	 */
	public void setSib10ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB10ONS",v);
		_Sib10ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib10auto=null;

	/**
	 * @return Returns the SIB10AUTO.
	 */
	public Integer getSib10auto() {
		try{
			if (_Sib10auto==null){
				_Sib10auto=getIntegerProperty("SIB10AUTO");
				return _Sib10auto;
			}else {
				return _Sib10auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB10AUTO.
	 * @param v Value to Set.
	 */
	public void setSib10auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB10AUTO",v);
		_Sib10auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib10sex=null;

	/**
	 * @return Returns the SIB10SEX.
	 */
	public Integer getSib10sex() {
		try{
			if (_Sib10sex==null){
				_Sib10sex=getIntegerProperty("SIB10SEX");
				return _Sib10sex;
			}else {
				return _Sib10sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB10SEX.
	 * @param v Value to Set.
	 */
	public void setSib10sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB10SEX",v);
		_Sib10sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib11id=null;

	/**
	 * @return Returns the SIB11ID.
	 */
	public String getSib11id(){
		try{
			if (_Sib11id==null){
				_Sib11id=getStringProperty("SIB11ID");
				return _Sib11id;
			}else {
				return _Sib11id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB11ID.
	 * @param v Value to Set.
	 */
	public void setSib11id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB11ID",v);
		_Sib11id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib11yob=null;

	/**
	 * @return Returns the SIB11YOB.
	 */
	public Integer getSib11yob() {
		try{
			if (_Sib11yob==null){
				_Sib11yob=getIntegerProperty("SIB11YOB");
				return _Sib11yob;
			}else {
				return _Sib11yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB11YOB.
	 * @param v Value to Set.
	 */
	public void setSib11yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB11YOB",v);
		_Sib11yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib11liv=null;

	/**
	 * @return Returns the SIB11LIV.
	 */
	public Integer getSib11liv() {
		try{
			if (_Sib11liv==null){
				_Sib11liv=getIntegerProperty("SIB11LIV");
				return _Sib11liv;
			}else {
				return _Sib11liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB11LIV.
	 * @param v Value to Set.
	 */
	public void setSib11liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB11LIV",v);
		_Sib11liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib11yod=null;

	/**
	 * @return Returns the SIB11YOD.
	 */
	public Integer getSib11yod() {
		try{
			if (_Sib11yod==null){
				_Sib11yod=getIntegerProperty("SIB11YOD");
				return _Sib11yod;
			}else {
				return _Sib11yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB11YOD.
	 * @param v Value to Set.
	 */
	public void setSib11yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB11YOD",v);
		_Sib11yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib11dem=null;

	/**
	 * @return Returns the SIB11DEM.
	 */
	public Integer getSib11dem() {
		try{
			if (_Sib11dem==null){
				_Sib11dem=getIntegerProperty("SIB11DEM");
				return _Sib11dem;
			}else {
				return _Sib11dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB11DEM.
	 * @param v Value to Set.
	 */
	public void setSib11dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB11DEM",v);
		_Sib11dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib11ons=null;

	/**
	 * @return Returns the SIB11ONS.
	 */
	public Integer getSib11ons() {
		try{
			if (_Sib11ons==null){
				_Sib11ons=getIntegerProperty("SIB11ONS");
				return _Sib11ons;
			}else {
				return _Sib11ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB11ONS.
	 * @param v Value to Set.
	 */
	public void setSib11ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB11ONS",v);
		_Sib11ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib11auto=null;

	/**
	 * @return Returns the SIB11AUTO.
	 */
	public Integer getSib11auto() {
		try{
			if (_Sib11auto==null){
				_Sib11auto=getIntegerProperty("SIB11AUTO");
				return _Sib11auto;
			}else {
				return _Sib11auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB11AUTO.
	 * @param v Value to Set.
	 */
	public void setSib11auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB11AUTO",v);
		_Sib11auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib11sex=null;

	/**
	 * @return Returns the SIB11SEX.
	 */
	public Integer getSib11sex() {
		try{
			if (_Sib11sex==null){
				_Sib11sex=getIntegerProperty("SIB11SEX");
				return _Sib11sex;
			}else {
				return _Sib11sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB11SEX.
	 * @param v Value to Set.
	 */
	public void setSib11sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB11SEX",v);
		_Sib11sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib12id=null;

	/**
	 * @return Returns the SIB12ID.
	 */
	public String getSib12id(){
		try{
			if (_Sib12id==null){
				_Sib12id=getStringProperty("SIB12ID");
				return _Sib12id;
			}else {
				return _Sib12id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB12ID.
	 * @param v Value to Set.
	 */
	public void setSib12id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB12ID",v);
		_Sib12id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib12yob=null;

	/**
	 * @return Returns the SIB12YOB.
	 */
	public Integer getSib12yob() {
		try{
			if (_Sib12yob==null){
				_Sib12yob=getIntegerProperty("SIB12YOB");
				return _Sib12yob;
			}else {
				return _Sib12yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB12YOB.
	 * @param v Value to Set.
	 */
	public void setSib12yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB12YOB",v);
		_Sib12yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib12liv=null;

	/**
	 * @return Returns the SIB12LIV.
	 */
	public Integer getSib12liv() {
		try{
			if (_Sib12liv==null){
				_Sib12liv=getIntegerProperty("SIB12LIV");
				return _Sib12liv;
			}else {
				return _Sib12liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB12LIV.
	 * @param v Value to Set.
	 */
	public void setSib12liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB12LIV",v);
		_Sib12liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib12yod=null;

	/**
	 * @return Returns the SIB12YOD.
	 */
	public Integer getSib12yod() {
		try{
			if (_Sib12yod==null){
				_Sib12yod=getIntegerProperty("SIB12YOD");
				return _Sib12yod;
			}else {
				return _Sib12yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB12YOD.
	 * @param v Value to Set.
	 */
	public void setSib12yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB12YOD",v);
		_Sib12yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib12dem=null;

	/**
	 * @return Returns the SIB12DEM.
	 */
	public Integer getSib12dem() {
		try{
			if (_Sib12dem==null){
				_Sib12dem=getIntegerProperty("SIB12DEM");
				return _Sib12dem;
			}else {
				return _Sib12dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB12DEM.
	 * @param v Value to Set.
	 */
	public void setSib12dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB12DEM",v);
		_Sib12dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib12ons=null;

	/**
	 * @return Returns the SIB12ONS.
	 */
	public Integer getSib12ons() {
		try{
			if (_Sib12ons==null){
				_Sib12ons=getIntegerProperty("SIB12ONS");
				return _Sib12ons;
			}else {
				return _Sib12ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB12ONS.
	 * @param v Value to Set.
	 */
	public void setSib12ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB12ONS",v);
		_Sib12ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib12auto=null;

	/**
	 * @return Returns the SIB12AUTO.
	 */
	public Integer getSib12auto() {
		try{
			if (_Sib12auto==null){
				_Sib12auto=getIntegerProperty("SIB12AUTO");
				return _Sib12auto;
			}else {
				return _Sib12auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB12AUTO.
	 * @param v Value to Set.
	 */
	public void setSib12auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB12AUTO",v);
		_Sib12auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib12sex=null;

	/**
	 * @return Returns the SIB12SEX.
	 */
	public Integer getSib12sex() {
		try{
			if (_Sib12sex==null){
				_Sib12sex=getIntegerProperty("SIB12SEX");
				return _Sib12sex;
			}else {
				return _Sib12sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB12SEX.
	 * @param v Value to Set.
	 */
	public void setSib12sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB12SEX",v);
		_Sib12sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib13id=null;

	/**
	 * @return Returns the SIB13ID.
	 */
	public String getSib13id(){
		try{
			if (_Sib13id==null){
				_Sib13id=getStringProperty("SIB13ID");
				return _Sib13id;
			}else {
				return _Sib13id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB13ID.
	 * @param v Value to Set.
	 */
	public void setSib13id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB13ID",v);
		_Sib13id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib13yob=null;

	/**
	 * @return Returns the SIB13YOB.
	 */
	public Integer getSib13yob() {
		try{
			if (_Sib13yob==null){
				_Sib13yob=getIntegerProperty("SIB13YOB");
				return _Sib13yob;
			}else {
				return _Sib13yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB13YOB.
	 * @param v Value to Set.
	 */
	public void setSib13yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB13YOB",v);
		_Sib13yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib13liv=null;

	/**
	 * @return Returns the SIB13LIV.
	 */
	public Integer getSib13liv() {
		try{
			if (_Sib13liv==null){
				_Sib13liv=getIntegerProperty("SIB13LIV");
				return _Sib13liv;
			}else {
				return _Sib13liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB13LIV.
	 * @param v Value to Set.
	 */
	public void setSib13liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB13LIV",v);
		_Sib13liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib13yod=null;

	/**
	 * @return Returns the SIB13YOD.
	 */
	public Integer getSib13yod() {
		try{
			if (_Sib13yod==null){
				_Sib13yod=getIntegerProperty("SIB13YOD");
				return _Sib13yod;
			}else {
				return _Sib13yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB13YOD.
	 * @param v Value to Set.
	 */
	public void setSib13yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB13YOD",v);
		_Sib13yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib13dem=null;

	/**
	 * @return Returns the SIB13DEM.
	 */
	public Integer getSib13dem() {
		try{
			if (_Sib13dem==null){
				_Sib13dem=getIntegerProperty("SIB13DEM");
				return _Sib13dem;
			}else {
				return _Sib13dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB13DEM.
	 * @param v Value to Set.
	 */
	public void setSib13dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB13DEM",v);
		_Sib13dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib13ons=null;

	/**
	 * @return Returns the SIB13ONS.
	 */
	public Integer getSib13ons() {
		try{
			if (_Sib13ons==null){
				_Sib13ons=getIntegerProperty("SIB13ONS");
				return _Sib13ons;
			}else {
				return _Sib13ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB13ONS.
	 * @param v Value to Set.
	 */
	public void setSib13ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB13ONS",v);
		_Sib13ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib13auto=null;

	/**
	 * @return Returns the SIB13AUTO.
	 */
	public Integer getSib13auto() {
		try{
			if (_Sib13auto==null){
				_Sib13auto=getIntegerProperty("SIB13AUTO");
				return _Sib13auto;
			}else {
				return _Sib13auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB13AUTO.
	 * @param v Value to Set.
	 */
	public void setSib13auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB13AUTO",v);
		_Sib13auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib13sex=null;

	/**
	 * @return Returns the SIB13SEX.
	 */
	public Integer getSib13sex() {
		try{
			if (_Sib13sex==null){
				_Sib13sex=getIntegerProperty("SIB13SEX");
				return _Sib13sex;
			}else {
				return _Sib13sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB13SEX.
	 * @param v Value to Set.
	 */
	public void setSib13sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB13SEX",v);
		_Sib13sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib14id=null;

	/**
	 * @return Returns the SIB14ID.
	 */
	public String getSib14id(){
		try{
			if (_Sib14id==null){
				_Sib14id=getStringProperty("SIB14ID");
				return _Sib14id;
			}else {
				return _Sib14id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB14ID.
	 * @param v Value to Set.
	 */
	public void setSib14id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB14ID",v);
		_Sib14id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib14yob=null;

	/**
	 * @return Returns the SIB14YOB.
	 */
	public Integer getSib14yob() {
		try{
			if (_Sib14yob==null){
				_Sib14yob=getIntegerProperty("SIB14YOB");
				return _Sib14yob;
			}else {
				return _Sib14yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB14YOB.
	 * @param v Value to Set.
	 */
	public void setSib14yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB14YOB",v);
		_Sib14yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib14liv=null;

	/**
	 * @return Returns the SIB14LIV.
	 */
	public Integer getSib14liv() {
		try{
			if (_Sib14liv==null){
				_Sib14liv=getIntegerProperty("SIB14LIV");
				return _Sib14liv;
			}else {
				return _Sib14liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB14LIV.
	 * @param v Value to Set.
	 */
	public void setSib14liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB14LIV",v);
		_Sib14liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib14yod=null;

	/**
	 * @return Returns the SIB14YOD.
	 */
	public Integer getSib14yod() {
		try{
			if (_Sib14yod==null){
				_Sib14yod=getIntegerProperty("SIB14YOD");
				return _Sib14yod;
			}else {
				return _Sib14yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB14YOD.
	 * @param v Value to Set.
	 */
	public void setSib14yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB14YOD",v);
		_Sib14yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib14dem=null;

	/**
	 * @return Returns the SIB14DEM.
	 */
	public Integer getSib14dem() {
		try{
			if (_Sib14dem==null){
				_Sib14dem=getIntegerProperty("SIB14DEM");
				return _Sib14dem;
			}else {
				return _Sib14dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB14DEM.
	 * @param v Value to Set.
	 */
	public void setSib14dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB14DEM",v);
		_Sib14dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib14ons=null;

	/**
	 * @return Returns the SIB14ONS.
	 */
	public Integer getSib14ons() {
		try{
			if (_Sib14ons==null){
				_Sib14ons=getIntegerProperty("SIB14ONS");
				return _Sib14ons;
			}else {
				return _Sib14ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB14ONS.
	 * @param v Value to Set.
	 */
	public void setSib14ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB14ONS",v);
		_Sib14ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib14auto=null;

	/**
	 * @return Returns the SIB14AUTO.
	 */
	public Integer getSib14auto() {
		try{
			if (_Sib14auto==null){
				_Sib14auto=getIntegerProperty("SIB14AUTO");
				return _Sib14auto;
			}else {
				return _Sib14auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB14AUTO.
	 * @param v Value to Set.
	 */
	public void setSib14auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB14AUTO",v);
		_Sib14auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib14sex=null;

	/**
	 * @return Returns the SIB14SEX.
	 */
	public Integer getSib14sex() {
		try{
			if (_Sib14sex==null){
				_Sib14sex=getIntegerProperty("SIB14SEX");
				return _Sib14sex;
			}else {
				return _Sib14sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB14SEX.
	 * @param v Value to Set.
	 */
	public void setSib14sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB14SEX",v);
		_Sib14sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib15id=null;

	/**
	 * @return Returns the SIB15ID.
	 */
	public String getSib15id(){
		try{
			if (_Sib15id==null){
				_Sib15id=getStringProperty("SIB15ID");
				return _Sib15id;
			}else {
				return _Sib15id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB15ID.
	 * @param v Value to Set.
	 */
	public void setSib15id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB15ID",v);
		_Sib15id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib15yob=null;

	/**
	 * @return Returns the SIB15YOB.
	 */
	public Integer getSib15yob() {
		try{
			if (_Sib15yob==null){
				_Sib15yob=getIntegerProperty("SIB15YOB");
				return _Sib15yob;
			}else {
				return _Sib15yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB15YOB.
	 * @param v Value to Set.
	 */
	public void setSib15yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB15YOB",v);
		_Sib15yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib15liv=null;

	/**
	 * @return Returns the SIB15LIV.
	 */
	public Integer getSib15liv() {
		try{
			if (_Sib15liv==null){
				_Sib15liv=getIntegerProperty("SIB15LIV");
				return _Sib15liv;
			}else {
				return _Sib15liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB15LIV.
	 * @param v Value to Set.
	 */
	public void setSib15liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB15LIV",v);
		_Sib15liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib15yod=null;

	/**
	 * @return Returns the SIB15YOD.
	 */
	public Integer getSib15yod() {
		try{
			if (_Sib15yod==null){
				_Sib15yod=getIntegerProperty("SIB15YOD");
				return _Sib15yod;
			}else {
				return _Sib15yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB15YOD.
	 * @param v Value to Set.
	 */
	public void setSib15yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB15YOD",v);
		_Sib15yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib15dem=null;

	/**
	 * @return Returns the SIB15DEM.
	 */
	public Integer getSib15dem() {
		try{
			if (_Sib15dem==null){
				_Sib15dem=getIntegerProperty("SIB15DEM");
				return _Sib15dem;
			}else {
				return _Sib15dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB15DEM.
	 * @param v Value to Set.
	 */
	public void setSib15dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB15DEM",v);
		_Sib15dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib15ons=null;

	/**
	 * @return Returns the SIB15ONS.
	 */
	public Integer getSib15ons() {
		try{
			if (_Sib15ons==null){
				_Sib15ons=getIntegerProperty("SIB15ONS");
				return _Sib15ons;
			}else {
				return _Sib15ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB15ONS.
	 * @param v Value to Set.
	 */
	public void setSib15ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB15ONS",v);
		_Sib15ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib155auto=null;

	/**
	 * @return Returns the SIB155AUTO.
	 */
	public Integer getSib155auto() {
		try{
			if (_Sib155auto==null){
				_Sib155auto=getIntegerProperty("SIB155AUTO");
				return _Sib155auto;
			}else {
				return _Sib155auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB155AUTO.
	 * @param v Value to Set.
	 */
	public void setSib155auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB155AUTO",v);
		_Sib155auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib155sex=null;

	/**
	 * @return Returns the SIB155SEX.
	 */
	public Integer getSib155sex() {
		try{
			if (_Sib155sex==null){
				_Sib155sex=getIntegerProperty("SIB155SEX");
				return _Sib155sex;
			}else {
				return _Sib155sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB155SEX.
	 * @param v Value to Set.
	 */
	public void setSib155sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB155SEX",v);
		_Sib155sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib16id=null;

	/**
	 * @return Returns the SIB16ID.
	 */
	public String getSib16id(){
		try{
			if (_Sib16id==null){
				_Sib16id=getStringProperty("SIB16ID");
				return _Sib16id;
			}else {
				return _Sib16id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB16ID.
	 * @param v Value to Set.
	 */
	public void setSib16id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB16ID",v);
		_Sib16id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib16yob=null;

	/**
	 * @return Returns the SIB16YOB.
	 */
	public Integer getSib16yob() {
		try{
			if (_Sib16yob==null){
				_Sib16yob=getIntegerProperty("SIB16YOB");
				return _Sib16yob;
			}else {
				return _Sib16yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB16YOB.
	 * @param v Value to Set.
	 */
	public void setSib16yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB16YOB",v);
		_Sib16yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib16liv=null;

	/**
	 * @return Returns the SIB16LIV.
	 */
	public Integer getSib16liv() {
		try{
			if (_Sib16liv==null){
				_Sib16liv=getIntegerProperty("SIB16LIV");
				return _Sib16liv;
			}else {
				return _Sib16liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB16LIV.
	 * @param v Value to Set.
	 */
	public void setSib16liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB16LIV",v);
		_Sib16liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib16yod=null;

	/**
	 * @return Returns the SIB16YOD.
	 */
	public Integer getSib16yod() {
		try{
			if (_Sib16yod==null){
				_Sib16yod=getIntegerProperty("SIB16YOD");
				return _Sib16yod;
			}else {
				return _Sib16yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB16YOD.
	 * @param v Value to Set.
	 */
	public void setSib16yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB16YOD",v);
		_Sib16yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib16dem=null;

	/**
	 * @return Returns the SIB16DEM.
	 */
	public Integer getSib16dem() {
		try{
			if (_Sib16dem==null){
				_Sib16dem=getIntegerProperty("SIB16DEM");
				return _Sib16dem;
			}else {
				return _Sib16dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB16DEM.
	 * @param v Value to Set.
	 */
	public void setSib16dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB16DEM",v);
		_Sib16dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib16ons=null;

	/**
	 * @return Returns the SIB16ONS.
	 */
	public Integer getSib16ons() {
		try{
			if (_Sib16ons==null){
				_Sib16ons=getIntegerProperty("SIB16ONS");
				return _Sib16ons;
			}else {
				return _Sib16ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB16ONS.
	 * @param v Value to Set.
	 */
	public void setSib16ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB16ONS",v);
		_Sib16ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib16auto=null;

	/**
	 * @return Returns the SIB16AUTO.
	 */
	public Integer getSib16auto() {
		try{
			if (_Sib16auto==null){
				_Sib16auto=getIntegerProperty("SIB16AUTO");
				return _Sib16auto;
			}else {
				return _Sib16auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB16AUTO.
	 * @param v Value to Set.
	 */
	public void setSib16auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB16AUTO",v);
		_Sib16auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib16sex=null;

	/**
	 * @return Returns the SIB16SEX.
	 */
	public Integer getSib16sex() {
		try{
			if (_Sib16sex==null){
				_Sib16sex=getIntegerProperty("SIB16SEX");
				return _Sib16sex;
			}else {
				return _Sib16sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB16SEX.
	 * @param v Value to Set.
	 */
	public void setSib16sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB16SEX",v);
		_Sib16sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib17id=null;

	/**
	 * @return Returns the SIB17ID.
	 */
	public String getSib17id(){
		try{
			if (_Sib17id==null){
				_Sib17id=getStringProperty("SIB17ID");
				return _Sib17id;
			}else {
				return _Sib17id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB17ID.
	 * @param v Value to Set.
	 */
	public void setSib17id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB17ID",v);
		_Sib17id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib17yob=null;

	/**
	 * @return Returns the SIB17YOB.
	 */
	public Integer getSib17yob() {
		try{
			if (_Sib17yob==null){
				_Sib17yob=getIntegerProperty("SIB17YOB");
				return _Sib17yob;
			}else {
				return _Sib17yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB17YOB.
	 * @param v Value to Set.
	 */
	public void setSib17yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB17YOB",v);
		_Sib17yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib17liv=null;

	/**
	 * @return Returns the SIB17LIV.
	 */
	public Integer getSib17liv() {
		try{
			if (_Sib17liv==null){
				_Sib17liv=getIntegerProperty("SIB17LIV");
				return _Sib17liv;
			}else {
				return _Sib17liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB17LIV.
	 * @param v Value to Set.
	 */
	public void setSib17liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB17LIV",v);
		_Sib17liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib17yod=null;

	/**
	 * @return Returns the SIB17YOD.
	 */
	public Integer getSib17yod() {
		try{
			if (_Sib17yod==null){
				_Sib17yod=getIntegerProperty("SIB17YOD");
				return _Sib17yod;
			}else {
				return _Sib17yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB17YOD.
	 * @param v Value to Set.
	 */
	public void setSib17yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB17YOD",v);
		_Sib17yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib17dem=null;

	/**
	 * @return Returns the SIB17DEM.
	 */
	public Integer getSib17dem() {
		try{
			if (_Sib17dem==null){
				_Sib17dem=getIntegerProperty("SIB17DEM");
				return _Sib17dem;
			}else {
				return _Sib17dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB17DEM.
	 * @param v Value to Set.
	 */
	public void setSib17dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB17DEM",v);
		_Sib17dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib17ons=null;

	/**
	 * @return Returns the SIB17ONS.
	 */
	public Integer getSib17ons() {
		try{
			if (_Sib17ons==null){
				_Sib17ons=getIntegerProperty("SIB17ONS");
				return _Sib17ons;
			}else {
				return _Sib17ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB17ONS.
	 * @param v Value to Set.
	 */
	public void setSib17ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB17ONS",v);
		_Sib17ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib17auto=null;

	/**
	 * @return Returns the SIB17AUTO.
	 */
	public Integer getSib17auto() {
		try{
			if (_Sib17auto==null){
				_Sib17auto=getIntegerProperty("SIB17AUTO");
				return _Sib17auto;
			}else {
				return _Sib17auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB17AUTO.
	 * @param v Value to Set.
	 */
	public void setSib17auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB17AUTO",v);
		_Sib17auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib17sex=null;

	/**
	 * @return Returns the SIB17SEX.
	 */
	public Integer getSib17sex() {
		try{
			if (_Sib17sex==null){
				_Sib17sex=getIntegerProperty("SIB17SEX");
				return _Sib17sex;
			}else {
				return _Sib17sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB17SEX.
	 * @param v Value to Set.
	 */
	public void setSib17sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB17SEX",v);
		_Sib17sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib18id=null;

	/**
	 * @return Returns the SIB18ID.
	 */
	public String getSib18id(){
		try{
			if (_Sib18id==null){
				_Sib18id=getStringProperty("SIB18ID");
				return _Sib18id;
			}else {
				return _Sib18id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB18ID.
	 * @param v Value to Set.
	 */
	public void setSib18id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB18ID",v);
		_Sib18id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib18yob=null;

	/**
	 * @return Returns the SIB18YOB.
	 */
	public Integer getSib18yob() {
		try{
			if (_Sib18yob==null){
				_Sib18yob=getIntegerProperty("SIB18YOB");
				return _Sib18yob;
			}else {
				return _Sib18yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB18YOB.
	 * @param v Value to Set.
	 */
	public void setSib18yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB18YOB",v);
		_Sib18yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib18liv=null;

	/**
	 * @return Returns the SIB18LIV.
	 */
	public Integer getSib18liv() {
		try{
			if (_Sib18liv==null){
				_Sib18liv=getIntegerProperty("SIB18LIV");
				return _Sib18liv;
			}else {
				return _Sib18liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB18LIV.
	 * @param v Value to Set.
	 */
	public void setSib18liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB18LIV",v);
		_Sib18liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib18yod=null;

	/**
	 * @return Returns the SIB18YOD.
	 */
	public Integer getSib18yod() {
		try{
			if (_Sib18yod==null){
				_Sib18yod=getIntegerProperty("SIB18YOD");
				return _Sib18yod;
			}else {
				return _Sib18yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB18YOD.
	 * @param v Value to Set.
	 */
	public void setSib18yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB18YOD",v);
		_Sib18yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib18dem=null;

	/**
	 * @return Returns the SIB18DEM.
	 */
	public Integer getSib18dem() {
		try{
			if (_Sib18dem==null){
				_Sib18dem=getIntegerProperty("SIB18DEM");
				return _Sib18dem;
			}else {
				return _Sib18dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB18DEM.
	 * @param v Value to Set.
	 */
	public void setSib18dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB18DEM",v);
		_Sib18dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib18ons=null;

	/**
	 * @return Returns the SIB18ONS.
	 */
	public Integer getSib18ons() {
		try{
			if (_Sib18ons==null){
				_Sib18ons=getIntegerProperty("SIB18ONS");
				return _Sib18ons;
			}else {
				return _Sib18ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB18ONS.
	 * @param v Value to Set.
	 */
	public void setSib18ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB18ONS",v);
		_Sib18ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib18auto=null;

	/**
	 * @return Returns the SIB18AUTO.
	 */
	public Integer getSib18auto() {
		try{
			if (_Sib18auto==null){
				_Sib18auto=getIntegerProperty("SIB18AUTO");
				return _Sib18auto;
			}else {
				return _Sib18auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB18AUTO.
	 * @param v Value to Set.
	 */
	public void setSib18auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB18AUTO",v);
		_Sib18auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib18sex=null;

	/**
	 * @return Returns the SIB18SEX.
	 */
	public Integer getSib18sex() {
		try{
			if (_Sib18sex==null){
				_Sib18sex=getIntegerProperty("SIB18SEX");
				return _Sib18sex;
			}else {
				return _Sib18sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB18SEX.
	 * @param v Value to Set.
	 */
	public void setSib18sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB18SEX",v);
		_Sib18sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib19id=null;

	/**
	 * @return Returns the SIB19ID.
	 */
	public String getSib19id(){
		try{
			if (_Sib19id==null){
				_Sib19id=getStringProperty("SIB19ID");
				return _Sib19id;
			}else {
				return _Sib19id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB19ID.
	 * @param v Value to Set.
	 */
	public void setSib19id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB19ID",v);
		_Sib19id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib19yob=null;

	/**
	 * @return Returns the SIB19YOB.
	 */
	public Integer getSib19yob() {
		try{
			if (_Sib19yob==null){
				_Sib19yob=getIntegerProperty("SIB19YOB");
				return _Sib19yob;
			}else {
				return _Sib19yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB19YOB.
	 * @param v Value to Set.
	 */
	public void setSib19yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB19YOB",v);
		_Sib19yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib19liv=null;

	/**
	 * @return Returns the SIB19LIV.
	 */
	public Integer getSib19liv() {
		try{
			if (_Sib19liv==null){
				_Sib19liv=getIntegerProperty("SIB19LIV");
				return _Sib19liv;
			}else {
				return _Sib19liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB19LIV.
	 * @param v Value to Set.
	 */
	public void setSib19liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB19LIV",v);
		_Sib19liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib19yod=null;

	/**
	 * @return Returns the SIB19YOD.
	 */
	public Integer getSib19yod() {
		try{
			if (_Sib19yod==null){
				_Sib19yod=getIntegerProperty("SIB19YOD");
				return _Sib19yod;
			}else {
				return _Sib19yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB19YOD.
	 * @param v Value to Set.
	 */
	public void setSib19yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB19YOD",v);
		_Sib19yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib19dem=null;

	/**
	 * @return Returns the SIB19DEM.
	 */
	public Integer getSib19dem() {
		try{
			if (_Sib19dem==null){
				_Sib19dem=getIntegerProperty("SIB19DEM");
				return _Sib19dem;
			}else {
				return _Sib19dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB19DEM.
	 * @param v Value to Set.
	 */
	public void setSib19dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB19DEM",v);
		_Sib19dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib19ons=null;

	/**
	 * @return Returns the SIB19ONS.
	 */
	public Integer getSib19ons() {
		try{
			if (_Sib19ons==null){
				_Sib19ons=getIntegerProperty("SIB19ONS");
				return _Sib19ons;
			}else {
				return _Sib19ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB19ONS.
	 * @param v Value to Set.
	 */
	public void setSib19ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB19ONS",v);
		_Sib19ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib19auto=null;

	/**
	 * @return Returns the SIB19AUTO.
	 */
	public Integer getSib19auto() {
		try{
			if (_Sib19auto==null){
				_Sib19auto=getIntegerProperty("SIB19AUTO");
				return _Sib19auto;
			}else {
				return _Sib19auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB19AUTO.
	 * @param v Value to Set.
	 */
	public void setSib19auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB19AUTO",v);
		_Sib19auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib19sex=null;

	/**
	 * @return Returns the SIB19SEX.
	 */
	public Integer getSib19sex() {
		try{
			if (_Sib19sex==null){
				_Sib19sex=getIntegerProperty("SIB19SEX");
				return _Sib19sex;
			}else {
				return _Sib19sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB19SEX.
	 * @param v Value to Set.
	 */
	public void setSib19sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB19SEX",v);
		_Sib19sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sib20id=null;

	/**
	 * @return Returns the SIB20ID.
	 */
	public String getSib20id(){
		try{
			if (_Sib20id==null){
				_Sib20id=getStringProperty("SIB20ID");
				return _Sib20id;
			}else {
				return _Sib20id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB20ID.
	 * @param v Value to Set.
	 */
	public void setSib20id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB20ID",v);
		_Sib20id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib20yob=null;

	/**
	 * @return Returns the SIB20YOB.
	 */
	public Integer getSib20yob() {
		try{
			if (_Sib20yob==null){
				_Sib20yob=getIntegerProperty("SIB20YOB");
				return _Sib20yob;
			}else {
				return _Sib20yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB20YOB.
	 * @param v Value to Set.
	 */
	public void setSib20yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB20YOB",v);
		_Sib20yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib20liv=null;

	/**
	 * @return Returns the SIB20LIV.
	 */
	public Integer getSib20liv() {
		try{
			if (_Sib20liv==null){
				_Sib20liv=getIntegerProperty("SIB20LIV");
				return _Sib20liv;
			}else {
				return _Sib20liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB20LIV.
	 * @param v Value to Set.
	 */
	public void setSib20liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB20LIV",v);
		_Sib20liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib20yod=null;

	/**
	 * @return Returns the SIB20YOD.
	 */
	public Integer getSib20yod() {
		try{
			if (_Sib20yod==null){
				_Sib20yod=getIntegerProperty("SIB20YOD");
				return _Sib20yod;
			}else {
				return _Sib20yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB20YOD.
	 * @param v Value to Set.
	 */
	public void setSib20yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB20YOD",v);
		_Sib20yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib20dem=null;

	/**
	 * @return Returns the SIB20DEM.
	 */
	public Integer getSib20dem() {
		try{
			if (_Sib20dem==null){
				_Sib20dem=getIntegerProperty("SIB20DEM");
				return _Sib20dem;
			}else {
				return _Sib20dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB20DEM.
	 * @param v Value to Set.
	 */
	public void setSib20dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB20DEM",v);
		_Sib20dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib20ons=null;

	/**
	 * @return Returns the SIB20ONS.
	 */
	public Integer getSib20ons() {
		try{
			if (_Sib20ons==null){
				_Sib20ons=getIntegerProperty("SIB20ONS");
				return _Sib20ons;
			}else {
				return _Sib20ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB20ONS.
	 * @param v Value to Set.
	 */
	public void setSib20ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB20ONS",v);
		_Sib20ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib20auto=null;

	/**
	 * @return Returns the SIB20AUTO.
	 */
	public Integer getSib20auto() {
		try{
			if (_Sib20auto==null){
				_Sib20auto=getIntegerProperty("SIB20AUTO");
				return _Sib20auto;
			}else {
				return _Sib20auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB20AUTO.
	 * @param v Value to Set.
	 */
	public void setSib20auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB20AUTO",v);
		_Sib20auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sib20sex=null;

	/**
	 * @return Returns the SIB20SEX.
	 */
	public Integer getSib20sex() {
		try{
			if (_Sib20sex==null){
				_Sib20sex=getIntegerProperty("SIB20SEX");
				return _Sib20sex;
			}else {
				return _Sib20sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SIB20SEX.
	 * @param v Value to Set.
	 */
	public void setSib20sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SIB20SEX",v);
		_Sib20sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kidchg=null;

	/**
	 * @return Returns the KIDCHG.
	 */
	public Integer getKidchg() {
		try{
			if (_Kidchg==null){
				_Kidchg=getIntegerProperty("KIDCHG");
				return _Kidchg;
			}else {
				return _Kidchg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KIDCHG.
	 * @param v Value to Set.
	 */
	public void setKidchg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KIDCHG",v);
		_Kidchg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kids=null;

	/**
	 * @return Returns the KIDS.
	 */
	public Integer getKids() {
		try{
			if (_Kids==null){
				_Kids=getIntegerProperty("KIDS");
				return _Kids;
			}else {
				return _Kids;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KIDS.
	 * @param v Value to Set.
	 */
	public void setKids(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KIDS",v);
		_Kids=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid1id=null;

	/**
	 * @return Returns the KID1ID.
	 */
	public String getKid1id(){
		try{
			if (_Kid1id==null){
				_Kid1id=getStringProperty("KID1ID");
				return _Kid1id;
			}else {
				return _Kid1id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID1ID.
	 * @param v Value to Set.
	 */
	public void setKid1id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID1ID",v);
		_Kid1id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid1yob=null;

	/**
	 * @return Returns the KID1YOB.
	 */
	public Integer getKid1yob() {
		try{
			if (_Kid1yob==null){
				_Kid1yob=getIntegerProperty("KID1YOB");
				return _Kid1yob;
			}else {
				return _Kid1yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID1YOB.
	 * @param v Value to Set.
	 */
	public void setKid1yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID1YOB",v);
		_Kid1yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid1liv=null;

	/**
	 * @return Returns the KID1LIV.
	 */
	public Integer getKid1liv() {
		try{
			if (_Kid1liv==null){
				_Kid1liv=getIntegerProperty("KID1LIV");
				return _Kid1liv;
			}else {
				return _Kid1liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID1LIV.
	 * @param v Value to Set.
	 */
	public void setKid1liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID1LIV",v);
		_Kid1liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid1yod=null;

	/**
	 * @return Returns the KID1YOD.
	 */
	public Integer getKid1yod() {
		try{
			if (_Kid1yod==null){
				_Kid1yod=getIntegerProperty("KID1YOD");
				return _Kid1yod;
			}else {
				return _Kid1yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID1YOD.
	 * @param v Value to Set.
	 */
	public void setKid1yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID1YOD",v);
		_Kid1yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid1dem=null;

	/**
	 * @return Returns the KID1DEM.
	 */
	public Integer getKid1dem() {
		try{
			if (_Kid1dem==null){
				_Kid1dem=getIntegerProperty("KID1DEM");
				return _Kid1dem;
			}else {
				return _Kid1dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID1DEM.
	 * @param v Value to Set.
	 */
	public void setKid1dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID1DEM",v);
		_Kid1dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid1ons=null;

	/**
	 * @return Returns the KID1ONS.
	 */
	public Integer getKid1ons() {
		try{
			if (_Kid1ons==null){
				_Kid1ons=getIntegerProperty("KID1ONS");
				return _Kid1ons;
			}else {
				return _Kid1ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID1ONS.
	 * @param v Value to Set.
	 */
	public void setKid1ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID1ONS",v);
		_Kid1ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid1auto=null;

	/**
	 * @return Returns the KID1AUTO.
	 */
	public Integer getKid1auto() {
		try{
			if (_Kid1auto==null){
				_Kid1auto=getIntegerProperty("KID1AUTO");
				return _Kid1auto;
			}else {
				return _Kid1auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID1AUTO.
	 * @param v Value to Set.
	 */
	public void setKid1auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID1AUTO",v);
		_Kid1auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid1sex=null;

	/**
	 * @return Returns the KID1SEX.
	 */
	public Integer getKid1sex() {
		try{
			if (_Kid1sex==null){
				_Kid1sex=getIntegerProperty("KID1SEX");
				return _Kid1sex;
			}else {
				return _Kid1sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID1SEX.
	 * @param v Value to Set.
	 */
	public void setKid1sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID1SEX",v);
		_Kid1sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid2id=null;

	/**
	 * @return Returns the KID2ID.
	 */
	public String getKid2id(){
		try{
			if (_Kid2id==null){
				_Kid2id=getStringProperty("KID2ID");
				return _Kid2id;
			}else {
				return _Kid2id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID2ID.
	 * @param v Value to Set.
	 */
	public void setKid2id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID2ID",v);
		_Kid2id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid2yob=null;

	/**
	 * @return Returns the KID2YOB.
	 */
	public Integer getKid2yob() {
		try{
			if (_Kid2yob==null){
				_Kid2yob=getIntegerProperty("KID2YOB");
				return _Kid2yob;
			}else {
				return _Kid2yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID2YOB.
	 * @param v Value to Set.
	 */
	public void setKid2yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID2YOB",v);
		_Kid2yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid2liv=null;

	/**
	 * @return Returns the KID2LIV.
	 */
	public Integer getKid2liv() {
		try{
			if (_Kid2liv==null){
				_Kid2liv=getIntegerProperty("KID2LIV");
				return _Kid2liv;
			}else {
				return _Kid2liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID2LIV.
	 * @param v Value to Set.
	 */
	public void setKid2liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID2LIV",v);
		_Kid2liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid2yod=null;

	/**
	 * @return Returns the KID2YOD.
	 */
	public Integer getKid2yod() {
		try{
			if (_Kid2yod==null){
				_Kid2yod=getIntegerProperty("KID2YOD");
				return _Kid2yod;
			}else {
				return _Kid2yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID2YOD.
	 * @param v Value to Set.
	 */
	public void setKid2yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID2YOD",v);
		_Kid2yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid2dem=null;

	/**
	 * @return Returns the KID2DEM.
	 */
	public Integer getKid2dem() {
		try{
			if (_Kid2dem==null){
				_Kid2dem=getIntegerProperty("KID2DEM");
				return _Kid2dem;
			}else {
				return _Kid2dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID2DEM.
	 * @param v Value to Set.
	 */
	public void setKid2dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID2DEM",v);
		_Kid2dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid2ons=null;

	/**
	 * @return Returns the KID2ONS.
	 */
	public Integer getKid2ons() {
		try{
			if (_Kid2ons==null){
				_Kid2ons=getIntegerProperty("KID2ONS");
				return _Kid2ons;
			}else {
				return _Kid2ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID2ONS.
	 * @param v Value to Set.
	 */
	public void setKid2ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID2ONS",v);
		_Kid2ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid2auto=null;

	/**
	 * @return Returns the KID2AUTO.
	 */
	public Integer getKid2auto() {
		try{
			if (_Kid2auto==null){
				_Kid2auto=getIntegerProperty("KID2AUTO");
				return _Kid2auto;
			}else {
				return _Kid2auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID2AUTO.
	 * @param v Value to Set.
	 */
	public void setKid2auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID2AUTO",v);
		_Kid2auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid2sex=null;

	/**
	 * @return Returns the KID2SEX.
	 */
	public Integer getKid2sex() {
		try{
			if (_Kid2sex==null){
				_Kid2sex=getIntegerProperty("KID2SEX");
				return _Kid2sex;
			}else {
				return _Kid2sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID2SEX.
	 * @param v Value to Set.
	 */
	public void setKid2sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID2SEX",v);
		_Kid2sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid3id=null;

	/**
	 * @return Returns the KID3ID.
	 */
	public String getKid3id(){
		try{
			if (_Kid3id==null){
				_Kid3id=getStringProperty("KID3ID");
				return _Kid3id;
			}else {
				return _Kid3id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID3ID.
	 * @param v Value to Set.
	 */
	public void setKid3id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID3ID",v);
		_Kid3id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid3yob=null;

	/**
	 * @return Returns the KID3YOB.
	 */
	public Integer getKid3yob() {
		try{
			if (_Kid3yob==null){
				_Kid3yob=getIntegerProperty("KID3YOB");
				return _Kid3yob;
			}else {
				return _Kid3yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID3YOB.
	 * @param v Value to Set.
	 */
	public void setKid3yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID3YOB",v);
		_Kid3yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid3liv=null;

	/**
	 * @return Returns the KID3LIV.
	 */
	public Integer getKid3liv() {
		try{
			if (_Kid3liv==null){
				_Kid3liv=getIntegerProperty("KID3LIV");
				return _Kid3liv;
			}else {
				return _Kid3liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID3LIV.
	 * @param v Value to Set.
	 */
	public void setKid3liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID3LIV",v);
		_Kid3liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid3yod=null;

	/**
	 * @return Returns the KID3YOD.
	 */
	public Integer getKid3yod() {
		try{
			if (_Kid3yod==null){
				_Kid3yod=getIntegerProperty("KID3YOD");
				return _Kid3yod;
			}else {
				return _Kid3yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID3YOD.
	 * @param v Value to Set.
	 */
	public void setKid3yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID3YOD",v);
		_Kid3yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid3dem=null;

	/**
	 * @return Returns the KID3DEM.
	 */
	public Integer getKid3dem() {
		try{
			if (_Kid3dem==null){
				_Kid3dem=getIntegerProperty("KID3DEM");
				return _Kid3dem;
			}else {
				return _Kid3dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID3DEM.
	 * @param v Value to Set.
	 */
	public void setKid3dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID3DEM",v);
		_Kid3dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid3ons=null;

	/**
	 * @return Returns the KID3ONS.
	 */
	public Integer getKid3ons() {
		try{
			if (_Kid3ons==null){
				_Kid3ons=getIntegerProperty("KID3ONS");
				return _Kid3ons;
			}else {
				return _Kid3ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID3ONS.
	 * @param v Value to Set.
	 */
	public void setKid3ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID3ONS",v);
		_Kid3ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid3auto=null;

	/**
	 * @return Returns the KID3AUTO.
	 */
	public Integer getKid3auto() {
		try{
			if (_Kid3auto==null){
				_Kid3auto=getIntegerProperty("KID3AUTO");
				return _Kid3auto;
			}else {
				return _Kid3auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID3AUTO.
	 * @param v Value to Set.
	 */
	public void setKid3auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID3AUTO",v);
		_Kid3auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid3sex=null;

	/**
	 * @return Returns the KID3SEX.
	 */
	public Integer getKid3sex() {
		try{
			if (_Kid3sex==null){
				_Kid3sex=getIntegerProperty("KID3SEX");
				return _Kid3sex;
			}else {
				return _Kid3sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID3SEX.
	 * @param v Value to Set.
	 */
	public void setKid3sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID3SEX",v);
		_Kid3sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid4id=null;

	/**
	 * @return Returns the KID4ID.
	 */
	public String getKid4id(){
		try{
			if (_Kid4id==null){
				_Kid4id=getStringProperty("KID4ID");
				return _Kid4id;
			}else {
				return _Kid4id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID4ID.
	 * @param v Value to Set.
	 */
	public void setKid4id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID4ID",v);
		_Kid4id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid4yob=null;

	/**
	 * @return Returns the KID4YOB.
	 */
	public Integer getKid4yob() {
		try{
			if (_Kid4yob==null){
				_Kid4yob=getIntegerProperty("KID4YOB");
				return _Kid4yob;
			}else {
				return _Kid4yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID4YOB.
	 * @param v Value to Set.
	 */
	public void setKid4yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID4YOB",v);
		_Kid4yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid4liv=null;

	/**
	 * @return Returns the KID4LIV.
	 */
	public Integer getKid4liv() {
		try{
			if (_Kid4liv==null){
				_Kid4liv=getIntegerProperty("KID4LIV");
				return _Kid4liv;
			}else {
				return _Kid4liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID4LIV.
	 * @param v Value to Set.
	 */
	public void setKid4liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID4LIV",v);
		_Kid4liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid4yod=null;

	/**
	 * @return Returns the KID4YOD.
	 */
	public Integer getKid4yod() {
		try{
			if (_Kid4yod==null){
				_Kid4yod=getIntegerProperty("KID4YOD");
				return _Kid4yod;
			}else {
				return _Kid4yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID4YOD.
	 * @param v Value to Set.
	 */
	public void setKid4yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID4YOD",v);
		_Kid4yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid4dem=null;

	/**
	 * @return Returns the KID4DEM.
	 */
	public Integer getKid4dem() {
		try{
			if (_Kid4dem==null){
				_Kid4dem=getIntegerProperty("KID4DEM");
				return _Kid4dem;
			}else {
				return _Kid4dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID4DEM.
	 * @param v Value to Set.
	 */
	public void setKid4dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID4DEM",v);
		_Kid4dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid4ons=null;

	/**
	 * @return Returns the KID4ONS.
	 */
	public Integer getKid4ons() {
		try{
			if (_Kid4ons==null){
				_Kid4ons=getIntegerProperty("KID4ONS");
				return _Kid4ons;
			}else {
				return _Kid4ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID4ONS.
	 * @param v Value to Set.
	 */
	public void setKid4ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID4ONS",v);
		_Kid4ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid4auto=null;

	/**
	 * @return Returns the KID4AUTO.
	 */
	public Integer getKid4auto() {
		try{
			if (_Kid4auto==null){
				_Kid4auto=getIntegerProperty("KID4AUTO");
				return _Kid4auto;
			}else {
				return _Kid4auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID4AUTO.
	 * @param v Value to Set.
	 */
	public void setKid4auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID4AUTO",v);
		_Kid4auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid4sex=null;

	/**
	 * @return Returns the KID4SEX.
	 */
	public Integer getKid4sex() {
		try{
			if (_Kid4sex==null){
				_Kid4sex=getIntegerProperty("KID4SEX");
				return _Kid4sex;
			}else {
				return _Kid4sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID4SEX.
	 * @param v Value to Set.
	 */
	public void setKid4sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID4SEX",v);
		_Kid4sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid5id=null;

	/**
	 * @return Returns the KID5ID.
	 */
	public String getKid5id(){
		try{
			if (_Kid5id==null){
				_Kid5id=getStringProperty("KID5ID");
				return _Kid5id;
			}else {
				return _Kid5id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID5ID.
	 * @param v Value to Set.
	 */
	public void setKid5id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID5ID",v);
		_Kid5id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid5yob=null;

	/**
	 * @return Returns the KID5YOB.
	 */
	public Integer getKid5yob() {
		try{
			if (_Kid5yob==null){
				_Kid5yob=getIntegerProperty("KID5YOB");
				return _Kid5yob;
			}else {
				return _Kid5yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID5YOB.
	 * @param v Value to Set.
	 */
	public void setKid5yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID5YOB",v);
		_Kid5yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid5liv=null;

	/**
	 * @return Returns the KID5LIV.
	 */
	public Integer getKid5liv() {
		try{
			if (_Kid5liv==null){
				_Kid5liv=getIntegerProperty("KID5LIV");
				return _Kid5liv;
			}else {
				return _Kid5liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID5LIV.
	 * @param v Value to Set.
	 */
	public void setKid5liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID5LIV",v);
		_Kid5liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid5yod=null;

	/**
	 * @return Returns the KID5YOD.
	 */
	public Integer getKid5yod() {
		try{
			if (_Kid5yod==null){
				_Kid5yod=getIntegerProperty("KID5YOD");
				return _Kid5yod;
			}else {
				return _Kid5yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID5YOD.
	 * @param v Value to Set.
	 */
	public void setKid5yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID5YOD",v);
		_Kid5yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid5dem=null;

	/**
	 * @return Returns the KID5DEM.
	 */
	public Integer getKid5dem() {
		try{
			if (_Kid5dem==null){
				_Kid5dem=getIntegerProperty("KID5DEM");
				return _Kid5dem;
			}else {
				return _Kid5dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID5DEM.
	 * @param v Value to Set.
	 */
	public void setKid5dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID5DEM",v);
		_Kid5dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid5ons=null;

	/**
	 * @return Returns the KID5ONS.
	 */
	public Integer getKid5ons() {
		try{
			if (_Kid5ons==null){
				_Kid5ons=getIntegerProperty("KID5ONS");
				return _Kid5ons;
			}else {
				return _Kid5ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID5ONS.
	 * @param v Value to Set.
	 */
	public void setKid5ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID5ONS",v);
		_Kid5ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid5auto=null;

	/**
	 * @return Returns the KID5AUTO.
	 */
	public Integer getKid5auto() {
		try{
			if (_Kid5auto==null){
				_Kid5auto=getIntegerProperty("KID5AUTO");
				return _Kid5auto;
			}else {
				return _Kid5auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID5AUTO.
	 * @param v Value to Set.
	 */
	public void setKid5auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID5AUTO",v);
		_Kid5auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid5sex=null;

	/**
	 * @return Returns the KID5SEX.
	 */
	public Integer getKid5sex() {
		try{
			if (_Kid5sex==null){
				_Kid5sex=getIntegerProperty("KID5SEX");
				return _Kid5sex;
			}else {
				return _Kid5sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID5SEX.
	 * @param v Value to Set.
	 */
	public void setKid5sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID5SEX",v);
		_Kid5sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid6id=null;

	/**
	 * @return Returns the KID6ID.
	 */
	public String getKid6id(){
		try{
			if (_Kid6id==null){
				_Kid6id=getStringProperty("KID6ID");
				return _Kid6id;
			}else {
				return _Kid6id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID6ID.
	 * @param v Value to Set.
	 */
	public void setKid6id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID6ID",v);
		_Kid6id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid6yob=null;

	/**
	 * @return Returns the KID6YOB.
	 */
	public Integer getKid6yob() {
		try{
			if (_Kid6yob==null){
				_Kid6yob=getIntegerProperty("KID6YOB");
				return _Kid6yob;
			}else {
				return _Kid6yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID6YOB.
	 * @param v Value to Set.
	 */
	public void setKid6yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID6YOB",v);
		_Kid6yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid6liv=null;

	/**
	 * @return Returns the KID6LIV.
	 */
	public Integer getKid6liv() {
		try{
			if (_Kid6liv==null){
				_Kid6liv=getIntegerProperty("KID6LIV");
				return _Kid6liv;
			}else {
				return _Kid6liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID6LIV.
	 * @param v Value to Set.
	 */
	public void setKid6liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID6LIV",v);
		_Kid6liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid6yod=null;

	/**
	 * @return Returns the KID6YOD.
	 */
	public Integer getKid6yod() {
		try{
			if (_Kid6yod==null){
				_Kid6yod=getIntegerProperty("KID6YOD");
				return _Kid6yod;
			}else {
				return _Kid6yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID6YOD.
	 * @param v Value to Set.
	 */
	public void setKid6yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID6YOD",v);
		_Kid6yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid6dem=null;

	/**
	 * @return Returns the KID6DEM.
	 */
	public Integer getKid6dem() {
		try{
			if (_Kid6dem==null){
				_Kid6dem=getIntegerProperty("KID6DEM");
				return _Kid6dem;
			}else {
				return _Kid6dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID6DEM.
	 * @param v Value to Set.
	 */
	public void setKid6dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID6DEM",v);
		_Kid6dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid6ons=null;

	/**
	 * @return Returns the KID6ONS.
	 */
	public Integer getKid6ons() {
		try{
			if (_Kid6ons==null){
				_Kid6ons=getIntegerProperty("KID6ONS");
				return _Kid6ons;
			}else {
				return _Kid6ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID6ONS.
	 * @param v Value to Set.
	 */
	public void setKid6ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID6ONS",v);
		_Kid6ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid6auto=null;

	/**
	 * @return Returns the KID6AUTO.
	 */
	public Integer getKid6auto() {
		try{
			if (_Kid6auto==null){
				_Kid6auto=getIntegerProperty("KID6AUTO");
				return _Kid6auto;
			}else {
				return _Kid6auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID6AUTO.
	 * @param v Value to Set.
	 */
	public void setKid6auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID6AUTO",v);
		_Kid6auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid6sex=null;

	/**
	 * @return Returns the KID6SEX.
	 */
	public Integer getKid6sex() {
		try{
			if (_Kid6sex==null){
				_Kid6sex=getIntegerProperty("KID6SEX");
				return _Kid6sex;
			}else {
				return _Kid6sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID6SEX.
	 * @param v Value to Set.
	 */
	public void setKid6sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID6SEX",v);
		_Kid6sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid7id=null;

	/**
	 * @return Returns the KID7ID.
	 */
	public String getKid7id(){
		try{
			if (_Kid7id==null){
				_Kid7id=getStringProperty("KID7ID");
				return _Kid7id;
			}else {
				return _Kid7id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID7ID.
	 * @param v Value to Set.
	 */
	public void setKid7id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID7ID",v);
		_Kid7id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid7yob=null;

	/**
	 * @return Returns the KID7YOB.
	 */
	public Integer getKid7yob() {
		try{
			if (_Kid7yob==null){
				_Kid7yob=getIntegerProperty("KID7YOB");
				return _Kid7yob;
			}else {
				return _Kid7yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID7YOB.
	 * @param v Value to Set.
	 */
	public void setKid7yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID7YOB",v);
		_Kid7yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid7liv=null;

	/**
	 * @return Returns the KID7LIV.
	 */
	public Integer getKid7liv() {
		try{
			if (_Kid7liv==null){
				_Kid7liv=getIntegerProperty("KID7LIV");
				return _Kid7liv;
			}else {
				return _Kid7liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID7LIV.
	 * @param v Value to Set.
	 */
	public void setKid7liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID7LIV",v);
		_Kid7liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid7yod=null;

	/**
	 * @return Returns the KID7YOD.
	 */
	public Integer getKid7yod() {
		try{
			if (_Kid7yod==null){
				_Kid7yod=getIntegerProperty("KID7YOD");
				return _Kid7yod;
			}else {
				return _Kid7yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID7YOD.
	 * @param v Value to Set.
	 */
	public void setKid7yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID7YOD",v);
		_Kid7yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid7dem=null;

	/**
	 * @return Returns the KID7DEM.
	 */
	public Integer getKid7dem() {
		try{
			if (_Kid7dem==null){
				_Kid7dem=getIntegerProperty("KID7DEM");
				return _Kid7dem;
			}else {
				return _Kid7dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID7DEM.
	 * @param v Value to Set.
	 */
	public void setKid7dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID7DEM",v);
		_Kid7dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid7ons=null;

	/**
	 * @return Returns the KID7ONS.
	 */
	public Integer getKid7ons() {
		try{
			if (_Kid7ons==null){
				_Kid7ons=getIntegerProperty("KID7ONS");
				return _Kid7ons;
			}else {
				return _Kid7ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID7ONS.
	 * @param v Value to Set.
	 */
	public void setKid7ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID7ONS",v);
		_Kid7ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid7auto=null;

	/**
	 * @return Returns the KID7AUTO.
	 */
	public Integer getKid7auto() {
		try{
			if (_Kid7auto==null){
				_Kid7auto=getIntegerProperty("KID7AUTO");
				return _Kid7auto;
			}else {
				return _Kid7auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID7AUTO.
	 * @param v Value to Set.
	 */
	public void setKid7auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID7AUTO",v);
		_Kid7auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid7sex=null;

	/**
	 * @return Returns the KID7SEX.
	 */
	public Integer getKid7sex() {
		try{
			if (_Kid7sex==null){
				_Kid7sex=getIntegerProperty("KID7SEX");
				return _Kid7sex;
			}else {
				return _Kid7sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID7SEX.
	 * @param v Value to Set.
	 */
	public void setKid7sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID7SEX",v);
		_Kid7sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid8id=null;

	/**
	 * @return Returns the KID8ID.
	 */
	public String getKid8id(){
		try{
			if (_Kid8id==null){
				_Kid8id=getStringProperty("KID8ID");
				return _Kid8id;
			}else {
				return _Kid8id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID8ID.
	 * @param v Value to Set.
	 */
	public void setKid8id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID8ID",v);
		_Kid8id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid8yob=null;

	/**
	 * @return Returns the KID8YOB.
	 */
	public Integer getKid8yob() {
		try{
			if (_Kid8yob==null){
				_Kid8yob=getIntegerProperty("KID8YOB");
				return _Kid8yob;
			}else {
				return _Kid8yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID8YOB.
	 * @param v Value to Set.
	 */
	public void setKid8yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID8YOB",v);
		_Kid8yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid8liv=null;

	/**
	 * @return Returns the KID8LIV.
	 */
	public Integer getKid8liv() {
		try{
			if (_Kid8liv==null){
				_Kid8liv=getIntegerProperty("KID8LIV");
				return _Kid8liv;
			}else {
				return _Kid8liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID8LIV.
	 * @param v Value to Set.
	 */
	public void setKid8liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID8LIV",v);
		_Kid8liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid8yod=null;

	/**
	 * @return Returns the KID8YOD.
	 */
	public Integer getKid8yod() {
		try{
			if (_Kid8yod==null){
				_Kid8yod=getIntegerProperty("KID8YOD");
				return _Kid8yod;
			}else {
				return _Kid8yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID8YOD.
	 * @param v Value to Set.
	 */
	public void setKid8yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID8YOD",v);
		_Kid8yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid8dem=null;

	/**
	 * @return Returns the KID8DEM.
	 */
	public Integer getKid8dem() {
		try{
			if (_Kid8dem==null){
				_Kid8dem=getIntegerProperty("KID8DEM");
				return _Kid8dem;
			}else {
				return _Kid8dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID8DEM.
	 * @param v Value to Set.
	 */
	public void setKid8dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID8DEM",v);
		_Kid8dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid8ons=null;

	/**
	 * @return Returns the KID8ONS.
	 */
	public Integer getKid8ons() {
		try{
			if (_Kid8ons==null){
				_Kid8ons=getIntegerProperty("KID8ONS");
				return _Kid8ons;
			}else {
				return _Kid8ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID8ONS.
	 * @param v Value to Set.
	 */
	public void setKid8ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID8ONS",v);
		_Kid8ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid8auto=null;

	/**
	 * @return Returns the KID8AUTO.
	 */
	public Integer getKid8auto() {
		try{
			if (_Kid8auto==null){
				_Kid8auto=getIntegerProperty("KID8AUTO");
				return _Kid8auto;
			}else {
				return _Kid8auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID8AUTO.
	 * @param v Value to Set.
	 */
	public void setKid8auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID8AUTO",v);
		_Kid8auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid8sex=null;

	/**
	 * @return Returns the KID8SEX.
	 */
	public Integer getKid8sex() {
		try{
			if (_Kid8sex==null){
				_Kid8sex=getIntegerProperty("KID8SEX");
				return _Kid8sex;
			}else {
				return _Kid8sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID8SEX.
	 * @param v Value to Set.
	 */
	public void setKid8sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID8SEX",v);
		_Kid8sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid9id=null;

	/**
	 * @return Returns the KID9ID.
	 */
	public String getKid9id(){
		try{
			if (_Kid9id==null){
				_Kid9id=getStringProperty("KID9ID");
				return _Kid9id;
			}else {
				return _Kid9id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID9ID.
	 * @param v Value to Set.
	 */
	public void setKid9id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID9ID",v);
		_Kid9id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid9yob=null;

	/**
	 * @return Returns the KID9YOB.
	 */
	public Integer getKid9yob() {
		try{
			if (_Kid9yob==null){
				_Kid9yob=getIntegerProperty("KID9YOB");
				return _Kid9yob;
			}else {
				return _Kid9yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID9YOB.
	 * @param v Value to Set.
	 */
	public void setKid9yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID9YOB",v);
		_Kid9yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid9liv=null;

	/**
	 * @return Returns the KID9LIV.
	 */
	public Integer getKid9liv() {
		try{
			if (_Kid9liv==null){
				_Kid9liv=getIntegerProperty("KID9LIV");
				return _Kid9liv;
			}else {
				return _Kid9liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID9LIV.
	 * @param v Value to Set.
	 */
	public void setKid9liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID9LIV",v);
		_Kid9liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid9yod=null;

	/**
	 * @return Returns the KID9YOD.
	 */
	public Integer getKid9yod() {
		try{
			if (_Kid9yod==null){
				_Kid9yod=getIntegerProperty("KID9YOD");
				return _Kid9yod;
			}else {
				return _Kid9yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID9YOD.
	 * @param v Value to Set.
	 */
	public void setKid9yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID9YOD",v);
		_Kid9yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid9dem=null;

	/**
	 * @return Returns the KID9DEM.
	 */
	public Integer getKid9dem() {
		try{
			if (_Kid9dem==null){
				_Kid9dem=getIntegerProperty("KID9DEM");
				return _Kid9dem;
			}else {
				return _Kid9dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID9DEM.
	 * @param v Value to Set.
	 */
	public void setKid9dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID9DEM",v);
		_Kid9dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid9ons=null;

	/**
	 * @return Returns the KID9ONS.
	 */
	public Integer getKid9ons() {
		try{
			if (_Kid9ons==null){
				_Kid9ons=getIntegerProperty("KID9ONS");
				return _Kid9ons;
			}else {
				return _Kid9ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID9ONS.
	 * @param v Value to Set.
	 */
	public void setKid9ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID9ONS",v);
		_Kid9ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid9auto=null;

	/**
	 * @return Returns the KID9AUTO.
	 */
	public Integer getKid9auto() {
		try{
			if (_Kid9auto==null){
				_Kid9auto=getIntegerProperty("KID9AUTO");
				return _Kid9auto;
			}else {
				return _Kid9auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID9AUTO.
	 * @param v Value to Set.
	 */
	public void setKid9auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID9AUTO",v);
		_Kid9auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid9sex=null;

	/**
	 * @return Returns the KID9SEX.
	 */
	public Integer getKid9sex() {
		try{
			if (_Kid9sex==null){
				_Kid9sex=getIntegerProperty("KID9SEX");
				return _Kid9sex;
			}else {
				return _Kid9sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID9SEX.
	 * @param v Value to Set.
	 */
	public void setKid9sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID9SEX",v);
		_Kid9sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid10id=null;

	/**
	 * @return Returns the KID10ID.
	 */
	public String getKid10id(){
		try{
			if (_Kid10id==null){
				_Kid10id=getStringProperty("KID10ID");
				return _Kid10id;
			}else {
				return _Kid10id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID10ID.
	 * @param v Value to Set.
	 */
	public void setKid10id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID10ID",v);
		_Kid10id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid10yob=null;

	/**
	 * @return Returns the KID10YOB.
	 */
	public Integer getKid10yob() {
		try{
			if (_Kid10yob==null){
				_Kid10yob=getIntegerProperty("KID10YOB");
				return _Kid10yob;
			}else {
				return _Kid10yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID10YOB.
	 * @param v Value to Set.
	 */
	public void setKid10yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID10YOB",v);
		_Kid10yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid10liv=null;

	/**
	 * @return Returns the KID10LIV.
	 */
	public Integer getKid10liv() {
		try{
			if (_Kid10liv==null){
				_Kid10liv=getIntegerProperty("KID10LIV");
				return _Kid10liv;
			}else {
				return _Kid10liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID10LIV.
	 * @param v Value to Set.
	 */
	public void setKid10liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID10LIV",v);
		_Kid10liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid10yod=null;

	/**
	 * @return Returns the KID10YOD.
	 */
	public Integer getKid10yod() {
		try{
			if (_Kid10yod==null){
				_Kid10yod=getIntegerProperty("KID10YOD");
				return _Kid10yod;
			}else {
				return _Kid10yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID10YOD.
	 * @param v Value to Set.
	 */
	public void setKid10yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID10YOD",v);
		_Kid10yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid10dem=null;

	/**
	 * @return Returns the KID10DEM.
	 */
	public Integer getKid10dem() {
		try{
			if (_Kid10dem==null){
				_Kid10dem=getIntegerProperty("KID10DEM");
				return _Kid10dem;
			}else {
				return _Kid10dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID10DEM.
	 * @param v Value to Set.
	 */
	public void setKid10dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID10DEM",v);
		_Kid10dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid10ons=null;

	/**
	 * @return Returns the KID10ONS.
	 */
	public Integer getKid10ons() {
		try{
			if (_Kid10ons==null){
				_Kid10ons=getIntegerProperty("KID10ONS");
				return _Kid10ons;
			}else {
				return _Kid10ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID10ONS.
	 * @param v Value to Set.
	 */
	public void setKid10ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID10ONS",v);
		_Kid10ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid10auto=null;

	/**
	 * @return Returns the KID10AUTO.
	 */
	public Integer getKid10auto() {
		try{
			if (_Kid10auto==null){
				_Kid10auto=getIntegerProperty("KID10AUTO");
				return _Kid10auto;
			}else {
				return _Kid10auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID10AUTO.
	 * @param v Value to Set.
	 */
	public void setKid10auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID10AUTO",v);
		_Kid10auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid10sex=null;

	/**
	 * @return Returns the KID10SEX.
	 */
	public Integer getKid10sex() {
		try{
			if (_Kid10sex==null){
				_Kid10sex=getIntegerProperty("KID10SEX");
				return _Kid10sex;
			}else {
				return _Kid10sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID10SEX.
	 * @param v Value to Set.
	 */
	public void setKid10sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID10SEX",v);
		_Kid10sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid11id=null;

	/**
	 * @return Returns the KID11ID.
	 */
	public String getKid11id(){
		try{
			if (_Kid11id==null){
				_Kid11id=getStringProperty("KID11ID");
				return _Kid11id;
			}else {
				return _Kid11id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID11ID.
	 * @param v Value to Set.
	 */
	public void setKid11id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID11ID",v);
		_Kid11id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid11yob=null;

	/**
	 * @return Returns the KID11YOB.
	 */
	public Integer getKid11yob() {
		try{
			if (_Kid11yob==null){
				_Kid11yob=getIntegerProperty("KID11YOB");
				return _Kid11yob;
			}else {
				return _Kid11yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID11YOB.
	 * @param v Value to Set.
	 */
	public void setKid11yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID11YOB",v);
		_Kid11yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid11liv=null;

	/**
	 * @return Returns the KID11LIV.
	 */
	public Integer getKid11liv() {
		try{
			if (_Kid11liv==null){
				_Kid11liv=getIntegerProperty("KID11LIV");
				return _Kid11liv;
			}else {
				return _Kid11liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID11LIV.
	 * @param v Value to Set.
	 */
	public void setKid11liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID11LIV",v);
		_Kid11liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid11yod=null;

	/**
	 * @return Returns the KID11YOD.
	 */
	public Integer getKid11yod() {
		try{
			if (_Kid11yod==null){
				_Kid11yod=getIntegerProperty("KID11YOD");
				return _Kid11yod;
			}else {
				return _Kid11yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID11YOD.
	 * @param v Value to Set.
	 */
	public void setKid11yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID11YOD",v);
		_Kid11yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid11dem=null;

	/**
	 * @return Returns the KID11DEM.
	 */
	public Integer getKid11dem() {
		try{
			if (_Kid11dem==null){
				_Kid11dem=getIntegerProperty("KID11DEM");
				return _Kid11dem;
			}else {
				return _Kid11dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID11DEM.
	 * @param v Value to Set.
	 */
	public void setKid11dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID11DEM",v);
		_Kid11dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid11ons=null;

	/**
	 * @return Returns the KID11ONS.
	 */
	public Integer getKid11ons() {
		try{
			if (_Kid11ons==null){
				_Kid11ons=getIntegerProperty("KID11ONS");
				return _Kid11ons;
			}else {
				return _Kid11ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID11ONS.
	 * @param v Value to Set.
	 */
	public void setKid11ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID11ONS",v);
		_Kid11ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid11auto=null;

	/**
	 * @return Returns the KID11AUTO.
	 */
	public Integer getKid11auto() {
		try{
			if (_Kid11auto==null){
				_Kid11auto=getIntegerProperty("KID11AUTO");
				return _Kid11auto;
			}else {
				return _Kid11auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID11AUTO.
	 * @param v Value to Set.
	 */
	public void setKid11auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID11AUTO",v);
		_Kid11auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid11sex=null;

	/**
	 * @return Returns the KID11SEX.
	 */
	public Integer getKid11sex() {
		try{
			if (_Kid11sex==null){
				_Kid11sex=getIntegerProperty("KID11SEX");
				return _Kid11sex;
			}else {
				return _Kid11sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID11SEX.
	 * @param v Value to Set.
	 */
	public void setKid11sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID11SEX",v);
		_Kid11sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid12id=null;

	/**
	 * @return Returns the KID12ID.
	 */
	public String getKid12id(){
		try{
			if (_Kid12id==null){
				_Kid12id=getStringProperty("KID12ID");
				return _Kid12id;
			}else {
				return _Kid12id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID12ID.
	 * @param v Value to Set.
	 */
	public void setKid12id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID12ID",v);
		_Kid12id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid12yob=null;

	/**
	 * @return Returns the KID12YOB.
	 */
	public Integer getKid12yob() {
		try{
			if (_Kid12yob==null){
				_Kid12yob=getIntegerProperty("KID12YOB");
				return _Kid12yob;
			}else {
				return _Kid12yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID12YOB.
	 * @param v Value to Set.
	 */
	public void setKid12yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID12YOB",v);
		_Kid12yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid12liv=null;

	/**
	 * @return Returns the KID12LIV.
	 */
	public Integer getKid12liv() {
		try{
			if (_Kid12liv==null){
				_Kid12liv=getIntegerProperty("KID12LIV");
				return _Kid12liv;
			}else {
				return _Kid12liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID12LIV.
	 * @param v Value to Set.
	 */
	public void setKid12liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID12LIV",v);
		_Kid12liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid12yod=null;

	/**
	 * @return Returns the KID12YOD.
	 */
	public Integer getKid12yod() {
		try{
			if (_Kid12yod==null){
				_Kid12yod=getIntegerProperty("KID12YOD");
				return _Kid12yod;
			}else {
				return _Kid12yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID12YOD.
	 * @param v Value to Set.
	 */
	public void setKid12yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID12YOD",v);
		_Kid12yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid12dem=null;

	/**
	 * @return Returns the KID12DEM.
	 */
	public Integer getKid12dem() {
		try{
			if (_Kid12dem==null){
				_Kid12dem=getIntegerProperty("KID12DEM");
				return _Kid12dem;
			}else {
				return _Kid12dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID12DEM.
	 * @param v Value to Set.
	 */
	public void setKid12dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID12DEM",v);
		_Kid12dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid12ons=null;

	/**
	 * @return Returns the KID12ONS.
	 */
	public Integer getKid12ons() {
		try{
			if (_Kid12ons==null){
				_Kid12ons=getIntegerProperty("KID12ONS");
				return _Kid12ons;
			}else {
				return _Kid12ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID12ONS.
	 * @param v Value to Set.
	 */
	public void setKid12ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID12ONS",v);
		_Kid12ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid12auto=null;

	/**
	 * @return Returns the KID12AUTO.
	 */
	public Integer getKid12auto() {
		try{
			if (_Kid12auto==null){
				_Kid12auto=getIntegerProperty("KID12AUTO");
				return _Kid12auto;
			}else {
				return _Kid12auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID12AUTO.
	 * @param v Value to Set.
	 */
	public void setKid12auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID12AUTO",v);
		_Kid12auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid12sex=null;

	/**
	 * @return Returns the KID12SEX.
	 */
	public Integer getKid12sex() {
		try{
			if (_Kid12sex==null){
				_Kid12sex=getIntegerProperty("KID12SEX");
				return _Kid12sex;
			}else {
				return _Kid12sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID12SEX.
	 * @param v Value to Set.
	 */
	public void setKid12sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID12SEX",v);
		_Kid12sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid13id=null;

	/**
	 * @return Returns the KID13ID.
	 */
	public String getKid13id(){
		try{
			if (_Kid13id==null){
				_Kid13id=getStringProperty("KID13ID");
				return _Kid13id;
			}else {
				return _Kid13id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID13ID.
	 * @param v Value to Set.
	 */
	public void setKid13id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID13ID",v);
		_Kid13id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid13yob=null;

	/**
	 * @return Returns the KID13YOB.
	 */
	public Integer getKid13yob() {
		try{
			if (_Kid13yob==null){
				_Kid13yob=getIntegerProperty("KID13YOB");
				return _Kid13yob;
			}else {
				return _Kid13yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID13YOB.
	 * @param v Value to Set.
	 */
	public void setKid13yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID13YOB",v);
		_Kid13yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid13liv=null;

	/**
	 * @return Returns the KID13LIV.
	 */
	public Integer getKid13liv() {
		try{
			if (_Kid13liv==null){
				_Kid13liv=getIntegerProperty("KID13LIV");
				return _Kid13liv;
			}else {
				return _Kid13liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID13LIV.
	 * @param v Value to Set.
	 */
	public void setKid13liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID13LIV",v);
		_Kid13liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid13yod=null;

	/**
	 * @return Returns the KID13YOD.
	 */
	public Integer getKid13yod() {
		try{
			if (_Kid13yod==null){
				_Kid13yod=getIntegerProperty("KID13YOD");
				return _Kid13yod;
			}else {
				return _Kid13yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID13YOD.
	 * @param v Value to Set.
	 */
	public void setKid13yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID13YOD",v);
		_Kid13yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid13dem=null;

	/**
	 * @return Returns the KID13DEM.
	 */
	public Integer getKid13dem() {
		try{
			if (_Kid13dem==null){
				_Kid13dem=getIntegerProperty("KID13DEM");
				return _Kid13dem;
			}else {
				return _Kid13dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID13DEM.
	 * @param v Value to Set.
	 */
	public void setKid13dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID13DEM",v);
		_Kid13dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid13ons=null;

	/**
	 * @return Returns the KID13ONS.
	 */
	public Integer getKid13ons() {
		try{
			if (_Kid13ons==null){
				_Kid13ons=getIntegerProperty("KID13ONS");
				return _Kid13ons;
			}else {
				return _Kid13ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID13ONS.
	 * @param v Value to Set.
	 */
	public void setKid13ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID13ONS",v);
		_Kid13ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid13auto=null;

	/**
	 * @return Returns the KID13AUTO.
	 */
	public Integer getKid13auto() {
		try{
			if (_Kid13auto==null){
				_Kid13auto=getIntegerProperty("KID13AUTO");
				return _Kid13auto;
			}else {
				return _Kid13auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID13AUTO.
	 * @param v Value to Set.
	 */
	public void setKid13auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID13AUTO",v);
		_Kid13auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid13sex=null;

	/**
	 * @return Returns the KID13SEX.
	 */
	public Integer getKid13sex() {
		try{
			if (_Kid13sex==null){
				_Kid13sex=getIntegerProperty("KID13SEX");
				return _Kid13sex;
			}else {
				return _Kid13sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID13SEX.
	 * @param v Value to Set.
	 */
	public void setKid13sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID13SEX",v);
		_Kid13sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid14id=null;

	/**
	 * @return Returns the KID14ID.
	 */
	public String getKid14id(){
		try{
			if (_Kid14id==null){
				_Kid14id=getStringProperty("KID14ID");
				return _Kid14id;
			}else {
				return _Kid14id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID14ID.
	 * @param v Value to Set.
	 */
	public void setKid14id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID14ID",v);
		_Kid14id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid14yob=null;

	/**
	 * @return Returns the KID14YOB.
	 */
	public Integer getKid14yob() {
		try{
			if (_Kid14yob==null){
				_Kid14yob=getIntegerProperty("KID14YOB");
				return _Kid14yob;
			}else {
				return _Kid14yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID14YOB.
	 * @param v Value to Set.
	 */
	public void setKid14yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID14YOB",v);
		_Kid14yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid14liv=null;

	/**
	 * @return Returns the KID14LIV.
	 */
	public Integer getKid14liv() {
		try{
			if (_Kid14liv==null){
				_Kid14liv=getIntegerProperty("KID14LIV");
				return _Kid14liv;
			}else {
				return _Kid14liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID14LIV.
	 * @param v Value to Set.
	 */
	public void setKid14liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID14LIV",v);
		_Kid14liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid14yod=null;

	/**
	 * @return Returns the KID14YOD.
	 */
	public Integer getKid14yod() {
		try{
			if (_Kid14yod==null){
				_Kid14yod=getIntegerProperty("KID14YOD");
				return _Kid14yod;
			}else {
				return _Kid14yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID14YOD.
	 * @param v Value to Set.
	 */
	public void setKid14yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID14YOD",v);
		_Kid14yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid14dem=null;

	/**
	 * @return Returns the KID14DEM.
	 */
	public Integer getKid14dem() {
		try{
			if (_Kid14dem==null){
				_Kid14dem=getIntegerProperty("KID14DEM");
				return _Kid14dem;
			}else {
				return _Kid14dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID14DEM.
	 * @param v Value to Set.
	 */
	public void setKid14dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID14DEM",v);
		_Kid14dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid14ons=null;

	/**
	 * @return Returns the KID14ONS.
	 */
	public Integer getKid14ons() {
		try{
			if (_Kid14ons==null){
				_Kid14ons=getIntegerProperty("KID14ONS");
				return _Kid14ons;
			}else {
				return _Kid14ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID14ONS.
	 * @param v Value to Set.
	 */
	public void setKid14ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID14ONS",v);
		_Kid14ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid14auto=null;

	/**
	 * @return Returns the KID14AUTO.
	 */
	public Integer getKid14auto() {
		try{
			if (_Kid14auto==null){
				_Kid14auto=getIntegerProperty("KID14AUTO");
				return _Kid14auto;
			}else {
				return _Kid14auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID14AUTO.
	 * @param v Value to Set.
	 */
	public void setKid14auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID14AUTO",v);
		_Kid14auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid14sex=null;

	/**
	 * @return Returns the KID14SEX.
	 */
	public Integer getKid14sex() {
		try{
			if (_Kid14sex==null){
				_Kid14sex=getIntegerProperty("KID14SEX");
				return _Kid14sex;
			}else {
				return _Kid14sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID14SEX.
	 * @param v Value to Set.
	 */
	public void setKid14sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID14SEX",v);
		_Kid14sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Kid15id=null;

	/**
	 * @return Returns the KID15ID.
	 */
	public String getKid15id(){
		try{
			if (_Kid15id==null){
				_Kid15id=getStringProperty("KID15ID");
				return _Kid15id;
			}else {
				return _Kid15id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID15ID.
	 * @param v Value to Set.
	 */
	public void setKid15id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID15ID",v);
		_Kid15id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid15yob=null;

	/**
	 * @return Returns the KID15YOB.
	 */
	public Integer getKid15yob() {
		try{
			if (_Kid15yob==null){
				_Kid15yob=getIntegerProperty("KID15YOB");
				return _Kid15yob;
			}else {
				return _Kid15yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID15YOB.
	 * @param v Value to Set.
	 */
	public void setKid15yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID15YOB",v);
		_Kid15yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid15liv=null;

	/**
	 * @return Returns the KID15LIV.
	 */
	public Integer getKid15liv() {
		try{
			if (_Kid15liv==null){
				_Kid15liv=getIntegerProperty("KID15LIV");
				return _Kid15liv;
			}else {
				return _Kid15liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID15LIV.
	 * @param v Value to Set.
	 */
	public void setKid15liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID15LIV",v);
		_Kid15liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid15yod=null;

	/**
	 * @return Returns the KID15YOD.
	 */
	public Integer getKid15yod() {
		try{
			if (_Kid15yod==null){
				_Kid15yod=getIntegerProperty("KID15YOD");
				return _Kid15yod;
			}else {
				return _Kid15yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID15YOD.
	 * @param v Value to Set.
	 */
	public void setKid15yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID15YOD",v);
		_Kid15yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid15dem=null;

	/**
	 * @return Returns the KID15DEM.
	 */
	public Integer getKid15dem() {
		try{
			if (_Kid15dem==null){
				_Kid15dem=getIntegerProperty("KID15DEM");
				return _Kid15dem;
			}else {
				return _Kid15dem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID15DEM.
	 * @param v Value to Set.
	 */
	public void setKid15dem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID15DEM",v);
		_Kid15dem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid15ons=null;

	/**
	 * @return Returns the KID15ONS.
	 */
	public Integer getKid15ons() {
		try{
			if (_Kid15ons==null){
				_Kid15ons=getIntegerProperty("KID15ONS");
				return _Kid15ons;
			}else {
				return _Kid15ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID15ONS.
	 * @param v Value to Set.
	 */
	public void setKid15ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID15ONS",v);
		_Kid15ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid15auto=null;

	/**
	 * @return Returns the KID15AUTO.
	 */
	public Integer getKid15auto() {
		try{
			if (_Kid15auto==null){
				_Kid15auto=getIntegerProperty("KID15AUTO");
				return _Kid15auto;
			}else {
				return _Kid15auto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID15AUTO.
	 * @param v Value to Set.
	 */
	public void setKid15auto(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID15AUTO",v);
		_Kid15auto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Kid15sex=null;

	/**
	 * @return Returns the KID15SEX.
	 */
	public Integer getKid15sex() {
		try{
			if (_Kid15sex==null){
				_Kid15sex=getIntegerProperty("KID15SEX");
				return _Kid15sex;
			}else {
				return _Kid15sex;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for KID15SEX.
	 * @param v Value to Set.
	 */
	public void setKid15sex(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/KID15SEX",v);
		_Kid15sex=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Relchg=null;

	/**
	 * @return Returns the RELCHG.
	 */
	public Integer getRelchg() {
		try{
			if (_Relchg==null){
				_Relchg=getIntegerProperty("RELCHG");
				return _Relchg;
			}else {
				return _Relchg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RELCHG.
	 * @param v Value to Set.
	 */
	public void setRelchg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RELCHG",v);
		_Relchg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Relsdem=null;

	/**
	 * @return Returns the RELSDEM.
	 */
	public Integer getRelsdem() {
		try{
			if (_Relsdem==null){
				_Relsdem=getIntegerProperty("RELSDEM");
				return _Relsdem;
			}else {
				return _Relsdem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RELSDEM.
	 * @param v Value to Set.
	 */
	public void setRelsdem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RELSDEM",v);
		_Relsdem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel1id=null;

	/**
	 * @return Returns the REL1ID.
	 */
	public String getRel1id(){
		try{
			if (_Rel1id==null){
				_Rel1id=getStringProperty("REL1ID");
				return _Rel1id;
			}else {
				return _Rel1id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL1ID.
	 * @param v Value to Set.
	 */
	public void setRel1id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL1ID",v);
		_Rel1id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel1yob=null;

	/**
	 * @return Returns the REL1YOB.
	 */
	public Integer getRel1yob() {
		try{
			if (_Rel1yob==null){
				_Rel1yob=getIntegerProperty("REL1YOB");
				return _Rel1yob;
			}else {
				return _Rel1yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL1YOB.
	 * @param v Value to Set.
	 */
	public void setRel1yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL1YOB",v);
		_Rel1yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel1liv=null;

	/**
	 * @return Returns the REL1LIV.
	 */
	public Integer getRel1liv() {
		try{
			if (_Rel1liv==null){
				_Rel1liv=getIntegerProperty("REL1LIV");
				return _Rel1liv;
			}else {
				return _Rel1liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL1LIV.
	 * @param v Value to Set.
	 */
	public void setRel1liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL1LIV",v);
		_Rel1liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel1yod=null;

	/**
	 * @return Returns the REL1YOD.
	 */
	public Integer getRel1yod() {
		try{
			if (_Rel1yod==null){
				_Rel1yod=getIntegerProperty("REL1YOD");
				return _Rel1yod;
			}else {
				return _Rel1yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL1YOD.
	 * @param v Value to Set.
	 */
	public void setRel1yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL1YOD",v);
		_Rel1yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel1ons=null;

	/**
	 * @return Returns the REL1ONS.
	 */
	public Integer getRel1ons() {
		try{
			if (_Rel1ons==null){
				_Rel1ons=getIntegerProperty("REL1ONS");
				return _Rel1ons;
			}else {
				return _Rel1ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL1ONS.
	 * @param v Value to Set.
	 */
	public void setRel1ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL1ONS",v);
		_Rel1ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel2id=null;

	/**
	 * @return Returns the REL2ID.
	 */
	public String getRel2id(){
		try{
			if (_Rel2id==null){
				_Rel2id=getStringProperty("REL2ID");
				return _Rel2id;
			}else {
				return _Rel2id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL2ID.
	 * @param v Value to Set.
	 */
	public void setRel2id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL2ID",v);
		_Rel2id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel2yob=null;

	/**
	 * @return Returns the REL2YOB.
	 */
	public Integer getRel2yob() {
		try{
			if (_Rel2yob==null){
				_Rel2yob=getIntegerProperty("REL2YOB");
				return _Rel2yob;
			}else {
				return _Rel2yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL2YOB.
	 * @param v Value to Set.
	 */
	public void setRel2yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL2YOB",v);
		_Rel2yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel2liv=null;

	/**
	 * @return Returns the REL2LIV.
	 */
	public Integer getRel2liv() {
		try{
			if (_Rel2liv==null){
				_Rel2liv=getIntegerProperty("REL2LIV");
				return _Rel2liv;
			}else {
				return _Rel2liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL2LIV.
	 * @param v Value to Set.
	 */
	public void setRel2liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL2LIV",v);
		_Rel2liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel2yod=null;

	/**
	 * @return Returns the REL2YOD.
	 */
	public Integer getRel2yod() {
		try{
			if (_Rel2yod==null){
				_Rel2yod=getIntegerProperty("REL2YOD");
				return _Rel2yod;
			}else {
				return _Rel2yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL2YOD.
	 * @param v Value to Set.
	 */
	public void setRel2yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL2YOD",v);
		_Rel2yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel2ons=null;

	/**
	 * @return Returns the REL2ONS.
	 */
	public Integer getRel2ons() {
		try{
			if (_Rel2ons==null){
				_Rel2ons=getIntegerProperty("REL2ONS");
				return _Rel2ons;
			}else {
				return _Rel2ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL2ONS.
	 * @param v Value to Set.
	 */
	public void setRel2ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL2ONS",v);
		_Rel2ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel3id=null;

	/**
	 * @return Returns the REL3ID.
	 */
	public String getRel3id(){
		try{
			if (_Rel3id==null){
				_Rel3id=getStringProperty("REL3ID");
				return _Rel3id;
			}else {
				return _Rel3id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL3ID.
	 * @param v Value to Set.
	 */
	public void setRel3id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL3ID",v);
		_Rel3id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel3yob=null;

	/**
	 * @return Returns the REL3YOB.
	 */
	public Integer getRel3yob() {
		try{
			if (_Rel3yob==null){
				_Rel3yob=getIntegerProperty("REL3YOB");
				return _Rel3yob;
			}else {
				return _Rel3yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL3YOB.
	 * @param v Value to Set.
	 */
	public void setRel3yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL3YOB",v);
		_Rel3yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel3liv=null;

	/**
	 * @return Returns the REL3LIV.
	 */
	public Integer getRel3liv() {
		try{
			if (_Rel3liv==null){
				_Rel3liv=getIntegerProperty("REL3LIV");
				return _Rel3liv;
			}else {
				return _Rel3liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL3LIV.
	 * @param v Value to Set.
	 */
	public void setRel3liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL3LIV",v);
		_Rel3liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel3yod=null;

	/**
	 * @return Returns the REL3YOD.
	 */
	public Integer getRel3yod() {
		try{
			if (_Rel3yod==null){
				_Rel3yod=getIntegerProperty("REL3YOD");
				return _Rel3yod;
			}else {
				return _Rel3yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL3YOD.
	 * @param v Value to Set.
	 */
	public void setRel3yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL3YOD",v);
		_Rel3yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel3ons=null;

	/**
	 * @return Returns the REL3ONS.
	 */
	public Integer getRel3ons() {
		try{
			if (_Rel3ons==null){
				_Rel3ons=getIntegerProperty("REL3ONS");
				return _Rel3ons;
			}else {
				return _Rel3ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL3ONS.
	 * @param v Value to Set.
	 */
	public void setRel3ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL3ONS",v);
		_Rel3ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel4id=null;

	/**
	 * @return Returns the REL4ID.
	 */
	public String getRel4id(){
		try{
			if (_Rel4id==null){
				_Rel4id=getStringProperty("REL4ID");
				return _Rel4id;
			}else {
				return _Rel4id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL4ID.
	 * @param v Value to Set.
	 */
	public void setRel4id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL4ID",v);
		_Rel4id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel4yob=null;

	/**
	 * @return Returns the REL4YOB.
	 */
	public Integer getRel4yob() {
		try{
			if (_Rel4yob==null){
				_Rel4yob=getIntegerProperty("REL4YOB");
				return _Rel4yob;
			}else {
				return _Rel4yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL4YOB.
	 * @param v Value to Set.
	 */
	public void setRel4yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL4YOB",v);
		_Rel4yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel4liv=null;

	/**
	 * @return Returns the REL4LIV.
	 */
	public Integer getRel4liv() {
		try{
			if (_Rel4liv==null){
				_Rel4liv=getIntegerProperty("REL4LIV");
				return _Rel4liv;
			}else {
				return _Rel4liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL4LIV.
	 * @param v Value to Set.
	 */
	public void setRel4liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL4LIV",v);
		_Rel4liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel4yod=null;

	/**
	 * @return Returns the REL4YOD.
	 */
	public Integer getRel4yod() {
		try{
			if (_Rel4yod==null){
				_Rel4yod=getIntegerProperty("REL4YOD");
				return _Rel4yod;
			}else {
				return _Rel4yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL4YOD.
	 * @param v Value to Set.
	 */
	public void setRel4yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL4YOD",v);
		_Rel4yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel4ons=null;

	/**
	 * @return Returns the REL4ONS.
	 */
	public Integer getRel4ons() {
		try{
			if (_Rel4ons==null){
				_Rel4ons=getIntegerProperty("REL4ONS");
				return _Rel4ons;
			}else {
				return _Rel4ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL4ONS.
	 * @param v Value to Set.
	 */
	public void setRel4ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL4ONS",v);
		_Rel4ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel5id=null;

	/**
	 * @return Returns the REL5ID.
	 */
	public String getRel5id(){
		try{
			if (_Rel5id==null){
				_Rel5id=getStringProperty("REL5ID");
				return _Rel5id;
			}else {
				return _Rel5id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL5ID.
	 * @param v Value to Set.
	 */
	public void setRel5id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL5ID",v);
		_Rel5id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel5yob=null;

	/**
	 * @return Returns the REL5YOB.
	 */
	public Integer getRel5yob() {
		try{
			if (_Rel5yob==null){
				_Rel5yob=getIntegerProperty("REL5YOB");
				return _Rel5yob;
			}else {
				return _Rel5yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL5YOB.
	 * @param v Value to Set.
	 */
	public void setRel5yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL5YOB",v);
		_Rel5yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel5liv=null;

	/**
	 * @return Returns the REL5LIV.
	 */
	public Integer getRel5liv() {
		try{
			if (_Rel5liv==null){
				_Rel5liv=getIntegerProperty("REL5LIV");
				return _Rel5liv;
			}else {
				return _Rel5liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL5LIV.
	 * @param v Value to Set.
	 */
	public void setRel5liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL5LIV",v);
		_Rel5liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel5yod=null;

	/**
	 * @return Returns the REL5YOD.
	 */
	public Integer getRel5yod() {
		try{
			if (_Rel5yod==null){
				_Rel5yod=getIntegerProperty("REL5YOD");
				return _Rel5yod;
			}else {
				return _Rel5yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL5YOD.
	 * @param v Value to Set.
	 */
	public void setRel5yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL5YOD",v);
		_Rel5yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel5ons=null;

	/**
	 * @return Returns the REL5ONS.
	 */
	public Integer getRel5ons() {
		try{
			if (_Rel5ons==null){
				_Rel5ons=getIntegerProperty("REL5ONS");
				return _Rel5ons;
			}else {
				return _Rel5ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL5ONS.
	 * @param v Value to Set.
	 */
	public void setRel5ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL5ONS",v);
		_Rel5ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel6id=null;

	/**
	 * @return Returns the REL6ID.
	 */
	public String getRel6id(){
		try{
			if (_Rel6id==null){
				_Rel6id=getStringProperty("REL6ID");
				return _Rel6id;
			}else {
				return _Rel6id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL6ID.
	 * @param v Value to Set.
	 */
	public void setRel6id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL6ID",v);
		_Rel6id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel6yob=null;

	/**
	 * @return Returns the REL6YOB.
	 */
	public Integer getRel6yob() {
		try{
			if (_Rel6yob==null){
				_Rel6yob=getIntegerProperty("REL6YOB");
				return _Rel6yob;
			}else {
				return _Rel6yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL6YOB.
	 * @param v Value to Set.
	 */
	public void setRel6yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL6YOB",v);
		_Rel6yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel6liv=null;

	/**
	 * @return Returns the REL6LIV.
	 */
	public Integer getRel6liv() {
		try{
			if (_Rel6liv==null){
				_Rel6liv=getIntegerProperty("REL6LIV");
				return _Rel6liv;
			}else {
				return _Rel6liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL6LIV.
	 * @param v Value to Set.
	 */
	public void setRel6liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL6LIV",v);
		_Rel6liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel6yod=null;

	/**
	 * @return Returns the REL6YOD.
	 */
	public Integer getRel6yod() {
		try{
			if (_Rel6yod==null){
				_Rel6yod=getIntegerProperty("REL6YOD");
				return _Rel6yod;
			}else {
				return _Rel6yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL6YOD.
	 * @param v Value to Set.
	 */
	public void setRel6yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL6YOD",v);
		_Rel6yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel6ons=null;

	/**
	 * @return Returns the REL6ONS.
	 */
	public Integer getRel6ons() {
		try{
			if (_Rel6ons==null){
				_Rel6ons=getIntegerProperty("REL6ONS");
				return _Rel6ons;
			}else {
				return _Rel6ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL6ONS.
	 * @param v Value to Set.
	 */
	public void setRel6ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL6ONS",v);
		_Rel6ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel7id=null;

	/**
	 * @return Returns the REL7ID.
	 */
	public String getRel7id(){
		try{
			if (_Rel7id==null){
				_Rel7id=getStringProperty("REL7ID");
				return _Rel7id;
			}else {
				return _Rel7id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL7ID.
	 * @param v Value to Set.
	 */
	public void setRel7id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL7ID",v);
		_Rel7id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel7yob=null;

	/**
	 * @return Returns the REL7YOB.
	 */
	public Integer getRel7yob() {
		try{
			if (_Rel7yob==null){
				_Rel7yob=getIntegerProperty("REL7YOB");
				return _Rel7yob;
			}else {
				return _Rel7yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL7YOB.
	 * @param v Value to Set.
	 */
	public void setRel7yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL7YOB",v);
		_Rel7yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel7liv=null;

	/**
	 * @return Returns the REL7LIV.
	 */
	public Integer getRel7liv() {
		try{
			if (_Rel7liv==null){
				_Rel7liv=getIntegerProperty("REL7LIV");
				return _Rel7liv;
			}else {
				return _Rel7liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL7LIV.
	 * @param v Value to Set.
	 */
	public void setRel7liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL7LIV",v);
		_Rel7liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel7yod=null;

	/**
	 * @return Returns the REL7YOD.
	 */
	public Integer getRel7yod() {
		try{
			if (_Rel7yod==null){
				_Rel7yod=getIntegerProperty("REL7YOD");
				return _Rel7yod;
			}else {
				return _Rel7yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL7YOD.
	 * @param v Value to Set.
	 */
	public void setRel7yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL7YOD",v);
		_Rel7yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel7ons=null;

	/**
	 * @return Returns the REL7ONS.
	 */
	public Integer getRel7ons() {
		try{
			if (_Rel7ons==null){
				_Rel7ons=getIntegerProperty("REL7ONS");
				return _Rel7ons;
			}else {
				return _Rel7ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL7ONS.
	 * @param v Value to Set.
	 */
	public void setRel7ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL7ONS",v);
		_Rel7ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel8id=null;

	/**
	 * @return Returns the REL8ID.
	 */
	public String getRel8id(){
		try{
			if (_Rel8id==null){
				_Rel8id=getStringProperty("REL8ID");
				return _Rel8id;
			}else {
				return _Rel8id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL8ID.
	 * @param v Value to Set.
	 */
	public void setRel8id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL8ID",v);
		_Rel8id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel8yob=null;

	/**
	 * @return Returns the REL8YOB.
	 */
	public Integer getRel8yob() {
		try{
			if (_Rel8yob==null){
				_Rel8yob=getIntegerProperty("REL8YOB");
				return _Rel8yob;
			}else {
				return _Rel8yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL8YOB.
	 * @param v Value to Set.
	 */
	public void setRel8yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL8YOB",v);
		_Rel8yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel8liv=null;

	/**
	 * @return Returns the REL8LIV.
	 */
	public Integer getRel8liv() {
		try{
			if (_Rel8liv==null){
				_Rel8liv=getIntegerProperty("REL8LIV");
				return _Rel8liv;
			}else {
				return _Rel8liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL8LIV.
	 * @param v Value to Set.
	 */
	public void setRel8liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL8LIV",v);
		_Rel8liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel8yod=null;

	/**
	 * @return Returns the REL8YOD.
	 */
	public Integer getRel8yod() {
		try{
			if (_Rel8yod==null){
				_Rel8yod=getIntegerProperty("REL8YOD");
				return _Rel8yod;
			}else {
				return _Rel8yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL8YOD.
	 * @param v Value to Set.
	 */
	public void setRel8yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL8YOD",v);
		_Rel8yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel8ons=null;

	/**
	 * @return Returns the REL8ONS.
	 */
	public Integer getRel8ons() {
		try{
			if (_Rel8ons==null){
				_Rel8ons=getIntegerProperty("REL8ONS");
				return _Rel8ons;
			}else {
				return _Rel8ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL8ONS.
	 * @param v Value to Set.
	 */
	public void setRel8ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL8ONS",v);
		_Rel8ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel9id=null;

	/**
	 * @return Returns the REL9ID.
	 */
	public String getRel9id(){
		try{
			if (_Rel9id==null){
				_Rel9id=getStringProperty("REL9ID");
				return _Rel9id;
			}else {
				return _Rel9id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL9ID.
	 * @param v Value to Set.
	 */
	public void setRel9id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL9ID",v);
		_Rel9id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel9yob=null;

	/**
	 * @return Returns the REL9YOB.
	 */
	public Integer getRel9yob() {
		try{
			if (_Rel9yob==null){
				_Rel9yob=getIntegerProperty("REL9YOB");
				return _Rel9yob;
			}else {
				return _Rel9yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL9YOB.
	 * @param v Value to Set.
	 */
	public void setRel9yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL9YOB",v);
		_Rel9yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel9liv=null;

	/**
	 * @return Returns the REL9LIV.
	 */
	public Integer getRel9liv() {
		try{
			if (_Rel9liv==null){
				_Rel9liv=getIntegerProperty("REL9LIV");
				return _Rel9liv;
			}else {
				return _Rel9liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL9LIV.
	 * @param v Value to Set.
	 */
	public void setRel9liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL9LIV",v);
		_Rel9liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel9yod=null;

	/**
	 * @return Returns the REL9YOD.
	 */
	public Integer getRel9yod() {
		try{
			if (_Rel9yod==null){
				_Rel9yod=getIntegerProperty("REL9YOD");
				return _Rel9yod;
			}else {
				return _Rel9yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL9YOD.
	 * @param v Value to Set.
	 */
	public void setRel9yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL9YOD",v);
		_Rel9yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel9ons=null;

	/**
	 * @return Returns the REL9ONS.
	 */
	public Integer getRel9ons() {
		try{
			if (_Rel9ons==null){
				_Rel9ons=getIntegerProperty("REL9ONS");
				return _Rel9ons;
			}else {
				return _Rel9ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL9ONS.
	 * @param v Value to Set.
	 */
	public void setRel9ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL9ONS",v);
		_Rel9ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel10id=null;

	/**
	 * @return Returns the REL10ID.
	 */
	public String getRel10id(){
		try{
			if (_Rel10id==null){
				_Rel10id=getStringProperty("REL10ID");
				return _Rel10id;
			}else {
				return _Rel10id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL10ID.
	 * @param v Value to Set.
	 */
	public void setRel10id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL10ID",v);
		_Rel10id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel10yob=null;

	/**
	 * @return Returns the REL10YOB.
	 */
	public Integer getRel10yob() {
		try{
			if (_Rel10yob==null){
				_Rel10yob=getIntegerProperty("REL10YOB");
				return _Rel10yob;
			}else {
				return _Rel10yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL10YOB.
	 * @param v Value to Set.
	 */
	public void setRel10yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL10YOB",v);
		_Rel10yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel10liv=null;

	/**
	 * @return Returns the REL10LIV.
	 */
	public Integer getRel10liv() {
		try{
			if (_Rel10liv==null){
				_Rel10liv=getIntegerProperty("REL10LIV");
				return _Rel10liv;
			}else {
				return _Rel10liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL10LIV.
	 * @param v Value to Set.
	 */
	public void setRel10liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL10LIV",v);
		_Rel10liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel10yod=null;

	/**
	 * @return Returns the REL10YOD.
	 */
	public Integer getRel10yod() {
		try{
			if (_Rel10yod==null){
				_Rel10yod=getIntegerProperty("REL10YOD");
				return _Rel10yod;
			}else {
				return _Rel10yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL10YOD.
	 * @param v Value to Set.
	 */
	public void setRel10yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL10YOD",v);
		_Rel10yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel10ons=null;

	/**
	 * @return Returns the REL10ONS.
	 */
	public Integer getRel10ons() {
		try{
			if (_Rel10ons==null){
				_Rel10ons=getIntegerProperty("REL10ONS");
				return _Rel10ons;
			}else {
				return _Rel10ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL10ONS.
	 * @param v Value to Set.
	 */
	public void setRel10ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL10ONS",v);
		_Rel10ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel11id=null;

	/**
	 * @return Returns the REL11ID.
	 */
	public String getRel11id(){
		try{
			if (_Rel11id==null){
				_Rel11id=getStringProperty("REL11ID");
				return _Rel11id;
			}else {
				return _Rel11id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL11ID.
	 * @param v Value to Set.
	 */
	public void setRel11id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL11ID",v);
		_Rel11id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel11yob=null;

	/**
	 * @return Returns the REL11YOB.
	 */
	public Integer getRel11yob() {
		try{
			if (_Rel11yob==null){
				_Rel11yob=getIntegerProperty("REL11YOB");
				return _Rel11yob;
			}else {
				return _Rel11yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL11YOB.
	 * @param v Value to Set.
	 */
	public void setRel11yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL11YOB",v);
		_Rel11yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel11liv=null;

	/**
	 * @return Returns the REL11LIV.
	 */
	public Integer getRel11liv() {
		try{
			if (_Rel11liv==null){
				_Rel11liv=getIntegerProperty("REL11LIV");
				return _Rel11liv;
			}else {
				return _Rel11liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL11LIV.
	 * @param v Value to Set.
	 */
	public void setRel11liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL11LIV",v);
		_Rel11liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel11yod=null;

	/**
	 * @return Returns the REL11YOD.
	 */
	public Integer getRel11yod() {
		try{
			if (_Rel11yod==null){
				_Rel11yod=getIntegerProperty("REL11YOD");
				return _Rel11yod;
			}else {
				return _Rel11yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL11YOD.
	 * @param v Value to Set.
	 */
	public void setRel11yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL11YOD",v);
		_Rel11yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel11ons=null;

	/**
	 * @return Returns the REL11ONS.
	 */
	public Integer getRel11ons() {
		try{
			if (_Rel11ons==null){
				_Rel11ons=getIntegerProperty("REL11ONS");
				return _Rel11ons;
			}else {
				return _Rel11ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL11ONS.
	 * @param v Value to Set.
	 */
	public void setRel11ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL11ONS",v);
		_Rel11ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel12id=null;

	/**
	 * @return Returns the REL12ID.
	 */
	public String getRel12id(){
		try{
			if (_Rel12id==null){
				_Rel12id=getStringProperty("REL12ID");
				return _Rel12id;
			}else {
				return _Rel12id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL12ID.
	 * @param v Value to Set.
	 */
	public void setRel12id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL12ID",v);
		_Rel12id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel12yob=null;

	/**
	 * @return Returns the REL12YOB.
	 */
	public Integer getRel12yob() {
		try{
			if (_Rel12yob==null){
				_Rel12yob=getIntegerProperty("REL12YOB");
				return _Rel12yob;
			}else {
				return _Rel12yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL12YOB.
	 * @param v Value to Set.
	 */
	public void setRel12yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL12YOB",v);
		_Rel12yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel12liv=null;

	/**
	 * @return Returns the REL12LIV.
	 */
	public Integer getRel12liv() {
		try{
			if (_Rel12liv==null){
				_Rel12liv=getIntegerProperty("REL12LIV");
				return _Rel12liv;
			}else {
				return _Rel12liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL12LIV.
	 * @param v Value to Set.
	 */
	public void setRel12liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL12LIV",v);
		_Rel12liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel12yod=null;

	/**
	 * @return Returns the REL12YOD.
	 */
	public Integer getRel12yod() {
		try{
			if (_Rel12yod==null){
				_Rel12yod=getIntegerProperty("REL12YOD");
				return _Rel12yod;
			}else {
				return _Rel12yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL12YOD.
	 * @param v Value to Set.
	 */
	public void setRel12yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL12YOD",v);
		_Rel12yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel12ons=null;

	/**
	 * @return Returns the REL12ONS.
	 */
	public Integer getRel12ons() {
		try{
			if (_Rel12ons==null){
				_Rel12ons=getIntegerProperty("REL12ONS");
				return _Rel12ons;
			}else {
				return _Rel12ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL12ONS.
	 * @param v Value to Set.
	 */
	public void setRel12ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL12ONS",v);
		_Rel12ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel13id=null;

	/**
	 * @return Returns the REL13ID.
	 */
	public String getRel13id(){
		try{
			if (_Rel13id==null){
				_Rel13id=getStringProperty("REL13ID");
				return _Rel13id;
			}else {
				return _Rel13id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL13ID.
	 * @param v Value to Set.
	 */
	public void setRel13id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL13ID",v);
		_Rel13id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel13yob=null;

	/**
	 * @return Returns the REL13YOB.
	 */
	public Integer getRel13yob() {
		try{
			if (_Rel13yob==null){
				_Rel13yob=getIntegerProperty("REL13YOB");
				return _Rel13yob;
			}else {
				return _Rel13yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL13YOB.
	 * @param v Value to Set.
	 */
	public void setRel13yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL13YOB",v);
		_Rel13yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel13liv=null;

	/**
	 * @return Returns the REL13LIV.
	 */
	public Integer getRel13liv() {
		try{
			if (_Rel13liv==null){
				_Rel13liv=getIntegerProperty("REL13LIV");
				return _Rel13liv;
			}else {
				return _Rel13liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL13LIV.
	 * @param v Value to Set.
	 */
	public void setRel13liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL13LIV",v);
		_Rel13liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel13yod=null;

	/**
	 * @return Returns the REL13YOD.
	 */
	public Integer getRel13yod() {
		try{
			if (_Rel13yod==null){
				_Rel13yod=getIntegerProperty("REL13YOD");
				return _Rel13yod;
			}else {
				return _Rel13yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL13YOD.
	 * @param v Value to Set.
	 */
	public void setRel13yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL13YOD",v);
		_Rel13yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel13ons=null;

	/**
	 * @return Returns the REL13ONS.
	 */
	public Integer getRel13ons() {
		try{
			if (_Rel13ons==null){
				_Rel13ons=getIntegerProperty("REL13ONS");
				return _Rel13ons;
			}else {
				return _Rel13ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL13ONS.
	 * @param v Value to Set.
	 */
	public void setRel13ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL13ONS",v);
		_Rel13ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel14id=null;

	/**
	 * @return Returns the REL14ID.
	 */
	public String getRel14id(){
		try{
			if (_Rel14id==null){
				_Rel14id=getStringProperty("REL14ID");
				return _Rel14id;
			}else {
				return _Rel14id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL14ID.
	 * @param v Value to Set.
	 */
	public void setRel14id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL14ID",v);
		_Rel14id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel14yob=null;

	/**
	 * @return Returns the REL14YOB.
	 */
	public Integer getRel14yob() {
		try{
			if (_Rel14yob==null){
				_Rel14yob=getIntegerProperty("REL14YOB");
				return _Rel14yob;
			}else {
				return _Rel14yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL14YOB.
	 * @param v Value to Set.
	 */
	public void setRel14yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL14YOB",v);
		_Rel14yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel14liv=null;

	/**
	 * @return Returns the REL14LIV.
	 */
	public Integer getRel14liv() {
		try{
			if (_Rel14liv==null){
				_Rel14liv=getIntegerProperty("REL14LIV");
				return _Rel14liv;
			}else {
				return _Rel14liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL14LIV.
	 * @param v Value to Set.
	 */
	public void setRel14liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL14LIV",v);
		_Rel14liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel14yod=null;

	/**
	 * @return Returns the REL14YOD.
	 */
	public Integer getRel14yod() {
		try{
			if (_Rel14yod==null){
				_Rel14yod=getIntegerProperty("REL14YOD");
				return _Rel14yod;
			}else {
				return _Rel14yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL14YOD.
	 * @param v Value to Set.
	 */
	public void setRel14yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL14YOD",v);
		_Rel14yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel14ons=null;

	/**
	 * @return Returns the REL14ONS.
	 */
	public Integer getRel14ons() {
		try{
			if (_Rel14ons==null){
				_Rel14ons=getIntegerProperty("REL14ONS");
				return _Rel14ons;
			}else {
				return _Rel14ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL14ONS.
	 * @param v Value to Set.
	 */
	public void setRel14ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL14ONS",v);
		_Rel14ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rel15id=null;

	/**
	 * @return Returns the REL15ID.
	 */
	public String getRel15id(){
		try{
			if (_Rel15id==null){
				_Rel15id=getStringProperty("REL15ID");
				return _Rel15id;
			}else {
				return _Rel15id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL15ID.
	 * @param v Value to Set.
	 */
	public void setRel15id(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL15ID",v);
		_Rel15id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel15yob=null;

	/**
	 * @return Returns the REL15YOB.
	 */
	public Integer getRel15yob() {
		try{
			if (_Rel15yob==null){
				_Rel15yob=getIntegerProperty("REL15YOB");
				return _Rel15yob;
			}else {
				return _Rel15yob;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL15YOB.
	 * @param v Value to Set.
	 */
	public void setRel15yob(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL15YOB",v);
		_Rel15yob=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel15liv=null;

	/**
	 * @return Returns the REL15LIV.
	 */
	public Integer getRel15liv() {
		try{
			if (_Rel15liv==null){
				_Rel15liv=getIntegerProperty("REL15LIV");
				return _Rel15liv;
			}else {
				return _Rel15liv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL15LIV.
	 * @param v Value to Set.
	 */
	public void setRel15liv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL15LIV",v);
		_Rel15liv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel15yod=null;

	/**
	 * @return Returns the REL15YOD.
	 */
	public Integer getRel15yod() {
		try{
			if (_Rel15yod==null){
				_Rel15yod=getIntegerProperty("REL15YOD");
				return _Rel15yod;
			}else {
				return _Rel15yod;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL15YOD.
	 * @param v Value to Set.
	 */
	public void setRel15yod(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL15YOD",v);
		_Rel15yod=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rel15ons=null;

	/**
	 * @return Returns the REL15ONS.
	 */
	public Integer getRel15ons() {
		try{
			if (_Rel15ons==null){
				_Rel15ons=getIntegerProperty("REL15ONS");
				return _Rel15ons;
			}else {
				return _Rel15ons;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REL15ONS.
	 * @param v Value to Set.
	 */
	public void setRel15ons(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REL15ONS",v);
		_Rel15ons=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsA3sbfmhstdata> getAllUdsA3sbfmhstdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA3sbfmhstdata> al = new ArrayList<org.nrg.xdat.om.UdsA3sbfmhstdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA3sbfmhstdata> getUdsA3sbfmhstdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA3sbfmhstdata> al = new ArrayList<org.nrg.xdat.om.UdsA3sbfmhstdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA3sbfmhstdata> getUdsA3sbfmhstdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA3sbfmhstdata> al = new ArrayList<org.nrg.xdat.om.UdsA3sbfmhstdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsA3sbfmhstdata getUdsA3sbfmhstdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:a3sbfmhstData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsA3sbfmhstdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
