/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianAaoevaldata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianAaoevaldataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianAaoevaldata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:aaoevalData";

	public AutoDianAaoevaldata(ItemI item)
	{
		super(item);
	}

	public AutoDianAaoevaldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianAaoevaldata(UserI user)
	 **/
	public AutoDianAaoevaldata(){}

	public AutoDianAaoevaldata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:aaoevalData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Evaldate=null;

	/**
	 * @return Returns the EVALDATE.
	 */
	public Object getEvaldate(){
		try{
			if (_Evaldate==null){
				_Evaldate=getProperty("EVALDATE");
				return _Evaldate;
			}else {
				return _Evaldate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EVALDATE.
	 * @param v Value to Set.
	 */
	public void setEvaldate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EVALDATE",v);
		_Evaldate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Informid=null;

	/**
	 * @return Returns the INFORMID.
	 */
	public String getInformid(){
		try{
			if (_Informid==null){
				_Informid=getStringProperty("INFORMID");
				return _Informid;
			}else {
				return _Informid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INFORMID.
	 * @param v Value to Set.
	 */
	public void setInformid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INFORMID",v);
		_Informid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Proxy=null;

	/**
	 * @return Returns the PROXY.
	 */
	public String getProxy(){
		try{
			if (_Proxy==null){
				_Proxy=getStringProperty("PROXY");
				return _Proxy;
			}else {
				return _Proxy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PROXY.
	 * @param v Value to Set.
	 */
	public void setProxy(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PROXY",v);
		_Proxy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Parentid=null;

	/**
	 * @return Returns the PARENTID.
	 */
	public String getParentid(){
		try{
			if (_Parentid==null){
				_Parentid=getStringProperty("PARENTID");
				return _Parentid;
			}else {
				return _Parentid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PARENTID.
	 * @param v Value to Set.
	 */
	public void setParentid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PARENTID",v);
		_Parentid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Normalyr=null;

	/**
	 * @return Returns the NORMALYR.
	 */
	public Integer getNormalyr() {
		try{
			if (_Normalyr==null){
				_Normalyr=getIntegerProperty("NORMALYR");
				return _Normalyr;
			}else {
				return _Normalyr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NORMALYR.
	 * @param v Value to Set.
	 */
	public void setNormalyr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NORMALYR",v);
		_Normalyr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Symptomyr=null;

	/**
	 * @return Returns the SYMPTOMYR.
	 */
	public Integer getSymptomyr() {
		try{
			if (_Symptomyr==null){
				_Symptomyr=getIntegerProperty("SYMPTOMYR");
				return _Symptomyr;
			}else {
				return _Symptomyr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SYMPTOMYR.
	 * @param v Value to Set.
	 */
	public void setSymptomyr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SYMPTOMYR",v);
		_Symptomyr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Memcogyr=null;

	/**
	 * @return Returns the MEMCOGYR.
	 */
	public Integer getMemcogyr() {
		try{
			if (_Memcogyr==null){
				_Memcogyr=getIntegerProperty("MEMCOGYR");
				return _Memcogyr;
			}else {
				return _Memcogyr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEMCOGYR.
	 * @param v Value to Set.
	 */
	public void setMemcogyr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEMCOGYR",v);
		_Memcogyr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Behavyr=null;

	/**
	 * @return Returns the BEHAVYR.
	 */
	public Integer getBehavyr() {
		try{
			if (_Behavyr==null){
				_Behavyr=getIntegerProperty("BEHAVYR");
				return _Behavyr;
			}else {
				return _Behavyr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BEHAVYR.
	 * @param v Value to Set.
	 */
	public void setBehavyr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BEHAVYR",v);
		_Behavyr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Motoryr=null;

	/**
	 * @return Returns the MOTORYR.
	 */
	public Integer getMotoryr() {
		try{
			if (_Motoryr==null){
				_Motoryr=getIntegerProperty("MOTORYR");
				return _Motoryr;
			}else {
				return _Motoryr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MOTORYR.
	 * @param v Value to Set.
	 */
	public void setMotoryr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MOTORYR",v);
		_Motoryr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Alzyr=null;

	/**
	 * @return Returns the ALZYR.
	 */
	public Integer getAlzyr() {
		try{
			if (_Alzyr==null){
				_Alzyr=getIntegerProperty("ALZYR");
				return _Alzyr;
			}else {
				return _Alzyr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ALZYR.
	 * @param v Value to Set.
	 */
	public void setAlzyr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ALZYR",v);
		_Alzyr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Deathyr=null;

	/**
	 * @return Returns the DEATHYR.
	 */
	public Integer getDeathyr() {
		try{
			if (_Deathyr==null){
				_Deathyr=getIntegerProperty("DEATHYR");
				return _Deathyr;
			}else {
				return _Deathyr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DEATHYR.
	 * @param v Value to Set.
	 */
	public void setDeathyr(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DEATHYR",v);
		_Deathyr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Deathcs=null;

	/**
	 * @return Returns the DEATHCS.
	 */
	public String getDeathcs(){
		try{
			if (_Deathcs==null){
				_Deathcs=getStringProperty("DEATHCS");
				return _Deathcs;
			}else {
				return _Deathcs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DEATHCS.
	 * @param v Value to Set.
	 */
	public void setDeathcs(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DEATHCS",v);
		_Deathcs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Autopsy=null;

	/**
	 * @return Returns the AUTOPSY.
	 */
	public Integer getAutopsy() {
		try{
			if (_Autopsy==null){
				_Autopsy=getIntegerProperty("AUTOPSY");
				return _Autopsy;
			}else {
				return _Autopsy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AUTOPSY.
	 * @param v Value to Set.
	 */
	public void setAutopsy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AUTOPSY",v);
		_Autopsy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Deathcdr=null;

	/**
	 * @return Returns the DEATHCDR.
	 */
	public Double getDeathcdr() {
		try{
			if (_Deathcdr==null){
				_Deathcdr=getDoubleProperty("DEATHCDR");
				return _Deathcdr;
			}else {
				return _Deathcdr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DEATHCDR.
	 * @param v Value to Set.
	 */
	public void setDeathcdr(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DEATHCDR",v);
		_Deathcdr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianAaoevaldata> getAllDianAaoevaldatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAaoevaldata> al = new ArrayList<org.nrg.xdat.om.DianAaoevaldata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianAaoevaldata> getDianAaoevaldatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAaoevaldata> al = new ArrayList<org.nrg.xdat.om.DianAaoevaldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianAaoevaldata> getDianAaoevaldatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianAaoevaldata> al = new ArrayList<org.nrg.xdat.om.DianAaoevaldata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianAaoevaldata getDianAaoevaldatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:aaoevalData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianAaoevaldata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
