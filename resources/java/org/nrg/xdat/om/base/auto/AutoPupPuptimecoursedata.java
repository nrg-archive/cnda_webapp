/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoPupPuptimecoursedata extends XnatPetassessordata implements org.nrg.xdat.model.PupPuptimecoursedataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoPupPuptimecoursedata.class);
	public static String SCHEMA_ELEMENT_NAME="pup:pupTimeCourseData";

	public AutoPupPuptimecoursedata(ItemI item)
	{
		super(item);
	}

	public AutoPupPuptimecoursedata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoPupPuptimecoursedata(UserI user)
	 **/
	public AutoPupPuptimecoursedata(){}

	public AutoPupPuptimecoursedata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "pup:pupTimeCourseData";
	}
	 private org.nrg.xdat.om.XnatPetassessordata _Petassessordata =null;

	/**
	 * petAssessorData
	 * @return org.nrg.xdat.om.XnatPetassessordata
	 */
	public org.nrg.xdat.om.XnatPetassessordata getPetassessordata() {
		try{
			if (_Petassessordata==null){
				_Petassessordata=((XnatPetassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("petAssessorData")));
				return _Petassessordata;
			}else {
				return _Petassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for petAssessorData.
	 * @param v Value to Set.
	 */
	public void setPetassessordata(ItemI v) throws Exception{
		_Petassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * petAssessorData
	 * set org.nrg.xdat.model.XnatPetassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatPetassessordataI> void setPetassessordata(A item) throws Exception{
	setPetassessordata((ItemI)item);
	}

	/**
	 * Removes the petAssessorData.
	 * */
	public void removePetassessordata() {
		_Petassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Proctype=null;

	/**
	 * @return Returns the procType.
	 */
	public String getProctype(){
		try{
			if (_Proctype==null){
				_Proctype=getStringProperty("procType");
				return _Proctype;
			}else {
				return _Proctype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for procType.
	 * @param v Value to Set.
	 */
	public void setProctype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/procType",v);
		_Proctype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Model=null;

	/**
	 * @return Returns the model.
	 */
	public String getModel(){
		try{
			if (_Model==null){
				_Model=getStringProperty("model");
				return _Model;
			}else {
				return _Model;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for model.
	 * @param v Value to Set.
	 */
	public void setModel(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/model",v);
		_Model=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tracer=null;

	/**
	 * @return Returns the tracer.
	 */
	public String getTracer(){
		try{
			if (_Tracer==null){
				_Tracer=getStringProperty("tracer");
				return _Tracer;
			}else {
				return _Tracer;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tracer.
	 * @param v Value to Set.
	 */
	public void setTracer(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tracer",v);
		_Tracer=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Templatetype=null;

	/**
	 * @return Returns the templateType.
	 */
	public String getTemplatetype(){
		try{
			if (_Templatetype==null){
				_Templatetype=getStringProperty("templateType");
				return _Templatetype;
			}else {
				return _Templatetype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for templateType.
	 * @param v Value to Set.
	 */
	public void setTemplatetype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/templateType",v);
		_Templatetype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Fsid=null;

	/**
	 * @return Returns the FSId.
	 */
	public String getFsid(){
		try{
			if (_Fsid==null){
				_Fsid=getStringProperty("FSId");
				return _Fsid;
			}else {
				return _Fsid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FSId.
	 * @param v Value to Set.
	 */
	public void setFsid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FSId",v);
		_Fsid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Mrid=null;

	/**
	 * @return Returns the MRId.
	 */
	public String getMrid(){
		try{
			if (_Mrid==null){
				_Mrid=getStringProperty("MRId");
				return _Mrid;
			}else {
				return _Mrid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MRId.
	 * @param v Value to Set.
	 */
	public void setMrid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MRId",v);
		_Mrid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mocoerror=null;

	/**
	 * @return Returns the mocoError.
	 */
	public Integer getMocoerror() {
		try{
			if (_Mocoerror==null){
				_Mocoerror=getIntegerProperty("mocoError");
				return _Mocoerror;
			}else {
				return _Mocoerror;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mocoError.
	 * @param v Value to Set.
	 */
	public void setMocoerror(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mocoError",v);
		_Mocoerror=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Regerror=null;

	/**
	 * @return Returns the regError.
	 */
	public Integer getRegerror() {
		try{
			if (_Regerror==null){
				_Regerror=getIntegerProperty("regError");
				return _Regerror;
			}else {
				return _Regerror;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regError.
	 * @param v Value to Set.
	 */
	public void setRegerror(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regError",v);
		_Regerror=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Suvrflag=null;

	/**
	 * @return Returns the suvrFlag.
	 */
	public Integer getSuvrflag() {
		try{
			if (_Suvrflag==null){
				_Suvrflag=getIntegerProperty("suvrFlag");
				return _Suvrflag;
			}else {
				return _Suvrflag;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for suvrFlag.
	 * @param v Value to Set.
	 */
	public void setSuvrflag(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/suvrFlag",v);
		_Suvrflag=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi> _Rois_roi =null;

	/**
	 * rois/roi
	 * @return Returns an List of org.nrg.xdat.om.PupPuptimecoursedataRoi
	 */
	public <A extends org.nrg.xdat.model.PupPuptimecoursedataRoiI> List<A> getRois_roi() {
		try{
			if (_Rois_roi==null){
				_Rois_roi=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("rois/roi"));
			}
			return (List<A>) _Rois_roi;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi>();}
	}

	/**
	 * Sets the value for rois/roi.
	 * @param v Value to Set.
	 */
	public void setRois_roi(ItemI v) throws Exception{
		_Rois_roi =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/rois/roi",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/rois/roi",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * rois/roi
	 * Adds org.nrg.xdat.model.PupPuptimecoursedataRoiI
	 */
	public <A extends org.nrg.xdat.model.PupPuptimecoursedataRoiI> void addRois_roi(A item) throws Exception{
	setRois_roi((ItemI)item);
	}

	/**
	 * Removes the rois/roi of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeRois_roi(int index) throws java.lang.IndexOutOfBoundsException {
		_Rois_roi =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/rois/roi",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> getAllPupPuptimecoursedatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> al = new ArrayList<org.nrg.xdat.om.PupPuptimecoursedata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> getPupPuptimecoursedatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> al = new ArrayList<org.nrg.xdat.om.PupPuptimecoursedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> getPupPuptimecoursedatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> al = new ArrayList<org.nrg.xdat.om.PupPuptimecoursedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static PupPuptimecoursedata getPupPuptimecoursedatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("pup:pupTimeCourseData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (PupPuptimecoursedata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //petAssessorData
	        XnatPetassessordata childPetassessordata = (XnatPetassessordata)this.getPetassessordata();
	            if (childPetassessordata!=null){
	              for(ResourceFile rf: ((XnatPetassessordata)childPetassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("petAssessorData[" + ((XnatPetassessordata)childPetassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("petAssessorData/" + ((XnatPetassessordata)childPetassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //rois/roi
	        for(org.nrg.xdat.model.PupPuptimecoursedataRoiI childRois_roi : this.getRois_roi()){
	            if (childRois_roi!=null){
	              for(ResourceFile rf: ((PupPuptimecoursedataRoi)childRois_roi).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("rois/roi[" + ((PupPuptimecoursedataRoi)childRois_roi).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("rois/roi/" + ((PupPuptimecoursedataRoi)childRois_roi).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
