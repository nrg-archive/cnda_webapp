/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoAdosAdos2001module2data extends XnatSubjectassessordata implements org.nrg.xdat.model.AdosAdos2001module2dataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoAdosAdos2001module2data.class);
	public static String SCHEMA_ELEMENT_NAME="ados:ados2001Module2Data";

	public AutoAdosAdos2001module2data(ItemI item)
	{
		super(item);
	}

	public AutoAdosAdos2001module2data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoAdosAdos2001module2data(UserI user)
	 **/
	public AutoAdosAdos2001module2data(){}

	public AutoAdosAdos2001module2data(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ados:ados2001Module2Data";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Commtot=null;

	/**
	 * @return Returns the commtot.
	 */
	public Integer getCommtot() {
		try{
			if (_Commtot==null){
				_Commtot=getIntegerProperty("commtot");
				return _Commtot;
			}else {
				return _Commtot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for commtot.
	 * @param v Value to Set.
	 */
	public void setCommtot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/commtot",v);
		_Commtot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CommtotNote=null;

	/**
	 * @return Returns the commtot_note.
	 */
	public String getCommtotNote(){
		try{
			if (_CommtotNote==null){
				_CommtotNote=getStringProperty("commtot_note");
				return _CommtotNote;
			}else {
				return _CommtotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for commtot_note.
	 * @param v Value to Set.
	 */
	public void setCommtotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/commtot_note",v);
		_CommtotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scintot=null;

	/**
	 * @return Returns the scintot.
	 */
	public Integer getScintot() {
		try{
			if (_Scintot==null){
				_Scintot=getIntegerProperty("scintot");
				return _Scintot;
			}else {
				return _Scintot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for scintot.
	 * @param v Value to Set.
	 */
	public void setScintot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/scintot",v);
		_Scintot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _ScintotNote=null;

	/**
	 * @return Returns the scintot_note.
	 */
	public String getScintotNote(){
		try{
			if (_ScintotNote==null){
				_ScintotNote=getStringProperty("scintot_note");
				return _ScintotNote;
			}else {
				return _ScintotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for scintot_note.
	 * @param v Value to Set.
	 */
	public void setScintotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/scintot_note",v);
		_ScintotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cmsitot=null;

	/**
	 * @return Returns the cmsitot.
	 */
	public Integer getCmsitot() {
		try{
			if (_Cmsitot==null){
				_Cmsitot=getIntegerProperty("cmsitot");
				return _Cmsitot;
			}else {
				return _Cmsitot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cmsitot.
	 * @param v Value to Set.
	 */
	public void setCmsitot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cmsitot",v);
		_Cmsitot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _CmsitotNote=null;

	/**
	 * @return Returns the cmsitot_note.
	 */
	public String getCmsitotNote(){
		try{
			if (_CmsitotNote==null){
				_CmsitotNote=getStringProperty("cmsitot_note");
				return _CmsitotNote;
			}else {
				return _CmsitotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cmsitot_note.
	 * @param v Value to Set.
	 */
	public void setCmsitotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cmsitot_note",v);
		_CmsitotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Playtot=null;

	/**
	 * @return Returns the playtot.
	 */
	public Integer getPlaytot() {
		try{
			if (_Playtot==null){
				_Playtot=getIntegerProperty("playtot");
				return _Playtot;
			}else {
				return _Playtot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for playtot.
	 * @param v Value to Set.
	 */
	public void setPlaytot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/playtot",v);
		_Playtot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _PlaytotNote=null;

	/**
	 * @return Returns the playtot_note.
	 */
	public String getPlaytotNote(){
		try{
			if (_PlaytotNote==null){
				_PlaytotNote=getStringProperty("playtot_note");
				return _PlaytotNote;
			}else {
				return _PlaytotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for playtot_note.
	 * @param v Value to Set.
	 */
	public void setPlaytotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/playtot_note",v);
		_PlaytotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Imaginetot=null;

	/**
	 * @return Returns the imaginetot.
	 */
	public Integer getImaginetot() {
		try{
			if (_Imaginetot==null){
				_Imaginetot=getIntegerProperty("imaginetot");
				return _Imaginetot;
			}else {
				return _Imaginetot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for imaginetot.
	 * @param v Value to Set.
	 */
	public void setImaginetot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/imaginetot",v);
		_Imaginetot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _ImaginetotNote=null;

	/**
	 * @return Returns the imaginetot_note.
	 */
	public String getImaginetotNote(){
		try{
			if (_ImaginetotNote==null){
				_ImaginetotNote=getStringProperty("imaginetot_note");
				return _ImaginetotNote;
			}else {
				return _ImaginetotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for imaginetot_note.
	 * @param v Value to Set.
	 */
	public void setImaginetotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/imaginetot_note",v);
		_ImaginetotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sbritot=null;

	/**
	 * @return Returns the sbritot.
	 */
	public Integer getSbritot() {
		try{
			if (_Sbritot==null){
				_Sbritot=getIntegerProperty("sbritot");
				return _Sbritot;
			}else {
				return _Sbritot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sbritot.
	 * @param v Value to Set.
	 */
	public void setSbritot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sbritot",v);
		_Sbritot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _SbritotNote=null;

	/**
	 * @return Returns the sbritot_note.
	 */
	public String getSbritotNote(){
		try{
			if (_SbritotNote==null){
				_SbritotNote=getStringProperty("sbritot_note");
				return _SbritotNote;
			}else {
				return _SbritotNote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sbritot_note.
	 * @param v Value to Set.
	 */
	public void setSbritotNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sbritot_note",v);
		_SbritotNote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_diagnosis_adosclas=null;

	/**
	 * @return Returns the ScoreForm/diagnosis/adosclas.
	 */
	public String getScoreform_diagnosis_adosclas(){
		try{
			if (_Scoreform_diagnosis_adosclas==null){
				_Scoreform_diagnosis_adosclas=getStringProperty("ScoreForm/diagnosis/adosclas");
				return _Scoreform_diagnosis_adosclas;
			}else {
				return _Scoreform_diagnosis_adosclas;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/diagnosis/adosclas.
	 * @param v Value to Set.
	 */
	public void setScoreform_diagnosis_adosclas(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/diagnosis/adosclas",v);
		_Scoreform_diagnosis_adosclas=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_diagnosis_overalldiag=null;

	/**
	 * @return Returns the ScoreForm/diagnosis/overallDiag.
	 */
	public String getScoreform_diagnosis_overalldiag(){
		try{
			if (_Scoreform_diagnosis_overalldiag==null){
				_Scoreform_diagnosis_overalldiag=getStringProperty("ScoreForm/diagnosis/overallDiag");
				return _Scoreform_diagnosis_overalldiag;
			}else {
				return _Scoreform_diagnosis_overalldiag;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/diagnosis/overallDiag.
	 * @param v Value to Set.
	 */
	public void setScoreform_diagnosis_overalldiag(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/diagnosis/overallDiag",v);
		_Scoreform_diagnosis_overalldiag=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_constrtasknote=null;

	/**
	 * @return Returns the ScoreForm/observation/constrTaskNote.
	 */
	public String getScoreform_observation_constrtasknote(){
		try{
			if (_Scoreform_observation_constrtasknote==null){
				_Scoreform_observation_constrtasknote=getStringProperty("ScoreForm/observation/constrTaskNote");
				return _Scoreform_observation_constrtasknote;
			}else {
				return _Scoreform_observation_constrtasknote;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/constrTaskNote.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_constrtasknote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/constrTaskNote",v);
		_Scoreform_observation_constrtasknote=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_respname=null;

	/**
	 * @return Returns the ScoreForm/observation/respName.
	 */
	public String getScoreform_observation_respname(){
		try{
			if (_Scoreform_observation_respname==null){
				_Scoreform_observation_respname=getStringProperty("ScoreForm/observation/respName");
				return _Scoreform_observation_respname;
			}else {
				return _Scoreform_observation_respname;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/respName.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_respname(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/respName",v);
		_Scoreform_observation_respname=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_makebelieve=null;

	/**
	 * @return Returns the ScoreForm/observation/makeBelieve.
	 */
	public String getScoreform_observation_makebelieve(){
		try{
			if (_Scoreform_observation_makebelieve==null){
				_Scoreform_observation_makebelieve=getStringProperty("ScoreForm/observation/makeBelieve");
				return _Scoreform_observation_makebelieve;
			}else {
				return _Scoreform_observation_makebelieve;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/makeBelieve.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_makebelieve(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/makeBelieve",v);
		_Scoreform_observation_makebelieve=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_jointplay=null;

	/**
	 * @return Returns the ScoreForm/observation/jointPlay.
	 */
	public String getScoreform_observation_jointplay(){
		try{
			if (_Scoreform_observation_jointplay==null){
				_Scoreform_observation_jointplay=getStringProperty("ScoreForm/observation/jointPlay");
				return _Scoreform_observation_jointplay;
			}else {
				return _Scoreform_observation_jointplay;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/jointPlay.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_jointplay(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/jointPlay",v);
		_Scoreform_observation_jointplay=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_convs=null;

	/**
	 * @return Returns the ScoreForm/observation/convs.
	 */
	public String getScoreform_observation_convs(){
		try{
			if (_Scoreform_observation_convs==null){
				_Scoreform_observation_convs=getStringProperty("ScoreForm/observation/convs");
				return _Scoreform_observation_convs;
			}else {
				return _Scoreform_observation_convs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/convs.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_convs(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/convs",v);
		_Scoreform_observation_convs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_respjointattn=null;

	/**
	 * @return Returns the ScoreForm/observation/respJointAttn.
	 */
	public String getScoreform_observation_respjointattn(){
		try{
			if (_Scoreform_observation_respjointattn==null){
				_Scoreform_observation_respjointattn=getStringProperty("ScoreForm/observation/respJointAttn");
				return _Scoreform_observation_respjointattn;
			}else {
				return _Scoreform_observation_respjointattn;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/respJointAttn.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_respjointattn(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/respJointAttn",v);
		_Scoreform_observation_respjointattn=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_demonst=null;

	/**
	 * @return Returns the ScoreForm/observation/demonst.
	 */
	public String getScoreform_observation_demonst(){
		try{
			if (_Scoreform_observation_demonst==null){
				_Scoreform_observation_demonst=getStringProperty("ScoreForm/observation/demonst");
				return _Scoreform_observation_demonst;
			}else {
				return _Scoreform_observation_demonst;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/demonst.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_demonst(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/demonst",v);
		_Scoreform_observation_demonst=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_descpicture=null;

	/**
	 * @return Returns the ScoreForm/observation/descPicture.
	 */
	public String getScoreform_observation_descpicture(){
		try{
			if (_Scoreform_observation_descpicture==null){
				_Scoreform_observation_descpicture=getStringProperty("ScoreForm/observation/descPicture");
				return _Scoreform_observation_descpicture;
			}else {
				return _Scoreform_observation_descpicture;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/descPicture.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_descpicture(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/descPicture",v);
		_Scoreform_observation_descpicture=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_tellstory=null;

	/**
	 * @return Returns the ScoreForm/observation/tellStory.
	 */
	public String getScoreform_observation_tellstory(){
		try{
			if (_Scoreform_observation_tellstory==null){
				_Scoreform_observation_tellstory=getStringProperty("ScoreForm/observation/tellStory");
				return _Scoreform_observation_tellstory;
			}else {
				return _Scoreform_observation_tellstory;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/tellStory.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_tellstory(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/tellStory",v);
		_Scoreform_observation_tellstory=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_freeplay=null;

	/**
	 * @return Returns the ScoreForm/observation/freePlay.
	 */
	public String getScoreform_observation_freeplay(){
		try{
			if (_Scoreform_observation_freeplay==null){
				_Scoreform_observation_freeplay=getStringProperty("ScoreForm/observation/freePlay");
				return _Scoreform_observation_freeplay;
			}else {
				return _Scoreform_observation_freeplay;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/freePlay.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_freeplay(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/freePlay",v);
		_Scoreform_observation_freeplay=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_bdayparty=null;

	/**
	 * @return Returns the ScoreForm/observation/bdayParty.
	 */
	public String getScoreform_observation_bdayparty(){
		try{
			if (_Scoreform_observation_bdayparty==null){
				_Scoreform_observation_bdayparty=getStringProperty("ScoreForm/observation/bdayParty");
				return _Scoreform_observation_bdayparty;
			}else {
				return _Scoreform_observation_bdayparty;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bdayParty.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bdayparty(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bdayParty",v);
		_Scoreform_observation_bdayparty=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_snack=null;

	/**
	 * @return Returns the ScoreForm/observation/snack.
	 */
	public String getScoreform_observation_snack(){
		try{
			if (_Scoreform_observation_snack==null){
				_Scoreform_observation_snack=getStringProperty("ScoreForm/observation/snack");
				return _Scoreform_observation_snack;
			}else {
				return _Scoreform_observation_snack;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/snack.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_snack(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/snack",v);
		_Scoreform_observation_snack=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_antcproutine=null;

	/**
	 * @return Returns the ScoreForm/observation/antcpRoutine.
	 */
	public String getScoreform_observation_antcproutine(){
		try{
			if (_Scoreform_observation_antcproutine==null){
				_Scoreform_observation_antcproutine=getStringProperty("ScoreForm/observation/antcpRoutine");
				return _Scoreform_observation_antcproutine;
			}else {
				return _Scoreform_observation_antcproutine;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/antcpRoutine.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_antcproutine(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/antcpRoutine",v);
		_Scoreform_observation_antcproutine=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_bubbleplaynotes=null;

	/**
	 * @return Returns the ScoreForm/observation/bubblePlayNotes.
	 */
	public String getScoreform_observation_bubbleplaynotes(){
		try{
			if (_Scoreform_observation_bubbleplaynotes==null){
				_Scoreform_observation_bubbleplaynotes=getStringProperty("ScoreForm/observation/bubblePlayNotes");
				return _Scoreform_observation_bubbleplaynotes;
			}else {
				return _Scoreform_observation_bubbleplaynotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubblePlayNotes.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbleplaynotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubblePlayNotes",v);
		_Scoreform_observation_bubbleplaynotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplaystrats_pointfing=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayStrats/pointFing.
	 */
	public Integer getScoreform_observation_bubbplaystrats_pointfing() {
		try{
			if (_Scoreform_observation_bubbplaystrats_pointfing==null){
				_Scoreform_observation_bubbplaystrats_pointfing=getIntegerProperty("ScoreForm/observation/bubbPlayStrats/pointFing");
				return _Scoreform_observation_bubbplaystrats_pointfing;
			}else {
				return _Scoreform_observation_bubbplaystrats_pointfing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayStrats/pointFing.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplaystrats_pointfing(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayStrats/pointFing",v);
		_Scoreform_observation_bubbplaystrats_pointfing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplaystrats_openreach=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayStrats/openReach.
	 */
	public Integer getScoreform_observation_bubbplaystrats_openreach() {
		try{
			if (_Scoreform_observation_bubbplaystrats_openreach==null){
				_Scoreform_observation_bubbplaystrats_openreach=getIntegerProperty("ScoreForm/observation/bubbPlayStrats/openReach");
				return _Scoreform_observation_bubbplaystrats_openreach;
			}else {
				return _Scoreform_observation_bubbplaystrats_openreach;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayStrats/openReach.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplaystrats_openreach(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayStrats/openReach",v);
		_Scoreform_observation_bubbplaystrats_openreach=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_bubbplaystrats_othergest=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayStrats/otherGest.
	 */
	public String getScoreform_observation_bubbplaystrats_othergest(){
		try{
			if (_Scoreform_observation_bubbplaystrats_othergest==null){
				_Scoreform_observation_bubbplaystrats_othergest=getStringProperty("ScoreForm/observation/bubbPlayStrats/otherGest");
				return _Scoreform_observation_bubbplaystrats_othergest;
			}else {
				return _Scoreform_observation_bubbplaystrats_othergest;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayStrats/otherGest.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplaystrats_othergest(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayStrats/otherGest",v);
		_Scoreform_observation_bubbplaystrats_othergest=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplaystrats_vocaliz=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayStrats/vocaliz.
	 */
	public Integer getScoreform_observation_bubbplaystrats_vocaliz() {
		try{
			if (_Scoreform_observation_bubbplaystrats_vocaliz==null){
				_Scoreform_observation_bubbplaystrats_vocaliz=getIntegerProperty("ScoreForm/observation/bubbPlayStrats/vocaliz");
				return _Scoreform_observation_bubbplaystrats_vocaliz;
			}else {
				return _Scoreform_observation_bubbplaystrats_vocaliz;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayStrats/vocaliz.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplaystrats_vocaliz(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayStrats/vocaliz",v);
		_Scoreform_observation_bubbplaystrats_vocaliz=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplaystrats_eyecont=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayStrats/eyeCont.
	 */
	public Integer getScoreform_observation_bubbplaystrats_eyecont() {
		try{
			if (_Scoreform_observation_bubbplaystrats_eyecont==null){
				_Scoreform_observation_bubbplaystrats_eyecont=getIntegerProperty("ScoreForm/observation/bubbPlayStrats/eyeCont");
				return _Scoreform_observation_bubbplaystrats_eyecont;
			}else {
				return _Scoreform_observation_bubbplaystrats_eyecont;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayStrats/eyeCont.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplaystrats_eyecont(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayStrats/eyeCont",v);
		_Scoreform_observation_bubbplaystrats_eyecont=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplaystrats_words=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayStrats/words.
	 */
	public Integer getScoreform_observation_bubbplaystrats_words() {
		try{
			if (_Scoreform_observation_bubbplaystrats_words==null){
				_Scoreform_observation_bubbplaystrats_words=getIntegerProperty("ScoreForm/observation/bubbPlayStrats/words");
				return _Scoreform_observation_bubbplaystrats_words;
			}else {
				return _Scoreform_observation_bubbplaystrats_words;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayStrats/words.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplaystrats_words(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayStrats/words",v);
		_Scoreform_observation_bubbplaystrats_words=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplaystrats_alone=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayStrats/alone.
	 */
	public Integer getScoreform_observation_bubbplaystrats_alone() {
		try{
			if (_Scoreform_observation_bubbplaystrats_alone==null){
				_Scoreform_observation_bubbplaystrats_alone=getIntegerProperty("ScoreForm/observation/bubbPlayStrats/alone");
				return _Scoreform_observation_bubbplaystrats_alone;
			}else {
				return _Scoreform_observation_bubbplaystrats_alone;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayStrats/alone.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplaystrats_alone(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayStrats/alone",v);
		_Scoreform_observation_bubbplaystrats_alone=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplaystrats_withvocaliz=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayStrats/withVocaliz.
	 */
	public Integer getScoreform_observation_bubbplaystrats_withvocaliz() {
		try{
			if (_Scoreform_observation_bubbplaystrats_withvocaliz==null){
				_Scoreform_observation_bubbplaystrats_withvocaliz=getIntegerProperty("ScoreForm/observation/bubbPlayStrats/withVocaliz");
				return _Scoreform_observation_bubbplaystrats_withvocaliz;
			}else {
				return _Scoreform_observation_bubbplaystrats_withvocaliz;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayStrats/withVocaliz.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplaystrats_withvocaliz(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayStrats/withVocaliz",v);
		_Scoreform_observation_bubbplaystrats_withvocaliz=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplaystrats_nonverb=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayStrats/nonverb.
	 */
	public Integer getScoreform_observation_bubbplaystrats_nonverb() {
		try{
			if (_Scoreform_observation_bubbplaystrats_nonverb==null){
				_Scoreform_observation_bubbplaystrats_nonverb=getIntegerProperty("ScoreForm/observation/bubbPlayStrats/nonverb");
				return _Scoreform_observation_bubbplaystrats_nonverb;
			}else {
				return _Scoreform_observation_bubbplaystrats_nonverb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayStrats/nonverb.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplaystrats_nonverb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayStrats/nonverb",v);
		_Scoreform_observation_bubbplaystrats_nonverb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplaystrats_withpoint=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayStrats/withPoint.
	 */
	public Integer getScoreform_observation_bubbplaystrats_withpoint() {
		try{
			if (_Scoreform_observation_bubbplaystrats_withpoint==null){
				_Scoreform_observation_bubbplaystrats_withpoint=getIntegerProperty("ScoreForm/observation/bubbPlayStrats/withPoint");
				return _Scoreform_observation_bubbplaystrats_withpoint;
			}else {
				return _Scoreform_observation_bubbplaystrats_withpoint;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayStrats/withPoint.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplaystrats_withpoint(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayStrats/withPoint",v);
		_Scoreform_observation_bubbplaystrats_withpoint=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplayobjs_mechanml=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayObjs/mechAnml.
	 */
	public Integer getScoreform_observation_bubbplayobjs_mechanml() {
		try{
			if (_Scoreform_observation_bubbplayobjs_mechanml==null){
				_Scoreform_observation_bubbplayobjs_mechanml=getIntegerProperty("ScoreForm/observation/bubbPlayObjs/mechAnml");
				return _Scoreform_observation_bubbplayobjs_mechanml;
			}else {
				return _Scoreform_observation_bubbplayobjs_mechanml;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayObjs/mechAnml.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplayobjs_mechanml(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayObjs/mechAnml",v);
		_Scoreform_observation_bubbplayobjs_mechanml=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplayobjs_balloon=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayObjs/balloon.
	 */
	public Integer getScoreform_observation_bubbplayobjs_balloon() {
		try{
			if (_Scoreform_observation_bubbplayobjs_balloon==null){
				_Scoreform_observation_bubbplayobjs_balloon=getIntegerProperty("ScoreForm/observation/bubbPlayObjs/balloon");
				return _Scoreform_observation_bubbplayobjs_balloon;
			}else {
				return _Scoreform_observation_bubbplayobjs_balloon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayObjs/balloon.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplayobjs_balloon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayObjs/balloon",v);
		_Scoreform_observation_bubbplayobjs_balloon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_observation_bubbplayobjs_bubbles=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayObjs/bubbles.
	 */
	public Integer getScoreform_observation_bubbplayobjs_bubbles() {
		try{
			if (_Scoreform_observation_bubbplayobjs_bubbles==null){
				_Scoreform_observation_bubbplayobjs_bubbles=getIntegerProperty("ScoreForm/observation/bubbPlayObjs/bubbles");
				return _Scoreform_observation_bubbplayobjs_bubbles;
			}else {
				return _Scoreform_observation_bubbplayobjs_bubbles;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayObjs/bubbles.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplayobjs_bubbles(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayObjs/bubbles",v);
		_Scoreform_observation_bubbplayobjs_bubbles=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Scoreform_observation_bubbplayobjs_otherobj=null;

	/**
	 * @return Returns the ScoreForm/observation/bubbPlayObjs/otherObj.
	 */
	public String getScoreform_observation_bubbplayobjs_otherobj(){
		try{
			if (_Scoreform_observation_bubbplayobjs_otherobj==null){
				_Scoreform_observation_bubbplayobjs_otherobj=getStringProperty("ScoreForm/observation/bubbPlayObjs/otherObj");
				return _Scoreform_observation_bubbplayobjs_otherobj;
			}else {
				return _Scoreform_observation_bubbplayobjs_otherobj;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/observation/bubbPlayObjs/otherObj.
	 * @param v Value to Set.
	 */
	public void setScoreform_observation_bubbplayobjs_otherobj(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/observation/bubbPlayObjs/otherObj",v);
		_Scoreform_observation_bubbplayobjs_otherobj=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_nonechoedlang=null;

	/**
	 * @return Returns the ScoreForm/sectionA/nonechoedLang.
	 */
	public Integer getScoreform_sectiona_nonechoedlang() {
		try{
			if (_Scoreform_sectiona_nonechoedlang==null){
				_Scoreform_sectiona_nonechoedlang=getIntegerProperty("ScoreForm/sectionA/nonechoedLang");
				return _Scoreform_sectiona_nonechoedlang;
			}else {
				return _Scoreform_sectiona_nonechoedlang;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/nonechoedLang.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_nonechoedlang(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/nonechoedLang",v);
		_Scoreform_sectiona_nonechoedlang=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_amtsocoverture=null;

	/**
	 * @return Returns the ScoreForm/sectionA/amtSocOverture.
	 */
	public Integer getScoreform_sectiona_amtsocoverture() {
		try{
			if (_Scoreform_sectiona_amtsocoverture==null){
				_Scoreform_sectiona_amtsocoverture=getIntegerProperty("ScoreForm/sectionA/amtSocOverture");
				return _Scoreform_sectiona_amtsocoverture;
			}else {
				return _Scoreform_sectiona_amtsocoverture;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/amtSocOverture.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_amtsocoverture(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/amtSocOverture",v);
		_Scoreform_sectiona_amtsocoverture=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_speechabnorm=null;

	/**
	 * @return Returns the ScoreForm/sectionA/speechAbnorm.
	 */
	public Integer getScoreform_sectiona_speechabnorm() {
		try{
			if (_Scoreform_sectiona_speechabnorm==null){
				_Scoreform_sectiona_speechabnorm=getIntegerProperty("ScoreForm/sectionA/speechAbnorm");
				return _Scoreform_sectiona_speechabnorm;
			}else {
				return _Scoreform_sectiona_speechabnorm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/speechAbnorm.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_speechabnorm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/speechAbnorm",v);
		_Scoreform_sectiona_speechabnorm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_immedecholalia=null;

	/**
	 * @return Returns the ScoreForm/sectionA/immedEcholalia.
	 */
	public Integer getScoreform_sectiona_immedecholalia() {
		try{
			if (_Scoreform_sectiona_immedecholalia==null){
				_Scoreform_sectiona_immedecholalia=getIntegerProperty("ScoreForm/sectionA/immedEcholalia");
				return _Scoreform_sectiona_immedecholalia;
			}else {
				return _Scoreform_sectiona_immedecholalia;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/immedEcholalia.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_immedecholalia(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/immedEcholalia",v);
		_Scoreform_sectiona_immedecholalia=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_stidiosyncwords=null;

	/**
	 * @return Returns the ScoreForm/sectionA/stIdiosyncWords.
	 */
	public Integer getScoreform_sectiona_stidiosyncwords() {
		try{
			if (_Scoreform_sectiona_stidiosyncwords==null){
				_Scoreform_sectiona_stidiosyncwords=getIntegerProperty("ScoreForm/sectionA/stIdiosyncWords");
				return _Scoreform_sectiona_stidiosyncwords;
			}else {
				return _Scoreform_sectiona_stidiosyncwords;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/stIdiosyncWords.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_stidiosyncwords(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/stIdiosyncWords",v);
		_Scoreform_sectiona_stidiosyncwords=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_conversation=null;

	/**
	 * @return Returns the ScoreForm/sectionA/conversation.
	 */
	public Integer getScoreform_sectiona_conversation() {
		try{
			if (_Scoreform_sectiona_conversation==null){
				_Scoreform_sectiona_conversation=getIntegerProperty("ScoreForm/sectionA/conversation");
				return _Scoreform_sectiona_conversation;
			}else {
				return _Scoreform_sectiona_conversation;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/conversation.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_conversation(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/conversation",v);
		_Scoreform_sectiona_conversation=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_pointing=null;

	/**
	 * @return Returns the ScoreForm/sectionA/pointing.
	 */
	public Integer getScoreform_sectiona_pointing() {
		try{
			if (_Scoreform_sectiona_pointing==null){
				_Scoreform_sectiona_pointing=getIntegerProperty("ScoreForm/sectionA/pointing");
				return _Scoreform_sectiona_pointing;
			}else {
				return _Scoreform_sectiona_pointing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/pointing.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_pointing(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/pointing",v);
		_Scoreform_sectiona_pointing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiona_descinformgest=null;

	/**
	 * @return Returns the ScoreForm/sectionA/descInformGest.
	 */
	public Integer getScoreform_sectiona_descinformgest() {
		try{
			if (_Scoreform_sectiona_descinformgest==null){
				_Scoreform_sectiona_descinformgest=getIntegerProperty("ScoreForm/sectionA/descInformGest");
				return _Scoreform_sectiona_descinformgest;
			}else {
				return _Scoreform_sectiona_descinformgest;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionA/descInformGest.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiona_descinformgest(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionA/descInformGest",v);
		_Scoreform_sectiona_descinformgest=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_unusleyecont=null;

	/**
	 * @return Returns the ScoreForm/sectionB/unuslEyeCont.
	 */
	public Integer getScoreform_sectionb_unusleyecont() {
		try{
			if (_Scoreform_sectionb_unusleyecont==null){
				_Scoreform_sectionb_unusleyecont=getIntegerProperty("ScoreForm/sectionB/unuslEyeCont");
				return _Scoreform_sectionb_unusleyecont;
			}else {
				return _Scoreform_sectionb_unusleyecont;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/unuslEyeCont.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_unusleyecont(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/unuslEyeCont",v);
		_Scoreform_sectionb_unusleyecont=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_facexproth=null;

	/**
	 * @return Returns the ScoreForm/sectionB/facExprOth.
	 */
	public Integer getScoreform_sectionb_facexproth() {
		try{
			if (_Scoreform_sectionb_facexproth==null){
				_Scoreform_sectionb_facexproth=getIntegerProperty("ScoreForm/sectionB/facExprOth");
				return _Scoreform_sectionb_facexproth;
			}else {
				return _Scoreform_sectionb_facexproth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/facExprOth.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_facexproth(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/facExprOth",v);
		_Scoreform_sectionb_facexproth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_shareenjoy=null;

	/**
	 * @return Returns the ScoreForm/sectionB/shareEnjoy.
	 */
	public Integer getScoreform_sectionb_shareenjoy() {
		try{
			if (_Scoreform_sectionb_shareenjoy==null){
				_Scoreform_sectionb_shareenjoy=getIntegerProperty("ScoreForm/sectionB/shareEnjoy");
				return _Scoreform_sectionb_shareenjoy;
			}else {
				return _Scoreform_sectionb_shareenjoy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/shareEnjoy.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_shareenjoy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/shareEnjoy",v);
		_Scoreform_sectionb_shareenjoy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_respname=null;

	/**
	 * @return Returns the ScoreForm/sectionB/respName.
	 */
	public Integer getScoreform_sectionb_respname() {
		try{
			if (_Scoreform_sectionb_respname==null){
				_Scoreform_sectionb_respname=getIntegerProperty("ScoreForm/sectionB/respName");
				return _Scoreform_sectionb_respname;
			}else {
				return _Scoreform_sectionb_respname;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/respName.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_respname(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/respName",v);
		_Scoreform_sectionb_respname=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_showing=null;

	/**
	 * @return Returns the ScoreForm/sectionB/showing.
	 */
	public Integer getScoreform_sectionb_showing() {
		try{
			if (_Scoreform_sectionb_showing==null){
				_Scoreform_sectionb_showing=getIntegerProperty("ScoreForm/sectionB/showing");
				return _Scoreform_sectionb_showing;
			}else {
				return _Scoreform_sectionb_showing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/showing.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_showing(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/showing",v);
		_Scoreform_sectionb_showing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_spontinitjntatten=null;

	/**
	 * @return Returns the ScoreForm/sectionB/spontInitJntAtten.
	 */
	public Integer getScoreform_sectionb_spontinitjntatten() {
		try{
			if (_Scoreform_sectionb_spontinitjntatten==null){
				_Scoreform_sectionb_spontinitjntatten=getIntegerProperty("ScoreForm/sectionB/spontInitJntAtten");
				return _Scoreform_sectionb_spontinitjntatten;
			}else {
				return _Scoreform_sectionb_spontinitjntatten;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/spontInitJntAtten.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_spontinitjntatten(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/spontInitJntAtten",v);
		_Scoreform_sectionb_spontinitjntatten=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_respjntatten=null;

	/**
	 * @return Returns the ScoreForm/sectionB/respJntAtten.
	 */
	public Integer getScoreform_sectionb_respjntatten() {
		try{
			if (_Scoreform_sectionb_respjntatten==null){
				_Scoreform_sectionb_respjntatten=getIntegerProperty("ScoreForm/sectionB/respJntAtten");
				return _Scoreform_sectionb_respjntatten;
			}else {
				return _Scoreform_sectionb_respjntatten;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/respJntAtten.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_respjntatten(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/respJntAtten",v);
		_Scoreform_sectionb_respjntatten=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_qualsocoverture=null;

	/**
	 * @return Returns the ScoreForm/sectionB/qualSocOverture.
	 */
	public Integer getScoreform_sectionb_qualsocoverture() {
		try{
			if (_Scoreform_sectionb_qualsocoverture==null){
				_Scoreform_sectionb_qualsocoverture=getIntegerProperty("ScoreForm/sectionB/qualSocOverture");
				return _Scoreform_sectionb_qualsocoverture;
			}else {
				return _Scoreform_sectionb_qualsocoverture;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/qualSocOverture.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_qualsocoverture(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/qualSocOverture",v);
		_Scoreform_sectionb_qualsocoverture=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_qualsocresp=null;

	/**
	 * @return Returns the ScoreForm/sectionB/qualSocResp.
	 */
	public Integer getScoreform_sectionb_qualsocresp() {
		try{
			if (_Scoreform_sectionb_qualsocresp==null){
				_Scoreform_sectionb_qualsocresp=getIntegerProperty("ScoreForm/sectionB/qualSocResp");
				return _Scoreform_sectionb_qualsocresp;
			}else {
				return _Scoreform_sectionb_qualsocresp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/qualSocResp.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_qualsocresp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/qualSocResp",v);
		_Scoreform_sectionb_qualsocresp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_amtrecipsoccomm=null;

	/**
	 * @return Returns the ScoreForm/sectionB/amtRecipSocComm.
	 */
	public Integer getScoreform_sectionb_amtrecipsoccomm() {
		try{
			if (_Scoreform_sectionb_amtrecipsoccomm==null){
				_Scoreform_sectionb_amtrecipsoccomm=getIntegerProperty("ScoreForm/sectionB/amtRecipSocComm");
				return _Scoreform_sectionb_amtrecipsoccomm;
			}else {
				return _Scoreform_sectionb_amtrecipsoccomm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/amtRecipSocComm.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_amtrecipsoccomm(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/amtRecipSocComm",v);
		_Scoreform_sectionb_amtrecipsoccomm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionb_overallqualrapp=null;

	/**
	 * @return Returns the ScoreForm/sectionB/overallQualRapp.
	 */
	public Integer getScoreform_sectionb_overallqualrapp() {
		try{
			if (_Scoreform_sectionb_overallqualrapp==null){
				_Scoreform_sectionb_overallqualrapp=getIntegerProperty("ScoreForm/sectionB/overallQualRapp");
				return _Scoreform_sectionb_overallqualrapp;
			}else {
				return _Scoreform_sectionb_overallqualrapp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionB/overallQualRapp.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionb_overallqualrapp(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionB/overallQualRapp",v);
		_Scoreform_sectionb_overallqualrapp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_funcplayobj=null;

	/**
	 * @return Returns the ScoreForm/sectionC/funcPlayObj.
	 */
	public Integer getScoreform_sectionc_funcplayobj() {
		try{
			if (_Scoreform_sectionc_funcplayobj==null){
				_Scoreform_sectionc_funcplayobj=getIntegerProperty("ScoreForm/sectionC/funcPlayObj");
				return _Scoreform_sectionc_funcplayobj;
			}else {
				return _Scoreform_sectionc_funcplayobj;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/funcPlayObj.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_funcplayobj(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/funcPlayObj",v);
		_Scoreform_sectionc_funcplayobj=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectionc_imagcreativ=null;

	/**
	 * @return Returns the ScoreForm/sectionC/imagCreativ.
	 */
	public Integer getScoreform_sectionc_imagcreativ() {
		try{
			if (_Scoreform_sectionc_imagcreativ==null){
				_Scoreform_sectionc_imagcreativ=getIntegerProperty("ScoreForm/sectionC/imagCreativ");
				return _Scoreform_sectionc_imagcreativ;
			}else {
				return _Scoreform_sectionc_imagcreativ;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionC/imagCreativ.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectionc_imagcreativ(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionC/imagCreativ",v);
		_Scoreform_sectionc_imagcreativ=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_unuslsensint=null;

	/**
	 * @return Returns the ScoreForm/sectionD/unuslSensInt.
	 */
	public Integer getScoreform_sectiond_unuslsensint() {
		try{
			if (_Scoreform_sectiond_unuslsensint==null){
				_Scoreform_sectiond_unuslsensint=getIntegerProperty("ScoreForm/sectionD/unuslSensInt");
				return _Scoreform_sectiond_unuslsensint;
			}else {
				return _Scoreform_sectiond_unuslsensint;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/unuslSensInt.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_unuslsensint(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/unuslSensInt",v);
		_Scoreform_sectiond_unuslsensint=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_handfgrcomplx=null;

	/**
	 * @return Returns the ScoreForm/sectionD/handFgrComplx.
	 */
	public Integer getScoreform_sectiond_handfgrcomplx() {
		try{
			if (_Scoreform_sectiond_handfgrcomplx==null){
				_Scoreform_sectiond_handfgrcomplx=getIntegerProperty("ScoreForm/sectionD/handFgrComplx");
				return _Scoreform_sectiond_handfgrcomplx;
			}else {
				return _Scoreform_sectiond_handfgrcomplx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/handFgrComplx.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_handfgrcomplx(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/handFgrComplx",v);
		_Scoreform_sectiond_handfgrcomplx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_selfinjurbehav=null;

	/**
	 * @return Returns the ScoreForm/sectionD/selfInjurBehav.
	 */
	public Integer getScoreform_sectiond_selfinjurbehav() {
		try{
			if (_Scoreform_sectiond_selfinjurbehav==null){
				_Scoreform_sectiond_selfinjurbehav=getIntegerProperty("ScoreForm/sectionD/selfInjurBehav");
				return _Scoreform_sectiond_selfinjurbehav;
			}else {
				return _Scoreform_sectiond_selfinjurbehav;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/selfInjurBehav.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_selfinjurbehav(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/selfInjurBehav",v);
		_Scoreform_sectiond_selfinjurbehav=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectiond_unusrepetinrst=null;

	/**
	 * @return Returns the ScoreForm/sectionD/unusRepetInrst.
	 */
	public Integer getScoreform_sectiond_unusrepetinrst() {
		try{
			if (_Scoreform_sectiond_unusrepetinrst==null){
				_Scoreform_sectiond_unusrepetinrst=getIntegerProperty("ScoreForm/sectionD/unusRepetInrst");
				return _Scoreform_sectiond_unusrepetinrst;
			}else {
				return _Scoreform_sectiond_unusrepetinrst;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionD/unusRepetInrst.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectiond_unusrepetinrst(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionD/unusRepetInrst",v);
		_Scoreform_sectiond_unusrepetinrst=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_overactiv=null;

	/**
	 * @return Returns the ScoreForm/sectionE/overactiv.
	 */
	public Integer getScoreform_sectione_overactiv() {
		try{
			if (_Scoreform_sectione_overactiv==null){
				_Scoreform_sectione_overactiv=getIntegerProperty("ScoreForm/sectionE/overactiv");
				return _Scoreform_sectione_overactiv;
			}else {
				return _Scoreform_sectione_overactiv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/overactiv.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_overactiv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/overactiv",v);
		_Scoreform_sectione_overactiv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_tantrmagress=null;

	/**
	 * @return Returns the ScoreForm/sectionE/tantrmAgress.
	 */
	public Integer getScoreform_sectione_tantrmagress() {
		try{
			if (_Scoreform_sectione_tantrmagress==null){
				_Scoreform_sectione_tantrmagress=getIntegerProperty("ScoreForm/sectionE/tantrmAgress");
				return _Scoreform_sectione_tantrmagress;
			}else {
				return _Scoreform_sectione_tantrmagress;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/tantrmAgress.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_tantrmagress(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/tantrmAgress",v);
		_Scoreform_sectione_tantrmagress=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scoreform_sectione_anxiety=null;

	/**
	 * @return Returns the ScoreForm/sectionE/anxiety.
	 */
	public Integer getScoreform_sectione_anxiety() {
		try{
			if (_Scoreform_sectione_anxiety==null){
				_Scoreform_sectione_anxiety=getIntegerProperty("ScoreForm/sectionE/anxiety");
				return _Scoreform_sectione_anxiety;
			}else {
				return _Scoreform_sectione_anxiety;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ScoreForm/sectionE/anxiety.
	 * @param v Value to Set.
	 */
	public void setScoreform_sectione_anxiety(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ScoreForm/sectionE/anxiety",v);
		_Scoreform_sectione_anxiety=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.AdosAdos2001module2data> getAllAdosAdos2001module2datas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdosAdos2001module2data> al = new ArrayList<org.nrg.xdat.om.AdosAdos2001module2data>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.AdosAdos2001module2data> getAdosAdos2001module2datasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdosAdos2001module2data> al = new ArrayList<org.nrg.xdat.om.AdosAdos2001module2data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.AdosAdos2001module2data> getAdosAdos2001module2datasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.AdosAdos2001module2data> al = new ArrayList<org.nrg.xdat.om.AdosAdos2001module2data>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static AdosAdos2001module2data getAdosAdos2001module2datasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ados:ados2001Module2Data/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (AdosAdos2001module2data) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
