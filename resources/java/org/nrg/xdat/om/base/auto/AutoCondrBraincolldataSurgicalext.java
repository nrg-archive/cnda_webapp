/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrBraincolldataSurgicalext extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CondrBraincolldataSurgicalextI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrBraincolldataSurgicalext.class);
	public static String SCHEMA_ELEMENT_NAME="condr:brainCollData_surgicalExt";

	public AutoCondrBraincolldataSurgicalext(ItemI item)
	{
		super(item);
	}

	public AutoCondrBraincolldataSurgicalext(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrBraincolldataSurgicalext(UserI user)
	 **/
	public AutoCondrBraincolldataSurgicalext(){}

	public AutoCondrBraincolldataSurgicalext(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr:brainCollData_surgicalExt";
	}

	//FIELD

	private String _Surgicalext=null;

	/**
	 * @return Returns the surgicalExt.
	 */
	public String getSurgicalext(){
		try{
			if (_Surgicalext==null){
				_Surgicalext=getStringProperty("surgicalExt");
				return _Surgicalext;
			}else {
				return _Surgicalext;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for surgicalExt.
	 * @param v Value to Set.
	 */
	public void setSurgicalext(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/surgicalExt",v);
		_Surgicalext=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CondrBraincolldataSurgicalextId=null;

	/**
	 * @return Returns the condr_brainCollData_surgicalExt_id.
	 */
	public Integer getCondrBraincolldataSurgicalextId() {
		try{
			if (_CondrBraincolldataSurgicalextId==null){
				_CondrBraincolldataSurgicalextId=getIntegerProperty("condr_brainCollData_surgicalExt_id");
				return _CondrBraincolldataSurgicalextId;
			}else {
				return _CondrBraincolldataSurgicalextId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for condr_brainCollData_surgicalExt_id.
	 * @param v Value to Set.
	 */
	public void setCondrBraincolldataSurgicalextId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/condr_brainCollData_surgicalExt_id",v);
		_CondrBraincolldataSurgicalextId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext> getAllCondrBraincolldataSurgicalexts(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext> getCondrBraincolldataSurgicalextsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext> getCondrBraincolldataSurgicalextsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext> al = new ArrayList<org.nrg.xdat.om.CondrBraincolldataSurgicalext>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrBraincolldataSurgicalext getCondrBraincolldataSurgicalextsByCondrBraincolldataSurgicalextId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr:brainCollData_surgicalExt/condr_brainCollData_surgicalExt_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrBraincolldataSurgicalext) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
