/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianFeedbackdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianFeedbackdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianFeedbackdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:feedbackData";

	public AutoDianFeedbackdata(ItemI item)
	{
		super(item);
	}

	public AutoDianFeedbackdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianFeedbackdata(UserI user)
	 **/
	public AutoDianFeedbackdata(){}

	public AutoDianFeedbackdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:feedbackData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Autopsy=null;

	/**
	 * @return Returns the AUTOPSY.
	 */
	public Integer getAutopsy() {
		try{
			if (_Autopsy==null){
				_Autopsy=getIntegerProperty("AUTOPSY");
				return _Autopsy;
			}else {
				return _Autopsy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for AUTOPSY.
	 * @param v Value to Set.
	 */
	public void setAutopsy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/AUTOPSY",v);
		_Autopsy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Decision=null;

	/**
	 * @return Returns the DECISION.
	 */
	public Integer getDecision() {
		try{
			if (_Decision==null){
				_Decision=getIntegerProperty("DECISION");
				return _Decision;
			}else {
				return _Decision;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for DECISION.
	 * @param v Value to Set.
	 */
	public void setDecision(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/DECISION",v);
		_Decision=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Refuse=null;

	/**
	 * @return Returns the REFUSE.
	 */
	public String getRefuse(){
		try{
			if (_Refuse==null){
				_Refuse=getStringProperty("REFUSE");
				return _Refuse;
			}else {
				return _Refuse;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REFUSE.
	 * @param v Value to Set.
	 */
	public void setRefuse(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REFUSE",v);
		_Refuse=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Refuseoth=null;

	/**
	 * @return Returns the REFUSEOTH.
	 */
	public String getRefuseoth(){
		try{
			if (_Refuseoth==null){
				_Refuseoth=getStringProperty("REFUSEOTH");
				return _Refuseoth;
			}else {
				return _Refuseoth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REFUSEOTH.
	 * @param v Value to Set.
	 */
	public void setRefuseoth(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REFUSEOTH",v);
		_Refuseoth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Clinfeed=null;

	/**
	 * @return Returns the CLINFEED.
	 */
	public Integer getClinfeed() {
		try{
			if (_Clinfeed==null){
				_Clinfeed=getIntegerProperty("CLINFEED");
				return _Clinfeed;
			}else {
				return _Clinfeed;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CLINFEED.
	 * @param v Value to Set.
	 */
	public void setClinfeed(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CLINFEED",v);
		_Clinfeed=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Provider=null;

	/**
	 * @return Returns the PROVIDER.
	 */
	public String getProvider(){
		try{
			if (_Provider==null){
				_Provider=getStringProperty("PROVIDER");
				return _Provider;
			}else {
				return _Provider;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PROVIDER.
	 * @param v Value to Set.
	 */
	public void setProvider(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PROVIDER",v);
		_Provider=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Provoth=null;

	/**
	 * @return Returns the PROVOTH.
	 */
	public String getProvoth(){
		try{
			if (_Provoth==null){
				_Provoth=getStringProperty("PROVOTH");
				return _Provoth;
			}else {
				return _Provoth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PROVOTH.
	 * @param v Value to Set.
	 */
	public void setProvoth(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PROVOTH",v);
		_Provoth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Present=null;

	/**
	 * @return Returns the PRESENT.
	 */
	public String getPresent(){
		try{
			if (_Present==null){
				_Present=getStringProperty("PRESENT");
				return _Present;
			}else {
				return _Present;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRESENT.
	 * @param v Value to Set.
	 */
	public void setPresent(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRESENT",v);
		_Present=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Presoth=null;

	/**
	 * @return Returns the PRESOTH.
	 */
	public String getPresoth(){
		try{
			if (_Presoth==null){
				_Presoth=getStringProperty("PRESOTH");
				return _Presoth;
			}else {
				return _Presoth;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PRESOTH.
	 * @param v Value to Set.
	 */
	public void setPresoth(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PRESOTH",v);
		_Presoth=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Feedto=null;

	/**
	 * @return Returns the FEEDTO.
	 */
	public String getFeedto(){
		try{
			if (_Feedto==null){
				_Feedto=getStringProperty("FEEDTO");
				return _Feedto;
			}else {
				return _Feedto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FEEDTO.
	 * @param v Value to Set.
	 */
	public void setFeedto(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FEEDTO",v);
		_Feedto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Toother=null;

	/**
	 * @return Returns the TOOTHER.
	 */
	public String getToother(){
		try{
			if (_Toother==null){
				_Toother=getStringProperty("TOOTHER");
				return _Toother;
			}else {
				return _Toother;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TOOTHER.
	 * @param v Value to Set.
	 */
	public void setToother(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TOOTHER",v);
		_Toother=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Topics=null;

	/**
	 * @return Returns the TOPICS.
	 */
	public String getTopics(){
		try{
			if (_Topics==null){
				_Topics=getStringProperty("TOPICS");
				return _Topics;
			}else {
				return _Topics;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for TOPICS.
	 * @param v Value to Set.
	 */
	public void setTopics(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/TOPICS",v);
		_Topics=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Medspec=null;

	/**
	 * @return Returns the MEDSPEC.
	 */
	public String getMedspec(){
		try{
			if (_Medspec==null){
				_Medspec=getStringProperty("MEDSPEC");
				return _Medspec;
			}else {
				return _Medspec;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MEDSPEC.
	 * @param v Value to Set.
	 */
	public void setMedspec(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MEDSPEC",v);
		_Medspec=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianFeedbackdata> getAllDianFeedbackdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianFeedbackdata> al = new ArrayList<org.nrg.xdat.om.DianFeedbackdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianFeedbackdata> getDianFeedbackdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianFeedbackdata> al = new ArrayList<org.nrg.xdat.om.DianFeedbackdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianFeedbackdata> getDianFeedbackdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianFeedbackdata> al = new ArrayList<org.nrg.xdat.om.DianFeedbackdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianFeedbackdata getDianFeedbackdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:feedbackData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianFeedbackdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
