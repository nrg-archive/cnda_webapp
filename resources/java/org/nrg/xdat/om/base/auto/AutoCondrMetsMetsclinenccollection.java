/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrMetsMetsclinenccollection extends CndaExtSacollection implements org.nrg.xdat.model.CondrMetsMetsclinenccollectionI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrMetsMetsclinenccollection.class);
	public static String SCHEMA_ELEMENT_NAME="condr_mets:metsClinEncCollection";

	public AutoCondrMetsMetsclinenccollection(ItemI item)
	{
		super(item);
	}

	public AutoCondrMetsMetsclinenccollection(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrMetsMetsclinenccollection(UserI user)
	 **/
	public AutoCondrMetsMetsclinenccollection(){}

	public AutoCondrMetsMetsclinenccollection(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr_mets:metsClinEncCollection";
	}
	 private org.nrg.xdat.om.CndaExtSacollection _Sacollection =null;

	/**
	 * saCollection
	 * @return org.nrg.xdat.om.CndaExtSacollection
	 */
	public org.nrg.xdat.om.CndaExtSacollection getSacollection() {
		try{
			if (_Sacollection==null){
				_Sacollection=((CndaExtSacollection)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("saCollection")));
				return _Sacollection;
			}else {
				return _Sacollection;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for saCollection.
	 * @param v Value to Set.
	 */
	public void setSacollection(ItemI v) throws Exception{
		_Sacollection =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/saCollection",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/saCollection",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * saCollection
	 * set org.nrg.xdat.model.CndaExtSacollectionI
	 */
	public <A extends org.nrg.xdat.model.CndaExtSacollectionI> void setSacollection(A item) throws Exception{
	setSacollection((ItemI)item);
	}

	/**
	 * Removes the saCollection.
	 * */
	public void removeSacollection() {
		_Sacollection =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/saCollection",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Encdate=null;

	/**
	 * @return Returns the encDate.
	 */
	public Object getEncdate(){
		try{
			if (_Encdate==null){
				_Encdate=getProperty("encDate");
				return _Encdate;
			}else {
				return _Encdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for encDate.
	 * @param v Value to Set.
	 */
	public void setEncdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/encDate",v);
		_Encdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Karperfstatus=null;

	/**
	 * @return Returns the karPerfStatus.
	 */
	public Integer getKarperfstatus() {
		try{
			if (_Karperfstatus==null){
				_Karperfstatus=getIntegerProperty("karPerfStatus");
				return _Karperfstatus;
			}else {
				return _Karperfstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for karPerfStatus.
	 * @param v Value to Set.
	 */
	public void setKarperfstatus(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/karPerfStatus",v);
		_Karperfstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Steroiddose=null;

	/**
	 * @return Returns the steroidDose.
	 */
	public Double getSteroiddose() {
		try{
			if (_Steroiddose==null){
				_Steroiddose=getDoubleProperty("steroidDose");
				return _Steroiddose;
			}else {
				return _Steroiddose;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for steroidDose.
	 * @param v Value to Set.
	 */
	public void setSteroiddose(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/steroidDose",v);
		_Steroiddose=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsMetsclinenccollection> getAllCondrMetsMetsclinenccollections(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsMetsclinenccollection> al = new ArrayList<org.nrg.xdat.om.CondrMetsMetsclinenccollection>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsMetsclinenccollection> getCondrMetsMetsclinenccollectionsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsMetsclinenccollection> al = new ArrayList<org.nrg.xdat.om.CondrMetsMetsclinenccollection>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrMetsMetsclinenccollection> getCondrMetsMetsclinenccollectionsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrMetsMetsclinenccollection> al = new ArrayList<org.nrg.xdat.om.CondrMetsMetsclinenccollection>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrMetsMetsclinenccollection getCondrMetsMetsclinenccollectionsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr_mets:metsClinEncCollection/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrMetsMetsclinenccollection) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //saCollection
	        CndaExtSacollection childSacollection = (CndaExtSacollection)this.getSacollection();
	            if (childSacollection!=null){
	              for(ResourceFile rf: ((CndaExtSacollection)childSacollection).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("saCollection[" + ((CndaExtSacollection)childSacollection).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("saCollection/" + ((CndaExtSacollection)childSacollection).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
