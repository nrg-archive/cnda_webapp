/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianDianvalidationdata extends XnatValidationdata implements org.nrg.xdat.model.DianDianvalidationdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianDianvalidationdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:dianValidationData";

	public AutoDianDianvalidationdata(ItemI item)
	{
		super(item);
	}

	public AutoDianDianvalidationdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianDianvalidationdata(UserI user)
	 **/
	public AutoDianDianvalidationdata(){}

	public AutoDianDianvalidationdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:dianValidationData";
	}
	 private org.nrg.xdat.om.XnatValidationdata _Validationdata =null;

	/**
	 * validationData
	 * @return org.nrg.xdat.om.XnatValidationdata
	 */
	public org.nrg.xdat.om.XnatValidationdata getValidationdata() {
		try{
			if (_Validationdata==null){
				_Validationdata=((XnatValidationdata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("validationData")));
				return _Validationdata;
			}else {
				return _Validationdata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for validationData.
	 * @param v Value to Set.
	 */
	public void setValidationdata(ItemI v) throws Exception{
		_Validationdata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/validationData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/validationData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * validationData
	 * set org.nrg.xdat.model.XnatValidationdataI
	 */
	public <A extends org.nrg.xdat.model.XnatValidationdataI> void setValidationdata(A item) throws Exception{
	setValidationdata((ItemI)item);
	}

	/**
	 * Removes the validationData.
	 * */
	public void removeValidationdata() {
		_Validationdata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/validationData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Id=null;

	/**
	 * @return Returns the ID.
	 */
	public Integer getId() {
		try{
			if (_Id==null){
				_Id=getIntegerProperty("ID");
				return _Id;
			}else {
				return _Id;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ID.
	 * @param v Value to Set.
	 */
	public void setId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ID",v);
		_Id=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sx=null;

	/**
	 * @return Returns the SX.
	 */
	public String getSx(){
		try{
			if (_Sx==null){
				_Sx=getStringProperty("SX");
				return _Sx;
			}else {
				return _Sx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SX.
	 * @param v Value to Set.
	 */
	public void setSx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SX",v);
		_Sx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rid=null;

	/**
	 * @return Returns the RID.
	 */
	public Integer getRid() {
		try{
			if (_Rid==null){
				_Rid=getIntegerProperty("RID");
				return _Rid;
			}else {
				return _Rid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RID.
	 * @param v Value to Set.
	 */
	public void setRid(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RID",v);
		_Rid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Siteid=null;

	/**
	 * @return Returns the SITEID.
	 */
	public Integer getSiteid() {
		try{
			if (_Siteid==null){
				_Siteid=getIntegerProperty("SITEID");
				return _Siteid;
			}else {
				return _Siteid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SITEID.
	 * @param v Value to Set.
	 */
	public void setSiteid(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SITEID",v);
		_Siteid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Viscode=null;

	/**
	 * @return Returns the VISCODE.
	 */
	public String getViscode(){
		try{
			if (_Viscode==null){
				_Viscode=getStringProperty("VISCODE");
				return _Viscode;
			}else {
				return _Viscode;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VISCODE.
	 * @param v Value to Set.
	 */
	public void setViscode(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VISCODE",v);
		_Viscode=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Entry=null;

	/**
	 * @return Returns the ENTRY.
	 */
	public Integer getEntry() {
		try{
			if (_Entry==null){
				_Entry=getIntegerProperty("ENTRY");
				return _Entry;
			}else {
				return _Entry;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ENTRY.
	 * @param v Value to Set.
	 */
	public void setEntry(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ENTRY",v);
		_Entry=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Verify=null;

	/**
	 * @return Returns the VERIFY.
	 */
	public Integer getVerify() {
		try{
			if (_Verify==null){
				_Verify=getIntegerProperty("VERIFY");
				return _Verify;
			}else {
				return _Verify;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for VERIFY.
	 * @param v Value to Set.
	 */
	public void setVerify(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/VERIFY",v);
		_Verify=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Userid=null;

	/**
	 * @return Returns the USERID.
	 */
	public String getUserid(){
		try{
			if (_Userid==null){
				_Userid=getStringProperty("USERID");
				return _Userid;
			}else {
				return _Userid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for USERID.
	 * @param v Value to Set.
	 */
	public void setUserid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/USERID",v);
		_Userid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Userdate=null;

	/**
	 * @return Returns the USERDATE.
	 */
	public Object getUserdate(){
		try{
			if (_Userdate==null){
				_Userdate=getProperty("USERDATE");
				return _Userdate;
			}else {
				return _Userdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for USERDATE.
	 * @param v Value to Set.
	 */
	public void setUserdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/USERDATE",v);
		_Userdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Userid2=null;

	/**
	 * @return Returns the USERID2.
	 */
	public String getUserid2(){
		try{
			if (_Userid2==null){
				_Userid2=getStringProperty("USERID2");
				return _Userid2;
			}else {
				return _Userid2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for USERID2.
	 * @param v Value to Set.
	 */
	public void setUserid2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/USERID2",v);
		_Userid2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Userdate2=null;

	/**
	 * @return Returns the USERDATE2.
	 */
	public Object getUserdate2(){
		try{
			if (_Userdate2==null){
				_Userdate2=getProperty("USERDATE2");
				return _Userdate2;
			}else {
				return _Userdate2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for USERDATE2.
	 * @param v Value to Set.
	 */
	public void setUserdate2(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/USERDATE2",v);
		_Userdate2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianDianvalidationdata> getAllDianDianvalidationdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianDianvalidationdata> al = new ArrayList<org.nrg.xdat.om.DianDianvalidationdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianDianvalidationdata> getDianDianvalidationdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianDianvalidationdata> al = new ArrayList<org.nrg.xdat.om.DianDianvalidationdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianDianvalidationdata> getDianDianvalidationdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianDianvalidationdata> al = new ArrayList<org.nrg.xdat.om.DianDianvalidationdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianDianvalidationdata getDianDianvalidationdatasByXnatValidationdataId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:dianValidationData/xnat_validationdata_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianDianvalidationdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //validationData
	        XnatValidationdata childValidationdata = (XnatValidationdata)this.getValidationdata();
	            if (childValidationdata!=null){
	              for(ResourceFile rf: ((XnatValidationdata)childValidationdata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("validationData[" + ((XnatValidationdata)childValidationdata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("validationData/" + ((XnatValidationdata)childValidationdata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
