/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:49:00 CST 2014
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCbatPairbinding extends XnatSubjectassessordata implements org.nrg.xdat.model.CbatPairbindingI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCbatPairbinding.class);
	public static String SCHEMA_ELEMENT_NAME="cbat:pairBinding";

	public AutoCbatPairbinding(ItemI item)
	{
		super(item);
	}

	public AutoCbatPairbinding(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCbatPairbinding(UserI user)
	 **/
	public AutoCbatPairbinding(){}

	public AutoCbatPairbinding(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cbat:pairBinding";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt1_accuracy=null;

	/**
	 * @return Returns the IntactT1/accuracy.
	 */
	public Integer getIntactt1_accuracy() {
		try{
			if (_Intactt1_accuracy==null){
				_Intactt1_accuracy=getIntegerProperty("IntactT1/accuracy");
				return _Intactt1_accuracy;
			}else {
				return _Intactt1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT1/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT1/accuracy",v);
		_Intactt1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt2_accuracy=null;

	/**
	 * @return Returns the IntactT2/accuracy.
	 */
	public Integer getIntactt2_accuracy() {
		try{
			if (_Intactt2_accuracy==null){
				_Intactt2_accuracy=getIntegerProperty("IntactT2/accuracy");
				return _Intactt2_accuracy;
			}else {
				return _Intactt2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT2/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT2/accuracy",v);
		_Intactt2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt3_accuracy=null;

	/**
	 * @return Returns the IntactT3/accuracy.
	 */
	public Integer getIntactt3_accuracy() {
		try{
			if (_Intactt3_accuracy==null){
				_Intactt3_accuracy=getIntegerProperty("IntactT3/accuracy");
				return _Intactt3_accuracy;
			}else {
				return _Intactt3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT3/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT3/accuracy",v);
		_Intactt3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt4_accuracy=null;

	/**
	 * @return Returns the IntactT4/accuracy.
	 */
	public Integer getIntactt4_accuracy() {
		try{
			if (_Intactt4_accuracy==null){
				_Intactt4_accuracy=getIntegerProperty("IntactT4/accuracy");
				return _Intactt4_accuracy;
			}else {
				return _Intactt4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT4/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT4/accuracy",v);
		_Intactt4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt5_accuracy=null;

	/**
	 * @return Returns the IntactT5/accuracy.
	 */
	public Integer getIntactt5_accuracy() {
		try{
			if (_Intactt5_accuracy==null){
				_Intactt5_accuracy=getIntegerProperty("IntactT5/accuracy");
				return _Intactt5_accuracy;
			}else {
				return _Intactt5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT5/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT5/accuracy",v);
		_Intactt5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt6_accuracy=null;

	/**
	 * @return Returns the IntactT6/accuracy.
	 */
	public Integer getIntactt6_accuracy() {
		try{
			if (_Intactt6_accuracy==null){
				_Intactt6_accuracy=getIntegerProperty("IntactT6/accuracy");
				return _Intactt6_accuracy;
			}else {
				return _Intactt6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT6/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT6/accuracy",v);
		_Intactt6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt7_accuracy=null;

	/**
	 * @return Returns the IntactT7/accuracy.
	 */
	public Integer getIntactt7_accuracy() {
		try{
			if (_Intactt7_accuracy==null){
				_Intactt7_accuracy=getIntegerProperty("IntactT7/accuracy");
				return _Intactt7_accuracy;
			}else {
				return _Intactt7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT7/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT7/accuracy",v);
		_Intactt7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt8_accuracy=null;

	/**
	 * @return Returns the IntactT8/accuracy.
	 */
	public Integer getIntactt8_accuracy() {
		try{
			if (_Intactt8_accuracy==null){
				_Intactt8_accuracy=getIntegerProperty("IntactT8/accuracy");
				return _Intactt8_accuracy;
			}else {
				return _Intactt8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT8/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT8/accuracy",v);
		_Intactt8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt9_accuracy=null;

	/**
	 * @return Returns the IntactT9/accuracy.
	 */
	public Integer getIntactt9_accuracy() {
		try{
			if (_Intactt9_accuracy==null){
				_Intactt9_accuracy=getIntegerProperty("IntactT9/accuracy");
				return _Intactt9_accuracy;
			}else {
				return _Intactt9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT9/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT9/accuracy",v);
		_Intactt9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt10_accuracy=null;

	/**
	 * @return Returns the IntactT10/accuracy.
	 */
	public Integer getIntactt10_accuracy() {
		try{
			if (_Intactt10_accuracy==null){
				_Intactt10_accuracy=getIntegerProperty("IntactT10/accuracy");
				return _Intactt10_accuracy;
			}else {
				return _Intactt10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT10/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT10/accuracy",v);
		_Intactt10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt11_accuracy=null;

	/**
	 * @return Returns the IntactT11/accuracy.
	 */
	public Integer getIntactt11_accuracy() {
		try{
			if (_Intactt11_accuracy==null){
				_Intactt11_accuracy=getIntegerProperty("IntactT11/accuracy");
				return _Intactt11_accuracy;
			}else {
				return _Intactt11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT11/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT11/accuracy",v);
		_Intactt11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Intactt12_accuracy=null;

	/**
	 * @return Returns the IntactT12/accuracy.
	 */
	public Integer getIntactt12_accuracy() {
		try{
			if (_Intactt12_accuracy==null){
				_Intactt12_accuracy=getIntegerProperty("IntactT12/accuracy");
				return _Intactt12_accuracy;
			}else {
				return _Intactt12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for IntactT12/accuracy.
	 * @param v Value to Set.
	 */
	public void setIntactt12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/IntactT12/accuracy",v);
		_Intactt12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt1_accuracy=null;

	/**
	 * @return Returns the MixedT1/accuracy.
	 */
	public Integer getMixedt1_accuracy() {
		try{
			if (_Mixedt1_accuracy==null){
				_Mixedt1_accuracy=getIntegerProperty("MixedT1/accuracy");
				return _Mixedt1_accuracy;
			}else {
				return _Mixedt1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT1/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT1/accuracy",v);
		_Mixedt1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt2_accuracy=null;

	/**
	 * @return Returns the MixedT2/accuracy.
	 */
	public Integer getMixedt2_accuracy() {
		try{
			if (_Mixedt2_accuracy==null){
				_Mixedt2_accuracy=getIntegerProperty("MixedT2/accuracy");
				return _Mixedt2_accuracy;
			}else {
				return _Mixedt2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT2/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT2/accuracy",v);
		_Mixedt2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt3_accuracy=null;

	/**
	 * @return Returns the MixedT3/accuracy.
	 */
	public Integer getMixedt3_accuracy() {
		try{
			if (_Mixedt3_accuracy==null){
				_Mixedt3_accuracy=getIntegerProperty("MixedT3/accuracy");
				return _Mixedt3_accuracy;
			}else {
				return _Mixedt3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT3/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT3/accuracy",v);
		_Mixedt3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt4_accuracy=null;

	/**
	 * @return Returns the MixedT4/accuracy.
	 */
	public Integer getMixedt4_accuracy() {
		try{
			if (_Mixedt4_accuracy==null){
				_Mixedt4_accuracy=getIntegerProperty("MixedT4/accuracy");
				return _Mixedt4_accuracy;
			}else {
				return _Mixedt4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT4/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT4/accuracy",v);
		_Mixedt4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt5_accuracy=null;

	/**
	 * @return Returns the MixedT5/accuracy.
	 */
	public Integer getMixedt5_accuracy() {
		try{
			if (_Mixedt5_accuracy==null){
				_Mixedt5_accuracy=getIntegerProperty("MixedT5/accuracy");
				return _Mixedt5_accuracy;
			}else {
				return _Mixedt5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT5/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT5/accuracy",v);
		_Mixedt5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt6_accuracy=null;

	/**
	 * @return Returns the MixedT6/accuracy.
	 */
	public Integer getMixedt6_accuracy() {
		try{
			if (_Mixedt6_accuracy==null){
				_Mixedt6_accuracy=getIntegerProperty("MixedT6/accuracy");
				return _Mixedt6_accuracy;
			}else {
				return _Mixedt6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT6/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT6/accuracy",v);
		_Mixedt6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt7_accuracy=null;

	/**
	 * @return Returns the MixedT7/accuracy.
	 */
	public Integer getMixedt7_accuracy() {
		try{
			if (_Mixedt7_accuracy==null){
				_Mixedt7_accuracy=getIntegerProperty("MixedT7/accuracy");
				return _Mixedt7_accuracy;
			}else {
				return _Mixedt7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT7/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT7/accuracy",v);
		_Mixedt7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt8_accuracy=null;

	/**
	 * @return Returns the MixedT8/accuracy.
	 */
	public Integer getMixedt8_accuracy() {
		try{
			if (_Mixedt8_accuracy==null){
				_Mixedt8_accuracy=getIntegerProperty("MixedT8/accuracy");
				return _Mixedt8_accuracy;
			}else {
				return _Mixedt8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT8/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT8/accuracy",v);
		_Mixedt8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt9_accuracy=null;

	/**
	 * @return Returns the MixedT9/accuracy.
	 */
	public Integer getMixedt9_accuracy() {
		try{
			if (_Mixedt9_accuracy==null){
				_Mixedt9_accuracy=getIntegerProperty("MixedT9/accuracy");
				return _Mixedt9_accuracy;
			}else {
				return _Mixedt9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT9/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT9/accuracy",v);
		_Mixedt9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt10_accuracy=null;

	/**
	 * @return Returns the MixedT10/accuracy.
	 */
	public Integer getMixedt10_accuracy() {
		try{
			if (_Mixedt10_accuracy==null){
				_Mixedt10_accuracy=getIntegerProperty("MixedT10/accuracy");
				return _Mixedt10_accuracy;
			}else {
				return _Mixedt10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT10/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT10/accuracy",v);
		_Mixedt10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt11_accuracy=null;

	/**
	 * @return Returns the MixedT11/accuracy.
	 */
	public Integer getMixedt11_accuracy() {
		try{
			if (_Mixedt11_accuracy==null){
				_Mixedt11_accuracy=getIntegerProperty("MixedT11/accuracy");
				return _Mixedt11_accuracy;
			}else {
				return _Mixedt11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT11/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT11/accuracy",v);
		_Mixedt11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mixedt12_accuracy=null;

	/**
	 * @return Returns the MixedT12/accuracy.
	 */
	public Integer getMixedt12_accuracy() {
		try{
			if (_Mixedt12_accuracy==null){
				_Mixedt12_accuracy=getIntegerProperty("MixedT12/accuracy");
				return _Mixedt12_accuracy;
			}else {
				return _Mixedt12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MixedT12/accuracy.
	 * @param v Value to Set.
	 */
	public void setMixedt12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MixedT12/accuracy",v);
		_Mixedt12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt1_accuracy=null;

	/**
	 * @return Returns the NewT1/accuracy.
	 */
	public Integer getNewt1_accuracy() {
		try{
			if (_Newt1_accuracy==null){
				_Newt1_accuracy=getIntegerProperty("NewT1/accuracy");
				return _Newt1_accuracy;
			}else {
				return _Newt1_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT1/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt1_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT1/accuracy",v);
		_Newt1_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt2_accuracy=null;

	/**
	 * @return Returns the NewT2/accuracy.
	 */
	public Integer getNewt2_accuracy() {
		try{
			if (_Newt2_accuracy==null){
				_Newt2_accuracy=getIntegerProperty("NewT2/accuracy");
				return _Newt2_accuracy;
			}else {
				return _Newt2_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT2/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt2_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT2/accuracy",v);
		_Newt2_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt3_accuracy=null;

	/**
	 * @return Returns the NewT3/accuracy.
	 */
	public Integer getNewt3_accuracy() {
		try{
			if (_Newt3_accuracy==null){
				_Newt3_accuracy=getIntegerProperty("NewT3/accuracy");
				return _Newt3_accuracy;
			}else {
				return _Newt3_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT3/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt3_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT3/accuracy",v);
		_Newt3_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt4_accuracy=null;

	/**
	 * @return Returns the NewT4/accuracy.
	 */
	public Integer getNewt4_accuracy() {
		try{
			if (_Newt4_accuracy==null){
				_Newt4_accuracy=getIntegerProperty("NewT4/accuracy");
				return _Newt4_accuracy;
			}else {
				return _Newt4_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT4/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt4_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT4/accuracy",v);
		_Newt4_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt5_accuracy=null;

	/**
	 * @return Returns the NewT5/accuracy.
	 */
	public Integer getNewt5_accuracy() {
		try{
			if (_Newt5_accuracy==null){
				_Newt5_accuracy=getIntegerProperty("NewT5/accuracy");
				return _Newt5_accuracy;
			}else {
				return _Newt5_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT5/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt5_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT5/accuracy",v);
		_Newt5_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt6_accuracy=null;

	/**
	 * @return Returns the NewT6/accuracy.
	 */
	public Integer getNewt6_accuracy() {
		try{
			if (_Newt6_accuracy==null){
				_Newt6_accuracy=getIntegerProperty("NewT6/accuracy");
				return _Newt6_accuracy;
			}else {
				return _Newt6_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT6/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt6_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT6/accuracy",v);
		_Newt6_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt7_accuracy=null;

	/**
	 * @return Returns the NewT7/accuracy.
	 */
	public Integer getNewt7_accuracy() {
		try{
			if (_Newt7_accuracy==null){
				_Newt7_accuracy=getIntegerProperty("NewT7/accuracy");
				return _Newt7_accuracy;
			}else {
				return _Newt7_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT7/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt7_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT7/accuracy",v);
		_Newt7_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt8_accuracy=null;

	/**
	 * @return Returns the NewT8/accuracy.
	 */
	public Integer getNewt8_accuracy() {
		try{
			if (_Newt8_accuracy==null){
				_Newt8_accuracy=getIntegerProperty("NewT8/accuracy");
				return _Newt8_accuracy;
			}else {
				return _Newt8_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT8/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt8_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT8/accuracy",v);
		_Newt8_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt9_accuracy=null;

	/**
	 * @return Returns the NewT9/accuracy.
	 */
	public Integer getNewt9_accuracy() {
		try{
			if (_Newt9_accuracy==null){
				_Newt9_accuracy=getIntegerProperty("NewT9/accuracy");
				return _Newt9_accuracy;
			}else {
				return _Newt9_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT9/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt9_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT9/accuracy",v);
		_Newt9_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt10_accuracy=null;

	/**
	 * @return Returns the NewT10/accuracy.
	 */
	public Integer getNewt10_accuracy() {
		try{
			if (_Newt10_accuracy==null){
				_Newt10_accuracy=getIntegerProperty("NewT10/accuracy");
				return _Newt10_accuracy;
			}else {
				return _Newt10_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT10/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt10_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT10/accuracy",v);
		_Newt10_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt11_accuracy=null;

	/**
	 * @return Returns the NewT11/accuracy.
	 */
	public Integer getNewt11_accuracy() {
		try{
			if (_Newt11_accuracy==null){
				_Newt11_accuracy=getIntegerProperty("NewT11/accuracy");
				return _Newt11_accuracy;
			}else {
				return _Newt11_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT11/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt11_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT11/accuracy",v);
		_Newt11_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Newt12_accuracy=null;

	/**
	 * @return Returns the NewT12/accuracy.
	 */
	public Integer getNewt12_accuracy() {
		try{
			if (_Newt12_accuracy==null){
				_Newt12_accuracy=getIntegerProperty("NewT12/accuracy");
				return _Newt12_accuracy;
			}else {
				return _Newt12_accuracy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NewT12/accuracy.
	 * @param v Value to Set.
	 */
	public void setNewt12_accuracy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NewT12/accuracy",v);
		_Newt12_accuracy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CbatPairbinding> getAllCbatPairbindings(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatPairbinding> al = new ArrayList<org.nrg.xdat.om.CbatPairbinding>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatPairbinding> getCbatPairbindingsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatPairbinding> al = new ArrayList<org.nrg.xdat.om.CbatPairbinding>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CbatPairbinding> getCbatPairbindingsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CbatPairbinding> al = new ArrayList<org.nrg.xdat.om.CbatPairbinding>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CbatPairbinding getCbatPairbindingsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cbat:pairBinding/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CbatPairbinding) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
