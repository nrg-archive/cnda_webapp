/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsB6bevgdsdata extends AutoUdsB6bevgdsdata {

	public BaseUdsB6bevgdsdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsB6bevgdsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB6bevgdsdata(UserI user)
	 **/
	public BaseUdsB6bevgdsdata()
	{}

	public BaseUdsB6bevgdsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
