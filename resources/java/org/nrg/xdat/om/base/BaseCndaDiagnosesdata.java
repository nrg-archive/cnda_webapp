/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaDiagnosesdata extends AutoCndaDiagnosesdata {

	public BaseCndaDiagnosesdata(ItemI item)
	{
		super(item);
	}

	public BaseCndaDiagnosesdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaDiagnosesdata(UserI user)
	 **/
	public BaseCndaDiagnosesdata()
	{}

	public BaseCndaDiagnosesdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
