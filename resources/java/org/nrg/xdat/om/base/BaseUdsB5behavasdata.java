/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsB5behavasdata extends AutoUdsB5behavasdata {

	public BaseUdsB5behavasdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsB5behavasdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB5behavasdata(UserI user)
	 **/
	public BaseUdsB5behavasdata()
	{}

	public BaseUdsB5behavasdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
