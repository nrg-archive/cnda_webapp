/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseIpipExercisedata extends AutoIpipExercisedata {

	public BaseIpipExercisedata(ItemI item)
	{
		super(item);
	}

	public BaseIpipExercisedata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseIpipExercisedata(UserI user)
	 **/
	public BaseIpipExercisedata()
	{}

	public BaseIpipExercisedata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
