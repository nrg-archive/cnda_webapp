/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCbatSimon extends AutoCbatSimon {

	public BaseCbatSimon(ItemI item)
	{
		super(item);
	}

	public BaseCbatSimon(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatSimon(UserI user)
	 **/
	public BaseCbatSimon()
	{}

	public BaseCbatSimon(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
