/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseLs2Lsdemographicdata extends AutoLs2Lsdemographicdata {

	public BaseLs2Lsdemographicdata(ItemI item)
	{
		super(item);
	}

	public BaseLs2Lsdemographicdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lsdemographicdata(UserI user)
	 **/
	public BaseLs2Lsdemographicdata()
	{}

	public BaseLs2Lsdemographicdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
