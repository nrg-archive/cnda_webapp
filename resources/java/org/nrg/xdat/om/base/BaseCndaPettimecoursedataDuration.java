/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaPettimecoursedataDuration extends AutoCndaPettimecoursedataDuration {

	public BaseCndaPettimecoursedataDuration(ItemI item)
	{
		super(item);
	}

	public BaseCndaPettimecoursedataDuration(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPettimecoursedataDuration(UserI user)
	 **/
	public BaseCndaPettimecoursedataDuration()
	{}

	public BaseCndaPettimecoursedataDuration(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
