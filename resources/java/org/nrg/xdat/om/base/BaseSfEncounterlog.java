/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseSfEncounterlog extends AutoSfEncounterlog {

	public BaseSfEncounterlog(ItemI item)
	{
		super(item);
	}

	public BaseSfEncounterlog(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSfEncounterlog(UserI user)
	 **/
	public BaseSfEncounterlog()
	{}

	public BaseSfEncounterlog(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
