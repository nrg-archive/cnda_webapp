/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseLs2Lsmedication extends AutoLs2Lsmedication {

	public BaseLs2Lsmedication(ItemI item)
	{
		super(item);
	}

	public BaseLs2Lsmedication(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lsmedication(UserI user)
	 **/
	public BaseLs2Lsmedication()
	{}

	public BaseLs2Lsmedication(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
