/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaAtrophynildata extends AutoCndaAtrophynildata {

	public BaseCndaAtrophynildata(ItemI item)
	{
		super(item);
	}

	public BaseCndaAtrophynildata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaAtrophynildata(UserI user)
	 **/
	public BaseCndaAtrophynildata()
	{}

	public BaseCndaAtrophynildata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
