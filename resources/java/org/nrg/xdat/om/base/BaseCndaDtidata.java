/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaDtidata extends AutoCndaDtidata {

	public BaseCndaDtidata(ItemI item)
	{
		super(item);
	}

	public BaseCndaDtidata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaDtidata(UserI user)
	 **/
	public BaseCndaDtidata()
	{}

	public BaseCndaDtidata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
