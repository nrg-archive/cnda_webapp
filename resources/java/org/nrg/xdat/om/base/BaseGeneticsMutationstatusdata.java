/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseGeneticsMutationstatusdata extends AutoGeneticsMutationstatusdata {

	public BaseGeneticsMutationstatusdata(ItemI item)
	{
		super(item);
	}

	public BaseGeneticsMutationstatusdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseGeneticsMutationstatusdata(UserI user)
	 **/
	public BaseGeneticsMutationstatusdata()
	{}

	public BaseGeneticsMutationstatusdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
