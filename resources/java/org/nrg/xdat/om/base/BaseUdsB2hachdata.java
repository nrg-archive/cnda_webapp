/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsB2hachdata extends AutoUdsB2hachdata {

	public BaseUdsB2hachdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsB2hachdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB2hachdata(UserI user)
	 **/
	public BaseUdsB2hachdata()
	{}

	public BaseUdsB2hachdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
