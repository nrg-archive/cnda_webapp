/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BasePupPuptimecoursedataRoi extends AutoPupPuptimecoursedataRoi {

	public BasePupPuptimecoursedataRoi(ItemI item)
	{
		super(item);
	}

	public BasePupPuptimecoursedataRoi(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BasePupPuptimecoursedataRoi(UserI user)
	 **/
	public BasePupPuptimecoursedataRoi()
	{}

	public BasePupPuptimecoursedataRoi(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
