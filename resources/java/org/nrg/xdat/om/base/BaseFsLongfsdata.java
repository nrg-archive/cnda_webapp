/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseFsLongfsdata extends AutoFsLongfsdata {

	public BaseFsLongfsdata(ItemI item)
	{
		super(item);
	}

	public BaseFsLongfsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsLongfsdata(UserI user)
	 **/
	public BaseFsLongfsdata()
	{}

	public BaseFsLongfsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
