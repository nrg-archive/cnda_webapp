/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseDianAdversedata extends AutoDianAdversedata {

	public BaseDianAdversedata(ItemI item)
	{
		super(item);
	}

	public BaseDianAdversedata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianAdversedata(UserI user)
	 **/
	public BaseDianAdversedata()
	{}

	public BaseDianAdversedata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
