/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsB4cdrdata extends AutoUdsB4cdrdata {

	public BaseUdsB4cdrdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsB4cdrdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB4cdrdata(UserI user)
	 **/
	public BaseUdsB4cdrdata()
	{}

	public BaseUdsB4cdrdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
