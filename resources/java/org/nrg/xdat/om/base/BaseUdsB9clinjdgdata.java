/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsB9clinjdgdata extends AutoUdsB9clinjdgdata {

	public BaseUdsB9clinjdgdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsB9clinjdgdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB9clinjdgdata(UserI user)
	 **/
	public BaseUdsB9clinjdgdata()
	{}

	public BaseUdsB9clinjdgdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
