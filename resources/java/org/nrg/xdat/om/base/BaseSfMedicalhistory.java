/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseSfMedicalhistory extends AutoSfMedicalhistory {

	public BaseSfMedicalhistory(ItemI item)
	{
		super(item);
	}

	public BaseSfMedicalhistory(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSfMedicalhistory(UserI user)
	 **/
	public BaseSfMedicalhistory()
	{}

	public BaseSfMedicalhistory(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
