/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsA4drugsdataA4drugs extends AutoUdsA4drugsdataA4drugs {

	public BaseUdsA4drugsdataA4drugs(ItemI item)
	{
		super(item);
	}

	public BaseUdsA4drugsdataA4drugs(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA4drugsdataA4drugs(UserI user)
	 **/
	public BaseUdsA4drugsdataA4drugs()
	{}

	public BaseUdsA4drugsdataA4drugs(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
