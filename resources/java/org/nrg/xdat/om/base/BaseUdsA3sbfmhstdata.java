/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsA3sbfmhstdata extends AutoUdsA3sbfmhstdata {

	public BaseUdsA3sbfmhstdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsA3sbfmhstdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA3sbfmhstdata(UserI user)
	 **/
	public BaseUdsA3sbfmhstdata()
	{}

	public BaseUdsA3sbfmhstdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
