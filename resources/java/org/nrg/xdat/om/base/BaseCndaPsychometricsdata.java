/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaPsychometricsdata extends AutoCndaPsychometricsdata {

	public BaseCndaPsychometricsdata(ItemI item)
	{
		super(item);
	}

	public BaseCndaPsychometricsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPsychometricsdata(UserI user)
	 **/
	public BaseCndaPsychometricsdata()
	{}

	public BaseCndaPsychometricsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
