/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseDianAdversedataAdverse extends AutoDianAdversedataAdverse {

	public BaseDianAdversedataAdverse(ItemI item)
	{
		super(item);
	}

	public BaseDianAdversedataAdverse(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianAdversedataAdverse(UserI user)
	 **/
	public BaseDianAdversedataAdverse()
	{}

	public BaseDianAdversedataAdverse(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
