/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseNihssNihstrokescaledata extends AutoNihssNihstrokescaledata {

	public BaseNihssNihstrokescaledata(ItemI item)
	{
		super(item);
	}

	public BaseNihssNihstrokescaledata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseNihssNihstrokescaledata(UserI user)
	 **/
	public BaseNihssNihstrokescaledata()
	{}

	public BaseNihssNihstrokescaledata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
