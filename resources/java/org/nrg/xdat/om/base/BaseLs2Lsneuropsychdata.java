/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseLs2Lsneuropsychdata extends AutoLs2Lsneuropsychdata {

	public BaseLs2Lsneuropsychdata(ItemI item)
	{
		super(item);
	}

	public BaseLs2Lsneuropsychdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lsneuropsychdata(UserI user)
	 **/
	public BaseLs2Lsneuropsychdata()
	{}

	public BaseLs2Lsneuropsychdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
