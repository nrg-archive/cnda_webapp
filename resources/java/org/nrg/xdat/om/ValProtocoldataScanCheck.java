/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:47:27 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class ValProtocoldataScanCheck extends BaseValProtocoldataScanCheck {

	public ValProtocoldataScanCheck(ItemI item)
	{
		super(item);
	}

	public ValProtocoldataScanCheck(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseValProtocoldataScanCheck(UserI user)
	 **/
	public ValProtocoldataScanCheck()
	{}

	public ValProtocoldataScanCheck(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
