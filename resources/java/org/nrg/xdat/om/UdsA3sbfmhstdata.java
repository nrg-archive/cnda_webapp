/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsA3sbfmhstdata extends BaseUdsA3sbfmhstdata {

	public UdsA3sbfmhstdata(ItemI item)
	{
		super(item);
	}

	public UdsA3sbfmhstdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA3sbfmhstdata(UserI user)
	 **/
	public UdsA3sbfmhstdata()
	{}

	public UdsA3sbfmhstdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
