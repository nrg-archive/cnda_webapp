/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class BehavioralTaskssummarydataTask extends BaseBehavioralTaskssummarydataTask {

	public BehavioralTaskssummarydataTask(ItemI item)
	{
		super(item);
	}

	public BehavioralTaskssummarydataTask(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseBehavioralTaskssummarydataTask(UserI user)
	 **/
	public BehavioralTaskssummarydataTask()
	{}

	public BehavioralTaskssummarydataTask(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
