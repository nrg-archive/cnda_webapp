/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class BclAbcl103ed121data extends BaseBclAbcl103ed121data {

	public BclAbcl103ed121data(ItemI item)
	{
		super(item);
	}

	public BclAbcl103ed121data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseBclAbcl103ed121data(UserI user)
	 **/
	public BclAbcl103ed121data()
	{}

	public BclAbcl103ed121data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
