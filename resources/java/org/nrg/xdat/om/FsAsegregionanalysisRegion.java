/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class FsAsegregionanalysisRegion extends BaseFsAsegregionanalysisRegion {

	public FsAsegregionanalysisRegion(ItemI item)
	{
		super(item);
	}

	public FsAsegregionanalysisRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsAsegregionanalysisRegion(UserI user)
	 **/
	public FsAsegregionanalysisRegion()
	{}

	public FsAsegregionanalysisRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
