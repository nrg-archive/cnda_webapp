/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB9clinjdgdata extends BaseUdsB9clinjdgdata {

	public UdsB9clinjdgdata(ItemI item)
	{
		super(item);
	}

	public UdsB9clinjdgdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB9clinjdgdata(UserI user)
	 **/
	public UdsB9clinjdgdata()
	{}

	public UdsB9clinjdgdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
