/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class Ls2Lssubstance extends BaseLs2Lssubstance {

	public Ls2Lssubstance(ItemI item)
	{
		super(item);
	}

	public Ls2Lssubstance(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lssubstance(UserI user)
	 **/
	public Ls2Lssubstance()
	{}

	public Ls2Lssubstance(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
