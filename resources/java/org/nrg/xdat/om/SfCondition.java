/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class SfCondition extends BaseSfCondition {

	public SfCondition(ItemI item)
	{
		super(item);
	}

	public SfCondition(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSfCondition(UserI user)
	 **/
	public SfCondition()
	{}

	public SfCondition(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
