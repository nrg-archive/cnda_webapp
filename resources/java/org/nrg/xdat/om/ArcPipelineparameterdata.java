/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:47:27 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class ArcPipelineparameterdata extends BaseArcPipelineparameterdata {

	public ArcPipelineparameterdata(ItemI item)
	{
		super(item);
	}

	public ArcPipelineparameterdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseArcPipelineparameterdata(UserI user)
	 **/
	public ArcPipelineparameterdata()
	{}

	public ArcPipelineparameterdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
