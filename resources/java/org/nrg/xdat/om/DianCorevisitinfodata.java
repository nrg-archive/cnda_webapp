/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianCorevisitinfodata extends BaseDianCorevisitinfodata {

	public DianCorevisitinfodata(ItemI item)
	{
		super(item);
	}

	public DianCorevisitinfodata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianCorevisitinfodata(UserI user)
	 **/
	public DianCorevisitinfodata()
	{}

	public DianCorevisitinfodata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
