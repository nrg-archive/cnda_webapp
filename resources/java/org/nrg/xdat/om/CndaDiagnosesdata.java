/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaDiagnosesdata extends BaseCndaDiagnosesdata {

	public CndaDiagnosesdata(ItemI item)
	{
		super(item);
	}

	public CndaDiagnosesdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaDiagnosesdata(UserI user)
	 **/
	public CndaDiagnosesdata()
	{}

	public CndaDiagnosesdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
