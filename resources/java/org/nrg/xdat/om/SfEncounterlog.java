/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class SfEncounterlog extends BaseSfEncounterlog {

	public SfEncounterlog(ItemI item)
	{
		super(item);
	}

	public SfEncounterlog(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSfEncounterlog(UserI user)
	 **/
	public SfEncounterlog()
	{}

	public SfEncounterlog(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
