/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB2hachdata extends BaseUdsB2hachdata {

	public UdsB2hachdata(ItemI item)
	{
		super(item);
	}

	public UdsB2hachdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB2hachdata(UserI user)
	 **/
	public UdsB2hachdata()
	{}

	public UdsB2hachdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
