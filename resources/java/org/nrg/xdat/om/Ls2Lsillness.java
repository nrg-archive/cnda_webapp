/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class Ls2Lsillness extends BaseLs2Lsillness {

	public Ls2Lsillness(ItemI item)
	{
		super(item);
	}

	public Ls2Lsillness(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lsillness(UserI user)
	 **/
	public Ls2Lsillness()
	{}

	public Ls2Lsillness(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
