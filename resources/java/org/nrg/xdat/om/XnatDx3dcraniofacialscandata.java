/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:47:27 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatDx3dcraniofacialscandata extends BaseXnatDx3dcraniofacialscandata {

	public XnatDx3dcraniofacialscandata(ItemI item)
	{
		super(item);
	}

	public XnatDx3dcraniofacialscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatDx3dcraniofacialscandata(UserI user)
	 **/
	public XnatDx3dcraniofacialscandata()
	{}

	public XnatDx3dcraniofacialscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
