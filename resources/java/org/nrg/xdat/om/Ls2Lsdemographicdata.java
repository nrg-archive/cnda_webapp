/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class Ls2Lsdemographicdata extends BaseLs2Lsdemographicdata {

	public Ls2Lsdemographicdata(ItemI item)
	{
		super(item);
	}

	public Ls2Lsdemographicdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lsdemographicdata(UserI user)
	 **/
	public Ls2Lsdemographicdata()
	{}

	public Ls2Lsdemographicdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
