/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class TissueTisscolldata extends BaseTissueTisscolldata {

	public TissueTisscolldata(ItemI item)
	{
		super(item);
	}

	public TissueTisscolldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseTissueTisscolldata(UserI user)
	 **/
	public TissueTisscolldata()
	{}

	public TissueTisscolldata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
