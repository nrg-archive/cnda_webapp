/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class GeneticsGenotypesessiondata extends BaseGeneticsGenotypesessiondata {

	public GeneticsGenotypesessiondata(ItemI item)
	{
		super(item);
	}

	public GeneticsGenotypesessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseGeneticsGenotypesessiondata(UserI user)
	 **/
	public GeneticsGenotypesessiondata()
	{}

	public GeneticsGenotypesessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
