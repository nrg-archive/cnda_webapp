/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class ClinPregdataPreg extends BaseClinPregdataPreg {

	public ClinPregdataPreg(ItemI item)
	{
		super(item);
	}

	public ClinPregdataPreg(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseClinPregdataPreg(UserI user)
	 **/
	public ClinPregdataPreg()
	{}

	public ClinPregdataPreg(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
