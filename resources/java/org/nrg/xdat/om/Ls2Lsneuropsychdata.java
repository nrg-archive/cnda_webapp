/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class Ls2Lsneuropsychdata extends BaseLs2Lsneuropsychdata {

	public Ls2Lsneuropsychdata(ItemI item)
	{
		super(item);
	}

	public Ls2Lsneuropsychdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lsneuropsychdata(UserI user)
	 **/
	public Ls2Lsneuropsychdata()
	{}

	public Ls2Lsneuropsychdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
