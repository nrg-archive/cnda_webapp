//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:38 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.text.NumberFormat;
import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaManualvolumetryregionSlice extends BaseCndaManualvolumetryregionSlice {

	public CndaManualvolumetryregionSlice(ItemI item)
	{
		super(item);
	}

	public CndaManualvolumetryregionSlice(UserI user)
	{
		super(user);
	}

	public CndaManualvolumetryregionSlice()
	{}

	public CndaManualvolumetryregionSlice(Hashtable properties, UserI user)
	{
		super(properties,user);
	}


	public String getAreaDisplay()
	{
	    Double f= this.getArea();
	    NumberFormat formatter = NumberFormat.getInstance();
		formatter.setGroupingUsed(false);
		formatter.setMaximumFractionDigits(2);
		formatter.setMinimumFractionDigits(0);
		return formatter.format(f);
	}

	public String getVolumeDisplay()
	{
	    Double f= this.getVolume();
	    NumberFormat formatter = NumberFormat.getInstance();
		formatter.setGroupingUsed(false);
		formatter.setMaximumFractionDigits(2);
		formatter.setMinimumFractionDigits(0);
		return formatter.format(f);
	}
}
