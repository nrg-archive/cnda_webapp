/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianCsintdata extends BaseDianCsintdata {

	public DianCsintdata(ItemI item)
	{
		super(item);
	}

	public DianCsintdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianCsintdata(UserI user)
	 **/
	public DianCsintdata()
	{}

	public DianCsintdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
