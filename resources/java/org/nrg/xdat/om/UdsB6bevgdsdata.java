/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB6bevgdsdata extends BaseUdsB6bevgdsdata {

	public UdsB6bevgdsdata(ItemI item)
	{
		super(item);
	}

	public UdsB6bevgdsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB6bevgdsdata(UserI user)
	 **/
	public UdsB6bevgdsdata()
	{}

	public UdsB6bevgdsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
