/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianCdrsuppdata extends BaseDianCdrsuppdata {

	public DianCdrsuppdata(ItemI item)
	{
		super(item);
	}

	public DianCdrsuppdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianCdrsuppdata(UserI user)
	 **/
	public DianCdrsuppdata()
	{}

	public DianCdrsuppdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
