/*
 * GENERATED FILE
 * Created on Wed Sep 28 13:56:40 IST 2011
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class PetFspettimecoursedata extends BasePetFspettimecoursedata {

	public PetFspettimecoursedata(ItemI item)
	{
		super(item);
	}

	public PetFspettimecoursedata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BasePetFspettimecoursedata(UserI user)
	 **/
	public PetFspettimecoursedata()
	{}

	public PetFspettimecoursedata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

    public PetFspettimecoursedataRoi GetRoi(String roiname) {
    	PetFspettimecoursedataRoi rtn = null;
    	List<PetFspettimecoursedataRoi> rois = this.getRois_roi();
    	for(int i=0; i< rois.size(); i++) {
    		PetFspettimecoursedataRoi roi = rois.get(i);
    		if (roi.getName().equals(roiname)) {
    			rtn = roi;
    			break;
    		}
    	}
    	return rtn;
    }


}
