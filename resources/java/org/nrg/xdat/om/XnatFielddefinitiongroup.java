/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:47:27 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatFielddefinitiongroup extends BaseXnatFielddefinitiongroup {

	public XnatFielddefinitiongroup(ItemI item)
	{
		super(item);
	}

	public XnatFielddefinitiongroup(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatFielddefinitiongroup(UserI user)
	 **/
	public XnatFielddefinitiongroup()
	{}

	public XnatFielddefinitiongroup(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
