/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CondrProgressionlog extends BaseCondrProgressionlog {

	public CondrProgressionlog(ItemI item)
	{
		super(item);
	}

	public CondrProgressionlog(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrProgressionlog(UserI user)
	 **/
	public CondrProgressionlog()
	{}

	public CondrProgressionlog(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
