/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:47:27 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatVolumetricregionSubregion extends BaseXnatVolumetricregionSubregion {

	public XnatVolumetricregionSubregion(ItemI item)
	{
		super(item);
	}

	public XnatVolumetricregionSubregion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatVolumetricregionSubregion(UserI user)
	 **/
	public XnatVolumetricregionSubregion()
	{}

	public XnatVolumetricregionSubregion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
