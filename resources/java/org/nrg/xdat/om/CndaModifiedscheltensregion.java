/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaModifiedscheltensregion extends BaseCndaModifiedscheltensregion {

	public CndaModifiedscheltensregion(ItemI item)
	{
		super(item);
	}

	public CndaModifiedscheltensregion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaModifiedscheltensregion(UserI user)
	 **/
	public CndaModifiedscheltensregion()
	{}

	public CndaModifiedscheltensregion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
