/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:47:27 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatXascandata extends BaseXnatXascandata {

	public XnatXascandata(ItemI item)
	{
		super(item);
	}

	public XnatXascandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatXascandata(UserI user)
	 **/
	public XnatXascandata()
	{}

	public XnatXascandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
