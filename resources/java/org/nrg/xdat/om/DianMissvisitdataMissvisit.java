/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianMissvisitdataMissvisit extends BaseDianMissvisitdataMissvisit {

	public DianMissvisitdataMissvisit(ItemI item)
	{
		super(item);
	}

	public DianMissvisitdataMissvisit(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianMissvisitdataMissvisit(UserI user)
	 **/
	public DianMissvisitdataMissvisit()
	{}

	public DianMissvisitdataMissvisit(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
