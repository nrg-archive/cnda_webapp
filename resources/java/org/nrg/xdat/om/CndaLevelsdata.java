/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaLevelsdata extends BaseCndaLevelsdata {

	public CndaLevelsdata(ItemI item)
	{
		super(item);
	}

	public CndaLevelsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaLevelsdata(UserI user)
	 **/
	public CndaLevelsdata()
	{}

	public CndaLevelsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
