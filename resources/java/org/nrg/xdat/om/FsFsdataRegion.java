/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class FsFsdataRegion extends BaseFsFsdataRegion {

	public FsFsdataRegion(ItemI item)
	{
		super(item);
	}

	public FsFsdataRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsFsdataRegion(UserI user)
	 **/
	public FsFsdataRegion()
	{}

	public FsFsdataRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
