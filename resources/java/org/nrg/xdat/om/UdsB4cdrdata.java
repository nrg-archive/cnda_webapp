/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB4cdrdata extends BaseUdsB4cdrdata {

	public UdsB4cdrdata(ItemI item)
	{
		super(item);
	}

	public UdsB4cdrdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB4cdrdata(UserI user)
	 **/
	public UdsB4cdrdata()
	{}

	public UdsB4cdrdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
