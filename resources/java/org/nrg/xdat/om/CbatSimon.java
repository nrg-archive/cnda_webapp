/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CbatSimon extends BaseCbatSimon {

	public CbatSimon(ItemI item)
	{
		super(item);
	}

	public CbatSimon(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatSimon(UserI user)
	 **/
	public CbatSimon()
	{}

	public CbatSimon(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
