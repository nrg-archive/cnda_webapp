/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:47:27 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatXa3dsessiondata extends BaseXnatXa3dsessiondata {

	public XnatXa3dsessiondata(ItemI item)
	{
		super(item);
	}

	public XnatXa3dsessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatXa3dsessiondata(UserI user)
	 **/
	public XnatXa3dsessiondata()
	{}

	public XnatXa3dsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
