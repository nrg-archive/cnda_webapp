/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class Ls2Lsmedication extends BaseLs2Lsmedication {

	public Ls2Lsmedication(ItemI item)
	{
		super(item);
	}

	public Ls2Lsmedication(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lsmedication(UserI user)
	 **/
	public Ls2Lsmedication()
	{}

	public Ls2Lsmedication(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
