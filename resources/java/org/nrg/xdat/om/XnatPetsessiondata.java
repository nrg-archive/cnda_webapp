// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Dec 06 09:45:34 CST 2006
 *
 */
package org.nrg.xdat.om;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import org.nrg.xdat.model.MpetManualpettimecoursedataRoiI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.base.BaseXnatPetsessiondata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.InvalidValueException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.search.TableSearch;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.StringUtils;

/**
 * @author XDAT
 *
 */
public class XnatPetsessiondata extends BaseXnatPetsessiondata {

    public XnatPetsessiondata(ItemI item)
    {
        super(item);
    }

    public XnatPetsessiondata(UserI user)
    {
        super(user);
    }

    /*
     * @deprecated Use BaseXnatPetsessiondata(UserI user)
     **/
    public XnatPetsessiondata()
    {}

    public XnatPetsessiondata(Hashtable properties, UserI user)
    {
        super(properties,user);
    }

    public boolean isRegionAssessed(XnatRegionresource region) {
        String dataType = "cnda:petTimeCourseData";
        boolean assessed = isRegionAssessed(region,dataType);
        return assessed;
    }

    public boolean isRegionAssessed(XnatRegionresource region, String dataType) {
        boolean assessed = false;
        ArrayList assessors = getAssessors(dataType);
        if (assessors == null || assessors.size() == 0) return assessed;
        if (dataType.equals("cnda:petTimeCourseData")) {
            CndaPettimecoursedata assessor = (CndaPettimecoursedata)assessors.get(0);
            ArrayList subRegions = (ArrayList)region.getSubregionlabels_label();
            if (subRegions != null && subRegions.size() > 0) {
                for (int i = 0; i < subRegions.size(); i++) {
                    XnatRegionresourceLabel resourceLabel = (XnatRegionresourceLabel)subRegions.get(i);
                    if (assessor.existsTimeSeries(resourceLabel.getLabel(), resourceLabel.getHemisphere())) {
                        assessed = true;
                        break;
                    }
                }
            }else {
                if (assessor.existsTimeSeries(region.getName(), region.getHemisphere())) {
                    assessed = true;
                }
            }
        }else if (dataType.equals("mpet:ManualPETTimeCourse")) {
            MpetManualpettimecoursedata assessor = (MpetManualpettimecoursedata)assessors.get(0);
            ArrayList subRegions = (ArrayList)region.getSubregionlabels_label();
            List<MpetManualpettimecoursedataRoiI> rois = assessor.getRois_roi();

            if (subRegions != null && subRegions.size() > 0) {
                for (int i = 0; i < subRegions.size(); i++) {
                    XnatRegionresourceLabel resourceLabel = (XnatRegionresourceLabel)subRegions.get(i);
                    for (int j=0; j < rois.size(); j++) {
                        MpetManualpettimecoursedataRoiI roi = rois.get(j);
                        if (roi.getName().equals(resourceLabel.getLabel().replace(" ", ""))) {
                            assessed = true;
                            break;
                        }
                    }
                }
            }else {
                for (int j=0; j < rois.size(); j++) {
                    MpetManualpettimecoursedataRoiI roi = rois.get(j);
                    if (roi.getName().equals(region.getName().replace(" ", ""))) {
                        assessed = true;
                    }
                }
            }
        }
        return assessed;
    }


    public ArrayList getUnAssessedRegions() {
        String dataType = "cnda:petTimeCourseData";
        return getUnAssessedRegions(dataType);
    }

    public ArrayList getUnAssessedRegions(String dataType) {
        ArrayList temp = new ArrayList();
        ArrayList regions = (ArrayList)this.getRegions_region();
        if (regions == null || regions.size() ==0) return temp;
        for (int i = 0; i < regions.size(); i++) {
            XnatRegionresource region = (XnatRegionresource)regions.get(i);
            if (!isRegionAssessed(region, dataType)) {
                temp.add(region);
            }
        }
        return temp;
    }

    public ArrayList getAssessedRegions() {
        return getAssessedRegions("cnda:petTimeCourseData");
    }

    public ArrayList getAssessedRegions(String dataType) {
        ArrayList temp = new ArrayList();
        ArrayList regions = (ArrayList)this.getRegions_region();
        if (regions == null || regions.size() ==0) return temp;
        for (int i = 0; i < regions.size(); i++) {
            XnatRegionresource region = (XnatRegionresource)regions.get(i);
            if (isRegionAssessed(region, dataType)) {
                temp.add(region);
            }
        }
        return temp;
    }

    public boolean hasMPRAGEInAtlasSpace() {
        return hasMPRAGEInAtlasSpace("MPRAGE_222");
    }

    public boolean hasMPRAGEInAtlasSpace(String content) {
            boolean _rtn = false;
            if (getResources_resource().size()>0) {
            List resources = getResources_resource();
            for (int i=0; i< resources.size(); i++) {
                if (resources.get(i) instanceof XnatResourcecatalog) {
                    XnatResourcecatalog rscCatalog = (XnatResourcecatalog)resources.get(i);
                    if (rscCatalog.getLabel().equalsIgnoreCase(content))
                        _rtn=true;
                    break;
                }
            }
            }
            return _rtn;
        }


    public boolean isAtlasRegistered() {
        boolean built = true;
        List<XnatReconstructedimagedata> recons = getReconstructionsByType("PET");
        if (recons == null || recons.size() == 0) return !built;
        return built;
    }



    public String getStartFrame() {
        return getComputation("START FRAME");
    }

    public String getTotalFrame() {
        return getComputation("FRAMES");
    }

    public String getComputation(String name) {
        String rtn = "";
        try {
            if (isAtlasRegistered()) {
                List<XnatReconstructedimagedata> recons = getReconstructionsByType("PET");
                XnatReconstructedimagedata reconstrcutedImage = (XnatReconstructedimagedata)recons.get(0);
                ArrayList computation_datums = (ArrayList)reconstrcutedImage.getComputations_datum();
                if (computation_datums == null || computation_datums.size() == 0) return rtn;
                for (int i = 0; i < computation_datums.size(); i++) {
                    XnatComputationdata datum = ((XnatComputationdata)computation_datums.get(i));
                    if (datum.getName().equalsIgnoreCase(name)) {
                        rtn = datum.getValue();
                        break;
                    }
                }

            }
        }catch (Exception e) {
            logger.error("",e);
        }
        return rtn;
    }


    public String getFirstFrame() {
        String rtn = "";
        String startFrames = getStartFrame();
        StringTokenizer st = new StringTokenizer(startFrames);
        if (st.hasMoreTokens())
            rtn = st.nextToken();
        return rtn;
    }

    public ArrayList getReconstructedFileByContent(String content) {
        ArrayList files = new ArrayList();
        if (isAtlasRegistered()) {
            List<XnatReconstructedimagedata> recons = getReconstructionsByType("PET");
            if (recons == null || recons.size() == 0) return files;
            XnatReconstructedimagedata reconData = (XnatReconstructedimagedata)recons.get(0);
            files = reconData.getOutFileByContent(content);
        }
        return files;
    }


    public XnatAbstractresourceI getResourceFileByContent(String content) {
        XnatAbstractresourceI _rtn = null;
        if (hasMPRAGEInAtlasSpace(content)) {
            if (getResources_resource().size()>0) {
            List resources = getResources_resource();
            for (int i=0; i< resources.size(); i++) {
                if (resources.get(i) instanceof XnatResourcecatalog) {
                    XnatResourcecatalog rscCatalog = (XnatResourcecatalog)resources.get(i);
                    if (rscCatalog.getLabel().equalsIgnoreCase(content))
                        _rtn=rscCatalog;
                    break;
                }
            }
            }

        }
        return _rtn;
    }

    public int isstaticScan() {
        int rtn = 2;
        java.sql.Timestamp startTime = (java.sql.Timestamp)this.getStartTimeScan();
        java.sql.Timestamp injectionTime = (java.sql.Timestamp)this.getStartTimeInjection();
        //big difference (more than 2 minutes =120000ms) implies static else Dynamic
        if (startTime!=null && injectionTime != null) {
        if ((startTime.getTime() - injectionTime.getTime())> 120000) {
            //Is static
            rtn  = 1;
        }else
            rtn = 0;
        }
        return rtn;
    }

    public ArrayList getUnionOfScansByType(String csvType) {
        ArrayList _return = new ArrayList();
        String[] types = csvType.split(",");
        if (types != null && types.length > 0) {
            for(int i = 0; i < types.length; i++) {
                ArrayList rtn = getScansByType(types[i].trim());
                if (rtn.size() > 0 )_return.addAll(rtn);
            }
        }
        _return.trimToSize();
        return _return;
    }

    public ArrayList getUnionOfScansByType(String csvType, boolean chronological) {
        ArrayList _return = new ArrayList();
        if (chronological) {
            String[] types = csvType.split(",");
            Hashtable scanTypes = new Hashtable();
            if (types != null && types.length > 0) {
                for(int i = 0; i < types.length; i++) {
                    scanTypes.put(types[i].trim(), "");
                }
            }
            for(XnatImagescandataI scan :  this.getScans_scan()){
                if (scan.getType() != null && scanTypes.containsKey(scan.getType())) {
                    _return.add(scan);
                }
            }
            _return.trimToSize();
            return _return;
        }else
            return getUnionOfScansByType(csvType);
    }

    public ArrayList getUnionOfScansByType(String csvType, String chronological) {
        return getUnionOfScansByType(csvType, new Boolean(chronological).booleanValue());
    }

    public static ArrayList<XnatImagesessiondata> GetAllPreviousImagingSessions(XnatImagesessiondata latestImagingSession, String projectId, XDATUser user) {
        String subject_id = latestImagingSession.getSubjectId();
        ArrayList<XnatImagesessiondata> previousSessions = new ArrayList<XnatImagesessiondata>();
        String login = null;
        if (user != null) {
            login = user.getUsername();
        }
        String query = "SELECT pet.id FROM xnat_petSessionData pet LEFT JOIN xnat_subjectAssessorData sad ON pet.ID=sad.ID LEFT JOIN xnat_experimentData ed ON sad.ID=ed.ID left join xnat_imagesessiondata im on ed.id = im.id  WHERE subject_id='" + subject_id +"' and ed.project='" + projectId + "' and date < '" +  latestImagingSession.getDate() +"' ORDER BY date DESC ";
        try {
            XFTTable table = TableSearch.Execute(query,user.getDBName(),login);
            if (table.size()>0) {
                table.resetRowCursor();
                Object pet_id = null;
                while(table.hasMoreRows()) {
                    pet_id = table.nextRowHash().get("id");
                    if (pet_id !=null) {
                        ItemI pet = ItemSearch.GetItem("xnat:petSessionData.ID",(String)pet_id,user,false);
                        if (pet != null) {
                            previousSessions.add(new XnatPetsessiondata(pet));
                        }
                    }
                }
            }
            return previousSessions;
        } catch (Exception e) {
            logger.error("",e);
            return null;
        }
    }
}
