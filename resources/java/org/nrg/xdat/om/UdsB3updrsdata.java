/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB3updrsdata extends BaseUdsB3updrsdata {

	public UdsB3updrsdata(ItemI item)
	{
		super(item);
	}

	public UdsB3updrsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB3updrsdata(UserI user)
	 **/
	public UdsB3updrsdata()
	{}

	public UdsB3updrsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
