/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class AdosAdos2001module2data extends BaseAdosAdos2001module2data {

	public AdosAdos2001module2data(ItemI item)
	{
		super(item);
	}

	public AdosAdos2001module2data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseAdosAdos2001module2data(UserI user)
	 **/
	public AdosAdos2001module2data()
	{}

	public AdosAdos2001module2data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
