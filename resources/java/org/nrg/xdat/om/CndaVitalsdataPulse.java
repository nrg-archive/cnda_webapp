/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaVitalsdataPulse extends BaseCndaVitalsdataPulse {

	public CndaVitalsdataPulse(ItemI item)
	{
		super(item);
	}

	public CndaVitalsdataPulse(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaVitalsdataPulse(UserI user)
	 **/
	public CndaVitalsdataPulse()
	{}

	public CndaVitalsdataPulse(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
