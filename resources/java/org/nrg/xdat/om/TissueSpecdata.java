/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:58 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class TissueSpecdata extends BaseTissueSpecdata {

	public TissueSpecdata(ItemI item)
	{
		super(item);
	}

	public TissueSpecdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseTissueSpecdata(UserI user)
	 **/
	public TissueSpecdata()
	{}

	public TissueSpecdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
