/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:47:27 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatHdsessiondata extends BaseXnatHdsessiondata {

	public XnatHdsessiondata(ItemI item)
	{
		super(item);
	}

	public XnatHdsessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatHdsessiondata(UserI user)
	 **/
	public XnatHdsessiondata()
	{}

	public XnatHdsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
