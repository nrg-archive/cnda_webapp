/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:48:59 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsA1subdemodata extends BaseUdsA1subdemodata {

	public UdsA1subdemodata(ItemI item)
	{
		super(item);
	}

	public UdsA1subdemodata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA1subdemodata(UserI user)
	 **/
	public UdsA1subdemodata()
	{}

	public UdsA1subdemodata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
