/*
 * GENERATED FILE
 * Created on Wed Dec 10 11:47:27 CST 2014
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatEsscandata extends BaseXnatEsscandata {

	public XnatEsscandata(ItemI item)
	{
		super(item);
	}

	public XnatEsscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatEsscandata(UserI user)
	 **/
	public XnatEsscandata()
	{}

	public XnatEsscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
