package org.nrg.xnat.restlet.representations;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTTable;
import org.restlet.data.MediaType;
import org.nrg.xnatCalendar.CalendarDataManager;

/**
 * This class will return the calendar in JSON format.  Original plans were to create a 
 * Javascript front end for the calendar.  I dont know what the priority is currently so this is
 * just a place holder for now. 
 *
 */
public final class JSONCalendarRepresentation extends CalendarRepresentation {
   
   private final static Logger _logger = Logger.getLogger(CalendarRepresentation.class);
   
   public JSONCalendarRepresentation(CalendarDataManager calendarData,MediaType mediaType) throws Exception {
      super(calendarData, mediaType);
   }
   
   @Override
   public void write(OutputStream os) throws IOException {
     OutputStreamWriter sw      = new OutputStreamWriter(os);
     BufferedWriter     writer  = new BufferedWriter(sw);
     /*
     String startDateStr, endDateStr;
     Date startDate;
      
      writer.write("[\n");
      
      for(int event = 0; event<_calendarData.getNumEvents(); event++){ 
         
         try{
            //get the start and end time of the event if one exists.
            startDate    = _calendarData.getEventStartDate(event);
            startDateStr = getDateString(startDate, _calendarData.getEventStartTime(event));
            endDateStr   = getDateString(startDate, _calendarData.getEventEndTime(event));
         }
         catch(Exception e){
            _logger.error("Unable to retrive the start or end time for session " + _calendarData.getSessionId(event) + ". Skipping ...");
            continue;
         }
            if(event != 0){ writer.write(",\n"); }
            
            writer.write("{ start_date:\"" + startDateStr +"\", ");
            writer.write("end_date:\"" + endDateStr + "\", ");
            writer.write("text:\"" + _calendarData.getEventSummary(event) + "\"}");
      }
      */
      writer.write("");
      writer.flush();
  }
   
   @Override
   protected String getDateString(final Date date) throws Exception{
      /*
      if(null == date || null == time){ return ""; }
      
      //Create the appropriate date formatters
      DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
      DateFormat tf = new SimpleDateFormat("HH:mm");
      
      //Parse dates and return the formatted dateStamp.
      return df.format(date) + " " + tf.format(time);
      */
      return "";
   }
}